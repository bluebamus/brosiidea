<?php 

class MailgunMailer { 

	const CRLF = "\r\n";

	private $api_key = ""; //mailgun domain api-key
	private $domain ="";
	private $mailgun_api_url = "https://api.mailgun.net/v3/";
	private $parameter="/messages";
	private $api_call_url ="";
	private $FromName="";
	private $ToName="";
	private $From = "관리자 <*** 개인정보보호를 위한 이메일주소 노출방지 ***>";
	private $To = "";
	private $Cc = "";
	private $Bcc = "";

	private $Subject = "";
	private $Text = "";
	private $Html = "";
	private $File = null;
	private $Tag = "";
	private $DeliveryTime = ""; //'Fri, 25 Oct 2013 23:10:10 -0000'

	public function setApiKey($api_key) {
		$this->api_key="api:".$api_key;
	}

	public function setDomain($domain) {
		$this->domain = $domain;
		$this->api_call_url = $this->mailgun_api_url.$domain.$this->parameter;
	}

	public function setFrom($from_name="",$from="") {
		$this->From = $from;
		$this->FromName = $from_name;
	}

	public function setTo($to_name="",$to="") {
		$this->To = $to;
		$this->ToName = $to_name;
	}

	public function setCc($cc) {
		$this->Cc = $cc;
	}

	public function setBcc($bcc) {
		$this->Bcc = $bcc;
	}

	public function setSubject($subject) {
		$this->Subject = $subject;
	}

	public function setContent($type,$content) {
		if($type==0)
			$this->Text = $content;
		else
			$this->Html = $content;
	}

	public function attachFile($file) {
		$this->File = $file;
	}

	public function setTag($tag) {
		$this->Tag=$tag;
	}

	public function DeliveryTime($delivery_time) {
		$this->DeliveryTime=$delivery_time;
	}

	public function send() {

	    if($this->DeliveryTime!=''){
            $array_data = array(
                'from'=> $this->FromName .'<'.$this->From.'>',
                'to'=>$this->ToName.'<'.$this->To.'>',
                'subject'=>$this->Subject,
                'html'=>$this->Html,
                'text'=>$this->Text,
                'o:tracking'=>'yes',
                'o:tracking-clicks'=>'yes',
                'o:tracking-opens'=>'yes',
                'o:tag'=>$this->Tag,
                'o:deliverytime'=>$this->DeliveryTime
            );
        }else{
            $array_data = array(
                'from'=> $this->FromName .'<'.$this->From.'>',
                'to'=>$this->ToName.'<'.$this->To.'>',
                'subject'=>$this->Subject,
                'html'=>$this->Html,
                'text'=>$this->Text,
                'o:tracking'=>'yes',
                'o:tracking-clicks'=>'yes',
                'o:tracking-opens'=>'yes',
                'o:tag'=>$this->Tag
            );
        }

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$this->api_call_url);
		curl_setopt($ch, CURLOPT_USERPWD, $this->api_key);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $array_data);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_ENCODING, 'UTF-8');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		// execute!
		$response = curl_exec($ch);

		// close the connection, release resources used
		curl_close($ch);
		$results = json_decode($response, true);

        //var_export($results);
		//var_export($results['message']);

		return $results['message'];
	}

	/**
	*
	* @param type  메일 본문 내용이 html인지 여부 0:text, 1:html
	*/
	function mailer($api_key,$domain,$from_name,$from,$to_name,$to,$subject,$type=0,$content,$file="",$cc="",$bcc="",$tag="",$delivery_time=""){

        $this->setApiKey($api_key);
        $this->setDomain($domain);
        $this->setFrom($from_name,$from);
        $this->setTo($to_name,$to);
        $this->setSubject($subject);
        $this->setContent($type,$content);
        $this->attachFile($file);
        $this->setCc($cc);
        $this->setBcc($bcc);
        $this->setTag($tag);
        $this->DeliveryTime($delivery_time);

		$response=$this->send();

		return $response;
	}
} 
