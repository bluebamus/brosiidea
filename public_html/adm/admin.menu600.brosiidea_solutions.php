<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018-04-30
 * Time: 오후 10:06
 */

$menu['menu600'] = array (
    array('600000', '인증 정보 관리', G5_ADMIN_URL.'/brosiidea_solutions/authentication_manager.php', 'bs_auth'),
    array('600100', '인증 정보 관리', G5_ADMIN_URL.'/brosiidea_solutions/authentication_manager.php', 'bs_auth'),
    array('600200', '작업 현황 관리', G5_ADMIN_URL.'/brosiidea_solutions/job_monitoring.php', 'bs_monitoring'),
    array('600300', '키워드 수집/관리', G5_ADMIN_URL.'/brosiidea_solutions/keyword_collector.php', 'bs_keyword'),
    array('600400', '키워드별 블로거 수집/관리 ', G5_ADMIN_URL.'/brosiidea_solutions/bloger_collector.php', 'bs_bloger'),
    array('600500', '체험단 사용자 수집/관리 ', G5_ADMIN_URL.'/brosiidea_solutions/exgroup_manager.php', 'bs_exgroup'),
    array('600600', '수신 거부 사용자 관리 ', G5_ADMIN_URL.'/brosiidea_solutions/unsubscribes_manager.php', 'bs_unsubscribes'),
    array('600700', '전송 실패 사용자 관리 ', G5_ADMIN_URL.'/brosiidea_solutions/bounces_manager.php', 'bs_bounces'),
    array('600800', '블랙 리스트 사용자 관리 ', G5_ADMIN_URL.'/brosiidea_solutions/blacklist_manager.php', 'bs_blacklist'),
    array('600900', '업체 관리 ', G5_ADMIN_URL.'/brosiidea_solutions/company_manager.php', 'bs_compny'),
    array('601000', '업체별 메일링 송/수신자 수집/관리 ', G5_ADMIN_URL.'/brosiidea_solutions/company_mailing_manager.php', 'bs_compny_mail'),
    array('601100', '업체별 체험단 사용자 수집/관리 ', G5_ADMIN_URL.'/brosiidea_solutions/company_exgroup_manager.php', 'bs_compny_exgroup'),
    array('601200', '작업 관리 ', G5_ADMIN_URL.'/brosiidea_solutions/job_manager.php', 'bs_job_mng'),
);