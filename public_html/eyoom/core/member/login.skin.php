<?php
if (!defined('_GNUBOARD_')) exit;

// 사용자 프로그램
@include_once(EYOOM_USER_PATH.'/member/login.skin.php');

// Template define
$tpl->define_template('member',$eyoom['member_skin'],'login.skin.html');

// 소셜로그인
if ($eyoom['use_social_login'] == 'y') {
	if((defined('G5_NAVER_OAUTH_CLIENT_ID') && G5_NAVER_OAUTH_CLIENT_ID) || (defined('G5_KAKAO_OAUTH_REST_API_KEY') && G5_KAKAO_OAUTH_REST_API_KEY) || (defined('G5_FACEBOOK_CLIENT_ID') && G5_FACEBOOK_CLIENT_ID) || (defined('G5_GOOGLE_CLIENT_ID') && G5_GOOGLE_CLIENT_ID)) {
		$social_oauth_url = G5_PLUGIN_URL.'/oauth/login.php?service=';
		$tpl->define(array(
			'oauth_bs' => 'skin_bs/member/' . $eyoom['member_skin'] . '/social_button.skin.html',
		));
		
		$tpl->assign('social_oauth_url', $social_oauth_url);
	}	
}

// Template assign
@include EYOOM_INC_PATH.'/tpl.assign.php';
$tpl->print_($tpl_name);