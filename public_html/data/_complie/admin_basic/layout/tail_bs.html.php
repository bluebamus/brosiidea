<?php /* Template_ 2.2.8 2018/04/30 10:45:15 /home1/bluebamus2/public_html/admin/admin_theme/admin_basic/layout/tail_bs.html 000001922 */ ?>
<?php if (!defined('_EYOOM_IS_ADMIN_')) exit; ?>

<?php if(!$GLOBALS["wmode"]){?>
	        </div>
	        <div class="footer">
	            <div class="copyright">
	                Copyright &copy; <?php echo $TPL_VAR["config"]["cf_title"]?>. All Rights Reserved. <?php echo $GLOBALS["print_version"]?>

	            </div>
	            <div class="clearfix"></div>
	        </div>
	    </div>
	</div>
	<div class="back-to-top">
        <i class="fa fa-angle-up"></i>
    </div>
</div>
<?php }?>

<script>var g5_admin_url = "<?php echo G5_ADMIN_URL?>";</script>
<script src="<?php echo G5_ADMIN_URL?>/admin.js?ver=<?php echo G5_JS_VER?>"></script>
<script src="/admin/admin_theme/admin_basic/plugins/jquery-migrate-1.2.1.min.js"></script>
<script src="/admin/admin_theme/admin_basic/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="/admin/admin_theme/admin_basic/plugins/malihu-custom-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="/admin/admin_theme/admin_basic/plugins/screenfull/screenfull.min.js"></script>
<script src="/admin/admin_theme/admin_basic/plugins/waves/waves.min.js"></script>
<script src="/admin/admin_theme/admin_basic/js/app.js"></script>
<script>
$(document).ready(function() {
	App.init();
});
</script>
<?php if($GLOBALS["sub_menu"]){?>
<script>
$(function() {
	var submenu_id = 'submenu_<?php echo $GLOBALS["sub_menu"]?>';
	$("#"+submenu_id).addClass('active');
});
</script>
<?php }?>
<!--[if lt IE 9]>
	<script src="/admin/admin_theme/admin_basic/plugins/respond.min.js"></script>
	<script src="/admin/admin_theme/admin_basic/plugins/html5shiv.min.js"></script>
	<script src="/admin/admin_theme/admin_basic/plugins/eyoom-form/js/eyoom-form-ie8.js"></script>
<![endif]-->

<?php $this->print_("tail_sub",$TPL_SCP,1);?>