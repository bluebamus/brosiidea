<?php /* Template_ 2.2.8 2018/04/30 10:45:15 /home1/bluebamus2/public_html/admin/admin_theme/admin_basic/layout/head_bs.html 000005859 */ 
$TPL_admmenu_1=empty($TPL_VAR["admmenu"])||!is_array($TPL_VAR["admmenu"])?0:count($TPL_VAR["admmenu"]);?>
<?php if (!defined('_EYOOM_IS_ADMIN_')) exit;

add_stylesheet('<link rel="stylesheet" href="/admin/admin_theme/admin_basic/plugins/bootstrap/css/bootstrap.min.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/admin/admin_theme/admin_basic/plugins/font-awesome/css/font-awesome.min.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/admin/admin_theme/admin_basic/plugins/eyoom-form/css/eyoom-form.min.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/admin/admin_theme/admin_basic/plugins/malihu-custom-scrollbar/jquery.mCustomScrollbar.min.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/admin/admin_theme/admin_basic/plugins/waves/waves.min.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/admin/admin_theme/admin_basic/css/common.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/admin/admin_theme/admin_basic/css/style.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/admin/admin_theme/admin_basic/css/custom.css" type="text/css" media="screen">',0);

?>

<?php if(!$GLOBALS["wmode"]){?>
<div class="wrapper sidebar-expand topbar-fixed">
	<div class="wrapper-in">
	    <div class="admin-container">
	        <div class="col-md-3 left-area side-fixed">
	            <div class="sidebar-wrap">
	                <div class="navbar nav-title">
	                    <a href="<?php echo EYOOM_ADMIN_URL?>" class="nav-title-logo waves-effect waves-light">
	                        <i class="fa fa-cogs"></i><span>관리자모드</span>
	                    </a>
	                </div>
	                <div class="clearfix"></div>
	                <div class="adm-member-box">
		                <div class="adm-member-photo">
<?php if(!$TPL_VAR["member"]["photo_url"]){?>
			                <i class="fa fa-user"></i>
<?php }else{?>
			                <img src="<?php echo $TPL_VAR["member"]["photo_url"]?>" class="img-responsive">
<?php }?>
		                </div>
		                <div class="adm-member-info">
			                <h5 class="ellipsis"><?php echo $TPL_VAR["config"]["cf_title"]?></h5>
			                <h6 class="ellipsis"><?php echo $TPL_VAR["member"]["mb_name"]?></h6>
			                <p class="info-left">Lv. <span><?php echo $TPL_VAR["eyoomer"]["level"]?></span></p>
			                <p class="info-right">P. <span><?php echo number_format($TPL_VAR["eyoomer"]["level_point"])?></span></p>
			                <div class="clearfix"></div>
		                </div>
		                <div class="clearfix"></div>
	                </div>
	                <div id="sidebar-menu" class="admin-menu">
	                    <div class="admin-menu-box">
	                        <ul class="nav sidebar-menu">
<?php if($TPL_admmenu_1){foreach($TPL_VAR["admmenu"] as $TPL_V1){
$TPL_submenu_2=empty($TPL_V1["submenu"])||!is_array($TPL_V1["submenu"])?0:count($TPL_V1["submenu"]);?>
		                        <li <?php echo $TPL_V1["active"]?>>
		                        	<a href="javascript:void(0)" class="waves-effect waves-light">
			                        	<i class="fa <?php echo $TPL_V1["fa_icon"]?>"></i> <?php echo $TPL_V1["menu"]?> <span class="fa fa-caret-down"></span>
			                        </a>
		                        	<ul class="nav sidebar-submenu">
<?php if($TPL_submenu_2){foreach($TPL_V1["submenu"] as $TPL_V2){?>
			                        	<li class="waves-effect waves-light" id="submenu_<?php echo $TPL_V2["skey"]?>"><a href="<?php echo $TPL_V2["href"]?>" <?php echo $TPL_V2["target"]?>><?php echo $TPL_V2["menu"]?></a></li>
<?php }}?>
		                        	</ul>
		                        </li>
<?php }}?>
	                        </ul>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <div class="topbar">
	            <div class="topbar-nav">
                    <div class="nav admin-toggle waves-effect">
                        <a class="#" id="topbar-toggle"><i class="fa fa-bars"></i></a>
                    </div>
                    <div class="topbar-nav-menu">
	                    <ul class="list-unstyled list-inline">
		                    <li class="hidden-xs screenfull tooltips" data-placement="bottom" data-toggle="tooltip" data-original-title="전체화면 ON-OFF">전체화면</li>
		                    <li class="hidden-xs"><a href="<?php echo EYOOM_ADMIN_URL?>">관리자메인</a></li>
		                    <li class="hidden-xs"><a href="<?php echo EYOOM_ADMIN_URL?>/?dir=config&pid=config_form">기본환경설정</a></li>
<?php if($GLOBALS["is_youngcart"]){?>
		                    <li class="hidden-xs"><a href="<?php echo EYOOM_ADMIN_URL?>/?dir=shop&pid=configform">쇼핑몰환경설정</a></li>
<?php }?>
		                    <li><a href="<?php echo G5_URL?>/?theme=<?php echo $TPL_VAR["eyoom_basic"]["theme"]?>">홈페이지메인</a></li>
<?php if($GLOBALS["is_youngcart"]){?>
		                    <li><a href="<?php echo G5_SHOP_URL?><?php if($TPL_VAR["eyoom_basic"]["shop_theme"]){?>/?shop_theme=<?php echo $TPL_VAR["eyoom_basic"]["shop_theme"]?><?php }?>">쇼핑몰메인</a></li>
<?php }?>
		                    <li class="last-li"><a href="<?php echo G5_BBS_URL?>/logout.php" class="btn-e btn-e-xs btn-e-dark"><i class="fa fa-sign-out"></i> 로그아웃</a></li>
	                    </ul>
                    </div>
	            </div>
	        </div>
	        <div class="right-area" role="main">
<?php }?>