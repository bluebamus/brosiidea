<?php /* Template_ 2.2.8 2018/04/30 10:48:38 /home1/bluebamus2/public_html/admin/admin_theme/admin_basic/skin_bs/board/basic/boardgroupmember_form.skin.html 000006232 */ 
$TPL_group_1=empty($TPL_VAR["group"])||!is_array($TPL_VAR["group"])?0:count($TPL_VAR["group"]);
$TPL_group_member_1=empty($TPL_VAR["group_member"])||!is_array($TPL_VAR["group_member"])?0:count($TPL_VAR["group_member"]);?>
<?php if (!defined('_GNUBOARD_')) exit;
add_stylesheet('<link rel="stylesheet" href="/admin/admin_theme/admin_basic/plugins/jsgrid/jsgrid.min.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/admin/admin_theme/admin_basic/plugins/jsgrid/jsgrid-theme.min.css" type="text/css" media="screen">',0);
?>

<div class="admin-boardgroupmember-form">
	<form id="fboardgroupmember_form" name="fboardgroupmember_form" action="<?php echo $GLOBALS["action_url"]?>" method="post" onsubmit="return boardgroupmember_form_check(this)" class="eyoom-form">
	<input type="hidden" name="mb_id" value="<?php echo $TPL_VAR["mb"]["mb_id"]?>" id="mb_id">
	<input type="hidden" name="token" value="" id="token">

	<div class="headline">
		<h4><strong>접근가능 그룹</strong></h4>
	</div>
	<div class="margin-bottom-30"></div>
	<div class="admin-search-box">
		<div class="row">
			<div class="col col-4">
			    <p class="padding-top-5 margin-bottom-0 sm-margin-bottom-10">아이디 <b><?php echo $TPL_VAR["mb"]["mb_id"]?></b>, 이름 <b><?php echo get_text($TPL_VAR["mb"]["mb_name"])?></b>, 닉네임 <b><?php echo $TPL_VAR["mb"]["mb_nick"]?></b></p>
			</div>
			<div class="col col-3">
				<label class="select margin-bottom-0">
					<select name="gr_id" id="gr_id">
						<option value="">접근가능 그룹을 선택하세요.</option>
<?php if($TPL_group_1){foreach($TPL_VAR["group"] as $TPL_V1){?>
						<option value="<?php echo $TPL_V1["gr_id"]?>"><?php echo $TPL_V1["gr_subject"]?></option>
<?php }}?>
					</select><i></i>
				</label>
			</div>
			<div class="col col-3">
				<input type="submit" name="" value="선택" class="btn-e btn-e-dark">
			</div>
		</div>
	</div>
	</form>
	<div class="margin-bottom-30"></div>

	<form name="fboardgroupmember" id="fboardgroupmember" action="<?php echo $GLOBALS["action_url"]?>" method="post" onsubmit="return fboardgroupmember_submit(this);" class="eyoom-form">
	<input type="hidden" name="sst" id="sst" value="<?php echo $GLOBALS["sst"]?>">
	<input type="hidden" name="sod" id="sod" value="<?php echo $GLOBALS["sod"]?>">
	<input type="hidden" name="sfl" value="<?php echo $GLOBALS["sfl"]?>">
	<input type="hidden" name="stx" value="<?php echo $GLOBALS["stx"]?>">
	<input type="hidden" name="page" value="<?php echo $GLOBALS["page"]?>">
	<input type="hidden" name="token" value="<?php echo $GLOBALS["token"]?>">
	<input type="hidden" name="mb_id" value="<?php echo $TPL_VAR["mb"]["mb_id"]?>" id="mb_id">
	<input type="hidden" name="w" value="d" id="w">

<?php if(G5_IS_MOBILE){?>
	<p class="font-size-11 color-grey text-right margin-bottom-5"><i class="fa fa-info-circle"></i> Note! 좌우스크롤 가능 (<i class="fa fa-arrows-h"></i>)</p>
<?php }?>

	<div id="boardgroupmember-list"></div>

	<div class="margin-top-20">
	    <input type="submit" name="" value="선택삭제" class="btn-e btn-e-xs btn-e-dark">
	</div>
	</form>
</div>

<script src="/admin/admin_theme/admin_basic/plugins/jsgrid/jsgrid.min.js"></script>
<script src="/admin/admin_theme/admin_basic/js/jsgrid.js"></script>
<script>
/*--------------------------------------
	Table
--------------------------------------*/
!function () {
    var db = {
        deleteItem: function (deletingClient) {
            var clientIndex = $.inArray(deletingClient, this.clients);
            this.clients.splice(clientIndex, 1)
        },
        insertItem: function (insertingClient) {
            this.clients.push(insertingClient)
        },
        loadData  : function (filter) {
            return $.grep(this.clients, function (client) {
                return !(filter.체크 && !(client.체크.indexOf(filter.체크) > -1) )
            })
        },
        updateItem: function (updatingClient) {}
    };
    window.db    = db,
    db.clients   = [
<?php if($TPL_group_member_1){$TPL_I1=-1;foreach($TPL_VAR["group_member"] as $TPL_V1){$TPL_I1++;?>
        {
	        체크: "<label for='chk_<?php echo $TPL_I1?>' class='checkbox'><input type='checkbox' name='chk[]' id='chk_<?php echo $TPL_I1?>' value='<?php echo $TPL_V1["gm_id"]?>'><i></i></label>",
	        그룹아이디: "<a href='<?php echo G5_BBS_URL?>/group.php?gr_id=<?php echo $TPL_V1["gr_id"]?>'><?php echo $TPL_V1["gr_id"]?></a>",
	        그룹: "<?php echo $TPL_V1["gr_subject"]?>",
	        처리일시: "<?php echo $TPL_V1["gm_datetime"]?>",
        },
<?php }}?>
    ]
}();

$(function() {
    $("#boardgroupmember-list").jsGrid({
        filtering      : false,
        editing        : false,
        sorting        : false,
        paging         : true,
        autoload       : true,
        controller     : db,
        deleteConfirm  : "정말로 삭제하시겠습니까?\n한번 삭제된 데이터는 복구할수 없습니다.",
        pageButtonCount: 5,
        pageSize       : 15,
        width          : "100%",
        height         : "auto",
        fields         : [
            { name: "체크", type: "text", align: "center", width: 40 },
            { name: "그룹아이디", type: "text", width: 100 },
            { name: "그룹", type: "text", width: 300 },
            { name: "처리일시", type: "text", width: 100 },
        ]
    });

	var $chk = $(".jsgrid-table th:first-child");
	if ($chk.text() == '체크') {
		var html = '<label for="chkall" class="checkbox"><input type="checkbox" name="chkall" id="chkall" value="1" onclick="check_all(this.form)"><i></i></label>';
		$chk.html(html);
	}
});

function fboardgroupmember_submit(f) {
    if (!is_checked("chk[]")) {
        alert("선택삭제 하실 항목을 하나 이상 선택하세요.");
        return false;
    }

    return true;
}

function boardgroupmember_form_check(f) {
    if (f.gr_id.value == '') {
        alert('접근가능 그룹을 선택하세요.');
        return false;
    }

    return true;
}
</script>