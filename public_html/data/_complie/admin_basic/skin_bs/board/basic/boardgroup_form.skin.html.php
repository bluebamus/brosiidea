<?php /* Template_ 2.2.8 2018/01/26 21:11:40 /home/bluebamus1/public_html/admin/admin_theme/admin_basic/skin_bs/board/basic/boardgroup_form.skin.html 000006994 */ 
$TPL_gr_extra_1=empty($TPL_VAR["gr_extra"])||!is_array($TPL_VAR["gr_extra"])?0:count($TPL_VAR["gr_extra"]);?>
<?php if (!defined('_GNUBOARD_')) exit; ?>

<div class="admin-boardgroup-form">
	<form name="fboardform" method="post" action="<?php echo $GLOBALS["action_url"]?>" onsubmit="return fboardgroup_check(this)" enctype="multipart/form-data" class="eyoom-form">
	<input type="hidden" name="w" value="<?php echo $GLOBALS["w"]?>">
	<input type="hidden" name="sfl" value="<?php echo $GLOBALS["sfl"]?>">
	<input type="hidden" name="stx" value="<?php echo $GLOBALS["stx"]?>">
	<input type="hidden" name="sst" value="<?php echo $GLOBALS["sst"]?>">
	<input type="hidden" name="sod" value="<?php echo $GLOBALS["sod"]?>">
	<input type="hidden" name="page" value="<?php echo $GLOBALS["page"]?>">
	<input type="hidden" name="token" value="">

	<div class="headline">
		<h4><strong>게시판 그룹 <?php echo $GLOBALS["html_title"]?></strong></h4>
	</div>
	<div class="margin-bottom-30"></div>

	<div class="group-form">
		<div class="adm-form-wrap margin-bottom-30">
			<header><strong><i class="fa fa-caret-right"></i> 게시판 그룹 설정정보</strong></header>
			<fieldset>
				<div class="cont-text-bg">
					<p class="bg-warning font-size-12 margin-bottom-0">
						<i class="fa fa-info-circle"></i> 게시판을 생성하시려면 1개 이상의 게시판그룹이 필요합니다. 게시판그룹을 이용하시면 더 효과적으로 게시판을 관리할 수 있습니다.
					</p>
				</div>
			</fieldset>
			<fieldset>
				<div class="row">
					<div class="col col-3">
						<section>
							<label for="gr_id" class="label">그룹 ID</label>
					        <label class="input">
					            <input type="text" name="gr_id" id="gr_id" value="<?php echo $TPL_VAR["group"]["gr_id"]?>" maxlength="20" <?php echo $GLOBALS["gr_id_attr"]?>>
					        </label>
<?php if($GLOBALS["w"]==''){?>
					        <div class="note margin-bottom-10"><strong>Note:</strong> 영문자, 숫자, _ 만 가능 (공백없이)</div>
<?php }?>
						</section>
					</div>
<?php if($GLOBALS["w"]=='u'){?>
					<div class="col col-9">
						<section class="label-height">
					        <a href="<?php echo G5_BBS_URL?>/group.php?gr_id=<?php echo $TPL_VAR["group"]["gr_id"]?>" target="_blank" class="btn-e btn-e-yellow">게시판 그룹 바로가기</a> <a href="<?php echo EYOOM_ADMIN_URL?>/?dir=board&amp;pid=board_form&amp;gr_id=<?php echo $TPL_VAR["group"]["gr_id"]?>" class="btn-e btn-e-dark">그룹 내 게시판 생성</a>
						</section>
					</div>
<?php }?>
				</div>
			</fieldset>

			<fieldset>
				<div class="row">
					<div class="col col-6">
						<section>
							<label for="gr_subject" class="label">그룹 제목</label>
					        <label class="input">
					            <input type="text" name="gr_subject" value="<?php echo get_text($TPL_VAR["group"]["gr_subject"])?>" id="gr_subject" required>
					        </label>
						</section>
					</div>
					<div class="col col-3">
						<section>
							<label for="gr_device" class="label">접속기기</label>
					        <label class="select">
					            <select id="gr_device" name="gr_device">
				                    <option value="both"<?php echo get_selected($TPL_VAR["group"]["gr_device"],'both')?>>PC와 모바일에서 모두 사용</option>
				                    <option value="pc"<?php echo get_selected($TPL_VAR["group"]["gr_device"],'pc')?>>PC 전용</option>
				                    <option value="mobile"<?php echo get_selected($TPL_VAR["group"]["gr_device"],'mobile')?>>모바일 전용</option>
					            </select><i></i>
					        </label>
					        <div class="note margin-bottom-10"><strong>Note:</strong> PC 와 모바일 사용을 구분합니다.</div>
						</section>
					</div>
				</div>
			</fieldset>

			<fieldset>
				<div class="row">
					<div class="col col-3">
						<section>
							<label <?php if($GLOBALS["is_admin"]=='super'){?>for="gr_admin"<?php }?> class="label">그룹 관리자</label>
					        <label class="input">
<?php if($GLOBALS["is_admin"]=='super'){?>
					            <input type="text" name="gr_admin" value="<?php echo $TPL_VAR["gr"]["gr_admin"]?>" id="gr_admin" maxlength="20">
<?php }else{?>
					            <input type="hidden" name="gr_admin" value="<?php echo $TPL_VAR["gr"]["gr_admin"]?>" id="gr_admin"><?php echo $TPL_VAR["gr"]["gr_admin"]?>

<?php }?>
					        </label>
						</section>
					</div>
					<div class="col col-9">
						<section>
							<label for="gr_use_access" class="label">접근회원사용</label>
					        <label class="checkbox">
					            <input type="checkbox" name="gr_use_access" value="1" id="gr_use_access" <?php if($TPL_VAR["gr"]["gr_use_access"]){?>checked<?php }?>><i></i> 사용 <strong class="color-orange">[ 접근회원수 : <a href="<?php echo EYOOM_ADMIN_URL?>/?dir=board&amp;pid=groupmember_list&amp;gr_id=<?php echo $GLOBALS["gr_id"]?>"><?php echo number_format($GLOBALS["grmember_cnt"])?></a> ]</strong>
					        </label>
					        <div class="note margin-bottom-10"><strong>Note:</strong> 사용에 체크하시면 이 그룹에 속한 게시판은 접근가능한 회원만 접근이 가능합니다.</div>
						</section>
					</div>
				</div>
			</fieldset>
		</div>
	</div>

	<div class="group-form">
		<div class="adm-form-wrap margin-bottom-30">
			<header><strong><i class="fa fa-caret-right"></i> 여분필드 설정</strong></header>

<?php if($TPL_gr_extra_1){foreach($TPL_VAR["gr_extra"] as $TPL_K1=>$TPL_V1){?>
			<fieldset>
				<div class="row">
					<div class="col col-6">
						<section>
							<label for="gr_<?php echo $TPL_K1?>_subj" class="label">여분필드 <?php echo $TPL_K1?> 제목</label>
					        <label class="input">
					        	<input type="text" name="gr_<?php echo $TPL_K1?>_subj" id="gr_<?php echo $TPL_K1?>_subj" value="<?php echo get_text($TPL_V1["gr_subject"])?>">
					        </label>
						</section>
					</div>
					<div class="col col-6">
						<section>
							<label for="gr_<?php echo $TPL_K1?>" class="label">여분필드 <?php echo $TPL_K1?> 내용</label>
					        <label class="input">
					        	<input type="text" name="gr_<?php echo $TPL_K1?>" id="gr_<?php echo $TPL_K1?>" value="<?php echo get_text($TPL_V1["gr_value"])?>">
					        </label>
						</section>
					</div>
				</div>
			</fieldset>
<?php }}?>
		</div>
	</div>

	<div class="text-center margin-top-30 margin-bottom-30">
		<input type="submit" value="확인" id="btn_submit" class="btn-e btn-e-lg btn-e-red" accesskey="s">
		<a href="<?php echo EYOOM_ADMIN_URL?>/?dir=board&amp;pid=boardgroup_list&amp;<?php echo $GLOBALS["qstr"]?>" class="btn-e btn-e-lg btn-e-dark">목록</a>
	</div>
	</form>
</div>

<script>
function fboardgroup_check(f) {
    return true;
}
</script>