<?php /* Template_ 2.2.8 2018/02/26 10:54:45 /home/bluebamus1/public_html/admin/admin_theme/admin_basic/skin_bs/board/basic/board_form_anchor.skin.html 000001099 */ ?>
<?php if (!defined('_GNUBOARD_')) exit; ?>

<div class="pg-anchor-in tab-e2">
    <ul class="nav nav-tabs">
	    <li <?php if($TPL_VAR["anc_id"]=='anc_bo_basic'){?>class="active"<?php }?>><a href="#anc_bo_basic">기본 설정</a></li>
	    <li <?php if($TPL_VAR["anc_id"]=='anc_bo_auth'){?>class="active"<?php }?>><a href="#anc_bo_auth">권한 설정</a></li>
	    <li <?php if($TPL_VAR["anc_id"]=='anc_bo_function'){?>class="active"<?php }?>><a href="#anc_bo_function">기능 설정</a></li>
	    <li <?php if($TPL_VAR["anc_id"]=='anc_bo_design'){?>class="active"<?php }?>><a href="#anc_bo_design">디자인/양식</a></li>
	    <li <?php if($TPL_VAR["anc_id"]=='anc_bo_point'){?>class="active"<?php }?>><a href="#anc_bo_point">포인트 설정</a></li>
	    <li <?php if($TPL_VAR["anc_id"]=='anc_bo_extra'){?>class="active"<?php }?>><a href="#anc_bo_extra">여분필드</a></li>
	</ul>
	<div class="tab-bottom-line"></div>
</div>