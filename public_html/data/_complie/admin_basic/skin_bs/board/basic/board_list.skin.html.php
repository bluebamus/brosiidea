<?php /* Template_ 2.2.8 2018/04/30 10:48:39 /home1/bluebamus2/public_html/admin/admin_theme/admin_basic/skin_bs/board/basic/board_list.skin.html 000016426 */  $this->include_("eb_admin_paging");
$TPL_board_list_1=empty($TPL_VAR["board_list"])||!is_array($TPL_VAR["board_list"])?0:count($TPL_VAR["board_list"]);?>
<?php if (!defined('_GNUBOARD_')) exit;
add_stylesheet('<link rel="stylesheet" href="/admin/admin_theme/admin_basic/plugins/jsgrid/jsgrid.min.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/admin/admin_theme/admin_basic/plugins/jsgrid/jsgrid-theme.min.css" type="text/css" media="screen">',0);
?>

<style>
@media screen and (max-width:600px) {
	.admin-board-list .eyoom-form .trans-col .col {width:inherit;float:left;margin-bottom:0}
	.admin-board-list .eyoom-form .trans-col .col-2 {width:40%}
	.admin-board-list .eyoom-form .trans-col .col-4 {width:60%}
	.admin-board-list .eyoom-form .trans-col .col .sm-margin-bottom-10 {margin-bottom:0}
}
</style>

<div class="admin-board-list">
	<form id="fsearch" name="fsearch" class="eyoom-form" action="./" method="get">
	<div class="headline">
		<h4><strong>게시판 관리</strong></h4>
<?php if(!$GLOBALS["wmode"]){?>
		<a href="<?php echo EYOOM_ADMIN_URL?>/?dir=board&pid=board_form" class="btn-e btn-e-purple btn-e-xs pull-right margin-top-5"><i class="fa fa-plus"></i> 게시판 추가</a>
<?php }?>
		<div class="clearfix"></div>
	</div>
	<div class="margin-bottom-30"></div>

	<div class="admin-search-box trans-col">
		<div class="row">
			<div class="col col-2">
				<label for="sfl" class="sound_only">검색대상</label>
				<label class="select margin-bottom-0">
					<select name="sfl" id="sfl">
					    <option value="bo_table"<?php echo get_selected($_GET["sfl"],"bo_table")?>>TABLE</option>
					    <option value="bo_subject"<?php echo get_selected($_GET["sfl"],"bo_subject")?>>제목</option>
					    <option value="a.gr_id"<?php echo get_selected($_GET["sfl"],"a.gr_id")?>>그룹ID</option>
					</select>
					<i></i>
				</label>
			</div>
			<div class="col col-4">
				<label for="stx" class="sound_only">검색어<strong class="sound_only"> 필수</strong></label>
				<input type="hidden" name="dir" value="<?php echo $GLOBALS["dir"]?>" id="dir">
				<input type="hidden" name="pid" value="<?php echo $GLOBALS["pid"]?>" id="pid">
				<label class="input input-button margin-bottom-0">
					<input type="text" name="stx" value="<?php echo $GLOBALS["stx"]?>" id="stx" required class="frm_input">
					<div class="button"><input type="submit" value="검색">검색</div>
				</label>
			</div>
		</div>
	</div>
	</form>
	<div class="margin-bottom-30"></div>

	<form name="fboardlist" id="fboardlist" action="<?php echo $GLOBALS["action_url"]?>" method="post" onsubmit="return fboardlist_submit(this);" class="eyoom-form">
	<input type="hidden" name="sst" id="sst" value="<?php echo $GLOBALS["sst"]?>">
	<input type="hidden" name="sod" id="sod" value="<?php echo $GLOBALS["sod"]?>">
	<input type="hidden" name="sfl" value="<?php echo $GLOBALS["sfl"]?>">
	<input type="hidden" name="stx" value="<?php echo $GLOBALS["stx"]?>">
	<input type="hidden" name="page" value="<?php echo $GLOBALS["page"]?>">
	<input type="hidden" name="token" value="<?php echo $GLOBALS["token"]?>">

	<div class="row">
		<div class="col col-9">
			<div class="local_ov01 local_ov padding-top-5 clearfix">
			    <span class="font-size-12 color-grey">
			    	<a href="<?php echo EYOOM_ADMIN_URL?>/?dir=<?php echo $GLOBALS["dir"]?>&amp;pid=<?php echo $GLOBALS["pid"]?>" class="ov_listall">[전체목록]</a><span class="margin-left-10 margin-right-10 color-light-grey">|</span>생성된 게시판수 <?php echo number_format($GLOBALS["total_count"])?>개
			    </span>
			</div>
		</div>
		<div class="col col-3">
			<section>
				<label for="sort_list" class="select">
					<select name="sort_list" id="sort_list" onchange="sorting_list(this.form, this.value);">
						<option value="">:: 정렬방식선택 ::</option>
						<option value="a.gr_id|asc" <?php if($GLOBALS["sst"]=='a.gr_id'&&$GLOBALS["sod"]=='asc'){?>selected<?php }?>>그룹 정방향 (↓)</option>
						<option value="a.gr_id|desc" <?php if($GLOBALS["sst"]=='a.gr_id'&&$GLOBALS["sod"]=='desc'){?>selected<?php }?>>그룹 역방향 (↑) </option>
						<option value="bo_table|asc" <?php if($GLOBALS["sst"]=='bo_table'&&$GLOBALS["sod"]=='asc'){?>selected<?php }?>>TABLE 정방향 (↓)</option>
						<option value="bo_table|desc" <?php if($GLOBALS["sst"]=='bo_table'&&$GLOBALS["sod"]=='desc'){?>selected<?php }?>>TABLE 역방향 (↑) </option>
						<option value="bo_skin|asc" <?php if($GLOBALS["sst"]=='bo_skin'&&$GLOBALS["sod"]=='asc'){?>selected<?php }?>>스킨 정방향 (↓)</option>
						<option value="bo_skin|desc" <?php if($GLOBALS["sst"]=='bo_skin'&&$GLOBALS["sod"]=='desc'){?>selected<?php }?>>스킨 역방향 (↑) </option>
						<option value="bo_mobile_skin|asc" <?php if($GLOBALS["sst"]=='bo_mobile_skin'&&$GLOBALS["sod"]=='asc'){?>selected<?php }?>>모바일스킨 정방향 (↓)</option>
						<option value="bo_mobile_skin|desc" <?php if($GLOBALS["sst"]=='bo_mobile_skin'&&$GLOBALS["sod"]=='desc'){?>selected<?php }?>>모바일스킨 역방향 (↑) </option>
						<option value="bo_subject|asc" <?php if($GLOBALS["sst"]=='bo_subject'&&$GLOBALS["sod"]=='asc'){?>selected<?php }?>>제목 정방향 (↓)</option>
						<option value="bo_subject|desc" <?php if($GLOBALS["sst"]=='bo_subject'&&$GLOBALS["sod"]=='desc'){?>selected<?php }?>>제목 역방향 (↑) </option>
						<option value="bo_use_sns|asc" <?php if($GLOBALS["sst"]=='bo_use_sns'&&$GLOBALS["sod"]=='asc'){?>selected<?php }?>>SNS사용 정방향 (↓)</option>
						<option value="bo_use_sns|desc" <?php if($GLOBALS["sst"]=='bo_use_sns'&&$GLOBALS["sod"]=='desc'){?>selected<?php }?>>SNS사용 역방향 (↑) </option>
						<option value="bo_use_search|asc" <?php if($GLOBALS["sst"]=='bo_use_search'&&$GLOBALS["sod"]=='asc'){?>selected<?php }?>>검색사용 정방향 (↓)</option>
						<option value="bo_use_search|desc" <?php if($GLOBALS["sst"]=='bo_use_search'&&$GLOBALS["sod"]=='desc'){?>selected<?php }?>>검색사용 역방향 (↑) </option>
						<option value="bo_order|asc" <?php if($GLOBALS["sst"]=='bo_order'&&$GLOBALS["sod"]=='asc'){?>selected<?php }?>>출력순서 정방향 (↓)</option>
						<option value="bo_order|desc" <?php if($GLOBALS["sst"]=='bo_order'&&$GLOBALS["sod"]=='desc'){?>selected<?php }?>>출력순서 역방향 (↑) </option>
					</select><i></i>
				</label>
			</section>
		</div>
	</div>

<?php if(G5_IS_MOBILE){?>
	<p class="font-size-11 color-grey text-right margin-bottom-5"><i class="fa fa-info-circle"></i> Note! 좌우스크롤 가능 (<i class="fa fa-arrows-h"></i>)</p>
<?php }?>

	<div id="board-list"></div>

	<div class="margin-top-20">
	    <input type="submit" name="act_button" value="선택수정" class="btn-e btn-e-xs btn-e-red" onclick="document.pressed=this.value">
<?php if($GLOBALS["is_admin"]=='super'){?>
	    <input type="submit" name="act_button" value="선택삭제" class="btn-e btn-e-xs btn-e-dark" onclick="document.pressed=this.value">
<?php }?>
	</div>
	</form>
</div>

<div class="modal fade board-copy-modal" tabindex="-1" role="dialog" aria-labelledby="boardCopyLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 id="boardCopyLabel" class="modal-title"><strong><i class="fa fa-clone"></i> 게시판 복사</strong></h4>
            </div>
            <div class="modal-body">
                <iframe id="board-copy-iframe" width="100%" frameborder="0"></iframe>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn-e btn-e-lg btn-e-dark" type="button"><i class="fa fa-close"></i> 닫기</button>
            </div>
        </div>
    </div>
</div>

<?php echo eb_admin_paging('basic')?>


<script src="/admin/admin_theme/admin_basic/plugins/jsgrid/jsgrid.min.js"></script>
<script src="/admin/admin_theme/admin_basic/js/jsgrid.js"></script>
<script>
/*--------------------------------------
	Modal
--------------------------------------*/
function eb_modal(href) {
    $('.board-copy-modal').modal('show').on('hidden.bs.modal', function () {
        $("#board-copy-iframe").attr("src", "");
        $('html').css({overflow: ''});
    });
    $('.board-copy-modal').modal('show').on('shown.bs.modal', function () {
        $("#board-copy-iframe").attr("src", href);
        $('#board-copy-iframe').height(450);
        $('html').css({overflow: 'hidden'});
    });
    return false;
}

/*--------------------------------------
	Table
--------------------------------------*/
!function () {
    var db = {
        deleteItem: function (deletingClient) {
            var clientIndex = $.inArray(deletingClient, this.clients);
            this.clients.splice(clientIndex, 1)
        },
        insertItem: function (insertingClient) {
            this.clients.push(insertingClient)
        },
        loadData  : function (filter) {
            return $.grep(this.clients, function (client) {
                return !(filter.No && !(client.No.indexOf(filter.No) > -1) || filter.회원구분 && !(client.회원구분.indexOf(filter.회원구분) > -1) || filter.아이디 && !(client.아이디.indexOf(filter.아이디) > -1) || filter.이름 && !(client.이름.indexOf(filter.이름) > -1) || filter.휴대전화 && !(client.휴대전화.indexOf(filter.휴대전화) > -1) || filter.전화번호 && !(client.전화번호.indexOf(filter.전화번호) > -1) || filter.이메일 && !(client.이메일.indexOf(filter.이메일) > -1) || filter.가입일 && !(client.가입일.indexOf(filter.가입일) > -1) || filter.최신로그인 && !(client.최신로그인.indexOf(filter.최신로그인) > -1) || filter.상태 && !(client.상태.indexOf(filter.상태) > -1) )
            })
        },
        updateItem: function (updatingClient) {}
    };
    window.db    = db,
    db.clients   = [
<?php if($TPL_board_list_1){$TPL_I1=-1;foreach($TPL_VAR["board_list"] as $TPL_V1){$TPL_I1++;?>
        {
	        체크: "<label for='chk_<?php echo $TPL_I1?>' class='checkbox'><input type='checkbox' name='chk[]' id='chk_<?php echo $TPL_I1?>' value='<?php echo $TPL_I1?>'><i></i></label>",
	        관리: "<a href='<?php echo EYOOM_ADMIN_URL?>/?dir=board&amp;pid=board_form&amp;w=u&amp;bo_table=<?php echo $TPL_V1["bo_table"]?>&amp;<?php echo $GLOBALS["qstr"]?>'><u>수정</u></a> <a href='<?php echo EYOOM_ADMIN_URL?>/?dir=board&amp;pid=board_copy&amp;bo_table=<?php echo $TPL_V1["bo_table"]?>&wmode=1' onclick='eb_modal(this.href, \"copy\"); return false;' class='margin-left-10'><u>복사</u></a> <a href='<?php echo EYOOM_ADMIN_URL?>/?dir=board&amp;pid=board_extend&amp;bo_table=<?php echo $TPL_V1["bo_table"]?>' class='margin-left-10'><u>확장</u></a>",
	        그룹: "<label class='select'><?php echo $TPL_V1["gr_select"]?><i></i></label>",
	        TABLE: "<input type='hidden' name='board_table[<?php echo $TPL_I1?>]' value='<?php echo $TPL_V1["bo_table"]?>'><a href='<?php echo G5_BBS_URL?>/board.php?bo_table=<?php echo $TPL_V1["bo_table"]?>'><a href='<?php echo G5_BBS_URL?>/board.php?bo_table=<?php echo $TPL_V1["bo_table"]?>' class='bo_href btn-e btn-e-dark' style='width:100%' target='_blank'><?php echo $TPL_V1["bo_table"]?></a>",
	        스킨: "<label class='select'><?php echo $TPL_V1["skin_select"]?><i></i></label>",
	        모바일스킨: "<label class='select'><?php echo $TPL_V1["mobile_skin_select"]?><i></i></label>",
	        제목: "<label class='input'><input type='text' name='bo_subject[<?php echo $TPL_I1?>]' id='bo_subject_<?php echo $TPL_I1?>' value='<?php echo get_text($TPL_V1["bo_subject"])?>' required></label>",
	        읽기P: "<label class='input'><input type='text' name='bo_read_point[<?php echo $TPL_I1?>]' id='bo_read_point_<?php echo $TPL_I1?>' value='<?php echo $TPL_V1["bo_read_point"]?>' style='text-align:right;'></label>",
	        쓰기P: "<label class='input'><input type='text' name='bo_write_point[<?php echo $TPL_I1?>]' id='bo_write_point_<?php echo $TPL_I1?>' value='<?php echo $TPL_V1["bo_write_point"]?>' style='text-align:right;'></label>",
	        댓글P: "<label class='input'><input type='text' name='bo_comment_point[<?php echo $TPL_I1?>]' id='bo_comment_point_<?php echo $TPL_I1?>' value='<?php echo $TPL_V1["bo_comment_point"]?>' style='text-align:right;'></label>",
	        다운P: "<label class='input'><input type='text' name='bo_download_point[<?php echo $TPL_I1?>]' id='bo_download_point_<?php echo $TPL_I1?>' value='<?php echo $TPL_V1["bo_download_point"]?>' style='text-align:right;'></label>",
	        SNS사용: "<label class='checkbox'><input type='checkbox' name='bo_use_sns[<?php echo $TPL_I1?>]' id='bo_use_sns_<?php echo $TPL_I1?>' value='1' <?php if($TPL_V1["bo_use_sns"]){?>checked<?php }?>><i></i></label>",
	        검색사용: "<label class='checkbox'><input type='checkbox' name='bo_use_search[<?php echo $TPL_I1?>]' id='bo_use_search_<?php echo $TPL_I1?>' value='1' <?php if($TPL_V1["bo_use_search"]){?>checked<?php }?>><i></i></label>",
	        출력순서: "<label class='input'><input type='text' name='bo_order[<?php echo $TPL_I1?>]' id='bo_order_<?php echo $TPL_I1?>' value='<?php echo $TPL_V1["bo_order"]?>' style='text-align:right;'></label>",
	        접속기기: "<label class='select'><select name='bo_device[<?php echo $TPL_I1?>]' id='bo_device_<?php echo $TPL_I1?>'><option value='both' <?php if($TPL_V1["bo_device"]=='both'){?>selected<?php }?>>모두</option><option value='pc' <?php if($TPL_V1["bo_device"]=='pc'){?>selected<?php }?>>PC</option><option value='mobile' <?php if($TPL_V1["bo_device"]=='mobile'){?>selected<?php }?>>모바일</option></select><i></i></label>",
        },
<?php }}?>
    ]
}();

$(function() {
    $("#board-list").jsGrid({
        filtering      : false,
        editing        : false,
        sorting        : false,
        paging         : true,
        autoload       : true,
        controller     : db,
        deleteConfirm  : "정말로 삭제하시겠습니까?\n한번 삭제된 데이터는 복구할수 없습니다.",
        pageButtonCount: 5,
        pageSize       : 15,
        width          : "100%",
        height         : "auto",
        fields         : [
            { name: "체크", type: "text", width: 40 },
            { name: "관리", type: "text", align: "center", width: 110, headercss: "set-btn-header", css: "set-btn-field" },
            { name: "그룹", type: "text", width: 150 },
            { name: "TABLE", type: "text", width: 150 },
            { name: "스킨", type: "text", width: 105 },
            { name: "모바일스킨", type: "number", width: 105 },
            { name: "제목", type: "text", width: 200 },
            { name: "읽기P", type: "text", width: 60 },
            { name: "쓰기P", type: "text", width: 60 },
            { name: "댓글P", type: "text", width: 60 },
            { name: "다운P", type: "text", width: 60 },
            { name: "SNS사용", type: "text", width: 70 },
            { name: "검색사용", type: "text", width: 70 },
            { name: "출력순서", type: "text", width: 70 },
            { name: "접속기기", type: "text", width: 105 },
        ]
    });

    var $chk = $(".jsgrid-table th:first-child");
	if ($chk.text() == '체크') {
		var html = '<label for="chkall" class="checkbox"><input type="checkbox" name="chkall" id="chkall" value="1" onclick="check_all(this.form)"><i></i></label>';
		$chk.html(html);
	}
});

function sorting_list(f, str) {
    var sort = str.split('|');

	$("#sst").val(sort[0]);
	$("#sod").val(sort[1]);

	if (sort[0] && sort[1]) {
		f.action = "<?php echo EYOOM_ADMIN_URL?>/?dir=<?php echo $GLOBALS["dir"]?>&pid=<?php echo $GLOBALS["pid"]?>";
		f.submit();
	}
}

function fboardlist_submit(f) {
    if (!is_checked("chk[]")) {
        alert(document.pressed+" 하실 항목을 하나 이상 선택하세요.");
        return false;
    }

    if(document.pressed == "선택삭제") {
        if(!confirm("선택한 자료를 정말 삭제하시겠습니까?")) {
            return false;
        }
    }

    return true;
}
</script>