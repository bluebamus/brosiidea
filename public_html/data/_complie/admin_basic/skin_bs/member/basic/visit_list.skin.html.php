<?php /* Template_ 2.2.8 2018/04/30 10:48:43 /home1/bluebamus2/public_html/admin/admin_theme/admin_basic/skin_bs/member/basic/visit_list.skin.html 000015277 */  $this->include_("eb_admin_paging");
$TPL_page_vi_count_1=empty($TPL_VAR["page_vi_count"])||!is_array($TPL_VAR["page_vi_count"])?0:count($TPL_VAR["page_vi_count"]);
$TPL_period_vi_count_1=empty($TPL_VAR["period_vi_count"])||!is_array($TPL_VAR["period_vi_count"])?0:count($TPL_VAR["period_vi_count"]);
$TPL_page_vi_browser_1=empty($TPL_VAR["page_vi_browser"])||!is_array($TPL_VAR["page_vi_browser"])?0:count($TPL_VAR["page_vi_browser"]);
$TPL_period_vi_browser_1=empty($TPL_VAR["period_vi_browser"])||!is_array($TPL_VAR["period_vi_browser"])?0:count($TPL_VAR["period_vi_browser"]);
$TPL_page_vi_domain_1=empty($TPL_VAR["page_vi_domain"])||!is_array($TPL_VAR["page_vi_domain"])?0:count($TPL_VAR["page_vi_domain"]);
$TPL_period_vi_domain_1=empty($TPL_VAR["period_vi_domain"])||!is_array($TPL_VAR["period_vi_domain"])?0:count($TPL_VAR["period_vi_domain"]);
$TPL_page_vi_os_1=empty($TPL_VAR["page_vi_os"])||!is_array($TPL_VAR["page_vi_os"])?0:count($TPL_VAR["page_vi_os"]);
$TPL_period_vi_os_1=empty($TPL_VAR["period_vi_os"])||!is_array($TPL_VAR["period_vi_os"])?0:count($TPL_VAR["period_vi_os"]);
$TPL_vi_list_1=empty($TPL_VAR["vi_list"])||!is_array($TPL_VAR["vi_list"])?0:count($TPL_VAR["vi_list"]);?>
<?php if (!defined('_GNUBOARD_')) exit;
add_stylesheet('<link rel="stylesheet" href="/admin/admin_theme/admin_basic/plugins/c3/c3.min.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/admin/admin_theme/admin_basic/plugins/jsgrid/jsgrid.min.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/admin/admin_theme/admin_basic/plugins/jsgrid/jsgrid-theme.min.css" type="text/css" media="screen">',0);
?>

<style>
/*----- Chart -----*/
.chart-wrap .chart-item {position:relative;float:left;width:50%}
.chart-wrap .chart-item .btn-group .dropdown-menu {left:inherit;right:0}
.chart-wrap .chart-item.item-left {padding-right:10px}
.chart-wrap .chart-item.item-right {padding-left:10px}
.chart-wrap .chart-item-in {position:relative;overflow:hidden;width:100%;height:auto;border:1px solid #d5d5d5;padding:10px 0;background:#fff;border-radius:2px !important}
.chart-wrap .chart-item-in.padding-left-10 {padding-left:10px}
.chart-wrap .chart-item-in.padding-right-10 {padding-right:10px}
.chart-wrap .chart-item-in.padding-x-10 {padding-left:10px;padding-right:10px}
@media (max-width: 767px) {
	.chart-wrap .chart-item {position:relative;float:left;width:100%;margin-bottom:30px}
	.chart-wrap .chart-item.item-left {padding-right:0}
	.chart-wrap .chart-item.item-right {padding-left:0}
}
</style>

<div class="admin-visit-list">
	<div class="headline">
		<h4><strong>접속자집계</strong></h4>
	</div>
	<div class="margin-bottom-30"></div>

	<form name="fvisit" id="fvisit" method="get" action="./" class="eyoom-form">
	<input type="hidden" name="dir" id="dir" value="<?php echo $GLOBALS["dir"]?>">
	<input type="hidden" name="pid" id="pid" value="<?php echo $GLOBALS["pid"]?>">
	<h5 class="margin-bottom-10"><strong class="color-indigo">기간별 검색</strong></h5>
	<div class="admin-search-box margin-bottom-30">
		<div class="row">
			<div class="col col-3">
				<section>
					<label for="fr_date" class="label">시작일</label>
					<label class="input">
						<i class="icon-append fa fa-calendar-o"></i>
						<input type="text" name="fr_date" id="fr_date" value="<?php echo $GLOBALS["fr_date"]?>" required>
					</label>
				</section>
			</div>
			<div class="col col-3">
				<section>
					<label for="to_date" class="label">종료일</label>
					<label class="input">
						<i class="icon-append fa fa-calendar-o"></i>
						<input type="text" name="to_date" id="to_date" value="<?php echo $GLOBALS["to_date"]?>" required>
					</label>
				</section>
			</div>
			<div class="col col-3">
				<section class="label-height">
					<input type="submit" value="검색" class="btn-e btn-e-dark">
				</section>
			</div>
		</div>
	</div>
	</form>

	<div class="chart-wrap">
		<div class="chart-item item-left">
			<div class="main-headline">
				<h5><strong>시간별 접속자</strong></h5>
				<div class="clearfix"></div>
			</div>
			<div class="chart-item-in">
				<div class="chart-canvas">
					<div id="chartTime"></div>
				</div>
			</div>
		</div>

		<div class="chart-item item-right">
			<div class="main-headline">
				<h5><strong>브라우저별 접속자 비율</strong></h5>
				<div class="clearfix"></div>
			</div>
			<div class="chart-item-in">
				<div class="chart-canvas">
					<div id="chartBrowser"></div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="margin-bottom-30 hidden-xs"></div>
	</div>

	<div class="margin-bottom-20"></div>

	<div class="chart-wrap">
		<div class="chart-item item-left">
			<div class="main-headline">
				<h5><strong>도메인별 접속자 비율</strong></h5>
				<div class="clearfix"></div>
			</div>
			<div class="chart-item-in">
				<div class="chart-canvas">
					<div id="chartDomain"></div>
				</div>
			</div>
		</div>

		<div class="chart-item item-right">
			<div class="main-headline">
				<h5><strong>OS별 접속자 비율</strong></h5>
				<div class="clearfix"></div>
			</div>
			<div class="chart-item-in">
				<div class="chart-canvas">
					<div id="chartOS"></div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="margin-bottom-30 hidden-xs"></div>
	</div>

	<div class="margin-bottom-20"></div>

<?php if(G5_IS_MOBILE){?>
	<p class="font-size-11 color-grey text-right margin-bottom-5"><i class="fa fa-info-circle"></i> Note! 좌우스크롤 가능 (<i class="fa fa-arrows-h"></i>)</p>
<?php }?>

	<div id="visit-list"></div>

</div>

<?php echo eb_admin_paging('basic')?>


<script src="/admin/admin_theme/admin_basic/plugins/d3/d3.min.js"></script>
<script src="/admin/admin_theme/admin_basic/plugins/c3/c3.min.js"></script>
<script src="/admin/admin_theme/admin_basic/plugins/jsgrid/jsgrid.min.js"></script>
<script src="/admin/admin_theme/admin_basic/js/jsgrid.js"></script>
<script src="/admin/admin_theme/admin_basic/plugins/eyoom-form/plugins/jquery-ui/jquery-ui.min.js"></script>

<script>
/*--------------------------------------
	Chart
--------------------------------------*/
// ----- 시간별 접속자, 회원가입 ----- //
var chartTime = c3.generate({
	bindto: '#chartTime',
    data: {
        x: '시간',
        columns: [
            ['시간', 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23],
            // 어제의 시간별 접속자
            ['접속자', <?php if($TPL_page_vi_count_1){$TPL_I1=-1;foreach($TPL_VAR["page_vi_count"] as $TPL_V1){$TPL_I1++;?><?php echo $TPL_V1?><?php if($TPL_page_vi_count_1!=$TPL_I1+ 1){?>,<?php }?><?php }}?>]
        ],
		types: {
			접속자: 'bar'
		}
    },
    color: {
        pattern: ['#007AFF']
    },
    axis: {
	    x: {
            label: {
                text: '시간 (단위: 시)',
                position: 'outer-center'
            }
	    },
		y: {
            tick: {
                format: d3.format(",")
            },
			label: {
				text: '접속자 (단위: 명)',
				position: 'outer-middle'
			}
		}
    }
});

setTimeout(function() {
    chartTime.load({
        columns: [
	        // 오늘의 시간별 접속자
            ['접속자', <?php if($TPL_period_vi_count_1){$TPL_I1=-1;foreach($TPL_VAR["period_vi_count"] as $TPL_V1){$TPL_I1++;?><?php echo $TPL_V1?><?php if($TPL_period_vi_count_1!=$TPL_I1+ 1){?>,<?php }?><?php }}?>]
        ]
    });
}, 1500);

// ----- 브라우저별 접속자	----- //
var chartBrowser = c3.generate({
	bindto: '#chartBrowser',
    data: {
	    // 어제의 브라우저별 접속자
        columns: [
<?php if($TPL_page_vi_browser_1){$TPL_I1=-1;foreach($TPL_VAR["page_vi_browser"] as $TPL_K1=>$TPL_V1){$TPL_I1++;?>
            ['<?php echo $TPL_K1?>', <?php echo $TPL_V1?>]<?php if($TPL_page_vi_browser_1!=$TPL_I1+ 1){?>,<?php }?>
<?php }}?>
        ],
        type : 'donut',
        onclick: function (d, i) { console.log("onclick", d, i); },
        onmouseover: function (d, i) { console.log("onmouseover", d, i); },
        onmouseout: function (d, i) { console.log("onmouseout", d, i); }
    },
    donut: {
        title: "브라우저별 접속자",
        label: {
            format: function (value, ratio, id) {
                return d3.format()(value);
            }
        }
    }
});

setTimeout(function() {
    chartBrowser.load({
	    // 오늘의 브라우저별 접속자
        columns: [
<?php if($TPL_period_vi_browser_1){$TPL_I1=-1;foreach($TPL_VAR["period_vi_browser"] as $TPL_K1=>$TPL_V1){$TPL_I1++;?>
            ['<?php echo $TPL_K1?>', <?php echo $TPL_V1?>]<?php if($TPL_period_vi_browser_1!=$TPL_I1+ 1){?>,<?php }?>
<?php }}?>
        ]
    });
}, 1500);

// ----- 도메인별 접속자 ----- //
var chartDomain = c3.generate({
	bindto: '#chartDomain',
    data: {
	    // 어제의 도메인별 접속자
        columns: [
<?php if($TPL_page_vi_domain_1){$TPL_I1=-1;foreach($TPL_VAR["page_vi_domain"] as $TPL_K1=>$TPL_V1){$TPL_I1++;?>
<?php if($TPL_K1&&$TPL_I1< 10){?>
            ['<?php echo $TPL_K1?>', <?php echo $TPL_V1?>]<?php if($TPL_page_vi_domain_1!=$TPL_I1+ 1){?>,<?php }?>
<?php }?>
<?php }}?>
        ],
        type : 'donut',
        onclick: function (d, i) { console.log("onclick", d, i); },
        onmouseover: function (d, i) { console.log("onmouseover", d, i); },
        onmouseout: function (d, i) { console.log("onmouseout", d, i); }
    },
    donut: {
        title: "도메인별 접속자",
        label: {
            format: function (value, ratio, id) {
                return d3.format()(value);
            }
        }
    }
});

setTimeout(function() {
    chartDomain.load({
	    // 도메인별 접속자
        columns: [
<?php if($TPL_period_vi_domain_1){$TPL_I1=-1;foreach($TPL_VAR["period_vi_domain"] as $TPL_K1=>$TPL_V1){$TPL_I1++;?>
<?php if($TPL_K1&&$TPL_I1< 10){?>
            ['<?php echo $TPL_K1?>', <?php echo $TPL_V1?>]<?php if($TPL_period_vi_domain_1!=$TPL_I1+ 1){?>,<?php }?>
<?php }?>
<?php }}?>
        ]
    });
}, 1500);

// ----- OS 접속자 비율 ----- //
var chartOS = c3.generate({
	bindto: '#chartOS',
    data: {
	    // 어제의 OS 접속자
        columns: [
<?php if($TPL_page_vi_os_1){$TPL_I1=-1;foreach($TPL_VAR["page_vi_os"] as $TPL_K1=>$TPL_V1){$TPL_I1++;?>
            ['<?php if(!$TPL_K1){?>기타<?php }else{?><?php echo $TPL_K1?><?php }?>', <?php echo $TPL_V1?>]<?php if($TPL_page_vi_os_1!=$TPL_I1+ 1){?>,<?php }?>
<?php }}?>
        ],
        type : 'donut',
        onclick: function (d, i) { console.log("onclick", d, i); },
        onmouseover: function (d, i) { console.log("onmouseover", d, i); },
        onmouseout: function (d, i) { console.log("onmouseout", d, i); }
    },
    donut: {
        title: "OS별 접속자",
        label: {
            format: function (value, ratio, id) {
                return d3.format()(value);
            }
        }
    }
});

setTimeout(function() {
    chartOS.load({
	    // 도메인별 접속자
        columns: [
<?php if($TPL_period_vi_os_1){$TPL_I1=-1;foreach($TPL_VAR["period_vi_os"] as $TPL_K1=>$TPL_V1){$TPL_I1++;?>
            ['<?php if(!$TPL_K1){?>기타<?php }else{?><?php echo $TPL_K1?><?php }?>', <?php echo $TPL_V1?>]<?php if($TPL_period_vi_os_1!=$TPL_I1+ 1){?>,<?php }?>
<?php }}?>
        ]
    });
}, 1500);

/*--------------------------------------
	Table
--------------------------------------*/
!function () {
    var db = {
        deleteItem: function (deletingClient) {
            var clientIndex = $.inArray(deletingClient, this.clients);
            this.clients.splice(clientIndex, 1)
        },
        insertItem: function (insertingClient) {
            this.clients.push(insertingClient)
        },
        loadData  : function (filter) {
            return $.grep(this.clients, function (client) {
                return !(filter.IP && !(client.IP.indexOf(filter.IP) > -1) || filter.접속경로 && !(client.접속경로.indexOf(filter.접속경로) > -1))
            })
        },
        updateItem: function (updatingClient) {}
    };
    window.db    = db,
    db.clients   = [
<?php if($TPL_vi_list_1){foreach($TPL_VAR["vi_list"] as $TPL_V1){?>
        {
	        IP: "<?php echo $TPL_V1["ip"]?>",
	        접속경로: "<?php if($TPL_V1["title"]){?><a href='<?php echo $TPL_V1["vi_referer"]?>' target='_blank'><?php echo $TPL_V1["title"]?></a><?php }?>",
	        브라우저: "<?php echo $TPL_V1["brow"]?>",
	        OS: "<?php echo $TPL_V1["os"]?>",
	        //접속기기: "<?php echo $TPL_V1["device"]?>",
	        일시: "<?php echo $TPL_V1["vi_date"]?> <?php echo $TPL_V1["vi_time"]?>"
        },
<?php }}?>
    ]
}();

$(document).ready(function() {
    $("#visit-list").jsGrid({
        filtering      : false,
        editing        : false,
        sorting        : false,
        paging         : true,
        autoload       : true,
        controller     : db,
        deleteConfirm  : "정말로 삭제하시겠습니까?\n한번 삭제된 데이터는 복구할수 없습니다.",
        pageButtonCount: 5,
        pageSize       : 15,
        width          : "100%",
        height         : "auto",
        fields         : [
            { name: "IP", type: "text", width: 130 },
            { name: "접속경로", type: "text", width: 300 },
            { name: "브라우저", type: "text", width: 100 },
            { name: "OS", type: "text", width: 100 },
            //{ name: "접속기기", type: "text", width: 100 },
            { name: "일시", type: "text", width: 130 },
        ]
    })
});

/*--------------------------------------
	Datepicker
--------------------------------------*/
$(document).ready(function() {
    $('#fr_date').datepicker({
        dateFormat: 'yy-mm-dd',
        prevText: '<i class="fa fa-angle-left"></i>',
        nextText: '<i class="fa fa-angle-right"></i>',
        showMonthAfterYear: true,
        monthNames: ['년 1월','년 2월','년 3월','년 4월','년 5월','년 6월','년 7월','년 8월','년 9월','년 10월','년 11월','년 12월'],
        monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
        dayNamesMin: ['일','월','화','수','목','금','토'],
        onSelect: function(selectedDate){
            $('#to_date').datepicker('option', 'minDate', selectedDate);
        }
    });
    $('#to_date').datepicker({
        dateFormat: 'yy-mm-dd',
        prevText: '<i class="fa fa-angle-left"></i>',
        nextText: '<i class="fa fa-angle-right"></i>',
        showMonthAfterYear: true,
        monthNames: ['년 1월','년 2월','년 3월','년 4월','년 5월','년 6월','년 7월','년 8월','년 9월','년 10월','년 11월','년 12월'],
        monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
        dayNamesMin: ['일','월','화','수','목','금','토'],
        onSelect: function(selectedDate){
            $('#fr_date').datepicker('option', 'maxDate', selectedDate);
        }
    });
})
</script>