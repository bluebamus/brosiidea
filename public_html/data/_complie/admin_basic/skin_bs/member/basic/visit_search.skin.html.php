<?php /* Template_ 2.2.8 2018/04/30 10:48:43 /home1/bluebamus2/public_html/admin/admin_theme/admin_basic/skin_bs/member/basic/visit_search.skin.html 000004895 */  $this->include_("eb_admin_paging");
$TPL_vi_search_1=empty($TPL_VAR["vi_search"])||!is_array($TPL_VAR["vi_search"])?0:count($TPL_VAR["vi_search"]);?>
<?php if (!defined('_GNUBOARD_')) exit;
add_stylesheet('<link rel="stylesheet" href="/admin/admin_theme/admin_basic/plugins/jsgrid/jsgrid.min.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/admin/admin_theme/admin_basic/plugins/jsgrid/jsgrid-theme.min.css" type="text/css" media="screen">',0);
?>

<div class="admin-visit-search">
	<div class="headline">
		<h4><strong>접속자검색</strong></h4>
	</div>
	<div class="margin-bottom-30"></div>

	<form name="fvisit" id="fvisit" method="get" class="eyoom-form" onsubmit="return fvisit_submit(this);">
	<input type="hidden" name="dir" id="dir" value="<?php echo $GLOBALS["dir"]?>">
	<input type="hidden" name="pid" id="pid" value="<?php echo $GLOBALS["pid"]?>">
	<h5 class="margin-bottom-10"><strong class="color-indigo">검색</strong></h5>
	<div class="admin-search-box margin-bottom-30">
		<div class="row">
			<div class="col col-2">
				<section>
					<label for="sch_sort" class="label">검색분류</label>
					<label class="select">
						<select name="sfl" id="sch_sort" class="search_sort">
					        <option value="vi_ip" <?php echo get_selected($GLOBALS["sfl"],'vi_ip')?>>IP</option>
					        <option value="vi_referer" <?php echo get_selected($GLOBALS["sfl"],'vi_referer')?>>접속경로</option>
					        <option value="vi_date" <?php echo get_selected($GLOBALS["sfl"],'vi_date')?>>날짜</option>
						</select><i></i>
					</label>
				</section>
			</div>
			<div class="col col-3">
				<section>
					<label for="sch_word" class="label">검색어</label>
					<label class="input">
						<input type="text" name="stx" id="sch_word" value="<?php echo stripslashes($GLOBALS["stx"])?>">
					</label>
				</section>
			</div>
			<div class="col col-2">
				<section class="label-height">
					<input type="submit" value="검색" class="btn-e btn-e-dark">
				</section>
			</div>
		</div>
	</div>
	</form>

<?php if(G5_IS_MOBILE){?>
	<p class="font-size-11 color-grey text-right margin-bottom-5"><i class="fa fa-info-circle"></i> Note! 좌우스크롤 가능 (<i class="fa fa-arrows-h"></i>)</p>
<?php }?>

	<div id="visit-search"></div>
</div>

<?php echo eb_admin_paging('basic')?>


<script src="/admin/admin_theme/admin_basic/plugins/jsgrid/jsgrid.min.js"></script>
<script src="/admin/admin_theme/admin_basic/js/jsgrid.js"></script>

<script>
!function () {
    var db = {
        deleteItem: function (deletingClient) {
            var clientIndex = $.inArray(deletingClient, this.clients);
            this.clients.splice(clientIndex, 1)
        },
        insertItem: function (insertingClient) {
            this.clients.push(insertingClient)
        },
        loadData  : function (filter) {
            return $.grep(this.clients, function (client) {
                return !(filter.IP && !(client.IP.indexOf(filter.IP) > -1) || filter.접속경로 && !(client.접속경로.indexOf(filter.접속경로) > -1))
            })
        },
        updateItem: function (updatingClient) {}
    };
    window.db    = db,
    db.clients   = [
<?php if($TPL_vi_search_1){foreach($TPL_VAR["vi_search"] as $TPL_V1){?>
        {
	        IP: "<?php echo $TPL_V1["ip"]?>",
	        접속경로: "<?php if($TPL_V1["title"]){?><a href='<?php echo $TPL_V1["vi_referer"]?>' target='_blank'><?php echo $TPL_V1["title"]?></a><?php }?>",
	        브라우저: "<?php echo $TPL_V1["brow"]?>",
	        OS: "<?php echo $TPL_V1["os"]?>",
	        //접속기기: "<?php echo $TPL_V1["device"]?>",
	        일시: "<?php echo $TPL_V1["vi_date"]?> <?php echo $TPL_V1["vi_time"]?>"
        },
<?php }}?>
    ]
}();

$(document).ready(function(){
    $("#visit-search").jsGrid({
        filtering      : false,
        editing        : false,
        sorting        : false,
        paging         : true,
        autoload       : true,
        controller     : db,
        deleteConfirm  : "정말로 삭제하시겠습니까?\n한번 삭제된 데이터는 복구할수 없습니다.",
        pageButtonCount: 5,
        pageSize       : 15,
        width          : "100%",
        height         : "auto",
        fields         : [
            { name: "IP", type: "text", width: 130 },
            { name: "접속경로", type: "text", width: 300 },
            { name: "브라우저", type: "text", width: 100 },
            { name: "OS", type: "text", width: 100 },
            //{ name: "접속기기", type: "text", width: 100 },
            { name: "일시", type: "text", width: 130 },
        ]
    })
});

function fvisit_submit(f) {
    return true;
}
</script>