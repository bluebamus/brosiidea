<?php /* Template_ 2.2.8 2018/04/30 10:48:43 /home1/bluebamus2/public_html/admin/admin_theme/admin_basic/skin_bs/member/basic/member_list.skin.html 000018308 */  $this->include_("eb_admin_paging");
$TPL_list_1=empty($TPL_VAR["list"])||!is_array($TPL_VAR["list"])?0:count($TPL_VAR["list"]);?>
<?php if (!defined('_GNUBOARD_')) exit;
add_stylesheet('<link rel="stylesheet" href="/admin/admin_theme/admin_basic/plugins/jsgrid/jsgrid.min.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/admin/admin_theme/admin_basic/plugins/jsgrid/jsgrid-theme.min.css" type="text/css" media="screen">',0);
?>

<style>
.admin-member-list .new-member-photo {position:relative;overflow:hidden;width:26px;height:26px;border:1px solid #c5c5c5;background:#fff;padding:1px;margin:0 auto;text-align:center;-webkit-border-radius:50% !important;-moz-border-radius:50% !important;border-radius:50% !important}
.admin-member-list .new-member-photo i {width:22px;height:22px;font-size:12px;line-height:22px;background:#b5b5b5;color:#fff;-webkit-border-radius:50% !important;-moz-border-radius:50% !important;border-radius:50% !important}
.admin-member-list .new-member-photo img {-webkit-border-radius:50% !important;-moz-border-radius:50% !important;border-radius:50% !important}
@media screen and (max-width:600px) {
	.eyoom-form .modal-trans-col .col {width:inherit;float:left;margin-bottom:0}
	.eyoom-form .modal-trans-col .col-2 {width:40%}
	.eyoom-form .modal-trans-col .col-4 {width:60%}
	.eyoom-form .modal-trans-col .col .sm-margin-bottom-10 {margin-bottom:0}
}
</style>

<div class="admin-member-list">
	<form id="form" name="form" class="eyoom-form" action="./" method="get">

<?php if(!$GLOBALS["wmode"]){?>
	<div class="headline">
		<h4><strong>회원 리스트</strong></h4>
		<a href="<?php echo EYOOM_ADMIN_URL?>/?dir=member&pid=member_form" class="btn-e btn-e-purple btn-e-xs pull-right margin-top-5"><i class="fa fa-plus"></i> 회원 추가</a>
		<div class="clearfix"></div>
	</div>

	<div class="cont-text-bg">
		<p class="bg-info font-size-12"><i class="fa fa-info-circle"></i> 회원자료 삭제 시 다른 회원이 기존 회원아이디를 사용하지 못하도록 회원아이디, 이름, 닉네임은 삭제하지 않고 영구 보관합니다.</p>
	</div>
<?php }?>

	<div class="admin-search-box modal-trans-col <?php if(!$GLOBALS["wmode"]){?>margin-top-30<?php }?>">
		<div class="row">
			<div class="col col-2">
				<label for="sfl" class="sound_only">검색대상</label>
				<label class="select margin-bottom-0">
					<select name="sfl" id="sfl">
					    <option value="mb_name"<?php echo get_selected($_GET["sfl"],"mb_name")?>>이름</option>
					    <option value="mb_id"<?php echo get_selected($_GET["sfl"],"mb_id")?>>회원아이디</option>
					    <option value="mb_nick"<?php echo get_selected($_GET["sfl"],"mb_nick")?>>닉네임</option>
					    <option value="mb_email"<?php echo get_selected($_GET["sfl"],"mb_email")?>>E-MAIL</option>
					    <option value="mb_tel"<?php echo get_selected($_GET["sfl"],"mb_tel")?>>전화번호</option>
					    <option value="mb_hp"<?php echo get_selected($_GET["sfl"],"mb_hp")?>>휴대폰번호</option>
					    <option value="mb_datetime"<?php echo get_selected($_GET["sfl"],"mb_datetime")?>>가입일시</option>
					</select>
					<i></i>
				</label>
			</div>
			<div class="col col-4">
				<label for="stx" class="sound_only">검색어<strong class="sound_only"> 필수</strong></label>
				<input type="hidden" name="dir" value="<?php echo $GLOBALS["dir"]?>" id="dir">
				<input type="hidden" name="pid" value="<?php echo $GLOBALS["pid"]?>" id="pid">
				<input type="hidden" name="wmode" value="<?php echo $GLOBALS["wmode"]?>" id="wmode">
				<label class="input input-button margin-bottom-0">
					<input type="text" name="stx" value="<?php echo $GLOBALS["stx"]?>" id="stx" required class="frm_input">
					<div class="button"><input type="submit" value="검색">검색</div>
				</label>
			</div>
		</div>
	</div>
	</form>
	<div class="margin-bottom-20"></div>

	<form name="fboardlist" id="fboardlist" action="<?php echo $GLOBALS["action_url"]?>" method="post" onsubmit="return fboardlist_submit(this);" class="eyoom-form">
	<input type="hidden" name="sst" id="sst" value="<?php echo $GLOBALS["sst"]?>">
	<input type="hidden" name="sod" id="sod" value="<?php echo $GLOBALS["sod"]?>">
	<input type="hidden" name="sfl" value="<?php echo $GLOBALS["sfl"]?>">
	<input type="hidden" name="stx" value="<?php echo $GLOBALS["stx"]?>">
	<input type="hidden" name="page" value="<?php echo $GLOBALS["page"]?>">
	<input type="hidden" name="token" value="<?php echo $GLOBALS["token"]?>">

	<div class="row">
		<div class="col col-9">
			<div class="padding-top-5">
			    <span class="font-size-12 color-grey">
			    	<a href="<?php echo EYOOM_ADMIN_URL?>/?dir=<?php echo $GLOBALS["dir"]?>&amp;pid=<?php echo $GLOBALS["pid"]?>" class="ov_listall">[전체목록]</a><span class="margin-left-10 margin-right-10 color-light-grey">|</span>총회원수 <?php echo number_format($GLOBALS["total_count"])?>명<?php if(!$GLOBALS["wmode"]){?> 중,
    <a href="<?php echo EYOOM_ADMIN_URL?>/?dir=member&amp;pid=member_list&amp;sst=mb_intercept_date&amp;sod=desc&amp;sfl=<?php echo $GLOBALS["sfl"]?>&amp;stx=<?php echo $GLOBALS["stx"]?>"><u>차단 <?php echo number_format($GLOBALS["intercept_count"])?></u></a>명,
    <a href="<?php echo EYOOM_ADMIN_URL?>/?dir=member&amp;pid=member_list&amp;sst=mb_leave_date&amp;sod=desc&amp;sfl=<?php echo $GLOBALS["sfl"]?>&amp;stx=<?php echo $GLOBALS["stx"]?>"><u>탈퇴 <?php echo number_format($GLOBALS["leave_count"])?></u></a>명<?php }?>
			    </span>
			</div>
		</div>
		<div class="col col-3">
			<section>
				<label for="sort_list" class="select">
					<select name="sort_list" id="sort_list" onchange="sorting_list(this.form, this.value);">
						<option value="">:: 정렬방식선택 ::</option>
						<option value="mb_id|asc" <?php if($GLOBALS["sst"]=='mb_id'&&$GLOBALS["sod"]=='asc'){?>selected<?php }?>>회원아이디 정방향 (↓)</option>
						<option value="mb_id|desc" <?php if($GLOBALS["sst"]=='mb_id'&&$GLOBALS["sod"]=='desc'){?>selected<?php }?>>회원아이디 역방향 (↑)</option>
						<option value="mb_name|asc" <?php if($GLOBALS["sst"]=='mb_name'&&$GLOBALS["sod"]=='asc'){?>selected<?php }?>>이름 정방향 (↓)</option>
						<option value="mb_name|desc" <?php if($GLOBALS["sst"]=='mb_name'&&$GLOBALS["sod"]=='desc'){?>selected<?php }?>>이름 역방향 (↑)</option>
						<option value="mb_nick|asc" <?php if($GLOBALS["sst"]=='mb_nick'&&$GLOBALS["sod"]=='asc'){?>selected<?php }?>>닉네임 정방향 (↓)</option>
						<option value="mb_nick|desc" <?php if($GLOBALS["sst"]=='mb_nick'&&$GLOBALS["sod"]=='desc'){?>selected<?php }?>>닉네임 역방향 (↑)</option>
						<option value="mb_certify|asc" <?php if($GLOBALS["sst"]=='mb_certify'&&$GLOBALS["sod"]=='asc'){?>selected<?php }?>>본인확인 정방향 (↓)</option>
						<option value="mb_certify|desc" <?php if($GLOBALS["sst"]=='mb_certify'&&$GLOBALS["sod"]=='desc'){?>selected<?php }?>>본인확인 역방향 (↑)</option>
						<option value="mb_email_certify|asc" <?php if($GLOBALS["sst"]=='mb_email_certify'&&$GLOBALS["sod"]=='asc'){?>selected<?php }?>>메일인증 정방향 (↓)</option>
						<option value="mb_email_certify|desc" <?php if($GLOBALS["sst"]=='mb_email_certify'&&$GLOBALS["sod"]=='desc'){?>selected<?php }?>>메일인증 역방향 (↑)</option>
						<option value="mb_open|asc" <?php if($GLOBALS["sst"]=='mb_open'&&$GLOBALS["sod"]=='asc'){?>selected<?php }?>>정보공개 정방향 (↓)</option>
						<option value="mb_open|desc" <?php if($GLOBALS["sst"]=='mb_open'&&$GLOBALS["sod"]=='desc'){?>selected<?php }?>>정보공개 역방향 (↑)</option>
						<option value="mb_mailling|asc" <?php if($GLOBALS["sst"]=='mb_mailling'&&$GLOBALS["sod"]=='asc'){?>selected<?php }?>>메일수신 정방향 (↓)</option>
						<option value="mb_mailling|desc" <?php if($GLOBALS["sst"]=='mb_mailling'&&$GLOBALS["sod"]=='desc'){?>selected<?php }?>>메일수신 역방향 (↑)</option>
						<option value="mb_sms|asc" <?php if($GLOBALS["sst"]=='mb_sms'&&$GLOBALS["sod"]=='asc'){?>selected<?php }?>>SMS수신 정방향 (↓)</option>
						<option value="mb_sms|desc" <?php if($GLOBALS["sst"]=='mb_sms'&&$GLOBALS["sod"]=='desc'){?>selected<?php }?>>SMS수신 역방향 (↑)</option>
						<option value="mb_level|asc" <?php if($GLOBALS["sst"]=='mb_level'&&$GLOBALS["sod"]=='asc'){?>selected<?php }?>>권한 정방향 (↓)</option>
						<option value="mb_level|desc" <?php if($GLOBALS["sst"]=='mb_level'&&$GLOBALS["sod"]=='desc'){?>selected<?php }?>>권한 역방향 (↑)</option>
						<option value="mb_adult|asc" <?php if($GLOBALS["sst"]=='mb_adult'&&$GLOBALS["sod"]=='asc'){?>selected<?php }?>>성인인증 정방향 (↓)</option>
						<option value="mb_adult|desc" <?php if($GLOBALS["sst"]=='mb_adult'&&$GLOBALS["sod"]=='desc'){?>selected<?php }?>>성인인증 역방향 (↑)</option>
						<option value="mb_intercept_date|asc" <?php if($GLOBALS["sst"]=='mb_intercept_date'&&$GLOBALS["sod"]=='asc'){?>selected<?php }?>>접근차단 정방향 (↓)</option>
						<option value="mb_intercept_date|desc" <?php if($GLOBALS["sst"]=='mb_intercept_date'&&$GLOBALS["sod"]=='desc'){?>selected<?php }?>>접근차단 역방향 (↑)</option>
						<option value="mb_today_login|asc" <?php if($GLOBALS["sst"]=='mb_today_login'&&$GLOBALS["sod"]=='asc'){?>selected<?php }?>>최종접속 정방향 (↓)</option>
						<option value="mb_today_login|desc" <?php if($GLOBALS["sst"]=='mb_today_login'&&$GLOBALS["sod"]=='desc'){?>selected<?php }?>>최종접속 역방향 (↑)</option>
						<option value="mb_point|asc" <?php if($GLOBALS["sst"]=='mb_point'&&$GLOBALS["sod"]=='asc'){?>selected<?php }?>>포인트 정방향 (↓)</option>
						<option value="mb_point|desc" <?php if($GLOBALS["sst"]=='mb_point'&&$GLOBALS["sod"]=='desc'){?>selected<?php }?>>포인트 역방향 (↑)</option>
						<option value="mb_datetime|asc" <?php if($GLOBALS["sst"]=='mb_datetime'&&$GLOBALS["sod"]=='asc'){?>selected<?php }?>>가입일 정방향 (↓)</option>
						<option value="mb_datetime|desc" <?php if($GLOBALS["sst"]=='mb_datetime'&&$GLOBALS["sod"]=='desc'){?>selected<?php }?>>가입일 역방향 (↑)</option>
					</select><i></i>
				</label>
			</section>
		</div>
	</div>
<?php if(G5_IS_MOBILE){?>
	<p class="font-size-11 color-grey text-right margin-bottom-5"><i class="fa fa-info-circle"></i> Note! 좌우스크롤 가능 (<i class="fa fa-arrows-h"></i>)</p>
<?php }?>

	<div id="member-list"></div>

<?php if(!$GLOBALS["wmode"]){?>
	<div class="margin-top-20">
	    <input type="submit" name="act_button" value="선택수정" class="btn-e btn-e-xs btn-e-red" onclick="document.pressed=this.value">
	    <input type="submit" name="act_button" value="선택삭제" class="btn-e btn-e-xs btn-e-dark" onclick="document.pressed=this.value">
	</div>
<?php }?>
	</form>
</div>

<?php echo eb_admin_paging('basic')?>


<script src="/admin/admin_theme/admin_basic/plugins/jsgrid/jsgrid.min.js"></script>
<script src="/admin/admin_theme/admin_basic/js/jsgrid.js"></script>
<script>
	//<a href="./member_form.php?'.$qstr.'&amp;w=u&amp;mb_id='.$row['mb_id'].'">수정</a>
!function () {
    var db = {
        deleteItem: function (deletingClient) {
            var clientIndex = $.inArray(deletingClient, this.clients);
            this.clients.splice(clientIndex, 1)
        },
        insertItem: function (insertingClient) {
            this.clients.push(insertingClient)
        },
        loadData  : function (filter) {
            return $.grep(this.clients, function (client) {
                return !(filter.체크 && !(client.체크.indexOf(filter.체크) > -1) || filter.아이디 && !(client.아이디.indexOf(filter.아이디) > -1) || filter.이름 && !(client.이름.indexOf(filter.이름) > -1) )
            })
        },
        updateItem: function (updatingClient) {}
    };
    window.db    = db,
    db.clients   = [
<?php if($TPL_list_1){$TPL_I1=-1;foreach($TPL_VAR["list"] as $TPL_V1){$TPL_I1++;?>
        {
<?php if(!$GLOBALS["wmode"]){?>
	        체크: "<input type='hidden' name='mb_id[<?php echo $TPL_I1?>]' value='<?php echo $TPL_V1["mb_id"]?>' id='mb_id_<?php echo $TPL_I1?>'><label for='chk_<?php echo $TPL_I1?>' class='checkbox'><input type='checkbox' name='chk[]' id='chk_<?php echo $TPL_I1?>' value='<?php echo $TPL_I1?>'><i></i></label>",
	        관리: "<?php if($GLOBALS["is_admin"]!='group'){?><a href='<?php echo EYOOM_ADMIN_URL?>/?dir=member&amp;pid=member_form&amp;mb_id=<?php echo $TPL_V1["mb_id"]?>&amp;w=u<?php if($GLOBALS["qstr"]){?>&amp;<?php echo $GLOBALS["qstr"]?><?php }?>'><u>수정</u></a><?php }?><?php if($TPL_VAR["config"]["cf_admin"]!=$TPL_V1["mb_id"]){?><a href='<?php echo EYOOM_ADMIN_URL?>/?dir=board&amp;pid=boardgroupmember_form&amp;mb_id=<?php echo $TPL_V1["mb_id"]?>' class='margin-left-10'><u>그룹</u></a><?php }?>",
	        포토: "<div class='new-member-photo'><?php if(!$TPL_V1["photo_url"]){?><i class='fa fa-user'></i><?php }else{?><img src='<?php echo $TPL_V1["photo_url"]?>' class='img-responsive'><?php }?></div>",
<?php }?>
	        아이디: "<span class='ellipsis'><?php echo $TPL_V1["mb_id"]?></span>",
	        이름: "<?php echo get_text($TPL_V1["mb_name"])?>",
	        닉네임: "<?php echo get_text($TPL_V1["mb_nick"])?>",
<?php if(!$GLOBALS["wmode"]){?>
	        권한: "<label class='select'><?php echo $TPL_V1["mb_level"]?><i></i></label>",
	        포인트: "<a href='<?php echo EYOOM_ADMIN_URL?>/?dir=member&amp;pid=point_list&amp;sfl=mb_id&amp;stx=<?php echo $TPL_V1["mb_id"]?>'><?php echo number_format($TPL_V1["mb_point"])?></a>",
	        메일인증: "<span class='<?php if($TPL_V1["email_certify"]=='Yes'){?>color-red<?php }elseif($TPL_V1["email_certify"]=='No'){?>color-dark<?php }?>'><?php echo $TPL_V1["email_certify"]?></span>",
	        정보공개: "<label class='checkbox'><input type='checkbox' name='mb_open[<?php echo $TPL_I1?>]' <?php if($TPL_V1["mb_open"]){?>checked<?php }?> value='1'><i></i></label>",
	        메일수신: "<label class='checkbox'><input type='checkbox' name='mb_mailling[<?php echo $TPL_I1?>]' <?php if($TPL_V1["mb_mailling"]){?>checked<?php }?> value='1'><i></i></label>",
	        SMS수신: "<label class='checkbox'><input type='checkbox' name='mb_sms[<?php echo $TPL_I1?>]' <?php if($TPL_V1["mb_sms"]){?>checked<?php }?> value='1'><i></i></label>",
	        성인인증: "<label class='checkbox'><input type='checkbox' name='mb_adult[<?php echo $TPL_I1?>]' <?php if($TPL_V1["mb_adult"]){?>checked<?php }?> value='1'><i></i></label><input type='hidden' name='mb_certify[<?php echo $TPL_I1?>]' value='<?php echo $TPL_V1["mb_certify"]?>'>",
	        접근차단: "<?php if(empty($TPL_V1["mb_leave_date"])){?><label class='checkbox'><input type='checkbox' name='mb_intercept_date[<?php echo $TPL_I1?>]' <?php if($TPL_V1["mb_intercept_date"]){?>checked<?php }?> value='<?php echo $TPL_V1["mb_intercept_date"]?>'><i></i></label><?php }?>",
	        상태: "<?php echo $TPL_V1["mb_status"]?>",
	        가입일: "<?php echo substr($TPL_V1["mb_datetime"], 0, - 9)?>",
	        최신로그인: "<?php echo substr($TPL_V1["mb_today_login"], 0, - 9)?>",
<?php }else{?>
	        선택하기: "<a href='javascript:;' data-mb-id='<?php echo $TPL_V1["mb_id"]?>' data-dismiss='modal' class='set_mbid btn-e btn-e-xs btn-e-indigo'>선택하기</a>",
<?php }?>
        },
<?php }}?>
    ]
}();

$(function() {
    $("#member-list").jsGrid({
        filtering      : false,
        editing        : false,
        sorting        : false,
        paging         : true,
        autoload       : true,
        controller     : db,
        deleteConfirm  : "정말로 삭제하시겠습니까?\n한번 삭제된 데이터는 복구할수 없습니다.",
        pageButtonCount: 5,
        pageSize       : 15,
        width          : "100%",
        height         : "auto",
        fields         : [
<?php if(!$GLOBALS["wmode"]){?>
            { name: "체크", type: "text", width: 40 },
            { name: "관리", type: "text", align: "center", width: 110, headercss: "set-btn-header", css: "set-btn-field" },
            { name: "포토", type: "text", align: "center", width: 60 },
<?php }?>
            { name: "아이디", type: "text", width: 100 },
            { name: "이름", type: "text", width: 100 },
            { name: "닉네임", type: "text", width: 100 },
<?php if(!$GLOBALS["wmode"]){?>
            { name: "권한", type: "text", width: 100 },
            { name: "포인트", type: "number", width: 80 },
            { name: "메일인증", type: "text", align: "center", width: 80 },
            { name: "정보공개", type: "text", align: "center", width: 80 },
            { name: "메일수신", type: "text", align: "center", width: 80 },
            { name: "SMS수신", type: "text", align: "center", width: 80 },
            { name: "성인인증", type: "text", align: "center", width: 80 },
            { name: "접근차단", type: "text", align: "center", width: 80 },
            { name: "상태", type: "text", align: "center", width: 70 },
            { name: "가입일", type: "text", align: "center", width: 110 },
            { name: "최신로그인", type: "text", align: "center", width: 110 },
<?php }else{?>
            { name: "선택하기", type: "text", align: "center", width: 80 },
<?php }?>
        ]
    });

	var $chk = $(".jsgrid-table th:first-child");
	if ($chk.text() == '체크') {
		var html = '<label for="chkall" class="checkbox"><input type="checkbox" name="chkall" id="chkall" value="1" onclick="check_all(this.form)"><i></i></label>';
		$chk.html(html);
	}

<?php if($GLOBALS["wmode"]){?>
    $(".set_mbid").click(function() {
        var mb_id = $(this).attr('data-mb-id');
        $('#mb_id', parent.document).val(mb_id);
        window.parent.closeModal();
    });
<?php }?>
});

function sorting_list(f, str) {
    var sort = str.split('|');

	$("#sst").val(sort[0]);
	$("#sod").val(sort[1]);

	if (sort[0] && sort[1]) {
		f.action = "<?php echo EYOOM_ADMIN_URL?>/?dir=<?php echo $GLOBALS["dir"]?>&pid=<?php echo $GLOBALS["pid"]?>";
		f.submit();
	}
}
</script>