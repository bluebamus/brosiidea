<?php /* Template_ 2.2.8 2018/04/30 10:48:51 /home1/bluebamus2/public_html/admin/admin_theme/admin_basic/skin_bs/theme/basic/member_list.skin.html 000014414 */  $this->include_("eb_admin_paging");
$TPL_member_list_1=empty($TPL_VAR["member_list"])||!is_array($TPL_VAR["member_list"])?0:count($TPL_VAR["member_list"]);?>
<?php if (!defined('_GNUBOARD_')) exit;
add_stylesheet('<link rel="stylesheet" href="/admin/admin_theme/admin_basic/plugins/jsgrid/jsgrid.min.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/admin/admin_theme/admin_basic/plugins/jsgrid/jsgrid-theme.min.css" type="text/css" media="screen">',0);
?>

<style>
.admin-member-list .level-member-photo {position:relative;overflow:hidden;width:26px;height:26px;border:1px solid #c5c5c5;background:#fff;padding:1px;margin:0 auto;text-align:center;-webkit-border-radius:50% !important;-moz-border-radius:50% !important;border-radius:50% !important}
.admin-member-list .level-member-photo i {width:22px;height:22px;font-size:12px;line-height:22px;background:#b5b5b5;color:#fff;-webkit-border-radius:50% !important;-moz-border-radius:50% !important;border-radius:50% !important}
.admin-member-list .level-member-photo img {-webkit-border-radius:50% !important;-moz-border-radius:50% !important;border-radius:50% !important}
</style>

<div class="admin-member-list">
	<form name="fmemberform" action="<?php echo $GLOBALS["action_url1"]?>" onsubmit="return fmemberform_submit(this);" class="eyoom-form" method="post">
	<input type="hidden" name="sst" id="sst" value="<?php echo $GLOBALS["sst"]?>">
	<input type="hidden" name="sod" id="sod" value="<?php echo $GLOBALS["sod"]?>">
	<input type="hidden" name="page" value="<?php echo $GLOBALS["page"]?>">
	<input type="hidden" name="token" value="">

	<div class="headline">
		<h4><strong>회원 레벨관리</strong></h4>
	</div>
	<div class="margin-bottom-30"></div>

	<div class="cont-text-bg margin-bottom-30">
		<p class="bg-info font-size-12">
			<i class="fa fa-info-circle"></i> 이윰레벨에서 사용설정한 그누레벨까지 레벨을 수정할 수 있습니다.<br>
			<i class="fa fa-info-circle"></i> 그누레벨을 수정하시면 이윰레벨 및 이윰<?php echo $TPL_VAR["levelset"]["eyoom_name"]?>의 값이 그누레벨에 맞게 자동으로 조정됩니다.<br>
			<i class="fa fa-info-circle"></i> 그누레벨을 1레벨로 설정하면 자동으로 회원탈퇴처리 합니다.<br>
			<i class="fa fa-info-circle"></i> 탈퇴회원의 레벨을 올리면 해당 계정은 자동으로 부활합니다.<br>
		</p>
	</div>

	<div class="row">
		<div class="col col-4">
			<div class="padding-top-5">
				<span class="font-size-12 color-grey">
					<a href="<?php echo EYOOM_ADMIN_URL?>/?dir=<?php echo $GLOBALS["dir"]?>&amp;pid=<?php echo $GLOBALS["pid"]?>" class="ov_listall">[전체목록]</a><span class="margin-left-10 margin-right-10 color-light-grey">|</span>전체 <?php echo number_format($GLOBALS["total_count"])?> 명
				</span>
			</div>
		</div>
		<div class="col col-5">
			<div class="row">
				<div class="col col-5">
					<label for="sfl" class="sound_only">검색대상</label>
					<label class="select margin-bottom-10">
						<select name="sfl" id="sfl">
						    <option value="em.mb_id" <?php echo get_selected($GLOBALS["sfl"],"em.mb_id")?>>회원아이디</option>
						    <option value="gm.mb_nick" <?php echo get_selected($GLOBALS["sfl"],"gm.mb_nick")?>>닉네임</option>
						    <option value="gm.mb_name" <?php echo get_selected($GLOBALS["sfl"],"gm.mb_name")?>>이름</option>
						    <option value="gm.mb_level" <?php echo get_selected($GLOBALS["sfl"],"gm.mb_level")?>>그누레벨</option>
						    <option value="em.level" <?php echo get_selected($GLOBALS["sfl"],"em.level")?>>이윰레벨</option>
						    <option value="gm.mb_point" <?php echo get_selected($GLOBALS["sfl"],"gm.mb_point")?>>그누포인트</option>
						    <option value="em.level_point" <?php echo get_selected($GLOBALS["sfl"],"em.level_point")?>>이윰경험치</option>
						</select>
						<i></i>
					</label>
				</div>
				<div class="col col-7">
					<label for="stx" class="sound_only">검색어<strong class="sound_only"> 필수</strong></label>
					<label class="input input-button">
						<div class="button"><input type="submit" value="검색">검색</div>
						<input type="text" name="stx" value="<?php echo $GLOBALS["stx"]?>" id="stx" required autocomplete="off">
					</label>
				</div>
			</div>
		</div>
		<div class="col col-3">
			<section>
				<label for="sort_list" class="select">
					<select name="sort_list" id="sort_list" onchange="sorting_list(this.form, this.value);">
						<option value="">:: 정렬방식선택 ::</option>
						<option value="gm.mb_id|asc" <?php if($GLOBALS["sst"]=='gm.mb_id'&&$GLOBALS["sod"]=='asc'){?>selected<?php }?>>회원아이디 정방향 (↓)</option>
						<option value="gm.mb_id|desc" <?php if($GLOBALS["sst"]=='gm.mb_id'&&$GLOBALS["sod"]=='desc'){?>selected<?php }?>>회원아이디 역방향 (↑)</option>
						<option value="gm.mb_name|asc" <?php if($GLOBALS["sst"]=='gm.mb_name'&&$GLOBALS["sod"]=='asc'){?>selected<?php }?>>이름 정방향 (↓)</option>
						<option value="gm.mb_name|desc" <?php if($GLOBALS["sst"]=='gm.mb_name'&&$GLOBALS["sod"]=='desc'){?>selected<?php }?>>이름 역방향 (↑)</option>
						<option value="gm.mb_nick|asc" <?php if($GLOBALS["sst"]=='gm.mb_nick'&&$GLOBALS["sod"]=='asc'){?>selected<?php }?>>닉네임 정방향 (↓)</option>
						<option value="gm.mb_nick|desc" <?php if($GLOBALS["sst"]=='gm.mb_nick'&&$GLOBALS["sod"]=='desc'){?>selected<?php }?>>닉네임 역방향 (↑)</option>
						<option value="gm.mb_level|asc" <?php if($GLOBALS["sst"]=='gm.mb_level'&&$GLOBALS["sod"]=='asc'){?>selected<?php }?>>그누레벨 정방향 (↓)</option>
						<option value="gm.mb_level|desc" <?php if($GLOBALS["sst"]=='gm.mb_level'&&$GLOBALS["sod"]=='desc'){?>selected<?php }?>>그누레벨 역방향 (↑)</option>
						<option value="gm.mb_point|asc" <?php if($GLOBALS["sst"]=='gm.mb_point'&&$GLOBALS["sod"]=='asc'){?>selected<?php }?>>그누<?php echo $TPL_VAR["levelset"]["gnu_name"]?> 정방향 (↓)</option>
						<option value="gm.mb_point|desc" <?php if($GLOBALS["sst"]=='gm.mb_point'&&$GLOBALS["sod"]=='desc'){?>selected<?php }?>>그누<?php echo $TPL_VAR["levelset"]["gnu_name"]?> 역방향 (↑)</option>
						<option value="em.level|asc" <?php if($GLOBALS["sst"]=='em.level'&&$GLOBALS["sod"]=='asc'){?>selected<?php }?>>이윰레벨 정방향 (↓)</option>
						<option value="em.level|desc" <?php if($GLOBALS["sst"]=='em.level'&&$GLOBALS["sod"]=='desc'){?>selected<?php }?>>이윰레벨 역방향 (↑)</option>
						<option value="em.level_point|asc" <?php if($GLOBALS["sst"]=='em.level_point'&&$GLOBALS["sod"]=='asc'){?>selected<?php }?>>이윰<?php echo $TPL_VAR["levelset"]["eyoom_name"]?> 정방향 (↓)</option>
						<option value="em.level_point|desc" <?php if($GLOBALS["sst"]=='em.level_point'&&$GLOBALS["sod"]=='desc'){?>selected<?php }?>>이윰<?php echo $TPL_VAR["levelset"]["eyoom_name"]?> 역방향 (↑)</option>
						<option value="mb_today_login|asc" <?php if($GLOBALS["sst"]=='mb_today_login'&&$GLOBALS["sod"]=='asc'){?>selected<?php }?>>최종접속 정방향 (↓)</option>
						<option value="mb_today_login|desc" <?php if($GLOBALS["sst"]=='mb_today_login'&&$GLOBALS["sod"]=='desc'){?>selected<?php }?>>최종접속 역방향 (↑)</option>
						<option value="mb_datetime|asc" <?php if($GLOBALS["sst"]=='mb_datetime'&&$GLOBALS["sod"]=='asc'){?>selected<?php }?>>가입일 정방향 (↓)</option>
						<option value="mb_datetime|desc" <?php if($GLOBALS["sst"]=='mb_datetime'&&$GLOBALS["sod"]=='desc'){?>selected<?php }?>>가입일 역방향 (↑)</option>
					</select><i></i>
				</label>
			</section>
		</div>
	</div>

	</form>

	<form name="fmemberlist" id="fmemberlist" method="post" action="<?php echo $GLOBALS["action_url2"]?>" onsubmit="return fmemberlist_submit(this);" class="eyoom-form">
	<input type="hidden" name="sst" value="<?php echo $GLOBALS["sst"]?>">
	<input type="hidden" name="sod" value="<?php echo $GLOBALS["sod"]?>">
	<input type="hidden" name="sfl" value="<?php echo $GLOBALS["sfl"]?>">
	<input type="hidden" name="stx" value="<?php echo $GLOBALS["stx"]?>">
	<input type="hidden" name="page" value="<?php echo $GLOBALS["page"]?>">
	<input type="hidden" name="token" value="">

<?php if(G5_IS_MOBILE){?>
	<p class="font-size-11 color-grey text-right margin-bottom-5"><i class="fa fa-info-circle"></i> Note! 좌우스크롤 가능 (<i class="fa fa-arrows-h"></i>)</p>
<?php }?>

	<div id="member-list"></div>

	<div class="margin-top-20">
	    <input type="submit" name="act_button" value="선택수정" class="btn-e btn-e-xs btn-e-red" onclick="document.pressed=this.value">
	</div>

	</form>

	<?php echo eb_admin_paging('basic')?>

</div>

<script src="/admin/admin_theme/admin_basic/plugins/jsgrid/jsgrid.min.js"></script>
<script src="/admin/admin_theme/admin_basic/js/jsgrid.js"></script>
<script>
/*--------------------------------------
	Modal
--------------------------------------*/
function eb_modal(href) {
    $('.member-list-modal').modal('show').on('hidden.bs.modal', function () {
        $("#member-list-iframe").attr("src", "");
        $('html').css({overflow: ''});
    });
    $('.member-list-modal').modal('show').on('shown.bs.modal', function () {
        $("#member-list-iframe").attr("src", href);
        $('#member-list-iframe').height(450);
        $('html').css({overflow: 'hidden'});
    });
    return false;
}

window.closeModal = function(){
    $('#member-list-modal').modal('hide');
};

!function () {
    var db = {
        deleteItem: function (deletingClient) {
            var clientIndex = $.inArray(deletingClient, this.clients);
            this.clients.splice(clientIndex, 1)
        },
        insertItem: function (insertingClient) {
            this.clients.push(insertingClient)
        },
        loadData  : function (filter) {
            return $.grep(this.clients, function (client) {
                return !(filter.체크 && !(client.체크.indexOf(filter.체크) > -1) || filter.회원아이디 && !(client.회원아이디.indexOf(filter.회원아이디) > -1))
            })
        },
        updateItem: function (updatingClient) {}
    };
    window.db    = db,
    db.clients   = [
<?php if($TPL_member_list_1){$TPL_I1=-1;foreach($TPL_VAR["member_list"] as $TPL_V1){$TPL_I1++;?>
        {
	        체크: "<label for='chk_<?php echo $TPL_I1?>' class='checkbox'><input type='checkbox' name='chk[]' id='chk_<?php echo $TPL_I1?>' value='<?php echo $TPL_I1?>'><i></i></label><input type='hidden' name='mb_id[<?php echo $TPL_I1?>]' value='<?php echo $TPL_V1["mb_id"]?>' id='mb_id_<?php echo $TPL_I1?>'>",
	        포토: "<div class='level-member-photo'><?php if(!$TPL_V1["photo_url"]){?><i class='fa fa-user'></i><?php }else{?><img src='<?php echo $TPL_V1["photo_url"]?>' class='img-responsive'><?php }?></div>",
	        회원아이디: "<span class='ellipsis'><?php echo $TPL_V1["mb_id"]?></span>",
	        이름: "<strong><?php echo $TPL_V1["mb_name"]?></strong>",
	        닉네임: "<span class='ellipsis'><?php echo $TPL_V1["mb_nick"]?></span>",
	        그누레벨: "<label class='select'><?php echo $TPL_V1["mb_select_level"]?><i></i></label><input type='hidden' name='mb_prev_level[<?php echo $TPL_I1?>]' value='<?php echo $TPL_V1["mb_level"]?>'>",
	        그누<?php echo $TPL_VAR["levelset"]["gnu_name"]?>: "<?php echo number_format($TPL_V1["mb_point"])?>",
	        이윰레벨: "<?php echo $TPL_V1["level"]?><input type='hidden' name='level[<?php echo $TPL_I1?>]' value='<?php echo $TPL_V1["level"]?>'>",
	        이윰<?php echo $TPL_VAR["levelset"]["eyoom_name"]?>: "<?php echo number_format($TPL_V1["level_point"])?><input type='hidden' name='level_point[<?php echo $TPL_I1?>]' value='<?php echo $TPL_V1["level_point"]?>'>",
	        최종접속: "<?php echo substr($TPL_V1["mb_today_login"], 2, 8)?>",
	        가입일: "<?php echo substr($TPL_V1["mb_datetime"], 2, 8)?>",
        },
<?php }}?>
    ]
}();

$(function() {
    $("#member-list").jsGrid({
        filtering      : false,
        editing        : false,
        sorting        : false,
        paging         : true,
        autoload       : true,
        controller     : db,
        deleteConfirm  : "정말로 삭제하시겠습니까?\n한번 삭제된 데이터는 복구할수 없습니다.",
        pageButtonCount: 5,
        pageSize       : 15,
        width          : "100%",
        height         : "auto",
        fields         : [
            { name: "체크", type: "text", width: 40 },
            { name: "포토", type: "text", align: "center", width: 60 },
            { name: "회원아이디", type: "text", width: 100 },
            { name: "이름", type: "text", width: 100 },
            { name: "닉네임", type: "text", width: 100 },
            { name: "그누레벨", type: "number", width: 80 },
            { name: "그누<?php echo $TPL_VAR["levelset"]["gnu_name"]?>", type: "number", width: 80 },
            { name: "이윰레벨", type: "number", width: 80 },
            { name: "이윰<?php echo $TPL_VAR["levelset"]["eyoom_name"]?>", type: "number", width: 80 },
            { name: "최종접속", type: "text", align: "center", width: 100 },
            { name: "가입일", type: "text", align: "center", width: 100 },
        ]
    });

    var $chk = $(".jsgrid-table th:first-child");
	if ($chk.text() == '체크') {
		var html = '<label for="chkall" class="checkbox"><input type="checkbox" name="chkall" id="chkall" value="1" onclick="check_all(this.form)"><i></i></label>';
		$chk.html(html);
	}
});

function sorting_list(f, str) {
    var sort = str.split('|');

	$("#sst").val(sort[0]);
	$("#sod").val(sort[1]);

	if (sort[0] && sort[1]) {
		f.action = "<?php echo EYOOM_ADMIN_URL?>/?dir=<?php echo $GLOBALS["dir"]?>&pid=<?php echo $GLOBALS["pid"]?>";
		f.submit();
	}
}

function fmemberform_submit(f) {
	if(f.stx.value == '') {
		alert('검색어를 입력해 주세요.');
		f.stx.focus();
		return false;
	}

	return true;
}

function fmemberlist_submit(f) {
    if ($("input:checked").length == 0) {
        alert(document.pressed+" 하실 항목을 하나 이상 선택하세요.");
        return false;
    }

    return true;
}
</script>