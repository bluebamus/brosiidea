<?php /* Template_ 2.2.8 2018/01/26 21:11:40 /home/bluebamus1/public_html/admin/admin_theme/admin_basic/skin_bs/theme/basic/ebslider_list.skin.html 000006936 */  $this->include_("eb_admin_paging");
$TPL_list_1=empty($TPL_VAR["list"])||!is_array($TPL_VAR["list"])?0:count($TPL_VAR["list"]);?>
<?php if (!defined('_GNUBOARD_')) exit;
add_stylesheet('<link rel="stylesheet" href="/admin/admin_theme/admin_basic/plugins/jsgrid/jsgrid.min.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/admin/admin_theme/admin_basic/plugins/jsgrid/jsgrid-theme.min.css" type="text/css" media="screen">',0);
?>

<div class="admin-ebslider-list">
	<div class="headline">
		<h4><strong>EB 슬라이더 - 마스터 관리</strong></h4>
	</div>
	<div class="margin-bottom-30"></div>

<?php $this->print_("theme_manager",$TPL_SCP,1);?>


	<form name="febsliderform" id="febsliderform" action="<?php echo $GLOBALS["action_url"]?>" method="post" onsubmit="return febsliderform_submit(this);" class="eyoom-form">
	<input type="hidden" name="theme" id="theme" value="<?php echo $TPL_VAR["this_theme"]?>">
	<input type="hidden" name="page" value="<?php echo $GLOBALS["page"]?>">
	<input type="hidden" name="loccd" value="<?php echo $GLOBALS["loccd"]?>">
	<input type="hidden" name="token" value="<?php echo $GLOBALS["token"]?>">

	<div class="headline">
		<h4><strong>[<span class="color-red"><?php echo $TPL_VAR["this_theme"]?></span>] EB 슬라이더 - 마스터 목록</strong></h4>
<?php if(!$GLOBALS["wmode"]){?>
		<a href="<?php echo EYOOM_ADMIN_URL?>/?dir=theme&amp;pid=ebslider_form&amp;thema=<?php echo $TPL_VAR["this_theme"]?>" class="btn-e btn-e-purple btn-e-xs pull-right margin-top-5"><i class="fa fa-plus"></i> EB 슬라이더 마스터 추가</a>
		<div class="clearfix"></div>
<?php }?>
	</div>
	<div class="margin-bottom-30"></div>

	<div class="row">
		<div class="col col-12">
			<div class="local_ov01 local_ov padding-top-5 clearfix">
			    <span class="font-size-12 color-grey">
			    	<a href="<?php echo EYOOM_ADMIN_URL?>/?dir=<?php echo $GLOBALS["dir"]?>&amp;pid=<?php echo $GLOBALS["pid"]?>" class="ov_listall">[전체목록]</a><span class="margin-left-10 margin-right-10 color-light-grey">|</span>등록된 슬라이더 마스터 <?php echo number_format($GLOBALS["total_count"])?>개
			    </span>
			</div>
		</div>
	</div>

<?php if(G5_IS_MOBILE){?>
	<p class="font-size-11 color-grey text-right margin-bottom-5"><i class="fa fa-info-circle"></i> Note! 좌우스크롤 가능 (<i class="fa fa-arrows-h"></i>)</p>
<?php }?>

	<div id="ebslider-list"></div>

	<div class="margin-top-20">
	    <input type="submit" name="act_button" value="선택수정" class="btn-e btn-e-xs btn-e-red" onclick="document.pressed=this.value">
<?php if($GLOBALS["is_admin"]=='super'){?>
	    <input type="submit" name="act_button" value="선택삭제" class="btn-e btn-e-xs btn-e-dark" onclick="document.pressed=this.value">
<?php }?>
	</div>
	</form>

</div>

<?php echo eb_admin_paging('basic')?>


<script src="/admin/admin_theme/admin_basic/plugins/jsgrid/jsgrid.min.js"></script>
<script src="/admin/admin_theme/admin_basic/js/jsgrid.js"></script>
<script>
!function () {
    var db = {
        deleteItem: function (deletingClient) {
            var clientIndex = $.inArray(deletingClient, this.clients);
            this.clients.splice(clientIndex, 1)
        },
        insertItem: function (insertingClient) {
            this.clients.push(insertingClient)
        },
        loadData  : function (filter) {
            return $.grep(this.clients, function (client) {
                return !(filter.체크 && !(client.체크.indexOf(filter.체크) > -1)  )
            })
        },
        updateItem: function (updatingClient) {}
    };
    window.db    = db,
    db.clients   = [
<?php if($TPL_list_1){$TPL_I1=-1;foreach($TPL_VAR["list"] as $TPL_V1){$TPL_I1++;?>
        {
	        체크: "<label for='chk_<?php echo $TPL_I1?>' class='checkbox'><input type='checkbox' name='chk[]' id='chk_<?php echo $TPL_I1?>' value='<?php echo $TPL_I1?>'><i></i></label><input type='hidden' name='es_no[<?php echo $TPL_I1?>]' value='<?php echo $TPL_V1["es_no"]?>'><input type='hidden' name='es_code[<?php echo $TPL_I1?>]' value='<?php echo $TPL_V1["es_code"]?>'>",
	        관리: "<a href='<?php echo EYOOM_ADMIN_URL?>/?dir=theme&amp;pid=ebslider_form&amp;thema=<?php echo $TPL_VAR["this_theme"]?>&amp;es_code=<?php echo $TPL_V1["es_code"]?>&amp;w=u'><u>수정</u></a>",
	        슬라이더제목: "<?php echo get_text($TPL_V1["es_subject"])?>",
	        치환코드: "<?php echo $TPL_V1["es_chg_code"]?>",
	        상태: "<label for='es_state_<?php echo $TPL_I1?>' class='select'><select name='es_state[<?php echo $TPL_I1?>]' id='es_state_<?php echo $TPL_I1?>'><option value=''>선택</option><option value='1' <?php if($TPL_V1["es_state"]=='1'){?>selected<?php }?>>활성</option><option value='2' <?php if($TPL_V1["es_state"]=='2'){?>selected<?php }?>>비활성</option></select><i></i></label>",
	        등록일: "<?php echo substr($TPL_V1["es_regdt"], 0, 10)?>",
        },
<?php }}?>
    ]
}();

$(function() {
    $("#ebslider-list").jsGrid({
        filtering      : false,
        editing        : false,
        sorting        : false,
        paging         : true,
        autoload       : true,
        controller     : db,
        deleteConfirm  : "정말로 삭제하시겠습니까?\n한번 삭제된 데이터는 복구할수 없습니다.",
        pageButtonCount: 5,
        pageSize       : 15,
        width          : "100%",
        height         : "auto",
        fields         : [
            { name: "체크", type: "text", width: 40 },
            { name: "관리", type: "text", align: "center", width: 80, headercss: "set-btn-header", css: "set-btn-field" },
            { name: "슬라이더제목", type: "text", width: 200 },
            { name: "치환코드", type: "text", align: "center", width: 150 },
            { name: "상태", type: "text", align: "center", width: 100 },
            { name: "등록일", type: "text", align: "center", width: 100 },
        ]
    });

    var $chk = $("#ebslider-list .jsgrid-table th:first-child");
	if ($chk.text() == '체크') {
		var html = '<label for="chkall" class="checkbox"><input type="checkbox" name="chkall" id="chkall" value="1" onclick="check_all(this.form)"><i></i></label>';
		$chk.html(html);
	}
});

function febsliderform_submit(f) {
    if (!is_checked("chk[]")) {
        alert(document.pressed+" 하실 항목을 하나 이상 선택하세요.");
        return false;
    }

    if(document.pressed == "선택삭제") {
        if(!confirm("선택한 자료를 정말 삭제하시겠습니까?")) {
            return false;
        }
    }

    return true;
}

function del_confirm() {
	if (confirm('배너/광고를 삭제하시겠습니까?')) {
		return true;
	} else {
		return false;
	}
}
</script>