<?php /* Template_ 2.2.8 2018/01/25 01:09:15 /home/bluebamus1/public_html/admin/admin_theme/admin_basic/skin_bs/theme/basic/banner_list.skin.html 000010338 */  $this->include_("eb_admin_paging");
$TPL_bn_loccd_1=empty($TPL_VAR["bn_loccd"])||!is_array($TPL_VAR["bn_loccd"])?0:count($TPL_VAR["bn_loccd"]);
$TPL_banner_list_1=empty($TPL_VAR["banner_list"])||!is_array($TPL_VAR["banner_list"])?0:count($TPL_VAR["banner_list"]);?>
<?php if (!defined('_GNUBOARD_')) exit;
add_stylesheet('<link rel="stylesheet" href="/admin/admin_theme/admin_basic/plugins/jsgrid/jsgrid.min.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/admin/admin_theme/admin_basic/plugins/jsgrid/jsgrid-theme.min.css" type="text/css" media="screen">',0);
?>

<div class="admin-banner-list">
	<div class="headline">
		<h4><strong>배너 / 광고 관리</strong></h4>
	</div>
	<div class="margin-bottom-30"></div>

<?php $this->print_("theme_manager",$TPL_SCP,1);?>


	<form name="fbannerform" id="fbannerlist" action="<?php echo $GLOBALS["action_url2"]?>" method="post" onsubmit="return fbannerform_submit(this);" class="eyoom-form">
	<input type="hidden" name="theme" id="theme" value="<?php echo $TPL_VAR["this_theme"]?>">
	<input type="hidden" name="page" value="<?php echo $GLOBALS["page"]?>">
	<input type="hidden" name="loccd" value="<?php echo $GLOBALS["loccd"]?>">
	<input type="hidden" name="token" value="<?php echo $GLOBALS["token"]?>">

	<div class="headline">
		<h4><strong>[<span class="color-red"><?php echo $TPL_VAR["this_theme"]?></span>] 배너 / 광고 리스트</strong></h4>
<?php if(!$GLOBALS["wmode"]){?>
		<a href="<?php echo EYOOM_ADMIN_URL?>/?dir=theme&pid=banner_form" class="btn-e btn-e-purple btn-e-xs pull-right margin-top-5"><i class="fa fa-plus"></i> 배너/광고 추가</a>
		<div class="clearfix"></div>
<?php }?>
	</div>
	<div class="margin-bottom-30"></div>

	<div class="row">
		<div class="col col-6">
			<div class="local_ov01 local_ov padding-top-5 clearfix">
			    <span class="font-size-12 color-grey">
			    	<a href="<?php echo EYOOM_ADMIN_URL?>/?dir=<?php echo $GLOBALS["dir"]?>&amp;pid=<?php echo $GLOBALS["pid"]?>" class="ov_listall">[전체목록]</a><span class="margin-left-10 margin-right-10 color-light-grey">|</span>등록된 배너수 <?php echo number_format($GLOBALS["total_count"])?>개
			    </span>
			</div>
		</div>
		<div class="col col-3">
			<section class="text-right">
				<a href="<?php echo EYOOM_ADMIN_URL?>/?dir=theme&amp;pid=banner_location&amp;thema=<?php echo $TPL_VAR["this_theme"]?>&wmode=1" class="btn-e btn-e-dark" onclick="eb_modal(this.href); return false;">배너/광고 위치 관리</a>
			</section>
		</div>
		<div class="col col-3">
			<section>
				<label for="loccd" class="select">
					<select name="loccd" id="loccd" onchange="this.form.submit();">
						<option value="">위치별 배너/광고 보기</option>
<?php if($TPL_bn_loccd_1){foreach($TPL_VAR["bn_loccd"] as $TPL_K1=>$TPL_V1){?>
						<option value="<?php echo $TPL_K1?>" <?php if($GLOBALS["loccd"]==$TPL_K1){?>selected<?php }?>><?php echo $TPL_V1?></option>
<?php }}?>
					</select><i></i>
				</label>
			</section>
		</div>
	</div>

<?php if(G5_IS_MOBILE){?>
	<p class="font-size-11 color-grey text-right margin-bottom-5"><i class="fa fa-info-circle"></i> Note! 좌우스크롤 가능 (<i class="fa fa-arrows-h"></i>)</p>
<?php }?>

	<div id="banner-list"></div>

	<div class="margin-top-20">
	    <input type="submit" name="act_button" value="선택수정" class="btn-e btn-e-xs btn-e-red" onclick="document.pressed=this.value">
<?php if($GLOBALS["is_admin"]=='super'){?>
	    <input type="submit" name="act_button" value="선택삭제" class="btn-e btn-e-xs btn-e-dark" onclick="document.pressed=this.value">
<?php }?>
	</div>
	</form>

</div>

<div class="modal fade banner-modal" tabindex="-1" role="dialog" aria-labelledby="boardCopyLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 id="boardCopyLabel" class="modal-title"><strong><i class="fa fa-ellipsis-v color-grey"></i> 배너/광고 위치 관리</strong></h4>
            </div>
            <div class="modal-body">
                <iframe id="banner-iframe" width="100%" frameborder="0"></iframe>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn-e btn-e-lg btn-e-dark" type="button"><i class="fa fa-close"></i> 닫기</button>
            </div>
        </div>
    </div>
</div>

<?php echo eb_admin_paging('basic')?>


<script src="/admin/admin_theme/admin_basic/plugins/jsgrid/jsgrid.min.js"></script>
<script src="/admin/admin_theme/admin_basic/js/jsgrid.js"></script>
<script>
/*--------------------------------------
	Modal
--------------------------------------*/
function eb_modal(href) {
    $('.banner-modal').modal('show').on('hidden.bs.modal', function () {
        $("#banner-iframe").attr("src", "");
        $('html').css({overflow: ''});
    });
    $('.banner-modal').modal('show').on('shown.bs.modal', function () {
        $("#banner-iframe").attr("src", href);
        $('#banner-iframe').height(685);
        $('html').css({overflow: 'hidden'});
    });
    return false;
}

window.closeModal = function(){
    $('.banner-modal').modal('hide');
};

/*--------------------------------------
	Table
--------------------------------------*/
!function () {
    var db = {
        deleteItem: function (deletingClient) {
            var clientIndex = $.inArray(deletingClient, this.clients);
            this.clients.splice(clientIndex, 1)
        },
        insertItem: function (insertingClient) {
            this.clients.push(insertingClient)
        },
        loadData  : function (filter) {
            return $.grep(this.clients, function (client) {
                return !(filter.체크 && !(client.체크.indexOf(filter.체크) > -1)  )
            })
        },
        updateItem: function (updatingClient) {}
    };
    window.db    = db,
    db.clients   = [
<?php if($TPL_banner_list_1){$TPL_I1=-1;foreach($TPL_VAR["banner_list"] as $TPL_V1){$TPL_I1++;?>
        {
	        체크: "<label for='chk_<?php echo $TPL_I1?>' class='checkbox'><input type='checkbox' name='chk[]' id='chk_<?php echo $TPL_I1?>' value='<?php echo $TPL_I1?>'><i></i></label><input type='hidden' name='bn_no[<?php echo $TPL_I1?>]' value='<?php echo $TPL_V1["bn_no"]?>'>",
	        관리: "<a href='<?php echo EYOOM_ADMIN_URL?>/?dir=theme&amp;pid=banner_form&amp;thema=<?php echo $TPL_VAR["this_theme"]?>&amp;bn_no=<?php echo $TPL_V1["bn_no"]?>&amp;w=u'><u>수정</u></a> <a href='<?php echo EYOOM_ADMIN_URL?>/?dir=theme&amp;pid=banner_delete&amp;theme=<?php echo $TPL_VAR["this_theme"]?>&amp;page=<?php echo $GLOBALS["page"]?>&amp;loccd=<?php echo $GLOBALS["loccd"]?>&amp;bn_no=<?php echo $TPL_V1["bn_no"]?>&amp;smode=1' onclick='return del_confirm();' class='margin-left-10'><u>삭제</u></a>",
	        배너위치: "<?php echo $TPL_V1["bn_location"]?>.<?php echo $TPL_V1["bn_loccd"]?>",
	        치환코드: "<?php echo $TPL_V1["bn_chg_code"]?>",
	        이미지: "<?php echo $TPL_V1["bn_image"]?>",
	        상태: "<label for='bn_state_<?php echo $TPL_I1?>' class='select'><select name='bn_state[<?php echo $TPL_I1?>]' id='bn_state_<?php echo $TPL_I1?>'><option value=''>선택</option><option value='1' <?php if($TPL_V1["bn_state"]=='1'){?>selected<?php }?>>보이기</option><option value='2' <?php if($TPL_V1["bn_state"]=='2'){?>selected<?php }?>>숨기기</option></select><i></i></label>",
	        노출수: "<?php echo number_format($TPL_V1["bn_exposed"])?>",
	        클릭수: "<?php echo number_format($TPL_V1["bn_clicked"])?>",
	        클릭률: "<?php echo $TPL_V1["bn_ratio"]?>%",
	        시작일: "<?php echo $TPL_V1["bn_start"]?>",
	        종료일: "<?php echo $TPL_V1["bn_end"]?>",
	        등록일: "<?php echo substr($TPL_V1["bn_regdt"], 0, 10)?>",
        },
<?php }}?>
    ]
}();

$(function() {
    $("#banner-list").jsGrid({
        filtering      : false,
        editing        : false,
        sorting        : false,
        paging         : true,
        autoload       : true,
        controller     : db,
        deleteConfirm  : "정말로 삭제하시겠습니까?\n한번 삭제된 데이터는 복구할수 없습니다.",
        pageButtonCount: 5,
        pageSize       : 15,
        width          : "100%",
        height         : "auto",
        fields         : [
            { name: "체크", type: "text", width: 40 },
            { name: "관리", type: "text", align: "center", width: 80, headercss: "set-btn-header", css: "set-btn-field" },
            { name: "배너위치", type: "text", width: 120 },
            { name: "치환코드", type: "text", align: "center", width: 260 },
            { name: "이미지", type: "text", align: "center", width: 120 },
            { name: "상태", type: "text", align: "center", width: 120 },
            { name: "노출수", type: "number", width: 60 },
            { name: "클릭수", type: "number", width: 60 },
            { name: "클릭률", type: "number", width: 60 },
            { name: "시작일", type: "text", align: "center", width: 80 },
            { name: "종료일", type: "text", align: "center", width: 80 },
            { name: "등록일", type: "text", align: "center", width: 80 },
        ]
    });

    var $chk = $("#banner-list .jsgrid-table th:first-child");
	if ($chk.text() == '체크') {
		var html = '<label for="chkall" class="checkbox"><input type="checkbox" name="chkall" id="chkall" value="1" onclick="check_all(this.form)"><i></i></label>';
		$chk.html(html);
	}
});

function fbannerform_submit(f) {
    if (!is_checked("chk[]")) {
        alert(document.pressed+" 하실 항목을 하나 이상 선택하세요.");
        return false;
    }

    if(document.pressed == "선택삭제") {
        if(!confirm("선택한 자료를 정말 삭제하시겠습니까?")) {
            return false;
        }
    }

    return true;
}

function del_confirm() {
	if (confirm('배너/광고를 삭제하시겠습니까?')) {
		return true;
	} else {
		return false;
	}
}
</script>