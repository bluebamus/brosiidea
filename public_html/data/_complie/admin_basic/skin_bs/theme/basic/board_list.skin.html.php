<?php /* Template_ 2.2.8 2018/02/02 18:27:10 /home/bluebamus1/public_html/admin/admin_theme/admin_basic/skin_bs/theme/basic/board_list.skin.html 000008517 */  $this->include_("eb_admin_paging");
$TPL_board_list_1=empty($TPL_VAR["board_list"])||!is_array($TPL_VAR["board_list"])?0:count($TPL_VAR["board_list"]);?>
<?php if (!defined('_GNUBOARD_')) exit;
add_stylesheet('<link rel="stylesheet" href="/admin/admin_theme/admin_basic/plugins/jsgrid/jsgrid.min.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/admin/admin_theme/admin_basic/plugins/jsgrid/jsgrid-theme.min.css" type="text/css" media="screen">',0);
?>

<div class="admin-board-list">
	<div class="headline">
		<h4><strong>게시판 설정</strong></h4>
	</div>
	<div class="margin-bottom-30"></div>

<?php $this->print_("theme_manager",$TPL_SCP,1);?>

	
	<form id="fsearch" name="fsearch" class="eyoom-form" action="./" method="get">
	<div class="headline">
		<h4><strong>[<span class="color-red"><?php echo $GLOBALS["this_theme"]?></span>] 게시판 리스트</strong></h4>
	</div>
	<div class="margin-bottom-30"></div>
	
	<div class="admin-search-box trans-col">
		<div class="row">
			<div class="col col-2">
				<label for="sfl" class="sound_only">검색대상</label>
				<label class="select margin-bottom-0">
					<select name="sfl" id="sfl">
					    <option value="bo_table"<?php echo get_selected($_GET["sfl"],"bo_table")?>>TABLE</option>
					    <option value="bo_subject"<?php echo get_selected($_GET["sfl"],"bo_subject")?>>제목</option>
					    <option value="a.gr_id"<?php echo get_selected($_GET["sfl"],"a.gr_id")?>>그룹ID</option>
					</select>
					<i></i>
				</label>
			</div>
			<div class="col col-4">
				<label for="stx" class="sound_only">검색어<strong class="sound_only"> 필수</strong></label>
				<input type="hidden" name="dir" value="<?php echo $GLOBALS["dir"]?>" id="dir">
				<input type="hidden" name="pid" value="<?php echo $GLOBALS["pid"]?>" id="pid">
				<label class="input input-button margin-bottom-0">
					<input type="text" name="stx" value="<?php echo $GLOBALS["stx"]?>" id="stx" required class="frm_input">
					<div class="button"><input type="submit" value="검색">검색</div>
				</label>
			</div>
		</div>
	</div>
	</form>
	<div class="margin-bottom-30"></div>
	
	<form name="fboardform" method="post" action="<?php echo $GLOBALS["action_url"]?>" onsubmit="return fboardform_submit(this);" class="eyoom-form">
	<input type="hidden" name="theme" value="<?php echo $GLOBALS["this_theme"]?>">
	<input type="hidden" name="sfl" value="<?php echo $GLOBALS["sfl"]?>">
	<input type="hidden" name="stx" value="<?php echo $GLOBALS["stx"]?>">
	<input type="hidden" name="page" value="<?php echo $GLOBALS["page"]?>">
	<input type="hidden" name="token" value="<?php echo $GLOBALS["token"]?>">

<?php if(G5_IS_MOBILE){?>
	<p class="font-size-11 color-grey text-right margin-bottom-5"><i class="fa fa-info-circle"></i> Note! 좌우스크롤 가능 (<i class="fa fa-arrows-h"></i>)</p>
<?php }?>

	<div id="board-list"></div>

	<div class="margin-top-20">
	    <input type="submit" name="act_button" value="선택수정" class="btn-e btn-e-xs btn-e-red" onclick="document.pressed=this.value">
	</div>

	</form>
</div>

<?php echo eb_admin_paging('basic')?>


<script src="/admin/admin_theme/admin_basic/plugins/jsgrid/jsgrid.min.js"></script>
<script src="/admin/admin_theme/admin_basic/js/jsgrid.js"></script>
<script>

/*--------------------------------------
	Table
--------------------------------------*/
!function () {
    var db = {
        deleteItem: function (deletingClient) {
            var clientIndex = $.inArray(deletingClient, this.clients);
            this.clients.splice(clientIndex, 1)
        },
        insertItem: function (insertingClient) {
            this.clients.push(insertingClient)
        },
        loadData  : function (filter) {
            return $.grep(this.clients, function (client) {
                return !(filter.No && !(client.No.indexOf(filter.No) > -1) || filter.회원구분 && !(client.회원구분.indexOf(filter.회원구분) > -1) || filter.아이디 && !(client.아이디.indexOf(filter.아이디) > -1) || filter.이름 && !(client.이름.indexOf(filter.이름) > -1) || filter.휴대전화 && !(client.휴대전화.indexOf(filter.휴대전화) > -1) || filter.전화번호 && !(client.전화번호.indexOf(filter.전화번호) > -1) || filter.이메일 && !(client.이메일.indexOf(filter.이메일) > -1) || filter.가입일 && !(client.가입일.indexOf(filter.가입일) > -1) || filter.최신로그인 && !(client.최신로그인.indexOf(filter.최신로그인) > -1) || filter.상태 && !(client.상태.indexOf(filter.상태) > -1) )
            })
        },
        updateItem: function (updatingClient) {}
    };
    window.db    = db,
    db.clients   = [
<?php if($TPL_board_list_1){$TPL_I1=-1;foreach($TPL_VAR["board_list"] as $TPL_V1){$TPL_I1++;?>
        {
	        체크: "<label for='chk_<?php echo $TPL_I1?>' class='checkbox'><input type='checkbox' name='chk[]' id='chk_<?php echo $TPL_I1?>' value='<?php echo $TPL_I1?>'><i></i></label>",
	        관리: "<a href='<?php echo EYOOM_ADMIN_URL?>/?dir=theme&amp;pid=board_form&amp;w=u&amp;bo_table=<?php echo $TPL_V1["bo_table"]?>&amp;page=<?php echo $GLOBALS["page"]?>&amp;thema=<?php echo $GLOBALS["this_theme"]?>'><u>수정</u></a>",
	        그룹: "<?php echo $TPL_V1["gr_subject"]?>",
	        제목: "<?php echo get_text($TPL_V1["bo_subject"])?>",
	        TABLE: "<input type='hidden' name='board_table[<?php echo $TPL_I1?>]' value='<?php echo $TPL_V1["bo_table"]?>'><a href='<?php echo G5_BBS_URL?>/board.php?bo_table=<?php echo $TPL_V1["bo_table"]?>'><a href='<?php echo G5_BBS_URL?>/board.php?bo_table=<?php echo $TPL_V1["bo_table"]?>&amp;theme=<?php echo $GLOBALS["this_theme"]?>' class='bo_href btn-e btn-e-dark' style='width:100%' target='_blank'><?php echo $TPL_V1["bo_table"]?></a>",
	        글작성회수제한: "<label class='input'><input type='text' name='bo_write_limit[<?php echo $TPL_I1?>]' id='bo_write_limit_<?php echo $TPL_I1?>' value='<?php echo $TPL_V1["bo_write_limit"]?>'></label>",
	        이윰스킨: "<label class='select'><?php echo $TPL_V1["bo_skin_select"]?><i></i></label>",
	        스킨선택: "<div class='inline-group'><label for='use_gnu_skin_<?php echo $TPL_I1?>_1' class='radio'><input type='radio' name='use_gnu_skin[<?php echo $TPL_I1?>]' id='use_gnu_skin_<?php echo $TPL_I1?>_1' value='n' <?php if($TPL_V1["use_gnu_skin"]=='n'){?>checked<?php }?>><i></i> 이윰빌더 스킨</label><label for='use_gnu_skin_<?php echo $TPL_I1?>_2' class='radio'><input type='radio' name='use_gnu_skin[<?php echo $TPL_I1?>]' id='use_gnu_skin_<?php echo $TPL_I1?>_2' value='y' <?php if($TPL_V1["use_gnu_skin"]=='y'){?>checked<?php }?>><i></i> 그누보드 스킨</label></div>",
        },
<?php }}?>
    ]
}();

$(function() {
    $("#board-list").jsGrid({
        filtering      : false,
        editing        : false,
        sorting        : false,
        paging         : true,
        autoload       : true,
        controller     : db,
        deleteConfirm  : "정말로 삭제하시겠습니까?\n한번 삭제된 데이터는 복구할수 없습니다.",
        pageButtonCount: 5,
        pageSize       : 15,
        width          : "100%",
        height         : "auto",
        fields         : [
            { name: "체크", type: "text", width: 40 },
            { name: "관리", type: "text", align: "center", width: 80, headercss: "set-btn-header", css: "set-btn-field" },
            { name: "그룹", type: "text", width: 120 },
            { name: "제목", type: "text", width: 150 },
            { name: "TABLE", type: "text", width: 120 },
            { name: "글작성회수제한", type: "number", width: 120 },
            { name: "이윰스킨", type: "text", align: "center", width: 140 },
            { name: "스킨선택", type: "text", align: "center", width: 230 },
        ]
    });

    var $chk = $("#board-list .jsgrid-table th:first-child");
	if ($chk.text() == '체크') {
		var html = '<label for="chkall" class="checkbox"><input type="checkbox" name="chkall" id="chkall" value="1" onclick="check_all(this.form)"><i></i></label>';
		$chk.html(html);
	}
});


function fboardform_submit(f) {
    if (!is_checked("chk[]")) {
        alert(document.pressed+" 하실 항목을 하나 이상 선택하세요.");
        return false;
    }

    return true;
}
</script>