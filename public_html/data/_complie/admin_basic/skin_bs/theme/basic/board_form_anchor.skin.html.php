<?php /* Template_ 2.2.8 2018/02/26 10:54:46 /home/bluebamus1/public_html/admin/admin_theme/admin_basic/skin_bs/theme/basic/board_form_anchor.skin.html 000001736 */ ?>
<?php if (!defined('_GNUBOARD_')) exit; ?>

<div class="pg-anchor-in tab-e2">
    <ul class="nav nav-tabs">
	    <li <?php if($TPL_VAR["anc_id"]=='anc_bo_common'){?>class="active"<?php }?>><a href="#anc_bo_common">기본 설정</a></li>
	    <li <?php if($TPL_VAR["anc_id"]=='anc_bo_blind'){?>class="active"<?php }?>><a href="#anc_bo_blind">신고/블라인드</a></li>
	    <li <?php if($TPL_VAR["anc_id"]=='anc_bo_rating'){?>class="active"<?php }?>><a href="#anc_bo_rating">별점 기능</a></li>
	    <li <?php if($TPL_VAR["anc_id"]=='anc_bo_tag'){?>class="active"<?php }?>><a href="#anc_bo_tag">태그 기능</a></li>
	    <li <?php if($TPL_VAR["anc_id"]=='anc_bo_automove'){?>class="active"<?php }?>><a href="#anc_bo_automove">자동 이동/복사</a></li>
	    <li <?php if($TPL_VAR["anc_id"]=='anc_bo_addon'){?>class="active"<?php }?>><a href="#anc_bo_addon">애드온 기능</a></li>
	    <li <?php if($TPL_VAR["anc_id"]=='anc_bo_cmtbest'){?>class="active"<?php }?>><a href="#anc_bo_cmtbest">댓글 베스트 기능</a></li>
	    <li <?php if($TPL_VAR["anc_id"]=='anc_bo_exif'){?>class="active"<?php }?>><a href="#anc_bo_exif">이미지 EXIF 정보</a></li>
	    <li <?php if($TPL_VAR["anc_id"]=='anc_bo_cmtpoint'){?>class="active"<?php }?>><a href="#anc_bo_cmtpoint">댓글포인트</a></li>
<?php if(preg_match('/adopt/i',$TPL_VAR["eyoom_board"]["bo_skin"])){?>
	    <li <?php if($TPL_VAR["anc_id"]=='anc_bo_adopt'){?>class="active"<?php }?>><a href="#anc_bo_adopt">채택게시판 설정</a></li>
<?php }?>
	</ul>
	<div class="tab-bottom-line"></div>
</div>