<?php /* Template_ 2.2.8 2018/04/30 10:48:51 /home1/bluebamus2/public_html/admin/admin_theme/admin_basic/skin_bs/theme/basic/level_config.skin.html 000026601 */ 
$TPL_level_table_1=empty($TPL_VAR["level_table"])||!is_array($TPL_VAR["level_table"])?0:count($TPL_VAR["level_table"]);?>
<?php if (!defined('_GNUBOARD_')) exit; ?>

<style>
.admin-level-config .min-width-80px {min-width:80px !important}
</style>

<div class="admin-level-config">
	<form name="flevelconfigform" method="post" action="<?php echo $GLOBALS["action_url"]?>" class="eyoom-form">
	<input type="hidden" name="token" value="">

	<div class="headline">
		<h4><strong>이윰레벨 환경설정</strong></h4>
	</div>
	<div class="margin-bottom-30"></div>

	<div class="level-config">
		<div class="adm-form-wrap margin-bottom-30">
			<header>
				<strong><i class="fa fa-caret-right"></i> 기본 설정</strong>
			</header>

			<fieldset>
				<div class="row">
					<div class="col col-6">
						<section>
							<label for="levelset_gnu_name" class="label">이윰레벨 사용</label>
					        <div class="inline-group">
						        <label for="levelset_use_eyoom_level1" class="radio"><input type="radio" name="levelset[use_eyoom_level]" id="levelset_use_eyoom_level1" value="y" <?php if($TPL_VAR["levelset"]["use_eyoom_level"]=='y'||!$TPL_VAR["levelset"]["use_eyoom_level"]){?>checked<?php }?>><i></i> 사용</label>
						        <label for="levelset_use_eyoom_level2" class="radio"><input type="radio" name="levelset[use_eyoom_level]" id="levelset_use_eyoom_level2" value="n" <?php if($TPL_VAR["levelset"]["use_eyoom_level"]=='n'){?>checked<?php }?>><i></i>사용 안함</label>
					        </label>
						</section>
					</div>
				</div>
			</fieldset>

			<fieldset>
				<div class="row">
					<div class="col col-6">
						<div class="row">
							<div class="col col-6">
								<section>
									<label for="levelset_gnu_name" class="label">그누 포인트 명칭</label>
									<label class="input">
										<input type="text" name="levelset[gnu_name]" id="levelset_gnu_name" value="<?php if($TPL_VAR["levelset"]["gnu_name"]){?><?php echo $TPL_VAR["levelset"]["gnu_name"]?><?php }else{?>포인트<?php }?>">
									</label>
								</section>
							</div>
						</div>
						<div class="note margin-bottom-10"><strong>Note:</strong> 그누 포인트 명칭을 설정합니다. 포인트값 설정은 <a href="<?php echo EYOOM_ADMIN_URL?>/?dir=config&amp;pid=config_form"><u>[환경설정 - 기본환경설정]</u></a>에서 설정해 주세요.</div>
					</div>
					<div class="col col-6">
						<div class="row">
							<div class="col col-6">
								<section>
									<label for="levelset_eyoom_name" class="label">이윰레벨 포인트 명칭</label>
									<label class="input">
										<input type="text" name="levelset[eyoom_name]" id="levelset_eyoom_name" value="<?php if($TPL_VAR["levelset"]["eyoom_name"]){?><?php echo $TPL_VAR["levelset"]["eyoom_name"]?><?php }else{?>경험치<?php }?>">
									</label>
								</section>
							</div>
						</div>
						<div class="note margin-bottom-10"><strong>Note:</strong> 이윰 레벨을 결정하는 포인트의 명칭을 설정합니다.</div>
					</div>
				</div>
			</fieldset>
		</div>
	</div>

	<div class="headline">
		<h4><strong>이윰레벨 포인트 설정</strong></h4>
	</div>
	<div class="margin-bottom-30"></div>

	<div class="level-config">
		<div class="adm-form-wrap margin-bottom-30">
			<header>
				<strong><i class="fa fa-caret-right"></i> 이윰레벨 포인트설정 [커뮤니티 활동]</strong>
			</header>

			<fieldset>
				<div class="cont-text-bg">
					<p class="bg-warning font-size-12 margin-bottom-0">
						<i class="fa fa-info-circle"></i> 이윰레벨 포인트는 사이트의 활동에 따른 경험치로서 이윰레벨를 결정하는 기준 점수입니다.
					</p>
				</div>
			</fieldset>

			<fieldset>
				<div class="row">
					<div class="col col-3">
						<section>
							<label for="levelset_login" class="label">로그인 <?php echo $TPL_VAR["levelset"]["eyoom_name"]?></label>
							<label class="input">
								<i class="icon-append">점</i>
								<input type="text" name="levelset[login]" id="levelset_login" value="<?php if($TPL_VAR["levelset"]["login"]){?><?php echo $TPL_VAR["levelset"]["login"]?><?php }else{?>20<?php }?>">
							</label>
						</section>
					</div>
					<div class="col col-3">
						<section>
							<label for="levelset_write" class="label">글쓰기 <?php echo $TPL_VAR["levelset"]["eyoom_name"]?></label>
							<label class="input">
								<i class="icon-append">점</i>
								<input type="text" name="levelset[write]" id="levelset_write" value="<?php if($TPL_VAR["levelset"]["write"]){?><?php echo $TPL_VAR["levelset"]["write"]?><?php }else{?>10<?php }?>">
							</label>
						</section>
					</div>
					<div class="col col-3">
						<section>
							<label for="levelset_reply" class="label">답글쓰기 <?php echo $TPL_VAR["levelset"]["eyoom_name"]?></label>
							<label class="input">
								<i class="icon-append">점</i>
								<input type="text" name="levelset[reply]" id="levelset_reply" value="<?php if($TPL_VAR["levelset"]["reply"]){?><?php echo $TPL_VAR["levelset"]["reply"]?><?php }else{?>10<?php }?>">
							</label>
						</section>
					</div>
					<div class="col col-3">
						<section>
							<label for="levelset_read" class="label">글읽기 <?php echo $TPL_VAR["levelset"]["eyoom_name"]?></label>
							<label class="input">
								<i class="icon-append">점</i>
								<input type="text" name="levelset[read]" id="levelset_read" value="<?php if($TPL_VAR["levelset"]["read"]){?><?php echo $TPL_VAR["levelset"]["read"]?><?php }else{?>1<?php }?>">
							</label>
						</section>
					</div>
				</div>
			</fieldset>

			<fieldset>
				<div class="row">
					<div class="col col-3">
						<section>
							<label for="levelset_cmt" class="label">댓글쓰기 <?php echo $TPL_VAR["levelset"]["eyoom_name"]?></label>
							<label class="input">
								<i class="icon-append">점</i>
								<input type="text" name="levelset[cmt]" id="levelset_cmt" value="<?php if($TPL_VAR["levelset"]["cmt"]){?><?php echo $TPL_VAR["levelset"]["cmt"]?><?php }else{?>5<?php }?>">
							</label>
						</section>
					</div>
					<div class="col col-3">
						<section>
							<label for="levelset_memo" class="label">쪽지쓰기 <?php echo $TPL_VAR["levelset"]["eyoom_name"]?></label>
							<label class="input">
								<i class="icon-append">점</i>
								<input type="text" name="levelset[memo]" id="levelset_memo" value="<?php if($TPL_VAR["levelset"]["memo"]){?><?php echo $TPL_VAR["levelset"]["memo"]?><?php }else{?>5<?php }?>">
							</label>
						</section>
					</div>
					<div class="col col-3">
						<section>
							<label for="levelset_rememo" class="label">쪽지받기 <?php echo $TPL_VAR["levelset"]["eyoom_name"]?></label>
							<label class="input">
								<i class="icon-append">점</i>
								<input type="text" name="levelset[rememo]" id="levelset_rememo" value="<?php if($TPL_VAR["levelset"]["rememo"]){?><?php echo $TPL_VAR["levelset"]["rememo"]?><?php }else{?>1<?php }?>">
							</label>
						</section>
					</div>
					<div class="col col-3">
						<section>
							<label for="levelset_scrap" class="label">스크랩하기 <?php echo $TPL_VAR["levelset"]["eyoom_name"]?></label>
							<label class="input">
								<i class="icon-append">점</i>
								<input type="text" name="levelset[scrap]" id="levelset_scrap" value="<?php if($TPL_VAR["levelset"]["scrap"]){?><?php echo $TPL_VAR["levelset"]["scrap"]?><?php }else{?>5<?php }?>">
							</label>
						</section>
					</div>
				</div>
			</fieldset>

			<fieldset>
				<div class="row">
					<div class="col col-3">
						<section>
							<label for="levelset_good" class="label">추천하기 <?php echo $TPL_VAR["levelset"]["eyoom_name"]?></label>
							<label class="input">
								<i class="icon-append">점</i>
								<input type="text" name="levelset[good]" id="levelset_good" value="<?php if($TPL_VAR["levelset"]["good"]){?><?php echo $TPL_VAR["levelset"]["good"]?><?php }else{?>5<?php }?>">
							</label>
						</section>
					</div>
					<div class="col col-3">
						<section>
							<label for="levelset_regood" class="label">추천받기 <?php echo $TPL_VAR["levelset"]["eyoom_name"]?></label>
							<label class="input">
								<i class="icon-append">점</i>
								<input type="text" name="levelset[regood]" id="levelset_regood" value="<?php if($TPL_VAR["levelset"]["regood"]){?><?php echo $TPL_VAR["levelset"]["regood"]?><?php }else{?>1<?php }?>">
							</label>
						</section>
					</div>
					<div class="col col-3">
						<section>
							<label for="levelset_nogood" class="label">비추천하기 <?php echo $TPL_VAR["levelset"]["eyoom_name"]?></label>
							<label class="input">
								<i class="icon-append">점</i>
								<input type="text" name="levelset[nogood]" id="levelset_nogood" value="<?php if($TPL_VAR["levelset"]["nogood"]){?><?php echo $TPL_VAR["levelset"]["nogood"]?><?php }else{?>3<?php }?>">
							</label>
						</section>
					</div>
					<div class="col col-3">
						<section>
							<label for="levelset_renogood" class="label">비추천받기 <?php echo $TPL_VAR["levelset"]["eyoom_name"]?></label>
							<label class="input">
								<i class="icon-append">점</i>
								<input type="text" name="levelset[renogood]" id="levelset_renogood" value="<?php if($TPL_VAR["levelset"]["renogood"]){?><?php echo $TPL_VAR["levelset"]["renogood"]?><?php }else{?>1<?php }?>">
							</label>
						</section>
					</div>
				</div>
			</fieldset>

			<fieldset>
				<div class="row">
					<div class="col col-3">
						<section>
							<label for="levelset_following" class="label">팔로우하기 <?php echo $TPL_VAR["levelset"]["eyoom_name"]?></label>
							<label class="input">
								<i class="icon-append">점</i>
								<input type="text" name="levelset[following]" id="levelset_following" value="<?php if($TPL_VAR["levelset"]["following"]){?><?php echo $TPL_VAR["levelset"]["following"]?><?php }else{?>5<?php }?>">
							</label>
						</section>
					</div>
					<div class="col col-3">
						<section>
							<label for="levelset_follower" class="label">팔로우받기 <?php echo $TPL_VAR["levelset"]["eyoom_name"]?></label>
							<label class="input">
								<i class="icon-append">점</i>
								<input type="text" name="levelset[follower]" id="levelset_follower" value="<?php if($TPL_VAR["levelset"]["follower"]){?><?php echo $TPL_VAR["levelset"]["follower"]?><?php }else{?>5<?php }?>">
							</label>
						</section>
					</div>
					<div class="col col-3">
						<section>
							<label for="levelset_banner" class="label">배너/광고클릭 <?php echo $TPL_VAR["levelset"]["eyoom_name"]?></label>
							<label class="input">
								<i class="icon-append">점</i>
								<input type="text" name="levelset[banner]" id="levelset_banner" value="<?php if($TPL_VAR["levelset"]["banner"]){?><?php echo $TPL_VAR["levelset"]["banner"]?><?php }else{?>20<?php }?>">
							</label>
						</section>
					</div>
					<div class="col col-3">
						<section>
							<label for="levelset_qa" class="label">1:1문의하기 <?php echo $TPL_VAR["levelset"]["eyoom_name"]?></label>
							<label class="input">
								<i class="icon-append">점</i>
								<input type="text" name="levelset[qa]" id="levelset_qa" value="<?php if($TPL_VAR["levelset"]["qa"]){?><?php echo $TPL_VAR["levelset"]["qa"]?><?php }else{?>5<?php }?>">
							</label>
						</section>
					</div>
				</div>
			</fieldset>

			<fieldset>
				<div class="row">
					<div class="col col-3">
						<section>
							<label for="levelset_star" class="label">별점주기 <?php echo $TPL_VAR["levelset"]["eyoom_name"]?></label>
							<label class="input">
								<i class="icon-append">점</i>
								<input type="text" name="levelset[star]" id="levelset_star" value="<?php if($TPL_VAR["levelset"]["star"]){?><?php echo $TPL_VAR["levelset"]["star"]?><?php }else{?>2<?php }?>">
							</label>
						</section>
					</div>
					<div class="col col-3">
						<section>
							<label for="levelset_restar" class="label">별점받기 <?php echo $TPL_VAR["levelset"]["eyoom_name"]?></label>
							<label class="input">
								<i class="icon-append">점</i>
								<input type="text" name="levelset[restar]" id="levelset_restar" value="<?php if($TPL_VAR["levelset"]["restar"]){?><?php echo $TPL_VAR["levelset"]["restar"]?><?php }else{?>1<?php }?>">
							</label>
						</section>
					</div>
					<div class="col col-3">
						<section>
							<label for="levelset_tag" class="label">태그작성 <?php echo $TPL_VAR["levelset"]["eyoom_name"]?></label>
							<label class="input">
								<i class="icon-append">점</i>
								<input type="text" name="levelset[tag]" id="levelset_tag" value="<?php if($TPL_VAR["levelset"]["tag"]){?><?php echo $TPL_VAR["levelset"]["tag"]?><?php }else{?>5<?php }?>">
							</label>
						</section>
					</div>
					<div class="col col-3">
						<section>
							<label for="levelset_poll" class="label">설문참여 <?php echo $TPL_VAR["levelset"]["eyoom_name"]?></label>
							<label class="input">
								<i class="icon-append">점</i>
								<input type="text" name="levelset[poll]" id="levelset_poll" value="<?php if($TPL_VAR["levelset"]["poll"]){?><?php echo $TPL_VAR["levelset"]["poll"]?><?php }else{?>10<?php }?>">
							</label>
						</section>
					</div>
				</div>
			</fieldset>
		</div>
	</div>

	<div class="level-config">
		<div class="adm-form-wrap margin-bottom-30">
			<header>
				<strong><i class="fa fa-caret-right"></i> 이윰레벨 포인트설정 [쇼핑몰 활동]</strong>
			</header>

			<fieldset>
				<div class="cont-text-bg">
					<p class="bg-warning font-size-12 margin-bottom-0">
						<i class="fa fa-info-circle"></i> 쇼핑몰 활동에 따른 이윰레벨 포인트를 설정합니다.
					</p>
				</div>
			</fieldset>

			<fieldset>
				<div class="row">
					<div class="col col-3">
						<section>
							<label for="levelset_goodsview" class="label">상품보기 <?php echo $TPL_VAR["levelset"]["eyoom_name"]?></label>
							<label class="input">
								<i class="icon-append">점</i>
								<input type="text" name="levelset[goodsview]" id="levelset_goodsview" value="<?php if($TPL_VAR["levelset"]["goodsview"]){?><?php echo $TPL_VAR["levelset"]["goodsview"]?><?php }else{?>20<?php }?>">
							</label>
						</section>
					</div>
					<div class="col col-3">
						<section>
							<label for="levelset_cart" class="label">장바구니담기 <?php echo $TPL_VAR["levelset"]["eyoom_name"]?></label>
							<label class="input">
								<i class="icon-append">점</i>
								<input type="text" name="levelset[cart]" id="levelset_cart" value="<?php if($TPL_VAR["levelset"]["cart"]){?><?php echo $TPL_VAR["levelset"]["cart"]?><?php }else{?>20<?php }?>">
							</label>
						</section>
					</div>
					<div class="col col-3">
						<section>
							<label for="levelset_wishlist" class="label">위시리스트 <?php echo $TPL_VAR["levelset"]["eyoom_name"]?></label>
							<label class="input">
								<i class="icon-append">점</i>
								<input type="text" name="levelset[wishlist]" id="levelset_wishlist" value="<?php if($TPL_VAR["levelset"]["wishlist"]){?><?php echo $TPL_VAR["levelset"]["wishlist"]?><?php }else{?>20<?php }?>">
							</label>
						</section>
					</div>
					<div class="col col-3">
						<section>
							<label for="levelset_recommend" class="label">상품추천 <?php echo $TPL_VAR["levelset"]["eyoom_name"]?></label>
							<label class="input">
								<i class="icon-append">점</i>
								<input type="text" name="levelset[recommend]" id="levelset_recommend" value="<?php if($TPL_VAR["levelset"]["recommend"]){?><?php echo $TPL_VAR["levelset"]["recommend"]?><?php }else{?>20<?php }?>">
							</label>
						</section>
					</div>
				</div>
			</fieldset>

			<fieldset>
				<div class="row">
					<div class="col col-3">
						<section>
							<label for="levelset_review" class="label">상품후기 <?php echo $TPL_VAR["levelset"]["eyoom_name"]?></label>
							<label class="input">
								<i class="icon-append">점</i>
								<input type="text" name="levelset[review]" id="levelset_review" value="<?php if($TPL_VAR["levelset"]["review"]){?><?php echo $TPL_VAR["levelset"]["review"]?><?php }else{?>20<?php }?>">
							</label>
						</section>
					</div>
					<div class="col col-3">
						<section>
							<label for="levelset_goodsqa" class="label">상품문의 <?php echo $TPL_VAR["levelset"]["eyoom_name"]?></label>
							<label class="input">
								<i class="icon-append">점</i>
								<input type="text" name="levelset[goodsqa]" id="levelset_goodsqa" value="<?php if($TPL_VAR["levelset"]["goodsqa"]){?><?php echo $TPL_VAR["levelset"]["goodsqa"]?><?php }else{?>20<?php }?>">
							</label>
						</section>
					</div>
					<div class="col col-3">
						<section>
							<label for="levelset_order" class="label">주문완료 <?php echo $TPL_VAR["levelset"]["eyoom_name"]?></label>
							<label class="input">
								<i class="icon-append">점</i>
								<input type="text" name="levelset[order]" id="levelset_order" value="<?php if($TPL_VAR["levelset"]["order"]){?><?php echo $TPL_VAR["levelset"]["order"]?><?php }else{?>20<?php }?>">
							</label>
						</section>
					</div>
					<div class="col col-3">
						<section>
							<label for="levelset_ordcancel" class="label">주문취소 <?php echo $TPL_VAR["levelset"]["eyoom_name"]?></label>
							<label class="input">
								<i class="icon-append">점</i>
								<input type="text" name="levelset[ordcancel]" id="levelset_ordcancel" value="<?php if($TPL_VAR["levelset"]["ordcancel"]){?><?php echo $TPL_VAR["levelset"]["ordcancel"]?><?php }else{?>20<?php }?>">
							</label>
						</section>
					</div>
				</div>
			</fieldset>
		</div>
	</div>

	<?php echo $TPL_VAR["frm_submit"]?>


	<div class="headline">
		<h4><strong>이윰레벨표 구성</strong></h4>
	</div>
	<div class="margin-bottom-30"></div>

	<div class="level-config">
		<div class="adm-form-wrap margin-bottom-30">
			<header>
				<strong><i class="fa fa-caret-right"></i> 이윰레벨 규칙</strong>
			</header>

			<fieldset>
				<div class="cont-text-bg">
					<p class="bg-warning font-size-12 margin-bottom-0">
						<i class="fa fa-info-circle"></i> 전체적인 이윰레벨 설정을 변경하기 위해서는 이윰레벨 규칙을 변경하고, 계산하기를 실행한 후, [확인]버튼을 클릭해야만 정상적으로 적용이 됩니다.
					</p>
				</div>
			</fieldset>

			<fieldset>
				<div class="row">
					<div class="col col-3">
						<section>
							<label for="max_use_gnu_level" class="label">이윰레벨로 활용 할 그누레벨</label>
							<label class="select">
								<?php echo get_member_level_select('max_use_gnu_level', 1, 9,$TPL_VAR["max_gnu_level"])?><i></i>
							</label>
						</section>
					</div>
					<div class="col col-3">
						<section class="label-height">
							<p class="note">설정된 그누레벨까지 이윰레벨로 활용</p>
						</section>
					</div>
					<div class="col col-3">
						<section>
							<label for="calc_level_point" class="label">기준포인트</label>
							<label class="input">
								<i class="icon-append">점</i>
								<input type="text" name="calc_level_point" id="calc_level_point" value="<?php if($TPL_VAR["levelset"]["calc_level_point"]){?><?php echo $TPL_VAR["setting_point"]?><?php }else{?>100<?php }?>">
							</label>
						</section>
					</div>
					<div class="col col-3">
						<section>
							<label for="calc_level_ratio" class="label">포인트 증가비율</label>
							<label class="input">
								<i class="icon-append">%</i>
								<input type="text" name="calc_level_ratio" id="calc_level_ratio" value="<?php if($TPL_VAR["levelset"]["calc_level_ratio"]){?><?php echo $TPL_VAR["levelset"]["calc_level_ratio"]?><?php }else{?>200<?php }?>">
							</label>
						</section>
					</div>
				</div>
				<div class="margin-bottom-15"></div>

<?php if(G5_IS_MOBILE){?>
				<p class="font-size-11 color-grey text-right margin-bottom-5"><i class="fa fa-info-circle"></i> Note! 좌우스크롤 가능 (<i class="fa fa-arrows-h"></i>)</p>
<?php }?>

				<div class="table-list-eb">
					<div class="table-responsive">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>그누레벨</th>
									<th>2레벨</th>
									<th>3레벨</th>
									<th>4레벨</th>
									<th>5레벨</th>
									<th>6레벨</th>
									<th>7레벨</th>
									<th>8레벨</th>
									<th>9레벨</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td style="white-space:nowrap">구간별 이윰레벨 갯수</th>
									<td><label class="input"><i class="icon-append">개</i><input type="text" name="cnt_gnu_level_2" id="cnt_gnu_level_2" value="<?php echo $TPL_VAR["cnt_gnu_level"][ 2]?>" class="text-right min-width-80px"></label></td>
									<td><label class="input"><i class="icon-append">개</i><input type="text" name="cnt_gnu_level_3" id="cnt_gnu_level_3" value="<?php echo $TPL_VAR["cnt_gnu_level"][ 3]?>" class="text-right min-width-80px"></label></td>
									<td><label class="input"><i class="icon-append">개</i><input type="text" name="cnt_gnu_level_4" id="cnt_gnu_level_4" value="<?php echo $TPL_VAR["cnt_gnu_level"][ 4]?>" class="text-right min-width-80px"></label></td>
									<td><label class="input"><i class="icon-append">개</i><input type="text" name="cnt_gnu_level_5" id="cnt_gnu_level_5" value="<?php echo $TPL_VAR["cnt_gnu_level"][ 5]?>" class="text-right min-width-80px"></label></td>
									<td><label class="input"><i class="icon-append">개</i><input type="text" name="cnt_gnu_level_6" id="cnt_gnu_level_6" value="<?php echo $TPL_VAR["cnt_gnu_level"][ 6]?>" class="text-right min-width-80px"></label></td>
									<td><label class="input"><i class="icon-append">개</i><input type="text" name="cnt_gnu_level_7" id="cnt_gnu_level_7" value="<?php echo $TPL_VAR["cnt_gnu_level"][ 7]?>" class="text-right min-width-80px"></label></td>
									<td><label class="input"><i class="icon-append">개</i><input type="text" name="cnt_gnu_level_8" id="cnt_gnu_level_8" value="<?php echo $TPL_VAR["cnt_gnu_level"][ 8]?>" class="text-right min-width-80px"></label></td>
									<td><label class="input"><i class="icon-append">개</i><input type="text" name="cnt_gnu_level_9" id="cnt_gnu_level_9" value="<?php echo $TPL_VAR["cnt_gnu_level"][ 9]?>" class="text-right min-width-80px"></label></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</fieldset>

			<fieldset>
				<div class="text-center margin-top-10 margin-bottom-10">
					<a href="javascript:;" onclick="calc_eyoom_level();" class="btn-e btn-e-lg btn-e-green">계산하기</a>
				</div>
			</fieldset>
		</div>
	</div>

	<?php echo $TPL_VAR["frm_submit"]?>


	<div class="headline" id="calc">
		<h4><strong>그누레벨 vs 이윰레벨 <?php echo $TPL_VAR["levelset"]["eyoom_name"]?>설정표</strong></h4>
	</div>
	<div class="margin-bottom-30"></div>

	<div class="level-config">
		<div class="adm-form-wrap margin-bottom-30">
			<header>
				<strong><i class="fa fa-caret-right"></i> 이윰레벨 규칙</strong>
			</header>

			<fieldset>
				<div class="margin-bottom-15"></div>
<?php if(G5_IS_MOBILE){?>
				<p class="font-size-11 color-grey text-right margin-bottom-5"><i class="fa fa-info-circle"></i> Note! 좌우스크롤 가능 (<i class="fa fa-arrows-h"></i>)</p>
<?php }?>

				<div class="table-list-eb">
					<div class="table-responsive">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>그누레벨</th>
									<th>그누레벨 별칭</th>
									<th>이윰레벨</th>
									<th>이윰레벨 별칭</th>
									<th>최소 포인트</th>
									<th>최대 포인트</th>
									<th>레벨업 포인트</th>
								</tr>
							</thead>
							<tbody>
<?php if($TPL_level_table_1){foreach($TPL_VAR["level_table"] as $TPL_V1){?>
								<tr>
<?php if($TPL_V1["rowspan"]){?>
									<td rowspan="<?php echo $TPL_V1["rowspan"]?>"><?php echo $TPL_V1["gnu_level"]?>레벨</td>
									<td rowspan="<?php echo $TPL_V1["rowspan"]?>">
										<label for="levelset_gnu_alias_<?php echo $TPL_V1["gnu_level"]?>" class="input">
											<input type="text" name="levelset[gnu_alias_<?php echo $TPL_V1["gnu_level"]?>]" id="levelset_gnu_alias_<?php echo $TPL_V1["gnu_level"]?>" value="<?php echo $TPL_V1["gnu_alias"]?>">
										</label>
									</td>
<?php }?>
									<td>Level <?php echo $TPL_V1["eyoom_level"]?></td>
									<td>
										<label for="levelinfo_<?php echo $TPL_V1["eyoom_level"]?>_name" class="input">
											<input type="text" name="levelinfo[<?php echo $TPL_V1["eyoom_level"]?>][name]" id="levelinfo_<?php echo $TPL_V1["eyoom_level"]?>_name" value="<?php echo $TPL_V1["eyoom_level_name"]?>">
										</label>
									</td>
									<td class="text-right"><?php echo number_format($TPL_V1["min"])?><input type="hidden" name="levelinfo[<?php echo $TPL_V1["eyoom_level"]?>][min]" value="<?php echo $TPL_V1["min"]?>"></td>
									<td class="text-right"><?php echo number_format($TPL_V1["max"])?><input type="hidden" name="levelinfo[<?php echo $TPL_V1["eyoom_level"]?>][max]" value="<?php echo $TPL_V1["max"]?>"></td>
									<td class="text-right"><?php echo number_format($TPL_V1["gap"])?></td>
								</tr>
<?php }}?>
							</tbody>
						</table>
					</div>
				</div>
			</fieldset>
		</div>
	</div>

	<?php echo $TPL_VAR["frm_submit"]?>


	</form>
</div>

<script>
function calc_eyoom_level() {
	var level = parseInt($("#max_use_gnu_level > option:selected").val());
	for(var i=2;i<=level;i++) {
		if(check_cnt_gnu_level(i)==false) {break; return false;}
	}
	if($("#calc_level_point").val() == '') {
		alert("기준포인트를 입력해 주세요.");
		$("#calc_level_point").focus();
		return false;
	}
	if($("#calc_level_ratio").val() == '') {
		alert("포인트 증가비율을 입력해 주세요.");
		$("#calc_level_ratio").focus();
		return false;
	}
	var form = document.flevelconfigform;
	form.action = '<?php echo EYOOM_ADMIN_URL?>/?dir=theme&pid=level_config#calc';
	form.submit();
}

function check_cnt_gnu_level(num) {
	if($("#cnt_gnu_level_"+num).val() == '') {
		alert("그누 "+num+"레벨 구간에 이윰레벨의 갯수를 설정해 주세요.");
		$("#cnt_gnu_level_"+num).focus();
		return false;
	}
}
</script>