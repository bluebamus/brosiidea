<?php /* Template_ 2.2.8 2018/04/30 10:48:50 /home1/bluebamus2/public_html/admin/admin_theme/admin_basic/skin_bs/theme/basic/config_form_anchor.skin.html 000001105 */ ?>
<?php if (!defined('_GNUBOARD_')) exit; ?>

<div class="pg-anchor-in tab-e2">
    <ul class="nav nav-tabs">
	    <li <?php if($TPL_VAR["anc_id"]=='anc_cf_domain'){?>class="active"<?php }?>><a href="#anc_cf_domain">도메인설정</a></li>
	    <li <?php if($TPL_VAR["anc_id"]=='anc_cf_alias'){?>class="active"<?php }?>><a href="#anc_cf_alias">테마별칭설정</a></li>
	    <li <?php if($TPL_VAR["anc_id"]=='anc_cf_skin'){?>class="active"<?php }?>><a href="#anc_cf_skin">스킨설정</a></li>
	    <li <?php if($TPL_VAR["anc_id"]=='anc_cf_func'){?>class="active"<?php }?>><a href="#anc_cf_func">기능설정</a></li>
	    <li <?php if($TPL_VAR["anc_id"]=='anc_cf_layout'){?>class="active"<?php }?>><a href="#anc_cf_layout">사이드 레이아웃</a></li>
	    <li <?php if($TPL_VAR["anc_id"]=='anc_cf_tag'){?>class="active"<?php }?>><a href="#anc_cf_tag">태그기능설정</a></li>
	</ul>
	<div class="tab-bottom-line"></div>
</div>