<?php /* Template_ 2.2.8 2018/02/02 18:27:10 /home/bluebamus1/public_html/admin/admin_theme/admin_basic/skin_bs/theme/basic/tag_list.skin.html 000015639 */  $this->include_("eb_admin_paging");
$TPL_exist_theme_1=empty($TPL_VAR["exist_theme"])||!is_array($TPL_VAR["exist_theme"])?0:count($TPL_VAR["exist_theme"]);
$TPL_tag_list_1=empty($TPL_VAR["tag_list"])||!is_array($TPL_VAR["tag_list"])?0:count($TPL_VAR["tag_list"]);?>
<?php if (!defined('_GNUBOARD_')) exit;
add_stylesheet('<link rel="stylesheet" href="/admin/admin_theme/admin_basic/plugins/jsgrid/jsgrid.min.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/admin/admin_theme/admin_basic/plugins/jsgrid/jsgrid-theme.min.css" type="text/css" media="screen">',0);
?>

<style>
.admin-tag-list .chg_dpmenu .fa-check {display:none}
.admin-tag-list .chg_dpmenu .fa-check.check-show {display:inline-block}
.admin-tag-list .chg_dpmenu .default-text {text-decoration:underline}
.admin-tag-list .chg_dpmenu .check-show-text {font-weight:bold;text-decoration:none}
.admin-tag-list .chg_dpmenu .margin-right-3 {margin-right:3px}
</style>

<div class="admin-tag-list">
	<div class="headline">
		<h4><strong>태그 관리</strong></h4>
	</div>
	<div class="margin-bottom-30"></div>

<?php $this->print_("theme_manager",$TPL_SCP,1);?>


	<form name="ftagform" id="ftagform" action="<?php echo $GLOBALS["action_url1"]?>" onsubmit="return ftagform_submit(this);" method="get" class="eyoom-form">
	<input type="hidden" name="sst" id="sst" value="<?php echo $GLOBALS["sst"]?>">
	<input type="hidden" name="sod" id="sod" value="<?php echo $GLOBALS["sod"]?>">
	<input type="hidden" name="sfl" id="sfl" value="tg_word">
	<input type="hidden" name="page" value="<?php echo $GLOBALS["page"]?>">
	<input type="hidden" name="token" value="<?php echo $GLOBALS["token"]?>">

	<div class="headline">
		<h4><strong>[<span class="color-red"><?php echo $TPL_VAR["this_theme"]?></span>] 태그 리스트</strong></h4>
	</div>
	<div class="margin-bottom-30"></div>

	<div class="row">
		<div class="col col-6">
			<div class="local_ov01 local_ov padding-top-5 clearfix">
			    <span class="font-size-12 color-grey">
			    	<a href="<?php echo EYOOM_ADMIN_URL?>/?dir=<?php echo $GLOBALS["dir"]?>&amp;pid=<?php echo $GLOBALS["pid"]?>" class="ov_listall">[전체목록]</a><span class="margin-left-10 margin-right-10 color-light-grey">|</span>등록된 태그수 <?php echo number_format($GLOBALS["total_count"])?>개
			    </span>
			</div>
		</div>
		<div class="col col-3">
			<section class="text-right">
				<label for="stx" class="sound_only">검색어<strong class="sound_only"> 필수</strong></label>
				<label class="input input-button">
					<input type="text" name="stx" value="<?php echo $GLOBALS["stx"]?>" id="stx" required placeholder="태그명">
					<div class="button"><input type="submit" value="검색">검색</div>
				</label>
			</section>
		</div>
		<div class="col col-3">
			<section>
				<label for="sort_list" class="select">
					<select name="sort_list" id="sort_list" onchange="sorting_list(this.form, this.value);">
						<option value="">:: 정렬방식선택 ::</option>
						<option value="tg_word|asc" <?php if($GLOBALS["sst"]=='tg_word'&&$GLOBALS["sod"]=='asc'){?>selected<?php }?>>태그명 정방향 (↓)</option>
						<option value="tg_word|desc" <?php if($GLOBALS["sst"]=='tg_word'&&$GLOBALS["sod"]=='desc'){?>selected<?php }?>>태그명 역방향 (↑)</option>
						<option value="tg_regcnt|asc" <?php if($GLOBALS["sst"]=='tg_regcnt'&&$GLOBALS["sod"]=='asc'){?>selected<?php }?>>등록수 정방향 (↓)</option>
						<option value="tg_regcnt|desc" <?php if($GLOBALS["sst"]=='tg_regcnt'&&$GLOBALS["sod"]=='desc'){?>selected<?php }?>>등록수 역방향 (↑)</option>
						<option value="tg_scnt|asc" <?php if($GLOBALS["sst"]=='tg_scnt'&&$GLOBALS["sod"]=='asc'){?>selected<?php }?>>검색수 정방향 (↓)</option>
						<option value="tg_scnt|desc" <?php if($GLOBALS["sst"]=='tg_scnt'&&$GLOBALS["sod"]=='desc'){?>selected<?php }?>>검색수 역방향 (↑)</option>
						<option value="tg_score|asc" <?php if($GLOBALS["sst"]=='tg_score'&&$GLOBALS["sod"]=='asc'){?>selected<?php }?>>노출점수 정방향 (↓)</option>
						<option value="tg_score|desc" <?php if($GLOBALS["sst"]=='tg_score'&&$GLOBALS["sod"]=='desc'){?>selected<?php }?>>노출점수 역방향 (↑)</option>
						<option value="tg_dpmenu|asc" <?php if($GLOBALS["sst"]=='tg_dpmenu'&&$GLOBALS["sod"]=='asc'){?>selected<?php }?>>메뉴노출 정방향 (↓)</option>
						<option value="tg_dpmenu|desc" <?php if($GLOBALS["sst"]=='tg_dpmenu'&&$GLOBALS["sod"]=='desc'){?>selected<?php }?>>메뉴노출 역방향 (↑)</option>
						<option value="tg_recommdt|asc" <?php if($GLOBALS["sst"]=='tg_recommdt'&&$GLOBALS["sod"]=='asc'){?>selected<?php }?>>추천일자 정방향 (↓)</option>
						<option value="tg_recommdt|desc" <?php if($GLOBALS["sst"]=='tg_recommdt'&&$GLOBALS["sod"]=='desc'){?>selected<?php }?>>추천일자 역방향 (↑)</option>
						<option value="tg_regdt|asc" <?php if($GLOBALS["sst"]=='tg_regdt'&&$GLOBALS["sod"]=='asc'){?>selected<?php }?>>등록일 정방향 (↓)</option>
						<option value="tg_regdt|desc" <?php if($GLOBALS["sst"]=='tg_regdt'&&$GLOBALS["sod"]=='desc'){?>selected<?php }?>>등록일 역방향 (↑)</option>
					</select><i></i>
				</label>
			</section>
		</div>
	</div>
	</form>

	<form name="ftaglist" id="ftaglist" action="<?php echo $GLOBALS["action_url2"]?>" method="post" onsubmit="return ftaglist_submit(this);" class="eyoom-form">
	<input type="hidden" name="theme" id="theme" value="<?php echo $TPL_VAR["this_theme"]?>">
	<input type="hidden" name="sst" value="<?php echo $GLOBALS["sst"]?>">
	<input type="hidden" name="sod" value="<?php echo $GLOBALS["sod"]?>">
	<input type="hidden" name="sfl" value="tg_word">
	<input type="hidden" name="stx" value="<?php echo $GLOBALS["stx"]?>">
	<input type="hidden" name="page" value="<?php echo $GLOBALS["page"]?>">
	<input type="hidden" name="token" value="<?php echo $GLOBALS["token"]?>">

<?php if(G5_IS_MOBILE){?>
	<p class="font-size-11 color-grey text-right margin-bottom-5"><i class="fa fa-info-circle"></i> Note! 좌우스크롤 가능 (<i class="fa fa-arrows-h"></i>)</p>
<?php }?>

	<div id="tag-list"></div>

	<div class="margin-top-20">
		<div class="row">
			<div class="col col-6">
			    <input type="submit" name="act_button" value="선택수정" class="btn-e btn-e-xs btn-e-red" onclick="document.pressed=this.value">
			    <input type="submit" name="act_button" value="선택삭제" class="btn-e btn-e-xs btn-e-dark" onclick="document.pressed=this.value">
			</div>
			<div class="col col-6">
				<div class="text-right">
					<div class="row">
						<div class="col col-6">
							<label class="input input-button">
								<input type="text" name="tg_new_word" value="" id="tg_new_word" placeholder="태그명">
								<div class="button"><input type="submit" name="act_button" value="태그추가" onclick="document.pressed=this.value">태그추가</div>
							</label>
						</div>
						<div class="col col-4">
							<label class="select">
								<select id="target_theme" name="target_theme">
									<option value="">:: 테마 선택 ::</option>
<?php if($TPL_exist_theme_1){foreach($TPL_VAR["exist_theme"] as $TPL_V1){?>
<?php if($TPL_V1!=$TPL_VAR["this_theme"]){?>
									<option value="<?php echo $TPL_V1?>">[<?php echo $TPL_V1?>] 테마에</option>
<?php }?>
<?php }}?>
								</select>
								<i></i>
							</label>
						</div>
						<div class="col col-2">
							<div class="text-right">
								<input type="submit" name="act_button" value="태그복사" class="btn-e btn-e-dark" onclick="document.pressed=this.value">
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	</form>
</div>

<?php echo eb_admin_paging('basic')?>


<script src="/admin/admin_theme/admin_basic/plugins/jsgrid/jsgrid.min.js"></script>
<script src="/admin/admin_theme/admin_basic/js/jsgrid.js"></script>
<script>
/*--------------------------------------
	Table
--------------------------------------*/
!function () {
    var db = {
        deleteItem: function (deletingClient) {
            var clientIndex = $.inArray(deletingClient, this.clients);
            this.clients.splice(clientIndex, 1)
        },
        insertItem: function (insertingClient) {
            this.clients.push(insertingClient)
        },
        loadData  : function (filter) {
            return $.grep(this.clients, function (client) {
                return !(filter.체크 && !(client.체크.indexOf(filter.체크) > -1)  )
            })
        },
        updateItem: function (updatingClient) {}
    };
    window.db    = db,
    db.clients   = [
<?php if($TPL_tag_list_1){$TPL_I1=-1;foreach($TPL_VAR["tag_list"] as $TPL_V1){$TPL_I1++;?>
        {
	        체크: "<label for='chk_<?php echo $TPL_I1?>' class='checkbox'><input type='checkbox' name='chk[]' id='chk_<?php echo $TPL_I1?>' value='<?php echo $TPL_I1?>'><i></i></label><input type='hidden' name='tg_id[<?php echo $TPL_I1?>]' value='<?php echo $TPL_V1["tg_id"]?>'>",
	        태그노출: "<a href='javascript:;' id='dpmenu_y_<?php echo $TPL_V1["tg_id"]?>' class='chg_dpmenu' data-tgid='<?php echo $TPL_V1["tg_id"]?>' data-tgyn='y'><i class='fa fa-check color-red margin-right-3 <?php if($TPL_V1["tg_dpmenu"]=='y'){?>check-show<?php }?>'></i><span class='default-text <?php if($TPL_V1["tg_dpmenu"]=='y'){?>check-show-text<?php }?>'>노출</span></a> <a href='javascript:;' id='dpmenu_n_<?php echo $TPL_V1["tg_id"]?>' class='chg_dpmenu margin-left-10' data-tgid='<?php echo $TPL_V1["tg_id"]?>' data-tgyn='n'><i class='fa fa-check color-red margin-right-3 <?php if($TPL_V1["tg_dpmenu"]=='n'){?>check-show<?php }?>'></i><span class='default-text <?php if($TPL_V1["tg_dpmenu"]=='n'){?>check-show-text<?php }?>'>미노출</span></a><input type='hidden' name='tg_dpmenu[<?php echo $TPL_I1?>]' value='<?php echo $TPL_V1["tg_dpmenu"]?>'>",
	        태그명: "<label for='tg_id_<?php echo $TPL_I1?>' class='input'><input type='text' name='tg_word[<?php echo $TPL_I1?>]' id='tg_id_<?php echo $TPL_I1?>' value='<?php echo $TPL_V1["tg_word"]?>'></label>",
	        등록수: "<label for='tg_regcnt_<?php echo $TPL_I1?>' class='input'><input type='text' name='tg_regcnt[<?php echo $TPL_I1?>]' id='tg_regcnt_<?php echo $TPL_I1?>' value='<?php echo $TPL_V1["tg_regcnt"]?>' class='text-right'></label>",
	        검색수: "<label for='tg_scnt_<?php echo $TPL_I1?>' class='input'><input type='text' name='tg_scnt[<?php echo $TPL_I1?>]' id='tg_scnt_<?php echo $TPL_I1?>' value='<?php echo $TPL_V1["tg_scnt"]?>' class='text-right'></label>",
	        노출점수: "<label for='tg_score_<?php echo $TPL_I1?>' class='input'><input type='text' name='tg_score[<?php echo $TPL_I1?>]' id='tg_score_<?php echo $TPL_I1?>' value='<?php echo $TPL_V1["tg_score"]?>' class='text-right'></label>",
	        태그추천: "<a href='javascript:;' id='tg_recommbtn_y_<?php echo $TPL_V1["tg_id"]?>' class='set_recommend' data-tgid='<?php echo $TPL_V1["tg_id"]?>' data-tgyn='y'><u>추천</u></a> <a href='javascript:;' id='tg_recommbtn_n_<?php echo $TPL_V1["tg_id"]?>' class='set_recommend margin-left-10' data-tgid='<?php echo $TPL_V1["tg_id"]?>' data-tgyn='n'><u>취소</u></a>",
	        추천일자: "<div id='tg_recommdt_<?php echo $TPL_V1["tg_id"]?>'><?php if($TPL_V1["tg_recommdt"]=='0000-00-00 00:00:00'){?>-<?php }else{?><?php echo $TPL_V1["tg_recommdt"]?><input type='hidden' name='tg_recommdt[<?php echo $TPL_I1?>]' value='<?php echo $TPL_V1["tg_recommdt"]?>'><?php }?></div>",
	        등록일: "<?php echo $TPL_V1["tg_regdt"]?><input type='hidden' name='tg_regdt[<?php echo $TPL_I1?>]' value='<?php echo $TPL_V1["tg_regdt"]?>'>",
        },
<?php }}?>
    ]
}();

$(function() {
    $("#tag-list").jsGrid({
        filtering      : false,
        editing        : false,
        sorting        : false,
        paging         : true,
        autoload       : true,
        controller     : db,
        deleteConfirm  : "정말로 삭제하시겠습니까?\n한번 삭제된 데이터는 복구할수 없습니다.",
        pageButtonCount: 5,
        pageSize       : 15,
        width          : "100%",
        height         : "auto",
        fields         : [
            { name: "체크", type: "text", width: 40 },
            { name: "태그노출", type: "text", align: "center", width: 100, headercss: "set-btn-header", css: "set-btn-field" },
            { name: "태그명", type: "text", width: 150 },
            { name: "등록수", type: "text", width: 70 },
            { name: "검색수", type: "text", width: 70 },
            { name: "노출점수", type: "text", width: 70 },
            { name: "태그추천", type: "text", align: "center", width: 80, headercss: "set-btn-header", css: "set-btn-field" },
            { name: "추천일자", type: "text", align: "center", width: 120 },
            { name: "등록일", type: "text", align: "center", width: 120 },
        ]
    });

    var $chk = $("#tag-list .jsgrid-table th:first-child");
	if ($chk.text() == '체크') {
		var html = '<label for="chkall" class="checkbox"><input type="checkbox" name="chkall" id="chkall" value="1" onclick="check_all(this.form)"><i></i></label>';
		$chk.html(html);
	}

	$(".chg_dpmenu").click(function() {
		var id = $(this).attr('data-tgid');
		var yn = $(this).attr('data-tgyn');
		var url = "<?php echo EYOOM_ADMIN_URL?>/?dir=theme&pid=tag_dpmenu&smode=1";
		$.post(url, {'id':id,'yn':yn}, function(data) {
			if(data.dpmenu) {
				if (data.dpmenu == 'n') {
					$('#dpmenu_y_'+id+' .fa-check').show();
					$('#dpmenu_n_'+id+' .fa-check').hide();
				} else if (data.dpmenu == 'y') {
					$('#dpmenu_y_'+id+' .fa-check').hide();
					$('#dpmenu_n_'+id+' .fa-check').show();
				}
			}
		},"json");
	});

	$(".set_recommend").click(function() {
		var id = $(this).attr('data-tgid');
		var yn = $(this).attr('data-tgyn');
		var url = "<?php echo EYOOM_ADMIN_URL?>/?dir=theme&pid=tag_recommend&smode=1";
		$.post(url, {'id':id,'yn':yn}, function(data) {
			if (data.recommdt && yn == 'y') {
				$("#tg_recommdt_"+id).text(data.recommdt);
			} else {
				$("#tg_recommdt_"+id).text('-');
			}
		},"json");
	});
});

function sorting_list(f, str) {
    var sort = str.split('|');

	$("#sst").val(sort[0]);
	$("#sod").val(sort[1]);

	if (sort[0] && sort[1]) {
		f.submit();
	}
}

function ftagform_submit(f) {
	if (f.stx.value == '') {
		alert('검색하고자 하는 태그명을 입력해 주세요.');
		f.stx.focus();
		return false;
	}
}

function ftaglist_submit(f) {
    if(document.pressed == "태그추가") {
        if(f.tg_new_word.value == '') {
	        alert('태그명을 입력해 주세요.');
	        f.tg_new_word.focus();
	        return false;
        }
    } else {
	    if (!is_checked("chk[]")) {
	        alert(document.pressed+" 하실 항목을 하나 이상 선택하세요.");
	        return false;
	    }

	    if(document.pressed == "선택삭제") {
	        if(!confirm("선택한 자료를 정말 삭제하시겠습니까?")) {
	            return false;
	        }
	    }
	    if(document.pressed == "태그복사") {
	        if ($("#target_theme option:selected").val() == '') {
		        alert('태그복사할 테마를 선택해 주세요.');
		        $("#target_theme").focus();
		        return false;
	        }
	    }
    }

    return true;
}

function del_confirm() {
	if (confirm('태그를 삭제하시겠습니까?')) {
		return true;
	} else {
		return false;
	}
}
</script>