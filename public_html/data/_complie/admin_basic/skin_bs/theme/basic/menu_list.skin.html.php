<?php /* Template_ 2.2.8 2018/02/02 18:27:10 /home/bluebamus1/public_html/admin/admin_theme/admin_basic/skin_bs/theme/basic/menu_list.skin.html 000006262 */ 
$TPL_exist_theme_1=empty($TPL_VAR["exist_theme"])||!is_array($TPL_VAR["exist_theme"])?0:count($TPL_VAR["exist_theme"]);?>
<?php if (!defined('_GNUBOARD_')) exit;
add_stylesheet('<link rel="stylesheet" href="/admin/admin_theme/admin_basic/plugins/easyui/themes/default/easyui.css" type="text/css" media="screen">',0);
?>

<style>
.admin-menu-list .easyui-panel {padding:10px 0}
.admin-menu-list #ctrl-btns a {padding:2px 5px}
.admin-menu-list .panel {border-right:1px solid #95B8E7;margin-top:-2px}
</style>

<div class="admin-menu-list">
	<div class="headline">
		<h4><strong>커뮤니티 메뉴 설정</strong></h4>
	</div>
	<div class="margin-bottom-30"></div>

<?php $this->print_("theme_manager",$TPL_SCP,1);?>


	<form name="fmenuform" method="post" action="<?php echo $GLOBALS["action_url"]?>" class="eyoom-form">
	<input type="hidden" name="theme" value="<?php echo $TPL_VAR["this_theme"]?>">
	<input type="hidden" name="page" value="<?php echo $GLOBALS["page"]?>">
	<input type="hidden" name="token" value="<?php echo $GLOBALS["token"]?>">

	<div class="headline">
		<h4><strong>[<span class="color-red"><?php echo $TPL_VAR["this_theme"]?></span>] 커뮤니티 메뉴</strong></h4>
	</div>
	<div class="margin-bottom-30"></div>

	<div class="menutree-box">
		<div class="row">
			<div class="col col-4">
				<div class="row">
					<div class="col col-5" id="ctrl-btns">
						<a href="javascript:;" class="btn-e btn-e-xs btn-e-dark" onclick="collapseAll()">전체닫기</a>
						<a href="javascript:;" class="btn-e btn-e-xs btn-e-indigo" onclick="expandAll()">전체열기</a>
					</div>
					<div class="col col-7">
						<label for="target_theme" class="select" onchange="if(this.value!='') return clone_menu();">
							<select id="target_theme" name="target_theme">
								<option value="">:: 메뉴복사 테마선택 ::</option>
<?php if($TPL_exist_theme_1){foreach($TPL_VAR["exist_theme"] as $TPL_V1){?>
								<option value="<?php echo $TPL_V1?>">[<?php echo $TPL_V1?>]테마로 메뉴복사</option>
<?php }}?>
							</select><i></i>
						</label>
					</div>
				</div>
				<div class="margin-bottom-5"></div>
				<div class="easyui-panel">
					<ul id="menutree" class="easyui-tree" data-options="url:'<?php echo EYOOM_ADMIN_URL?>/?dir=theme&amp;pid=menu_json&amp;thema=<?php echo $TPL_VAR["this_theme"]?>&amp;<?php if($_GET["id"]){?>?id=<?php echo $_GET["id"]?><?php }?>&amp;smode=1',method:'get',animate:true,lines:true"></ul>
				</div>
				<div class="margin-bottom-20"></div>
			</div>
			<div class="col col-8">
				<h4 class="margin-top-0 margin-bottom-15"><strong class="color-indigo">커뮤니티 메뉴 설정</strong></h4>
				<iframe id="ifrm_menuform" name="ifrm_menuform" src="<?php echo EYOOM_ADMIN_URL?>/?dir=theme&pid=menu_form&wmode=1&thema=<?php echo $TPL_VAR["this_theme"]?><?php if($GLOBALS["me_id"]){?>&id=<?php echo $GLOBALS["me_id"]?><?php }?>" scrolling="yes" style="overflow-x:hidden;overflow:auto;width:100%;min-height:1350px" frameborder=0></iframe>
			</div>
		</div>
	</div>

	</form>
</div>

<div class="modal fade menuform-iframe-modal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title"><strong>폰트어썸</strong></h4>
            </div>
            <div class="modal-body">
                <iframe id="menuform-iframe" width="100%" frameborder="0"></iframe>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn-e btn-e-lg btn-e-dark" type="button"><i class="fa fa-close"></i> 닫기</button>
            </div>
        </div>
    </div>
</div>

<script src="/admin/admin_theme/admin_basic/plugins/easyui/jquery.easyui.min.js"></script>
<script>
$(function(){
	$('#menutree').tree('expandAll');
	$('#menutree').tree({
		dnd: false,
		onDrop: function(targetNode, source, point){
			var targetId = $('#menutree').tree('getNode', targetNode).id;
			var targetOrder = $('#menutree').tree('getNode', targetNode).order;
			$.ajax({
				url: '...',
				type: 'post',
				dataType: 'json',
				data: {
					id: source.id,
					targetId: targetId,
					point: point
				}
			});
		},
		onClick: function(source){
			var url='<?php echo EYOOM_ADMIN_URL?>/?dir=theme&pid=menu_form&thema=<?php echo $TPL_VAR["this_theme"]?>&wmode=1&id='+source.id;
			document.getElementById("ifrm_menuform").contentWindow.location.href = url;
		}
	});
});

function collapseAll() {
	$('#menutree').tree('collapseAll');
}

function expandAll() {
	$('#menutree').tree('expandAll');
}

function clone_menu() {
	var theme = '<?php echo $TPL_VAR["this_theme"]?>';
	var target = $("#target_theme option:selected").val();
	if (!target) {
		alert('메뉴를 복사할 테마를 선택해 주세요.');
		return false;
	}
	if (confirm("선택한 ["+target+"]테마의 기존 설정 메뉴는 모두 사라집니다.\n\n계속 진행할까요?")) {
		var url = "<?php echo EYOOM_ADMIN_URL?>/?dir=theme&pid=menu_copy&smode=1";
		$.post(url, {'theme':theme,'target':target,'me_shop':2}, function(data) {
			if(data.result == 'ok') {
				alert('정상적으로 메뉴를 복사하였습니다.\n['+target+']테마 메뉴로 이동합니다.');
				document.location.href = "<?php echo EYOOM_ADMIN_URL?>/?dir=theme&pid=menu_list&thema="+target;
			}
		},"json");
	} else {
		$("#target_theme option:eq(0)").attr("selected", "selected");
	}
}

/*--------------------------------------
	Modal
--------------------------------------*/
function menuform_modal(href) {
    $('.menuform-iframe-modal').modal('show').on('hidden.bs.modal', function () {
        $("#menuform-iframe").attr("src", "");
        $('html').css({overflow: ''});
    });
    $('.menuform-iframe-modal').modal('show').on('shown.bs.modal', function () {
        $("#menuform-iframe").attr("src", href);
        $('#menuform-iframe').height(700);
        $('html').css({overflow: 'hidden'});
    });
    return false;
}
</script>