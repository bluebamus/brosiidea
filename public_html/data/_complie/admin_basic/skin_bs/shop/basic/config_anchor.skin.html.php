<?php /* Template_ 2.2.8 2018/02/02 18:27:10 /home/bluebamus1/public_html/admin/admin_theme/admin_basic/skin_bs/shop/basic/config_anchor.skin.html 000001363 */ ?>
<?php if (!defined('_GNUBOARD_')) exit; ?>

<div class="pg-anchor-in tab-e2">
    <ul class="nav nav-tabs">
		<li <?php if($TPL_VAR["anc_id"]=='anc_scf_info'){?>class="active"<?php }?>><a href="#anc_scf_info">사업자정보</a></li>
		<li <?php if($TPL_VAR["anc_id"]=='anc_scf_skin'){?>class="active"<?php }?>><a href="#anc_scf_skin">스킨설정</a></li>
		<li <?php if($TPL_VAR["anc_id"]=='anc_scf_index'){?>class="active"<?php }?>><a href="#anc_scf_index">쇼핑몰 초기화면</a></li>
		<li <?php if($TPL_VAR["anc_id"]=='anc_mscf_index'){?>class="active"<?php }?>><a href="#anc_mscf_index">모바일 초기화면</a></li>
		<li <?php if($TPL_VAR["anc_id"]=='anc_scf_payment'){?>class="active"<?php }?>><a href="#anc_scf_payment">결제설정</a></li>
		<li <?php if($TPL_VAR["anc_id"]=='anc_scf_delivery'){?>class="active"<?php }?>><a href="#anc_scf_delivery">배송설정</a></li>
		<li <?php if($TPL_VAR["anc_id"]=='anc_scf_etc'){?>class="active"<?php }?>><a href="#anc_scf_etc">기타설정</a></li>
<?php if( 0){?><li <?php if($TPL_VAR["anc_id"]=='anc_scf_sms'){?>class="active"<?php }?>><a href="#anc_scf_sms">SMS설정</a></li><?php }?>
	</ul>
	<div class="tab-bottom-line"></div>
</div>