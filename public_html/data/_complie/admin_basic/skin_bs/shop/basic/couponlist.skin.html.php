<?php /* Template_ 2.2.8 2018/01/25 01:09:15 /home/bluebamus1/public_html/admin/admin_theme/admin_basic/skin_bs/shop/basic/couponlist.skin.html 000007739 */  $this->include_("eb_admin_paging");
$TPL_list_1=empty($TPL_VAR["list"])||!is_array($TPL_VAR["list"])?0:count($TPL_VAR["list"]);?>
<?php if (!defined('_GNUBOARD_')) exit;
add_stylesheet('<link rel="stylesheet" href="/admin/admin_theme/admin_basic/plugins/jsgrid/jsgrid.min.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/admin/admin_theme/admin_basic/plugins/jsgrid/jsgrid-theme.min.css" type="text/css" media="screen">',0);
?>

<div class="admin-shop-couponlist">
	<form name="fsearch" id="fsearch" action="./" class="eyoom-form" method="get">

	<div class="headline">
		<h4><strong>쿠폰관리</strong></h4>
		<a href="<?php echo EYOOM_ADMIN_URL?>/?dir=shop&pid=couponform" class="btn-e btn-e-purple btn-e-xs pull-right margin-top-5"><i class="fa fa-plus"></i> 쿠폰 추가</a>
		<div class="clearfix"></div>
	</div>
	<div class="margin-bottom-30"></div>

	<div class="admin-search-box trans-col">
		<div class="row">
			<div class="col col-2">
				<label for="sfl" class="sound_only">검색대상</label>
				<label class="select margin-bottom-0">
					<select name="sfl" id="sfl">
					    <option value="mb_id" <?php if($_GET["sfl"]=='mb_id'){?>selected<?php }?>>회원아이디</option>
					    <option value="cp_subject" <?php if($_GET["sfl"]=='cp_subject'){?>selected<?php }?>>쿠폰이름</option>
					    <option value="cp_id" <?php if($_GET["sfl"]=='pp_id'){?>selected<?php }?>>쿠폰코드</option>
					</select>
					<i></i>
				</label>
			</div>
			<div class="col col-4">
				<label for="stx" class="sound_only">검색어<strong class="sound_only"> 필수</strong></label>
				<input type="hidden" name="dir" value="<?php echo $GLOBALS["dir"]?>" id="dir">
				<input type="hidden" name="pid" value="<?php echo $GLOBALS["pid"]?>" id="pid">
				<label class="input input-button margin-bottom-0">
					<input type="text" name="stx" value="<?php echo $GLOBALS["stx"]?>" id="stx" required autocomplete="off">
					<div class="button"><input type="submit" value="검색">검색</div>
				</label>
			</div>
		</div>
	</div>
	</form>
	<div class="margin-bottom-20"></div>

	<form name="fcouponlist" id="fcouponlist" action="<?php echo $GLOBALS["action_url"]?>" onsubmit="return fcouponlist_submit(this);" method="post" autocomplete="off" class="eyoom-form">
	<input type="hidden" name="sst" id="sst" value="<?php echo $GLOBALS["sst"]?>">
	<input type="hidden" name="sod" id="sod" value="<?php echo $GLOBALS["sod"]?>">
	<input type="hidden" name="sfl" id="sfl" value="<?php echo $GLOBALS["sfl"]?>">
	<input type="hidden" name="stx" id="stx" value="<?php echo $GLOBALS["stx"]?>">
	<input type="hidden" name="page" id="page" value="<?php echo $GLOBALS["page"]?>">
	<input type="hidden" name="token" value="">

	<div class="row">
		<div class="col-sm-8">
			<div class="margin-bottom-5">
			    <span class="font-size-12 color-grey">
			    	<a href="<?php echo EYOOM_ADMIN_URL?>/?dir=<?php echo $GLOBALS["dir"]?>&amp;pid=<?php echo $GLOBALS["pid"]?>" class="ov_listall">[전체목록]</a><span class="margin-left-10 margin-right-10 color-light-grey">|</span>전체 <?php echo number_format($GLOBALS["total_count"])?>개
			    </span>
			</div>
		</div>
	</div>

<?php if(G5_IS_MOBILE){?>
	<p class="font-size-11 color-grey text-right margin-bottom-5"><i class="fa fa-info-circle"></i> Note! 좌우스크롤 가능 (<i class="fa fa-arrows-h"></i>)</p>
<?php }?>

	<div id="admin-shop-couponlist"></div>

<?php if(!$GLOBALS["wmode"]){?>
	<div class="margin-top-20">
	    <input type="submit" name="act_button" value="선택삭제" class="btn-e btn-e-xs btn-e-dark" onclick="document.pressed=this.value">
	</div>
<?php }?>
	</form>
</div>

<?php echo eb_admin_paging('basic')?>


<script src="/admin/admin_theme/admin_basic/plugins/jsgrid/jsgrid.min.js"></script>
<script src="/admin/admin_theme/admin_basic/js/jsgrid.js"></script>

<script>
!function () {
    var db = {
        deleteItem: function (deletingClient) {
            var clientIndex = $.inArray(deletingClient, this.clients);
            this.clients.splice(clientIndex, 1)
        },
        insertItem: function (insertingClient) {
            this.clients.push(insertingClient)
        },
        loadData  : function (filter) {
            return $.grep(this.clients, function (client) {
                return !(filter.체크 && !(client.체크.indexOf(filter.체크) > -1) )
            })
        },
        updateItem: function (updatingClient) {}
    };
    window.db    = db,
    db.clients   = [
<?php if($TPL_list_1){$TPL_I1=-1;foreach($TPL_VAR["list"] as $TPL_V1){$TPL_I1++;?>
        {
	        체크: "<input type='hidden' name='cp_id[<?php echo $TPL_I1?>]' value='<?php echo $TPL_V1["cp_id"]?>' id='cp_id_<?php echo $TPL_I1?>'><label for='chk_<?php echo $TPL_I1?>' class='checkbox'><input type='checkbox' name='chk[]' value='<?php echo $TPL_I1?>' id='chk_<?php echo $TPL_I1?>'><i></i></label>",
	        관리: "<span class='text-center grid-buttons'><a href='<?php echo EYOOM_ADMIN_URL?>/?dir=shop&pid=couponform&w=u&cp_id=<?php echo $TPL_V1["cp_id"]?><?php if($GLOBALS["qstr"]){?>&<?php echo $GLOBALS["qstr"]?><?php }?>'><u>수정</u></a></span>",
	        쿠폰종류: "<?php echo $TPL_V1["cp_method"]?>",
	        쿠폰코드: "<?php echo $TPL_V1["cp_id"]?>",
	        쿠폰이름: "<?php echo $TPL_V1["cp_subject"]?>",
	        적용대상: "<?php echo $TPL_V1["cp_target"]?>",
	        회원아이디: "<?php echo $TPL_V1["mb_id"]?>",
	        사용기한: "<?php echo substr($TPL_V1["cp_start"], 2, 8)?> ~ <?php echo substr($TPL_V1["cp_end"], 2, 8)?>",
	        사용회수: "<?php echo number_format($TPL_V1["used_count"])?>"
        },
<?php }}?>
    ]
}();

$(document).ready(function(){
    $("#admin-shop-couponlist").jsGrid({
        filtering      : false,
        editing        : false,
        sorting        : false,
        paging         : true,
        autoload       : true,
        controller     : db,
        deleteConfirm  : "정말로 삭제하시겠습니까?\n한번 삭제된 데이터는 복구할수 없습니다.",
        pageButtonCount: 5,
        pageSize       : 15,
        width          : "100%",
        height         : "auto",
        fields         : [
            { name: "체크", type: "text", width: 40 },
            { name: "관리", type: "text", align: "center", width: 70, headercss: "set-btn-header", css: "set-btn-field" },
            { name: "쿠폰종류", type: "text", width: 80 },
            { name: "쿠폰코드", type: "text", align: "center", width: 120 },
            { name: "쿠폰이름", type: "text", width: 200 },
            { name: "적용대상", type: "text", width: 150 },
            { name: "회원아이디", type: "text", width: 100 },
            { name: "사용기한", type: "text", align: "center", width: 150 },
            { name: "사용회수", type: "number", width: 80 }
        ]
    });

    var $chk = $(".jsgrid-table th:first-child");
	if ($chk.text() == '체크') {
		var html = '<label for="chkall" class="checkbox"><input type="checkbox" name="chkall" id="chkall" value="1" onclick="check_all(this.form)"><i></i></label>';
		$chk.html(html);
	}
});
</script>

<script>
function fcouponlist_submit(f) {
    if (!is_checked("chk[]")) {
        alert(document.pressed+" 하실 항목을 하나 이상 선택하세요.");
        return false;
    }

    if(document.pressed == "선택삭제") {
        if(!confirm("선택한 자료를 정말 삭제하시겠습니까?")) {
            return false;
        }
    }

    return true;
}
</script>