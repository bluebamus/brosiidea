<?php /* Template_ 2.2.8 2018/04/30 10:48:44 /home1/bluebamus2/public_html/admin/admin_theme/admin_basic/skin_bs/paging/basic/paging.skin.html 000002406 */ 
$TPL_paging_1=empty($TPL_VAR["paging"])||!is_array($TPL_VAR["paging"])?0:count($TPL_VAR["paging"]);?>
<?php if (!defined("_GNUBOARD_")) exit; ?>

<style>
.eb-pagination-wrap {position:relative;text-align:center;padding:7px;margin-top:30px}
.eb-pagination {position:relative;list-style:none;display:inline-block;padding:0;margin:0}
.eb-pagination li {display:inline}
.eb-pagination a {display:inline-block;font-size:12px;text-decoration:none;min-width:28px;height:30px;padding:0 5px;color:#fff;border:1px solid transparent;line-height:28px;text-align:center;color:#656565;margin:0 3px;position:relative;z-index:1}
.eb-pagination a:active {outline:none}
.eb-pagination a:hover {color:#000;background:#e5e5e5}
.eb-pagination a.active {cursor:default;background:#656565;color:#fff}
.eb-pagination a.active:hover {color:#fff;background:#656565}
.eb-pagination a.next,.eb-pagination a.prev {color:#959595}
.eb-pagination a.next:hover,.eb-pagination a.prev:hover {color:#000}
</style>

<div class="eb-pagination-wrap">
	<ul class="eb-pagination">
		<li><a href="<?php echo $TPL_VAR["url"]?>1<?php echo $TPL_VAR["add"]?>"><i class="fa fa-step-backward"></i></a></li>
		<li><a href="<?php echo $TPL_VAR["url"]?><?php if(($TPL_VAR["cur_page"]- 1)<= 0){?>1<?php }else{?><?php echo ($TPL_VAR["cur_page"]- 1)?><?php }?><?php echo $TPL_VAR["add"]?>" class="prev"><i class="fa fa-caret-left"></i></a></li>
<?php if($TPL_paging_1){foreach($TPL_VAR["paging"] as $TPL_K1=>$TPL_V1){?>
		<li><a href="<?php echo $TPL_V1["url"]?>" <?php if($TPL_V1["on"]){?>class="active"<?php }?>><?php echo $TPL_K1?><span class="sound_only">페이지</span></a></li>
<?php }}else{?>
		<li><a href="<?php echo $TPL_VAR["url"]?>1<?php echo $TPL_VAR["add"]?>" class="active">1<span class="sound_only">페이지</span></a></li>
<?php }?>
		<li><a href="<?php echo $TPL_VAR["url"]?><?php if(($TPL_VAR["cur_page"]+ 1)>$TPL_VAR["total_page"]){?><?php echo $TPL_VAR["total_page"]?><?php }else{?><?php echo ($TPL_VAR["cur_page"]+ 1)?><?php }?><?php echo $TPL_VAR["add"]?>" class="next"><i class="fa fa-caret-right"></i></a></li>
		<li><a href="<?php echo $TPL_VAR["url"]?><?php echo $TPL_VAR["total_page"]?><?php echo $TPL_VAR["add"]?>"><i class="fa fa-step-forward"></i></a></li>
	</ul>
</div>