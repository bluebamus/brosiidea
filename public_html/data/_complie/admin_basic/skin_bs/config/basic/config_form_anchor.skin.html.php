<?php /* Template_ 2.2.8 2018/04/30 10:48:41 /home1/bluebamus2/public_html/admin/admin_theme/admin_basic/skin_bs/config/basic/config_form_anchor.skin.html 000001886 */ ?>
<?php if (!defined('_GNUBOARD_')) exit; ?>

<div class="pg-anchor-in tab-e2">
    <ul class="nav nav-tabs">
	    <li <?php if($TPL_VAR["anc_id"]=='anc_cf_basic'){?>class="active"<?php }?>><a href="#anc_cf_basic">기본환경</a></li>
	    <li <?php if($TPL_VAR["anc_id"]=='anc_cf_board'){?>class="active"<?php }?>><a href="#anc_cf_board">게시판기본</a></li>
	    <li <?php if($TPL_VAR["anc_id"]=='anc_cf_join'){?>class="active"<?php }?>><a href="#anc_cf_join">회원가입</a></li>
	    <li <?php if($TPL_VAR["anc_id"]=='anc_cf_cert'){?>class="active"<?php }?>><a href="#anc_cf_cert">본인확인</a></li>
	    <li <?php if($TPL_VAR["anc_id"]=='anc_cf_mail'){?>class="active"<?php }?>><a href="#anc_cf_mail">메일환경설정</a></li>
<?php if( 0){?>
	    <li <?php if($TPL_VAR["anc_id"]=='anc_cf_article_mail'){?>class="active"<?php }?>><a href="#anc_cf_article_mail">글작성메일</a></li>
	    <li <?php if($TPL_VAR["anc_id"]=='anc_cf_join_mail'){?>class="active"<?php }?>><a href="#anc_cf_join_mail">가입메일</a></li>
	    <li <?php if($TPL_VAR["anc_id"]=='anc_cf_vote_mail'){?>class="active"<?php }?>><a href="#anc_cf_vote_mail">투표메일</a></li>
<?php }?>
	    <li <?php if($TPL_VAR["anc_id"]=='anc_cf_sns'){?>class="active"<?php }?>><a href="#anc_cf_sns">SNS</a></li>
	    <li <?php if($TPL_VAR["anc_id"]=='anc_cf_lay'){?>class="active"<?php }?>><a href="#anc_cf_lay">레이아웃 추가설정</a></li>
	    <li <?php if($TPL_VAR["anc_id"]=='anc_cf_sms'){?>class="active"<?php }?>><a href="#anc_cf_sms">SMS</a></li>
	    <li <?php if($TPL_VAR["anc_id"]=='anc_cf_extra'){?>class="active"<?php }?>><a href="#anc_cf_extra">여분필드</a></li>
	</ul>
	<div class="tab-bottom-line"></div>
</div>