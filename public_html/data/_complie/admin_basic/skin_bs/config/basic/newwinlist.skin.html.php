<?php /* Template_ 2.2.8 2018/02/02 18:27:10 /home/bluebamus1/public_html/admin/admin_theme/admin_basic/skin_bs/config/basic/newwinlist.skin.html 000004919 */  $this->include_("eb_admin_paging");
$TPL_newwin_1=empty($TPL_VAR["newwin"])||!is_array($TPL_VAR["newwin"])?0:count($TPL_VAR["newwin"]);?>
<?php if (!defined('_GNUBOARD_')) exit;
add_stylesheet('<link rel="stylesheet" href="/admin/admin_theme/admin_basic/plugins/jsgrid/jsgrid.min.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/admin/admin_theme/admin_basic/plugins/jsgrid/jsgrid-theme.min.css" type="text/css" media="screen">',0);
?>

<div class="admin-newwinlist">
	<div class="headline">
		<h4><strong>팝업레이어 관리</strong></h4>
<?php if(!$GLOBALS["wmode"]){?>
		<a href="<?php echo EYOOM_ADMIN_URL?>/?dir=config&pid=newwinform" class="btn-e btn-e-purple btn-e-xs pull-right margin-top-5"><i class="fa fa-plus"></i> 팝업레이어 추가</a>
<?php }?>
		<div class="clearfix"></div>
	</div>
	<div class="margin-bottom-30"></div>

	<div class="margin-bottom-5">
	    <span class="font-size-12 color-grey">
	    	전체 <?php echo number_format($GLOBALS["total_count"])?>건
	    </span>
    </div>

<?php if(G5_IS_MOBILE){?>
	<p class="font-size-11 color-grey text-right margin-bottom-5"><i class="fa fa-info-circle"></i> Note! 좌우스크롤 가능 (<i class="fa fa-arrows-h"></i>)</p>
<?php }?>

	<div id="newwin-list"></div>
</div>

<?php echo eb_admin_paging('basic')?>


<script src="/admin/admin_theme/admin_basic/plugins/jsgrid/jsgrid.min.js"></script>
<script src="/admin/admin_theme/admin_basic/js/jsgrid.js"></script>
<script>
!function () {
    var db = {
        deleteItem: function (deletingClient) {
            var clientIndex = $.inArray(deletingClient, this.clients);
            this.clients.splice(clientIndex, 1)
        },
        insertItem: function (insertingClient) {
            this.clients.push(insertingClient)
        },
        loadData  : function (filter) {
            return $.grep(this.clients, function (client) {
                return !(filter.No && !(client.No.indexOf(filter.No) > -1) || filter.제목 && !(client.제목.indexOf(filter.제목) > -1))
            })
        },
        updateItem: function (updatingClient) {}
    };
    window.db    = db,
    db.clients   = [
<?php if($TPL_newwin_1){foreach($TPL_VAR["newwin"] as $TPL_V1){?>
        {
	        No: "<?php echo $TPL_V1["nw_id"]?>",
	        관리: "<a href='<?php echo EYOOM_ADMIN_URL?>/?dir=config&amp;pid=newwinform&amp;w=u&amp;nw_id=<?php echo $TPL_V1["nw_id"]?>'><u>수정</u></a> <a href='<?php echo EYOOM_ADMIN_URL?>/?dir=config&amp;pid=newwinform_update&amp;w=d&amp;nw_id=<?php echo $TPL_V1["nw_id"]?>&amp;smode=1' class='margin-left-10' onclick='return delete_confirm(this);'><u>삭제</u></a>",
	        제목: "<span class='ellipsis'><?php echo $TPL_V1["nw_subject"]?></span>",
	        접속기기: "<?php echo $TPL_V1["device"]?>",
	        시작일시: "<?php echo substr($TPL_V1["nw_begin_time"], 2, 14)?>",
	        종료일시: "<?php echo substr($TPL_V1["nw_end_time"], 2, 14)?>",
	        시간: "<?php echo $TPL_V1["nw_disable_hours"]?>",
	        Left: "<?php echo $TPL_V1["nw_left"]?>",
	        Top: "<?php echo $TPL_V1["nw_top"]?>",
	        Width: "<?php echo $TPL_V1["nw_width"]?>",
	        Height: "<?php echo $TPL_V1["nw_height"]?>"
        },
<?php }}?>
    ]
}();

$(document).ready(function() {
    $("#newwin-list").jsGrid({
        filtering      : false,
        editing        : false,
        sorting        : false,
        paging         : true,
        autoload       : true,
        controller     : db,
        deleteConfirm  : "정말로 삭제하시겠습니까?\n한번 삭제된 데이터는 복구할수 없습니다.",
        pageButtonCount: 5,
        pageSize       : 15,
        width          : "100%",
        height         : "auto",
        fields         : [
            { name: "No", type: "number", align: "center", width: 40 },
            { name: "관리", type: "button", align: "center", width: 110, headercss: "set-btn-header", css: "set-btn-field" },
            { name: "제목", type: "text", width: 200 },
            { name: "접속기기", type: "text", align: "center", width: 80 },
            { name: "시작일시", type: "text", align: "center", width: 110 },
            { name: "종료일시", type: "text", align: "center", width: 110 },
            { name: "시간", type: "text", align: "center", width: 60 },
            { name: "Left", type: "number", width: 60 },
            { name: "Top", type: "number", width: 60 },
            { name: "Width", type: "number", width: 60 },
            { name: "Height", type: "number", width: 60 },
        ]
    })
});

function delete_confirm() {
	if (confirm("정말로 해당 팝업을 삭제하시겠습니까?")) {
		return true;
	} else return false;
}
</script>