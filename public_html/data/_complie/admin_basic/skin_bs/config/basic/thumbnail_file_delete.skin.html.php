<?php /* Template_ 2.2.8 2018/01/25 20:00:43 /home/bluebamus1/public_html/admin/admin_theme/admin_basic/skin_bs/config/basic/thumbnail_file_delete.skin.html 000001609 */ 
$TPL__print_html_1=empty($GLOBALS["print_html"])||!is_array($GLOBALS["print_html"])?0:count($GLOBALS["print_html"]);?>
<?php if (!defined('_GNUBOARD_')) exit; ?>

<div class="admin-thumbnail-file-delete">
	<div class="headline">
		<h4><strong>썸네일파일 일괄 삭제</strong></h4>
	</div>

	<div class="cont-text-bg">
		<p class="bg-danger font-size-12"><i class="fa fa-exclamation-circle"></i> 완료 메세지가 나오기 전에 프로그램의 실행을 중지하지 마십시오.</p>
	</div>

<?php if($GLOBALS["no_print"]){?>
	<div class="alert alert-warning padding-all-10 margin-top-30 margin-bottom-30">
		<p><?php echo $GLOBALS["no_print"]?></p>
	</div>
<?php }?>

	<div class="alert alert-warning padding-all-10 margin-top-30 margin-bottom-30">
		<ul>
			<li>완료됨</li>
<?php if($TPL__print_html_1){foreach($GLOBALS["print_html"] as $TPL_V1){?>
			<li><?php echo $TPL_V1?></li>
<?php }}?>
		</ul>
	</div>

	<div class="margin-bottom-15">
		<strong class="font-size-14 color-red">
			<span class="fa-stack fa-lg">
				<i class="fa fa-circle fa-stack-2x"></i>
				<i class="fa fa-check fa-stack-1x fa-inverse"></i>
			</span>
			<span class="color-black">썸네일 <span class="color-red"><?php echo $GLOBALS["cnt"]?></span> 건 삭제 완료됐습니다.</span>
		</strong>
	</div>

	<div class="cont-text-bg"><p class="bg-info font-size-12">... 프로그램의 실행을 끝마치셔도 좋습니다.</p></div>
</div>