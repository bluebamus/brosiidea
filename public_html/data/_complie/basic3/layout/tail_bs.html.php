<?php /* Template_ 2.2.8 2018/11/26 21:04:35 /home1/bluebamus2/public_html/eyoom/theme/basic3/layout/tail_bs.html 000005941 */ ?>
<?php if (!defined('_GNUBOARD_')) exit; ?>

<?php if(!$GLOBALS["wmode"]){?>
				</section>
<?php if($TPL_VAR["side_layout"]["use"]=='yes'){?>
<?php if($TPL_VAR["side_layout"]["pos"]=='right'){?>
<?php $this->print_("side_bs",$TPL_SCP,1);?>

<?php }?>
<?php }?>
				<div class="clearfix"></div>
			</div>		    <?php if(!defined('_INDEX_')){?>
		    </div>
<?php }?>
	    </div>
<?php if($TPL_VAR["footer_top"]=='yes'){?>
	    <div class="footer-top">
		    <div class="container">
			    <div class="footer-top-content">
				    <div class="footer-top-logo">
			            <a href="<?php echo G5_URL?>">
<?php if($TPL_VAR["logo"]=='text'){?>
							<h2><?php echo $TPL_VAR["config"]["cf_title"]?></h2>
<?php }elseif($TPL_VAR["logo"]=='image'){?>
							<img src="/eyoom/theme/basic3/image/site_logo.png" class="img-responsive">
<?php }?>
						</a>
				    </div>
				    <div class="footer-top-info">
						<strong><?php echo $TPL_VAR["config"]["cf_title"]?></strong>
						<span class="info-divider">|</span>
						<span>대표 : 임태연</span>
						<span class="info-divider">|</span>
						<span>사업자등록번호 : 420-44-00469</span>
						<span class="info-divider">|</span>
						<span>주소 : 서울 금천구 독산로 50길 23 307호<a href="<?php echo G5_URL?>/page/?pid=contactus" class="btn-e btn-e-xs btn-e-dark margin-left-5">상세지도</a></span>
						<span class="info-divider">|</span><br>
						<span>T. 02-6465-1888</span>
						<span class="info-divider">|</span>
						<span>F. 0504-167-1125</span>
						<span class="info-divider">|</span>
						<span><a href="<?php echo G5_URL?>/page/?pid=provision">이용약관</a></span>
						<span class="info-divider">|</span>
						<span><a href="<?php echo G5_URL?>/page/?pid=privacy">개인정보 취급방침</a></span>
						<span class="info-divider">|</span>
						<span><a href="<?php echo G5_URL?>/page/?pid=noemail">이메일 무단수집거부</a></span>
				    </div>
			    </div>
		    </div>
		</div>
<?php }?>

	    <footer class="footer footer-dark">
		    <div class="footer-left">
			    <ul class="list-unstyled list-inline">
				    <li>
				    	<span class="footer-site-name hidden-xs"><?php echo $TPL_VAR["config"]["cf_title"]?></span>
						<div class="btn-group dropup visible-xs">
							<span data-toggle="dropdown" class="footer-info-btn dropdown-toggle"><?php echo $TPL_VAR["config"]["cf_title"]?> 정보<i class="fa fa-caret-up margin-left-5"></i></span>
							<ul role="menu" class="dropdown-menu">
								<li><a href="<?php echo G5_URL?>/page/?pid=aboutus">회사소개</a></li>
								<li class="divider"></li>
								<li><a href="<?php echo G5_URL?>/page/?pid=provision">이용약관</a></li>
								<li><a href="<?php echo G5_URL?>/page/?pid=privacy">개인정보처리방침</a></li>
								<li><a href="<?php echo G5_URL?>/page/?pid=noemail">이메일무단수집거부</a></li>
							</ul>
						</div>
				    </li>
				    <li class="hidden-xs"><a href="<?php echo G5_URL?>/page/?pid=aboutus">회사소개</a><a href="<?php echo G5_URL?>/page/?pid=provision">이용약관</a><a href="<?php echo G5_URL?>/page/?pid=privacy">개인정보처리방침</a><a href="<?php echo G5_URL?>/page/?pid=noemail">이메일무단수집거부</a></li>
			    </ul>
		    </div>
		    <div class="footer-right">
			    <p><span class="hidden-xs">Copyright </span>&copy; <?php echo $TPL_VAR["config"]["cf_title"]?>. All Rights Reserved.</p>
		    </div>
	    </footer>
    </div>
	<div class="back-to-top">
        <i class="fa fa-angle-up"></i>
    </div>
</div><?php }?>

<div class="sidebar-left-mask sidebar-left-trigger" data-action="toggle" data-side="left"></div>
<div class="sidebar-right-mask sidebar-right-trigger" data-action="toggle" data-side="right"></div>

<?php $this->print_("misc_bs",$TPL_SCP,1);?>


<?php if($GLOBALS["is_member"]&&$TPL_VAR["eyoomer"]["onoff_push"]=='on'&&$TPL_VAR["eyoom"]["is_community_theme"]=='y'){?>
<?php $this->print_("push_bs",$TPL_SCP,1);?>

<?php }?>

<script src="/eyoom/theme/basic3/plugins/jquery-migrate-1.2.1.min.js"></script>
<script src="/eyoom/theme/basic3/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="/eyoom/theme/basic3/plugins/jquery.bootstrap-hover-dropdown.min.js"></script>
<script src="/eyoom/theme/basic3/plugins/sidebar/jquery.sidebar.min.js"></script>
<?php if($TPL_VAR["eyoom"]["use_shop_itemtype"]=='y'){?>
<script src="/eyoom/theme/basic3/plugins/sticky-kit/sticky-kit.min.js"></script>
<?php }?>
<script src="/eyoom/theme/basic3/js/app.js"></script>
<script>
$(document).ready(function() {
    App.init();
});

<?php if($TPL_VAR["eyoom"]["use_shop_itemtype"]=='y'){?>
function item_wish_for_list(it_id) {
	var f = document.fitem_for_list;
	f.url.value = "<?php echo G5_SHOP_URL?>/wishupdate.php?it_id="+it_id;
	f.it_id.value = it_id;
	f.action = "<?php echo G5_SHOP_URL?>/wishupdate.php";
	f.submit();
}

// 쇼핑몰 퀵메뉴
$(document).ready(function() {
	$(".quick-menu-wrap").stick_in_parent({
<?php if($TPL_VAR["sticky"]=='yes'){?> // 메뉴바 sticky 적용

<?php if(defined('_INDEX_')){?> // 메뉴바 sticky 적용 메인일때
				offset_top:93
<?php }else{?> // 메뉴바 sticky 적용 메인이 아닐때
				offset_top:118
<?php }?>

<?php }else{?> // 메뉴바 sticky 미적용
<?php if(defined('_INDEX_')){?> // 메뉴바 sticky 미적용 메인일때
				offset_top:32
<?php }else{?> // 메뉴바 sticky 미적용 메인이 아닐때
				offset_top:58
<?php }?>
<?php }?>
	});
});
<?php }?>
</script>
<!--[if lt IE 9]>
	<script src="/eyoom/theme/basic3/plugins/respond.min.js"></script>
	<script src="/eyoom/theme/basic3/plugins/html5shiv.min.js"></script>
	<script src="/eyoom/theme/basic3/plugins/eyoom-form/js/eyoom-form-ie8.js"></script>
<![endif]-->

<?php $this->print_("tail_sub",$TPL_SCP,1);?>