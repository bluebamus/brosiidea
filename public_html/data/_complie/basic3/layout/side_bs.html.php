<?php /* Template_ 2.2.8 2018/01/24 01:17:10 /home1/bluebamus3/public_html/petmari/eyoom/theme/basic3/layout/side_bs.html 000004842 */  $this->include_("eb_outlogin","eb_poll","eb_popular","eb_tagmenu","eb_visit");
$TPL_sidemenu_1=empty($TPL_VAR["sidemenu"])||!is_array($TPL_VAR["sidemenu"])?0:count($TPL_VAR["sidemenu"]);?>
<?php if (!defined('_GNUBOARD_')) exit;
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/basic3/plugins/sly/tab_scroll_category.css" type="text/css" media="screen">',0);
?>

<aside class="basic-body-side <?php if($TPL_VAR["side_layout"]["pos"]=='left'){?>left<?php }else{?>right<?php }?>-side col-md-3">
	<div class="side-pc-area">
<?php if($TPL_VAR["eyoom"]["use_gnu_outlogin"]=='y'){?>
			<?php echo outlogin('basic')?>

<?php }else{?>
			<?php echo eb_outlogin($TPL_VAR["eyoom"]["outlogin_skin"])?>

<?php }?>

			<?php echo $TPL_VAR["latest"]->latest_eyoom('notice_roll','bo_table=notify||count=5||cut_subject=50')?>


<?php if(!defined('_INDEX_')){?>
			<ul class="sidebar-nav-e1 list-group" id="sidebar-nav">
<?php if($TPL_sidemenu_1){foreach($TPL_VAR["sidemenu"] as $TPL_K1=>$TPL_V1){
$TPL_submenu_2=empty($TPL_V1["submenu"])||!is_array($TPL_V1["submenu"])?0:count($TPL_V1["submenu"]);?>
			    <li class="list-group-item list-toggle <?php if($TPL_V1["active"]){?>active<?php }?>">
					<a <?php if(G5_IS_MOBILE&&$TPL_V1["submenu"]){?>data-toggle="collapse" data-parent="#sidebar-nav" href="#collapse-<?php echo $TPL_K1?>"<?php }else{?>href="<?php echo $TPL_V1["me_link"]?>" target="_<?php echo $TPL_V1["me_target"]?>"<?php }?>>
						<?php echo $TPL_V1["me_name"]?><?php if($TPL_V1["new"]){?><i class="fa fa-circle color-red margin-left-5"></i><?php }?>
					</a>
			        <ul id="collapse-<?php echo $TPL_K1?>" class="collapse <?php if($TPL_V1["active"]){?>in<?php }?>">
<?php if($TPL_submenu_2){foreach($TPL_V1["submenu"] as $TPL_V2){?>
			            <li class="<?php if($TPL_V2["active"]){?>active<?php }?>">
			            	<a href="<?php echo $TPL_V2["me_link"]?>" target="_<?php echo $TPL_V2["me_target"]?>">
				            	<?php echo $TPL_V2["me_name"]?>

<?php if($TPL_V2["new"]){?>
				            	<i class="fa fa-circle color-red margin-left-5"></i>
<?php }?>
			            	</a>
			            </li>
<?php }}?>
			        </ul>
			    </li>
<?php }}?>
			</ul>
<?php }?>

			<div class="sidebar-tab">
			    <div class="tab-bg tab-bg-dark">
			        <ul class="nav nav-tabs">
			            <li class="active"><a href="#side-tn-1" data-toggle="tab">새글</a></li>
			            <li class="last"><a href="#side-tn-2" data-toggle="tab">새댓글</a></li>
			        </ul>
			        <div class="tab-content">
			            <div class="tab-pane fade active in" id="side-tn-1">
			                <div class="tab-content-wrap">
			                    <?php echo $TPL_VAR["latest"]->latest_newpost('tab_newpost','count=5||cut_subject=30||photo=y')?>

			                </div>
			            </div>
			            <div class="tab-pane fade in" id="side-tn-2">
			                <div class="tab-content-wrap">
				                <?php echo $TPL_VAR["latest"]->latest_newpost('tab_newcomment','count=5||cut_subject=30||photo=y')?>

			                </div>
			            </div>
			        </div>
			    </div>
			</div>

<?php if($TPL_VAR["eyoom"]["use_gnu_poll"]=='y'){?>
			<?php echo poll('basic')?>

<?php }else{?>
			<?php echo eb_poll($TPL_VAR["eyoom"]["poll_skin"])?>

<?php }?>

			<?php echo $TPL_VAR["latest"]->latest_rankset('basic','10')?>


<?php if($TPL_VAR["eyoom"]["use_gnu_popular"]=='y'){?>
			<?php echo popular('basic')?>

<?php }else{?>
			<?php echo eb_popular($TPL_VAR["eyoom"]["popular_skin"], 10, 30)?>

<?php }?>

<?php if($TPL_VAR["eyoom"]["use_tag"]=='y'){?>
			<?php echo eb_tagmenu($TPL_VAR["eyoom"]["tag_skin"])?>

<?php }?>

<?php if($GLOBALS["is_admin"]){?>
<?php if($TPL_VAR["eyoom"]["use_gnu_visit"]=='y'){?>
			<?php echo visit('basic')?>

<?php }else{?>
			<?php echo eb_visit($TPL_VAR["eyoom"]["visit_skin"])?>

<?php }?>
<?php }?>
	</div>
</aside>

<script src="/eyoom/theme/basic3/plugins/sly/vendor_plugins.min.js"></script>
<script src="/eyoom/theme/basic3/plugins/sly/sly.min.js"></script>
<script>
$(function() {
	var $frame = $('#tab-page-category');
	var $wrap  = $frame.parent();
	$frame.sly({
		horizontal: 1,
		itemNav: 'centered',
		smart: 1,
		activateOn: 'click',
		mouseDragging: 1,
		touchDragging: 1,
		releaseSwing: 1,
		scrollBar: $wrap.find('.scrollbar'),
		scrollBy: 1,
		speed: 300,
		elasticBounds: 1,
		easing: 'easeOutExpo',
		dragHandle: 1,
		dynamicHandle: 1,
		clickBar: 1,
		prev: $wrap.find('.prev'),
		next: $wrap.find('.next')
	});
	var tabWidth = $('#tab-page-category').width();
	var categoryWidth = $('.page-category-list').width();
	if (tabWidth < categoryWidth) {
		$('.controls').show();
	}
});
</script>