<?php /* Template_ 2.2.8 2019/09/30 19:56:49 /home1/bluebamus2/public_html/eyoom/theme/basic3/layout/head_bs.html 000019147 */ 
$TPL_menu_1=empty($TPL_VAR["menu"])||!is_array($TPL_VAR["menu"])?0:count($TPL_VAR["menu"]);
$TPL__boxtoday_1=empty($GLOBALS["boxtoday"])||!is_array($GLOBALS["boxtoday"])?0:count($GLOBALS["boxtoday"]);
$TPL_sidemenu_1=empty($TPL_VAR["sidemenu"])||!is_array($TPL_VAR["sidemenu"])?0:count($TPL_VAR["sidemenu"]);?>
<?php if (!defined('_GNUBOARD_')) exit;
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/basic3/plugins/bootstrap/css/bootstrap.min.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/basic3/plugins/font-awesome/css/font-awesome.min.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/basic3/plugins/eyoom-form/css/eyoom-form.min.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/basic3/css/common.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/basic3/css/style.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/basic3/css/custom.css" type="text/css" media="screen">',0);
?>

<?php
// ------------- 테마 설정 옵션 시작 ------------- //
/**
 * 레이아웃 종류 : 'basic' || 'boxed'
 */
 $TPL_VAR["layout"] = 'basic';

/**
 * 로고 타입 : 'image' || 'text'
 */
 $TPL_VAR["logo"] = 'image';

/**
 * PC버전 메뉴바 sticky 적용 : 'yes' || 'no'
 */
 $TPL_VAR["sticky"] = 'yes';

/**
 * 상단 topbar의 쪽지, 내글반응 드롭다운 마우스온 작동 : 'yes' || 'no'
 */
 $TPL_VAR["dropdown_hover"] = 'no';

/**
 * Footer Top (하단 사이트 상세 정보) 출력 : 'yes' || 'no'
 */
 $TPL_VAR["footer_top"] = 'yes';

/**
 * 상품 이미지 미리보기 종류 : zoom or slider
 */
 $TPL_VAR["item_view"] = 'zoom';

// ------------- 테마 설정 옵션 끝 ------------- //
?>

<?php if(!$GLOBALS["wmode"]){?>
<div class="wrapper">
    <header class="header-topbar">
      <!--  <div class="topbar-left">
            <div class="header-logo">
	            <a href="<?php echo G5_URL?>" class="navbar-brand">
					&lt;!&ndash;<?php if($TPL_VAR["logo"]=='text'){?>&ndash;&gt;
					<?php echo $TPL_VAR["config"]["cf_title"]?>

					&lt;!&ndash;<?php }elseif($TPL_VAR["logo"]=='image'){?>&ndash;&gt;
					<img src="/eyoom/theme/basic3/image/site_logo.png" class="img-responsive" alt="<?php echo $TPL_VAR["config"]["cf_title"]?> LOGO">
					&lt;!&ndash;<?php }?>&ndash;&gt;
				</a>
            </div>
        </div>-->

<?php if(!G5_IS_MOBILE){?>
        <div class="topbar-center hidden-xs hidden-sm">

        </div>
<?php }?>

        <ul class="topbar-right list-unstyled">
	        <div class="member-menu">
<?php if($TPL_VAR["eyoom"]["is_shop_theme"]=='y'){?>
<?php if(defined('_SHOP_')&&$TPL_VAR["eyoom"]["use_layout_community"]=='y'){?>
		    	<!--<li><a href="<?php echo G5_URL?>"><i class="fa fa-shopping-cart margin-right-5"></i>커뮤니티</a></li>-->
<?php }else{?>
				<!--<li><a href="<?php echo G5_SHOP_URL?>"><i class="fa fa-shopping-cart margin-right-5"></i>쇼핑몰</a></li>-->
<?php }?>
<?php }?>

<?php if($GLOBALS["is_member"]){?>
<?php if($GLOBALS["is_admin"]){?>
				<li><a href="<?php echo G5_ADMIN_URL?>"><i class="fa fa-cog margin-right-5"></i>관리자</a></li>
<?php }?>
	            <li><a href="<?php echo G5_BBS_URL?>/logout.php"><i class="fa fa-sign-out margin-right-5"></i>로그아웃</a></li>
	            <li><a href="<?php echo G5_URL?>/mypage/"><i class="fa fa-user-circle-o margin-right-5"></i>마이페이지</a></li>

<?php }else{?>
	        <!--    <li><a href="<?php echo G5_BBS_URL?>/register.php"><i class="fa fa-user-plus margin-right-5"></i>회원가입</a></li>
	            <li><a href="<?php echo G5_BBS_URL?>/login.php"><i class="fa fa-unlock-alt margin-right-5"></i>로그인</a></li> -->
<?php }?>
	            <li class="topbar-add-menu">
	            	<!--<a href="#" data-toggle="dropdown" data-hover="dropdown"><i class="fa fa-plus-circle margin-right-5"></i>추가메뉴</a>-->
	            	<ul class="dropdown-menu" role="menu">
		            	<!--<li><a href="<?php echo G5_BBS_URL?>/new.php">새글</a></li>-->
		            	<!--<li><a href="<?php echo G5_BBS_URL?>/faq.php">FAQ</a></li>-->
		            	<!--<li><a href="<?php echo G5_BBS_URL?>/qalist.php">1:1문의</a></li>-->
<?php if($GLOBALS["is_member"]){?>
		            	<li><a href="<?php echo G5_BBS_URL?>/member_confirm.php?url=register_form.php">회원정보수정</a></li>
<?php }?>
		            </ul>
	            </li>
			</div>
            <li class="mobile-nav-trigger">
	            <a href="#" class="sidebar-left-trigger" data-action="toggle" data-side="left">
	                <span class="sr-only">Toggle navigation</span>
	                <span class="fa fa-search font-size-25 color-white margin-right-10"></span>
	                <span class="fa fa-bars font-size-25 color-white"></span>
	            </a>
            </li>
        </ul>
    </header>

<?php if($TPL_VAR["layout"]=='basic'){?>
	<div <?php if($TPL_VAR["sticky"]=='yes'){?>id="header-fixed"<?php }?> class="basic-layout <?php if(defined('_INDEX_')){?>fixed-main<?php }?>">
<?php }elseif($TPL_VAR["layout"]=='boxed'){?>
	<div <?php if($TPL_VAR["sticky"]=='yes'){?>id="header-fixed"<?php }?> class="boxed-layout container <?php if(defined('_INDEX_')){?>fixed-main<?php }?>">
<?php }?>
	    <div class="header-title">
		    <div class="header-title-in">
		        <div class="container">
			        <a href="<?php echo G5_URL?>">
<?php if($TPL_VAR["logo"]=='text'){?>
						<h1><?php echo $TPL_VAR["config"]["cf_title"]?></h1>
<?php }elseif($TPL_VAR["logo"]=='image'){?>
						<img src="/eyoom/theme/basic3/image/site_logo.png" class="title-image-center" alt="<?php echo $TPL_VAR["config"]["cf_title"]?> LOGO">
<?php }?>
					</a>
		        </div>
		    </div>
	    </div>

	    <div class="header-nav header-sticky">
	        <div class="navbar" role="navigation">
	            <div class="container">
<?php if(G5_IS_MOBILE){?>
			        <div class="nav-center hidden-md hidden-lg">
						<?php echo $TPL_VAR["latest"]->latest_eyoom('notice_roll','bo_table=게시판id||count=5||cut_subject=50')?>

			        </div>
<?php }?>

		            <nav class="sidebar left">
			            <div class="sidebar-left-content ">
				            <div class="sidebar-member-menu">
								<!--<form name="fsearchbox" method="get" action="<?php echo G5_BBS_URL?>/search.php" onsubmit="return fsearchbox_submit(this);" class="eyoom-form margin-bottom-20">
								<input type="hidden" name="sfl" value="wr_subject||wr_content">
								<input type="hidden" name="sop" value="and">
								<label for="side_sch_stx" class="sound_only">검색어<strong class="sound_only"> 필수</strong></label>
								<div class="input input-button">
									<input type="text" name="stx" id="side_sch_stx" class="sch_stx" maxlength="20" placeholder="전체검색">
									<div class="button"><input type="submit">검색</div>
								</div>
								</form>-->
<?php if($TPL_VAR["eyoom"]["is_shop_theme"]=='y'){?>
					            <!--<a href="<?php echo G5_SHOP_URL?>" class="sidebar-lg-btn btn-e btn-e-lg btn-e-red btn-e-block">
						            <i class="fa fa-shopping-cart margin-right-5"></i>쇼핑몰 메인
						        </a>-->
<?php }?>
<?php if($GLOBALS["is_admin"]){?>
					            <a href="<?php echo G5_ADMIN_URL?>" class="sidebar-lg-btn btn-e btn-e-lg btn-e-yellow btn-e-block">
						            <i class="fa fa-cogs margin-right-5"></i>관리자 페이지
						        </a>
<?php }?>
<?php if($GLOBALS["is_member"]){?>
					            <a href="<?php echo G5_URL?>/mypage/" class="sidebar-member-btn-box">
					            	<div class="sidebar-other-btn">
						            	<i class="fa fa-user-circle-o margin-right-5"></i>마이페이지
						            </div>
					            </a>
					            <a <?php if(!G5_IS_MOBILE){?>href="javascript:void(0);" onclick="scrap_modal();"<?php }else{?>href="<?php echo G5_BBS_URL?>/scrap.php" target="_blank"<?php }?> class="sidebar-member-btn-box">
					            	<div class="sidebar-other-btn pull-right">
						            	<i class="fa fa-clipboard margin-right-5"></i>스크랩
						            </div>
					            </a>
					            <div class="margin-bottom-10"></div>
					            <!--<a <?php if(!G5_IS_MOBILE){?>href="javascript:void(0);" onclick="memo_modal();"<?php }else{?>href="<?php echo G5_BBS_URL?>/memo.php" target="_blank"<?php }?> class="sidebar-member-btn-box">
					            	<div class="sidebar-other-btn">
						            	쪽지&lt;!&ndash;<?php if($GLOBALS["memo_not_read"]){?>&ndash;&gt;<span class="badge rounded-3x badge-e badge-red margin-left-5"><?php echo $GLOBALS["memo_not_read"]?></span>&lt;!&ndash;<?php }?>&ndash;&gt;
						            </div>
					            </a>-->
					            <!--<a href="<?php echo G5_BBS_URL?>/respond.php" class="sidebar-member-btn-box">
					            	<div class="sidebar-other-btn pull-right">
						            	내글반응&lt;!&ndash;<?php if($TPL_VAR["respond"]){?>&ndash;&gt;<span class="badge rounded-3x badge-e badge-red margin-left-5"><?php echo $TPL_VAR["respond"]?></span>&lt;!&ndash;<?php }?>&ndash;&gt;
						            </div>
					            </a>-->
					            <div class="margin-bottom-10"></div>
					            <a href="<?php echo G5_BBS_URL?>/member_confirm.php?url=register_form.php" class="sidebar-member-btn-box">
					            	<div class="sidebar-member-btn"><i class="fa fa-info-circle margin-right-5"></i>정보수정</div>
					            </a>
					            <a href="<?php echo G5_BBS_URL?>/logout.php" class="sidebar-member-btn-box">
					            	<div class="sidebar-member-btn pull-right"><i class="fa fa-sign-out margin-right-5"></i>로그아웃</div>
					            </a>
<?php }else{?>
								<a href="<?php echo G5_BBS_URL?>/register.php" class="sidebar-member-btn-box">
					            	<div class="sidebar-member-btn"><i class="fa fa-user-plus margin-right-5"></i>회원가입</div>
					            </a>
					            <a href="<?php echo G5_BBS_URL?>/login.php" class="sidebar-member-btn-box">
					            	<div class="sidebar-member-btn pull-right"><i class="fa fa-unlock-alt margin-right-5"></i>로그인</div>
					            </a>
<?php }?>
								<div class="clearfix"></div>
							</div>

		                    <ul class="nav navbar-nav">
	                            <li>
	                                <a href="<?php echo G5_URL?>"><i class="fa fa-navicon nav-icon-pre hidden-xs hidden-sm"></i>HOME</a>
	                            </li>
	                            <!--<li class="nav-li-divider">|</li>-->
<?php if($TPL_menu_1){foreach($TPL_VAR["menu"] as $TPL_V1){
$TPL_submenu_2=empty($TPL_V1["submenu"])||!is_array($TPL_V1["submenu"])?0:count($TPL_V1["submenu"]);?>
		                        <li class="<?php if($TPL_V1["active"]){?>active<?php }?> <?php if($TPL_V1["submenu"]){?>dropdown<?php }?>">
		                            <a href="<?php echo $TPL_V1["me_link"]?>" target="_<?php echo $TPL_V1["me_target"]?>" class="dropdown-toggle" <?php if(G5_IS_MOBILE&&$TPL_V1["submenu"]){?>data-toggle="dropdown"<?php }else{?>data-hover="dropdown"<?php }?>>
<?php if($TPL_V1["me_icon"]){?><i class="fa <?php echo $TPL_V1["me_icon"]?>"></i> <?php }?><?php echo $TPL_V1["me_name"]?>

		                            </a>
<?php if($TPL_submenu_2){$TPL_I2=-1;foreach($TPL_V1["submenu"] as $TPL_V2){$TPL_I2++;
$TPL_subsub_3=empty($TPL_V2["subsub"])||!is_array($TPL_V2["subsub"])?0:count($TPL_V2["subsub"]);?>
<?php if($TPL_I2== 0){?>
		                            <ul class="dropdown-menu">
<?php }?>
		                                <li class="dropdown-submenu <?php if($TPL_V2["active"]){?>active<?php }?>">
		                                    <a href="<?php echo $TPL_V2["me_link"]?>" target="_<?php echo $TPL_V2["me_target"]?>"><?php if($TPL_V2["me_icon"]){?><i class="fa <?php echo $TPL_V2["me_icon"]?>"></i> <?php }?><?php echo $TPL_V2["me_name"]?><?php if($TPL_V2["new"]){?>&nbsp;<i class="fa fa-check-circle color-red margin-left-5"></i><?php }?><?php if($TPL_V2["sub"]=='on'){?><i class="fa fa-angle-right sub-caret hidden-sm hidden-xs"></i><i class="fa fa-angle-down sub-caret hidden-md hidden-lg"></i><?php }?></a>
<?php if($TPL_subsub_3){$TPL_I3=-1;foreach($TPL_V2["subsub"] as $TPL_V3){$TPL_I3++;?>
<?php if($TPL_I3== 0){?>
		                                    <ul class="dropdown-menu">
<?php }?>
		                                        <li class="dropdown-submenu <?php if($TPL_V3["active"]){?>active<?php }?>">
		                                            <a href="<?php echo $TPL_V3["me_link"]?>" target="_<?php echo $TPL_V3["me_target"]?>"><?php if($TPL_V3["me_icon"]){?><i class="fa <?php echo $TPL_V3["me_icon"]?>"></i> <?php }?><?php echo $TPL_V3["me_name"]?><?php if($TPL_V3["new"]){?>&nbsp;<i class="fa fa-check-circle color-red margin-left-5"></i><?php }?><?php if($TPL_V3["sub"]=='on'){?><i class="fa fa-angle-right sub-caret hidden-sm hidden-xs"></i><i class="fa fa-angle-down sub-caret hidden-md hidden-lg"></i><?php }?></a>
		                                        </li>
<?php if($TPL_I3==$TPL_subsub_3- 1){?>
		                                    </ul>
<?php }?>
<?php }}?>
		                                </li>
<?php if($TPL_I2==$TPL_submenu_2- 1){?>
		                            </ul>
<?php }?>
<?php }}?>
		                        </li>
<?php }}?>
	                            <!--<li class="hidden-xs hidden-sm"><a href="#" data-toggle="modal" data-target=".all-search-modal"><i class="fa fa-search"></i></a></li>-->

	                            <!--<li class="visible-xs visible-sm nav-li-space"></li>
				            	<li class="visible-xs visible-sm"><a href="<?php echo G5_BBS_URL?>/new.php">새글</a></li>
				            	<li class="visible-xs visible-sm"><a href="<?php echo G5_BBS_URL?>/faq.php">FAQ</a></li>
				            	<li class="visible-xs visible-sm"><a href="<?php echo G5_BBS_URL?>/qalist.php">1:1문의</a></li>-->
		                    </ul>
			            </div>
		            </nav>
	            </div>
	        </div>
	    </div>

	    <div class="header-sticky-space"></div>

<?php if(!defined('_INDEX_')){?>
		<div class="page-title-wrap">
			<div class="container">
<?php if(!defined('_EYOOM_MYPAGE_')){?>
			    <h2 class="pull-left">
<?php if(!$GLOBALS["it_id"]){?>
				    <i class="fa fa-map-marker"></i> <?php echo $TPL_VAR["subinfo"]["title"]?>

<?php }else{?>
				    <span class="font-size-14"><i class="fa fa-map-marker"></i> <?php echo $TPL_VAR["subinfo"]["title"]?></span>
<?php }?>
				</h2>
<?php if(!$GLOBALS["it_id"]){?>
	            <ul class="breadcrumb pull-right hidden-xs">
					<?php echo $TPL_VAR["subinfo"]["path"]?>

	            </ul>
<?php }?>
	            <div class="clearfix"></div>
<?php }else{?>
				<h2><i class="fa fa-map-marker"></i> 마이페이지</h2>
<?php }?>
		    </div>
		</div>
<?php }?>

	    <div class="basic-body container <?php if(!defined('_INDEX_')){?>page-padding <?php if($TPL_VAR["footer_top"]=='yes'){?>ft-padding<?php }?><?php }?>">
<?php if($TPL_VAR["eyoom"]["use_shop_itemtype"]=='y'){?>
			<div class="quick-menu-wrap">
				<div class="quick-menu <?php if(defined('_INDEX_')){?>quick-menu-main<?php }?>">
					<div class="quick-menu-box">
						<a href="<?php echo G5_SHOP_URL?>/cart.php">
							<i class="fa fa-shopping-basket"></i>
							<span>장바구니</span>
						</a>
					</div>
					<div class="quick-menu-box">
						<a href="<?php echo G5_SHOP_URL?>/wishlist.php">
							<i class="fa fa-heart"></i>
							<span>위시리스트</span>
						</a>
					</div>
					<div class="quick-menu-box">
						<a href="<?php echo G5_SHOP_URL?>/orderinquiry.php">
							<i class="fa fa-truck"></i>
							<span>주문/배송</span>
						</a>
					</div>
					<div class="quick-menu-box">
						<a href="<?php echo G5_BBS_URL?>/faq.php">
							<i class="fa fa-question-circle"></i>
							<span>FAQ</span>
						</a>
					</div>
					<div class="quick-menu-box heading-current">
						<span><strong>최근본상품</strong></span>
					</div>
					<div class="quick-menu-box current-view">
						<div class="quick-carousel">
							<div id="quickCarousel" class="carousel slide carousel-e1 item-carousel">
								<div class="carousel-inner">
<?php if($TPL__boxtoday_1){$TPL_I1=-1;foreach($GLOBALS["boxtoday"] as $TPL_V1){$TPL_I1++;?>
									<div class="item <?php if($TPL_I1== 0){?>active<?php }?>">
										<?php echo $TPL_V1["img"]?>

									</div>
<?php }}else{?>
									<div class="item active"><p>최근 본 상품이 없습니다.</p></div>
<?php }?>
								</div>
								<div class="carousel-arrow">
									<a class="left carousel-control pull-left" href="#quickCarousel" data-slide="prev">이전</a>
									<a class="right carousel-control pull-right" href="#quickCarousel" data-slide="next">다음</a>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="quick-scroll-btn top-btn">
						<i class="fa fa-caret-up"></i>
						<span>TOP</span>
					</div>
					<div class="quick-scroll-btn down-btn">
						<span>DOWN</span>
						<i class="fa fa-caret-down"></i>
					</div>
				</div>
			</div>
<?php }?>

<?php if(!defined('_INDEX_')){?>
		    <div class="basic-body-page">
<?php if($TPL_VAR["side_layout"]["use"]=='yes'){?>
			    <div class="category-mobile-area">
<?php if($TPL_VAR["sidemenu"]){?>
				    <div class="tab-scroll-page-category">
						<div class="scrollbar">
							<div class="handle">
								<div class="mousearea"></div>
							</div>
						</div>
						<div id="tab-page-category">
							<div class="page-category-list">
<?php if($TPL_sidemenu_1){foreach($TPL_VAR["sidemenu"] as $TPL_V1){?>
						        <span <?php if($TPL_V1["active"]){?>class="active"<?php }?>><a href="<?php echo $TPL_V1["me_link"]?>" target="_<?php echo $TPL_V1["me_target"]?>"><?php echo $TPL_V1["me_name"]?></a></span>
<?php }}?>
						        <span class="fake-span"></span>
							</div>
							<div class="controls">
								<button class="btn prev"><i class="fa fa-caret-left"></i></button>
								<button class="btn next"><i class="fa fa-caret-right"></i></button>
							</div>
						</div>
						<div class="tab-page-category-divider"></div>
					</div>
<?php }?>
			    </div>
<?php }?>
<?php }?>
	    	<div class="row">
<?php if($TPL_VAR["side_layout"]["use"]=='yes'){?>
<?php if($TPL_VAR["side_layout"]["pos"]=='left'){?>
<?php $this->print_("side_bs",$TPL_SCP,1);?>

<?php }?>
				<section class="basic-body-main <?php if(!defined('_INDEX_')){?>page-padding<?php }else{?><?php if($TPL_VAR["footer_top"]=='yes'){?>ft-padding<?php }?><?php }?> <?php if($TPL_VAR["side_layout"]["pos"]=='left'){?>right<?php }else{?>left<?php }?>-main col-md-9">
<?php }else{?>
				<section class="basic-body-main col-md-12 <?php if($TPL_VAR["footer_top"]=='yes'){?>ft-padding<?php }?>">
<?php }?>
<?php }?>