<?php /* Template_ 2.2.8 2018/01/24 01:16:23 /home1/bluebamus3/public_html/petmari/eyoom/theme/basic3/skin_bs/visit/basic/visit.skin.html 000001973 */ ?>
<?php if (!defined('_GNUBOARD_')) exit; ?>

<style>
.statistics-wrap {position:relative;border:1px solid #e5e5e5;padding:10px;margin:0px 20px 30px;background:#fff;-webkit-border-radius:3px !important;-moz-border-radius:3px !important;border-radius:3px !important}
.statistics-wrap ul {margin-bottom:0}
.statistics-list li {color:#474A5E;font-size:12px;padding:6px 0;display:block;border-top:1px dotted #e5e5e5}
.statistics-list li:first-child {border-top:none}
.statistics-list li a {color:#000;font-size:12px;display:block}
.statistics-list li strong {color:#474A5E;float:right}
</style>

<div class="statistics-wrap">
	<div class="headline"><h5><strong>사이트 통계</strong></h5></div>
	<ul class="list-unstyled statistics-list">
		<li><a href="<?php echo G5_BBS_URL?>/current_connect.php">현재접속자 : <strong><?php echo $TPL_VAR["connect"]["total_cnt"]?><?php if($TPL_VAR["connect"]["mb_cnt"]){?> (<span class='color-red'>Member <?php echo $TPL_VAR["connect"]["mb_cnt"]?></span>)<?php }?></strong></a></li>
		<li>오늘방문자 : <strong><?php echo $TPL_VAR["counter"]["visit_today"]?></strong></li>
		<li>어제방문자 : <strong><?php echo $TPL_VAR["counter"]["visit_yesterday"]?></strong></li>
		<li>최대방문자 : <strong><?php echo $TPL_VAR["counter"]["visit_max"]?></strong></li>
		<li>전체방문자 : <strong><?php echo $TPL_VAR["counter"]["visit_total"]?></strong></li>
<?php if( 0){?>
		<li>신규회원수 : <strong><?php echo $TPL_VAR["counter"]["newby"]?></strong></li>
<?php }?>
		<li>전체회원수 : <strong><?php echo $TPL_VAR["counter"]["members"]?></strong></li>
		<li>전체게시물 : <strong><?php echo $TPL_VAR["counter"]["write"]?></strong></li>
<?php if( 0){?>
		<li>전체코멘트 : <strong><?php echo $TPL_VAR["counter"]["comment"]?></strong></li>
<?php }?>
	</ul>
</div>