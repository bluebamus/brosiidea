<?php /* Template_ 2.2.8 2018/01/24 01:16:22 /home1/bluebamus3/public_html/petmari/eyoom/theme/basic3/skin_bs/latest/gallery/latest.skin.html 000005678 */ 
$TPL_loop_1=empty($TPL_VAR["loop"])||!is_array($TPL_VAR["loop"])?0:count($TPL_VAR["loop"]);?>
<?php if (!defined('_GNUBOARD_')) exit; ?>

<style>
.gallery-latest {position:relative;margin-left:-10px;margin-right:-10px}
.gallery-item {position:relative;width:25%;padding-left:10px;padding-right:10px;float:left}
.gallery-latest .gallery-img {position:relative;overflow:hidden;margin-bottom:10px}
.gallery-latest .img-box {position:relative;overflow:hidden;height:94px;background:#353535;line-height:94px;text-align:center}
.gallery-latest .img-box .no-image {color:#858585;font-size:11px}
.gallery-latest .img-comment {position:absolute;top:8px;left:8px;display:inline-block;min-width:35px;padding:0px 3px;font-size:10px;font-weight:300;line-height:13px;color:#fff;text-align:center;white-space:nowrap;vertical-align:baseline;background:#757575}
.gallery-latest .img-box .video-icon {position:absolute;top:5px;right:5px;color:#fff;font-size:20px;line-height:20px}
.gallery-latest .img-caption {color:#fff;font-size:11px;position:absolute;left:0;bottom:-26px;display:block;z-index:1;background:rgba(0, 0, 0, 0.7);text-align:left;width:100%;height:26px;line-height:20px;margin-bottom:0;padding:3px 10px}
.gallery-latest .img-caption span {margin-right:7px}
.gallery-latest .img-caption span i {color:#a5a5a5}
.gallery-latest .gallery-txt {position:relative;margin-bottom:20px}
.gallery-latest .txt-subj {margin-bottom:5px}
.gallery-latest .txt-subj h5 {font-size:13px;font-weight:bold;margin:0}
.gallery-latest .gallery-txt a:hover .txt-subj h5 {color:#005cff;text-decoration:underline}
.gallery-latest .txt-cont {position:relative;overflow:hidden;height:34px;font-size:11px;color:#858585;margin-bottom:10px}
.gallery-latest .txt-photo {display:inline-block;width:26px;height:26px;margin-right:2px;border:1px solid #e5e5e5;padding:1px;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;-webkit-border-radius:50% !important;-moz-border-radius:50% !important;border-radius:50% !important}
.gallery-latest .txt-photo img {width:100%;height:auto;-webkit-border-radius:50% !important;-moz-border-radius:50% !important;border-radius:50% !important}
.gallery-latest .txt-photo .txt-user-icon {width:22px;height:22px;font-size:14px;line-height:22px;text-align:center;background:#959595;color:#fff;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;display:inline-block;white-space:nowrap;vertical-align:baseline;-webkit-border-radius:50% !important;-moz-border-radius:50% !important;border-radius:50% !important}
.gallery-latest .txt-nick {font-size:11px;color:#555555}
.gallery-latest .txt-time {font-size:11px;color:#555555;margin-left:5px}
.gallery-latest .txt-time .i-color {color:#b5b5b5}
@media (max-width:660px) {
	.gallery-item {width:33.33333%}
}
@media (max-width:500px) {
	.gallery-item {width:50%}
}
</style>

<div class="headline">
	<h5><strong><?php echo $TPL_VAR["title"]?></strong></h5>
</div>

<div class="gallery-latest">
	<div class="gallery-latest-in">
<?php if($TPL_loop_1){foreach($TPL_VAR["loop"] as $TPL_V1){?>
		<div class="gallery-item">
			<div class="gallery-img">
				<a href="<?php echo $TPL_V1["href"]?>">
					<div class="img-box">
<?php if($TPL_V1["image"]){?>
						<img class="img-responsive" src="<?php echo $TPL_V1["image"]?>">
<?php if($TPL_V1["wr_comment"]){?><span class="img-comment">+<?php echo number_format($TPL_V1["wr_comment"])?></span><?php }?>
<?php if($TPL_V1["is_video"]){?><span class="video-icon"><i class="fa fa-play-circle-o"></i></span><?php }?>
<?php }else{?>
						<span class="no-image">No Image</span>
<?php }?>
						<div class="img-caption">
							<span><i class="fa fa-eye"></i> <?php echo number_format($TPL_V1["wr_hit"])?></span>
<?php if($TPL_V1["wr_good"]){?><span><i class="fa fa-thumbs-up"></i> <?php echo number_format($TPL_V1["wr_good"])?></span><?php }?>
<?php if($TPL_V1["wr_nogood"]){?><span><i class="fa fa-thumbs-down"></i> <?php echo number_format($TPL_V1["wr_nogood"])?></span><?php }?>
						</div>
					</div>
				</a>
			</div>
			<div class="gallery-txt">
				<a href="<?php echo $TPL_V1["href"]?>">
					<div class="txt-subj">
						<h5 class="ellipsis"><?php echo $TPL_V1["wr_subject"]?></h5>
					</div>
<?php if($TPL_VAR["content"]=='y'){?>
					<p class="txt-cont"><?php echo $TPL_V1["wr_content"]?></p>
<?php }?>
<?php if($TPL_VAR["photo"]=='y'){?>
					<span class="txt-photo">
<?php if($TPL_V1["mb_photo"]){?>
						<?php echo $TPL_V1["mb_photo"]?>

<?php }else{?>
						<span class="txt-user-icon"><i class="fa fa-user"></i></span>
<?php }?>
					</span>
<?php }?>
					<span class="txt-nick"><?php echo $TPL_V1["mb_nick"]?></span>
					<span class="txt-time"><i class="fa fa-clock-o <?php if($TPL_V1["new"]){?>color-red<?php }else{?>i-color<?php }?>"></i> <?php echo $TPL_VAR["eb"]->date_time('Y-m-d',$TPL_V1["datetime"])?></span>
				</a>
			</div>
		</div>
<?php }}else{?>
		<p class="text-center color-grey font-size-13 margin-top-30"><i class="fa fa-exclamation-circle"></i> 최신글이 없습니다.</p>
<?php }?>
		<div class="clearfix"></div>
	</div>
</div>

<script>
$(function(){
    var duration = 120;
    var $img_cap = $('.gallery-latest .gallery-img');
    $img_cap.find('.img-box')
        .on('mouseover', function(){
            $(this).find('.img-caption').stop(true).animate({bottom: '0px'}, duration);
        })
        .on('mouseout', function(){
            $(this).find('.img-caption').stop(true).animate({bottom: '-26px'}, duration);
        });
});
</script>