<?php /* Template_ 2.2.8 2018/11/26 21:04:52 /home1/bluebamus2/public_html/eyoom/theme/basic3/skin_bs/latest/notice_roll/latest.skin.html 000002175 */ 
$TPL_loop_1=empty($TPL_VAR["loop"])||!is_array($TPL_VAR["loop"])?0:count($TPL_VAR["loop"]);?>
<?php if (!defined('_GNUBOARD_')) exit; ?>

<style>
.notice-roll {position:relative;overflow:hidden;height:35px;border:1px solid #d5d5d5;background:#fff;margin:0 20px 30px;padding:0 10px;box-sizing:content-box;border-radius:2px !important}
.notice-roll ul {position:absolute;width:100%;list-style:none;margin:0;padding:0}
.notice-roll ul li {position:relative;height:35px;padding-right:20px;box-sizing:content-box}
.notice-roll ul li a {line-height:36px;font-size:12px;margin-left:43px}
.notice-roll ul li span {line-height:36px;font-size:12px;margin-left:43px}
.notice-roll .label {position:absolute;top:9px;left;10px}
</style>

<div class="notice-roll">
	<span class="label label-purple">공지</span>
	<ul>
<?php if($TPL_loop_1){foreach($TPL_VAR["loop"] as $TPL_V1){?>
		<li><a href="<?php echo $TPL_V1["href"]?>" class="ellipsis"><?php echo $TPL_V1["wr_subject"]?></a></li>
<?php }}else{?>
		<li><span class="color-grey ellipsis"><i class="fa fa-exclamation-circle"></i> 출력할 최신글이 없습니다.</span></li>
<?php }?>
	</ul>
</div>

<script>
$(function() {
	var noticeRollUl = $('.notice-roll ul'),
		noticeRollLi = noticeRollUl.append(noticeRollUl.html()).children(),
		noticeRollHeight = $('.notice-roll').height() * -1,
		scrollSpeed = 600,
		timer,
		speed = 3000 + scrollSpeed;

	if (noticeRollLi.length > 2) {
		function sliderText() {
			var noticeRollFoucs = noticeRollUl.position().top / noticeRollHeight;

			noticeRollFoucs = (noticeRollFoucs + 1) % noticeRollLi.length;
			noticeRollUl.animate({
				top: noticeRollFoucs * noticeRollHeight
			}, scrollSpeed, function() {
				if (noticeRollFoucs == noticeRollLi.length / 2) {
					noticeRollUl.css('top', 0);
				}
			});
			timer = setTimeout(sliderText, speed);
		}

		noticeRollLi.hover(function() {
			clearTimeout(timer);
		}, function() {
			timer = setTimeout(sliderText, speed);
		});

		timer = setTimeout(sliderText, speed);
	}
});
</script>