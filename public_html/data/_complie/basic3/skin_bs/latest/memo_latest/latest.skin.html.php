<?php /* Template_ 2.2.8 2018/01/24 01:16:22 /home1/bluebamus3/public_html/petmari/eyoom/theme/basic3/skin_bs/latest/memo_latest/latest.skin.html 000005967 */ 
$TPL_loop_1=empty($TPL_VAR["loop"])||!is_array($TPL_VAR["loop"])?0:count($TPL_VAR["loop"]);?>
<?php if (!defined('_GNUBOARD_')) exit;
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/basic3/plugins/perfect-scrollbar/perfect-scrollbar.min.css" type="text/css" media="screen">',0);
?>

<style>
li.dropdown-memo > .dropdown-menu .memo-list {padding:0 15px 10px}
li.dropdown-memo > a {cursor:pointer}
li.dropdown-memo > a > span.badge {font-family:'Helvetica Neue', sans-serif}
li.dropdown-memo > .dropdown-menu .dropdown-menu-list > li {position:relative;overflow:hidden;min-width:246px;padding:0}
li.dropdown-memo > .dropdown-menu .dropdown-menu-list > li a {border-top:1px dotted #e5e5e5}
li.dropdown-memo > .dropdown-menu .dropdown-menu-list > li.read-disable a {color:#b5b5b5 !important}
li.dropdown-memo > .dropdown-menu .dropdown-menu-list > li.read-disable span {color:#b5b5b5 !important}
li.dropdown-memo > .dropdown-menu .dropdown-menu-list > li:first-child a {border-top:0}
li.dropdown-memo > .dropdown-menu .dropdown-menu-list > li .photo {float:left;margin:0 10px 10px 0}
li.dropdown-memo > .dropdown-menu .dropdown-menu-list > li .photo img {height:28px;width:28px;-webkit-border-radius:50% !important;-moz-border-radius:50% !important;-ms-border-radius:50% !important;-o-border-radius:50% !important;border-radius:50% !important}
li.dropdown-memo > .dropdown-menu .dropdown-menu-list > li .photo .memo-photo {height:28px;width:28px;font-size:16px;line-height:28px;text-align:center;background:#858585;color:#fff;-webkit-border-radius:50% !important;-moz-border-radius:50% !important;-ms-border-radius:50% !important;-o-border-radius:50% !important;border-radius:50% !important}
li.dropdown-memo > .dropdown-menu .dropdown-menu-list > li .description {display:block;margin-left:38px}
li.dropdown-memo > .dropdown-menu .dropdown-menu-list > li .description .from {font-size:12px;font-weight:600}
li.dropdown-memo > .dropdown-menu .dropdown-menu-list > li .subject {display:block !important;font-size:12px;line-height:1.3;margin-left:38px;margin-top:5px;color:#000}
li.dropdown-memo > .dropdown-menu .dropdown-menu-list > li a:hover .subject {text-decoration:underline;color:#005cff}
li.dropdown-memo > .dropdown-menu .dropdown-menu-list > li a .time {display:block !important;font-size:10px;color:#999;float:right;margin-top:5px;margin-right:7px}
li.dropdown-memo .ps-container .ps-scrollbar-y-rail {width:4px;-webkit-border-radius:4px !important;-moz-border-radius:4px !important;border-radius:4px !important;background-color:#c5c5c5}
li.dropdown-memo .ps-container .ps-scrollbar-y {width:4px;-webkit-border-radius:4px !important;-moz-border-radius:4px !important;border-radius:4px !important;background-color:#151515;margin-right:-2px}
li.dropdown-memo .ps-container>.ps-scrollbar-y-rail:hover>.ps-scrollbar-y, li.dropdown-memo .ps-container>.ps-scrollbar-y-rail:active>.ps-scrollbar-y {width:4px}
li.dropdown-memo .ps-container:hover.ps-in-scrolling.ps-y>.ps-scrollbar-y-rail>.ps-scrollbar-y {background-color:#000;width:4px}
li.dropdown-memo .shadow-spacer {width:276px;position:relative;margin-left:-15px;margin-top:5px}
li.dropdown-memo .shadow-spacer .shadow-mask {overflow:hidden;height:15px}
li.dropdown-memo .shadow-spacer .shadow-mask:after {content:'';display:block;margin:-20px auto 0;width:100%;height:20px;border-radius:125px / 12px !important;box-shadow:0 0 8px black}
</style>

<li class="dropdown dropdown-extended dropdown-memo">
	<a class="dropdown-toggle" data-toggle="dropdown" <?php if($TPL_VAR["dropdown_hover"]=='yes'){?>data-hover="dropdown"<?php }?> data-target="#">
		쪽지
<?php if($GLOBALS["memo_not_read"]){?>
		<span class="badge rounded-3x badge-e badge-red"><?php echo $GLOBALS["memo_not_read"]?></span>
<?php }?>
	</a>
	<ul class="dropdown-menu">
		<li class="external">
			<h5>미확인 <strong class="color-red"><?php echo $GLOBALS["memo_not_read"]?>건</strong> 쪽지가 있습니다.</h5>
			<a <?php if(!G5_IS_MOBILE){?>href="javascript:void(0);" onclick="memo_modal();"<?php }else{?>href="<?php echo G5_BBS_URL?>/memo.php" target="_blank"<?php }?>>more</a>
		</li>
		<li class="memo-list">
			<div class="shadow-spacer">
				<div class="shadow-mask"></div>
			</div>
			<ul class="dropdown-menu-list scroll-box-cml">
<?php if($TPL_loop_1){foreach($TPL_VAR["loop"] as $TPL_V1){?>
<?php if($TPL_V1["is_read"]){?>
				<li class="read-disable">
<?php }else{?>
				<li>
<?php }?>
					<a <?php if(!G5_IS_MOBILE){?>href="<?php echo $TPL_V1["href"]?>" onclick="memo_view_modal(this.href); return false;"<?php }else{?>href="<?php echo $TPL_V1["href"]?>" target="_blank" class="win_memo"<?php }?>>
<?php if($TPL_VAR["photo"]=='y'){?>
						<span class="photo">
<?php if($TPL_V1["mb_photo"]){?><?php echo $TPL_V1["mb_photo"]?><?php }else{?><span class="user_icon"><i class="fa fa-user memo-photo"></i></span><?php }?>
						</span>
<?php }?>
						<span class="description">
							<span class="from color-black"><?php echo $TPL_V1["mb_name"]?></span>
						</span>
						<span class="subject"><?php echo $TPL_V1["memo"]?></span>
						<span class="time"><?php if($TPL_V1["is_read"]){?>읽음<?php }else{?><strong class="color-red">읽지않음</strong><?php }?> <?php echo $TPL_VAR["eb"]->date_time('Y-m-d H:i',$TPL_V1["datetime"])?></span>
						<div class="clearfix"></div>
					</a>
				</li>
<?php }}else{?>
				<li><p class="text-center margin-top-20 font-size-12 color-grey"><i class="fa fa-exclamation-circle"></i> 확인하지 않은 쪽지가 없습니다.</p></li>
<?php }?>
			</ul>
		</li>
	</ul>
</li>

<script type="text/javascript" src="/eyoom/theme/basic3/plugins/perfect-scrollbar/perfect-scrollbar.jquery.min.js"></script>
<script>
jQuery(document).ready(function(){
    $('.scroll-box-cml').height(275).perfectScrollbar();
});
</script>