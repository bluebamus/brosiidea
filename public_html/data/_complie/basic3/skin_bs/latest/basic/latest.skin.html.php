<?php /* Template_ 2.2.8 2018/01/24 01:16:22 /home1/bluebamus3/public_html/petmari/eyoom/theme/basic3/skin_bs/latest/basic/latest.skin.html 000002964 */ 
$TPL_loop_1=empty($TPL_VAR["loop"])||!is_array($TPL_VAR["loop"])?0:count($TPL_VAR["loop"]);?>
<?php if (!defined('_GNUBOARD_')) exit; ?>

<style>
.basic-latest {position:relative;overflow:hidden}
.basic-latest ul {margin-bottom:0}
.basic-latest li {position:relative;overflow:hidden;padding:2px 0;font-size:12px}
.basic-latest .basic-subj {position:relative;width:70%;padding-right:0;padding-left:0;float:left}
.basic-latest .basic-comment {display: inline-block;min-width:38px;padding:1px 3px;font-size:10px;font-weight:300;line-height:11px;color:#fff;text-align:center;white-space:nowrap;vertical-align:baseline;background-color:#757575}
.basic-latest .basic-time {position:relative;overflow:hidden;width:30%;font-size:12px;text-align:right;color:#555555;float:right}
.basic-latest .basic-time .i-color {color:#b5b5b5}
.basic-latest .basic-member {position:relative;overflow:hidden;display:inline-block;width:30%;float:right}
.basic-latest .basic-photo img {width:17px;height:17px;margin-left:7px;float:right}
.basic-latest .basic-photo .basic-user-icon {width:17px;height:17px;font-size:11px;line-height:17px;text-align:center;background:#858585;color:#fff;margin-left:7px;float:right}
.basic-latest .basic-nick {font-size:12px;color:#858585;display:block;text-overflow:ellipsis;white-space:nowrap;word-wrap:normal;overflow:hidden;float:right}
.basic-latest a:hover .basic-subj {text-decoration:underline}
.basic-latest a:hover .basic-nick {color:#000}
.basic-latest a:hover .basic-time i {color:#FF2900}
</style>

<div class="headline">
	<h5><strong><?php echo $TPL_VAR["title"]?></strong></h5>
</div>

<div class="basic-latest">
	<ul class="list-unstyled">
<?php if($TPL_loop_1){foreach($TPL_VAR["loop"] as $TPL_V1){?>
		<li>
			<a href="<?php echo $TPL_V1["href"]?>">
				<div class="basic-subj ellipsis">
					- <?php if($TPL_V1["wr_comment"]){?><span class="basic-comment">+<?php echo number_format($TPL_V1["wr_comment"])?></span> <?php }?><?php echo $TPL_V1["wr_subject"]?>

				</div>
<?php if( 1){?>
				<div class="basic-time">
					<i class="fa fa-clock-o <?php if($TPL_V1["new"]){?>color-red<?php }else{?>i-color<?php }?>"></i> <?php echo $TPL_VAR["eb"]->date_time('m-d',$TPL_V1["datetime"])?>

				</div>
<?php }else{?>
				<div class="basic-member">
<?php if($TPL_VAR["photo"]=='y'){?>
					<div class="basic-photo">
<?php if($TPL_V1["mb_photo"]){?>
						<?php echo $TPL_V1["mb_photo"]?>

<?php }else{?>
						<span class="basic-user-icon"><i class="fa fa-user"></i></span>
<?php }?>
					</div>
<?php }?>
					<div class="basic-nick"><?php echo $TPL_V1["mb_nick"]?></div>
				</div>
<?php }?>
			</a>
		</li>
<?php }}else{?>
		<li><p class="text-center color-grey font-size-13 margin-top-30"><i class="fa fa-exclamation-circle"></i> 최신글이 없습니다.</p></li>
<?php }?>
	</ul>
</div>