<?php /* Template_ 2.2.8 2018/11/26 21:04:51 /home1/bluebamus2/public_html/eyoom/theme/basic3/skin_bs/emoticon/basic/emoticon.skin.html 000002101 */ 
$TPL__emo_type_1=empty($GLOBALS["emo_type"])||!is_array($GLOBALS["emo_type"])?0:count($GLOBALS["emo_type"]);
$TPL__emoticon_1=empty($GLOBALS["emoticon"])||!is_array($GLOBALS["emoticon"])?0:count($GLOBALS["emoticon"]);?>
<?php if (!defined('_GNUBOARD_')) exit;
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/basic3/plugins/eyoom-form/css/eyoom-form.min.css" type="text/css" media="screen">',0);
?>

<style>
.emoticon-list .eyoom-form {margin:15px 15px 25px}
.emoticon-list .emoticon-label {width:100px;float:right;text-align:right;padding:7px 10px 0 0}
.emoticon-list .emoticon-select {width:170px;float:right}
.emoticon-list .emoticons {list-style:none;margin:0;padding:0}
.emoticon-list .emoticons li {float:left;min-height:65px;margin:5px 13px}
.emoticon-list .emoticons li img {width:40px}
</style>

<div class="emoticon-list">
	<div class="eyoom-form">
		<div class="emoticon-select">
			<label class="select">
				<select name="eom" id="emo" onchange="change_emoticon(this.value);">
<?php if($TPL__emo_type_1){foreach($GLOBALS["emo_type"] as $TPL_V1){?>
					<option value="<?php echo $TPL_V1?>" <?php if($GLOBALS["emo"]==$TPL_V1){?>selected<?php }?>><?php echo $TPL_V1?></option>
<?php }}?>
				</select>
				<i></i>
			</label>
		</div>
		<div class="emoticon-label">
			<label class="label">이모티콘 선택</label>
		</div>
		<div class="clearfix"></div>
	</div>
	<ul class="emoticons">
<?php if($TPL__emoticon_1){foreach($GLOBALS["emoticon"] as $TPL_V1){?>
		<li><a href="javascript:void(0);" onclick="set_emoticon('<?php echo $TPL_V1["emoticon"]?>');"><img src="<?php echo $TPL_V1["url"]?>"></a></li>
<?php }}?>
	</ul>
</div>

<script>
function change_emoticon(emo) {
	var url = './emoticon.php?emo='+emo;
	$(location).attr('href',url);
}
function set_emoticon(emoticon) {
	parent.set_emoticon(emoticon);
	parent.jQuery('.vbox-close, .vbox-overlay').trigger('click');
}
</script>

<?php $this->print_("tail_sub",$TPL_SCP,1);?>