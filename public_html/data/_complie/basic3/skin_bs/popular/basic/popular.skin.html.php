<?php /* Template_ 2.2.8 2018/01/24 01:16:23 /home1/bluebamus3/public_html/petmari/eyoom/theme/basic3/skin_bs/popular/basic/popular.skin.html 000001313 */ 
$TPL_list_1=empty($TPL_VAR["list"])||!is_array($TPL_VAR["list"])?0:count($TPL_VAR["list"]);?>
<?php if (!defined('_GNUBOARD_')) exit; ?>

<style>
.popular-box {position:relative;border:1px solid #e5e5e5;padding:10px;margin:0px 20px 30px;background:#fff;-webkit-border-radius:3px !important;-moz-border-radius:3px !important;border-radius:3px !important}
.popular ul {margin-bottom:0}
.popular li {float:left;margin:0 3px 5px}
</style>

<div class="popular-box">
	<div class="headline">
		<h5><strong>인기검색어</strong></h5>
	</div>
	<div class="popular">
        <ul class="list-unstyled">
<?php if($TPL_list_1){foreach($TPL_VAR["list"] as $TPL_V1){?>
            <li>
            	<a href="<?php echo G5_BBS_URL?>/search.php?sfl=wr_subject&amp;sop=and&amp;stx=<?php echo urlencode($TPL_V1["pp_word"])?>">
	            	<span class="label label-light"><?php echo $TPL_V1["pp_word"]?></span>
            	</a>
            </li>
<?php }}else{?>
			<p class="text-center color-grey font-size-13"><i class="fa fa-exclamation-circle"></i> 출력할 인기검색어가 없습니다.</p>
<?php }?>
			<div class="clearfix"></div>
        </ul>
	</div>
</div>