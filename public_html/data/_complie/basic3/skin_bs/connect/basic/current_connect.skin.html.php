<?php /* Template_ 2.2.8 2018/11/26 21:04:50 /home1/bluebamus2/public_html/eyoom/theme/basic3/skin_bs/connect/basic/current_connect.skin.html 000002584 */  $this->include_("eb_nameview");
$TPL_list_1=empty($TPL_VAR["list"])||!is_array($TPL_VAR["list"])?0:count($TPL_VAR["list"]);?>
<?php if (!defined('_GNUBOARD_')) exit; ?>

<style>
.current-connect .table-list-eb .table thead > tr > th {border-bottom:1px solid #000;text-align:center;padding:10px 5px}
.current-connect .table-list-eb .table tbody > tr > td {border-top:1px solid #ededed;padding:7px 5px}
.current-connect .table-list-eb thead {border-top:2px solid #000;border-bottom:1px solid #000}
.current-connect .table-list-eb th {color:#000;font-weight:bold;white-space:nowrap;font-size:13px}
.current-connect .table-list-eb .td-mobile td {border-top:1px solid #f0f0f0;padding:5px !important;font-size:11px;background:#fbfbfb;text-align:right}

</style>

<div class="current-connect">
	<div class="table-list-eb">
	    <div class="board-list-body">
	        <table class="table table-hover">
	            <thead>
	                <tr>
				        <th class="th-num">번호</th>
				        <th>이름</th>
				        <th class="hidden-xs">위치</th>
	                </tr>
	            </thead>
	            <tbody>
<?php if($TPL_list_1){foreach($TPL_VAR["list"] as $TPL_V1){?>
			        <tr>
			            <td class="text-left"><?php echo $TPL_V1["num"]?></td>
			            <td><?php if($TPL_V1["mb_id"]){?><?php echo eb_nameview('basic',$TPL_V1["mb_id"],$TPL_V1["mb_nick"],$TPL_V1["mb_email"],$TPL_V1["mb_homepage"])?><?php }else{?><?php echo $TPL_V1["name"]?><?php }?></td>
			            <td class="td-width hidden-xs">
				            <div class="td-location">
<?php if($TPL_V1["lo_url"]&&$GLOBALS["is_admin"]=='super'){?>
								<a href="<?php echo $TPL_V1["lo_url"]?>"><?php echo $TPL_V1["lo_location"]?></a>
<?php }else{?>
								<?php echo $TPL_V1["lo_location"]?>

<?php }?>
				            </div>
						</td>
			        </tr>
			        <tr class="td-mobile visible-xs">				        <td colspan="2">
<?php if($TPL_V1["lo_url"]&&$GLOBALS["is_admin"]=='super'){?>
							<a href="<?php echo $TPL_V1["lo_url"]?>"><?php echo $TPL_V1["lo_location"]?></a>
<?php }else{?>
							<?php echo $TPL_V1["lo_location"]?>

<?php }?>
				        </td>
			        </tr>
<?php }}else{?>
					<tr><td colspan="3" class="text-center"><span class="color-grey"><i class="fa fa-exclamation-circle"></i> 현재 접속자가 없습니다.</span></td></tr>
<?php }?>
	            </tbody>
	        </table>
	    </div>
	</div>
</div>