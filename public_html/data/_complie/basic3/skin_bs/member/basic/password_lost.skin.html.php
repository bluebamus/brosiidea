<?php /* Template_ 2.2.8 2018/11/26 21:04:51 /home1/bluebamus2/public_html/eyoom/theme/basic3/skin_bs/member/basic/password_lost.skin.html 000003783 */ ?>
<?php if (!defined('_GNUBOARD_')) exit;
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/basic3/plugins/bootstrap/css/bootstrap.min.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/basic3/plugins/font-awesome/css/font-awesome.min.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/basic3/plugins/eyoom-form/css/eyoom-form.min.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/basic3/css/common.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/basic3/css/style.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/basic3/css/custom.css" type="text/css" media="screen">',0);
?>

<style>
.find-info {padding:15px}
.find-info input {vertical-align:inherit}
.find-info .btn-e-lg {padding:7px 16px;font-size:14px}
</style>

<div class="find-info">
	<h4 class="margin-bottom-30"><strong>회원정보 찾기</strong></h4>
	<div class="alert alert-warning">
	    <p><i class="fa fa-exclamation-circle"></i> 회원가입 시 등록하신 이메일 주소를 입력해 주세요. 해당 이메일로 아이디와 비밀번호 정보를 보내드립니다.</p>
	</div>
    
    <form name="fpasswordlost" action="<?php echo $GLOBALS["action_url"]?>" onsubmit="return fpasswordlost_submit(this);" method="post" autocomplete="off" class="eyoom-form">
    <div id="info_fs">
        <section>
	        <label for="mb_email" class="label">E-mail 주소<strong class="sound_only">필수</strong></label>
	        <label class="input">
	        	<i class="icon-append fa fa-envelope-o"></i>
	        	<input type="text" name="mb_email" id="mb_email" required size="30">
	        </label>
        </section>
		<div class="margin-hr-15"></div>
        <section>
            <label class="label">자동등록방지</label>
            <div class="vc-captcha"><?php echo captcha_html()?></div>
        </section>
    </div>
    <div class="text-center margin-top-30">
        <input type="submit" value="확인" class="btn-e btn-e-lg btn-e-red">
        <button type="button" onclick="window.close();" class="btn-e btn-e-lg btn-e-dark">창닫기</button>
    </div>
    </form>
</div>

<script src="/eyoom/theme/basic3/plugins/jquery-migrate-1.2.1.min.js"></script>
<script src="/eyoom/theme/basic3/plugins/bootstrap/js/bootstrap.min.js"></script>
<script>
function fpasswordlost_submit(f) {
    <?php echo chk_captcha_js()?>


    return true;
}

$(function() {
    var sw = screen.width;
    var sh = screen.height;
    var cw = document.body.clientWidth;
    var ch = document.body.clientHeight;
    var top  = sh / 2 - ch / 2 - 100;
    var left = sw / 2 - cw / 2;
    moveTo(left, top);
});

$("input, textarea, select").on({ 'touchstart' : function() {
	zoomDisable();
}});
$("input, textarea, select").on({ 'touchend' : function() {
	setTimeout(zoomEnable, 500);
}});
function zoomDisable(){
	$('head meta[name=viewport]').remove();
	$('head').prepend('<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">');
}
function zoomEnable(){
	$('head meta[name=viewport]').remove();
	$('head').prepend('<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=1">');
}
</script>
<!--[if lt IE 9]>
	<script src="/eyoom/theme/basic3/plugins/respond.min.js"></script>
	<script src="/eyoom/theme/basic3/plugins/html5shiv.min.js"></script>
	<script src="/eyoom/theme/basic3/plugins/eyoom-form/js/eyoom-form-ie8.js"></script>
<![endif]-->