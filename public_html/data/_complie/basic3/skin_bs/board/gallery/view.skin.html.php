<?php /* Template_ 2.2.8 2018/11/26 21:04:51 /home1/bluebamus2/public_html/eyoom/theme/basic3/skin_bs/board/gallery/view.skin.html 000043523 */  $this->include_("eb_nameview");
$TPL_view_file_1=empty($TPL_VAR["view_file"])||!is_array($TPL_VAR["view_file"])?0:count($TPL_VAR["view_file"]);
$TPL_view_link_1=empty($TPL_VAR["view_link"])||!is_array($TPL_VAR["view_link"])?0:count($TPL_VAR["view_link"]);
$TPL_view_tags_1=empty($TPL_VAR["view_tags"])||!is_array($TPL_VAR["view_tags"])?0:count($TPL_VAR["view_tags"]);?>
<?php if (!defined('_GNUBOARD_')) exit;
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/basic3/plugins/sweetalert/sweetalert.min.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/basic3/plugins/magnific-popup/magnific-popup.min.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/basic3/plugins/prism/prism.min.css" type="text/css" media="screen">',0);
?>

<style>
.board-view .board-view-info {position:relative;box-sizing:content-box;height:46px;border-top:3px solid #454545;border-bottom:1px solid #454545;padding:7px 0;margin-top:15px;background:#fbfbfb}
.board-view .board-view-info .view-photo-box {position:absolute;top:7px;left:0}
.board-view .board-view-info .view-info-box {position:relative;<?php if($TPL_VAR["eyoom_board"]["bo_use_profile_photo"]== 1){?>margin-left:56px<?php }?>}
.board-view .board-view-info .view-member-progress {position:absolute;top:10px;right:0;width:150px}
.board-view .board-view-info .view-photo img {width:46px;height:46px;margin-right:3px;-webkit-border-radius:2px !important;-moz-border-radius:2px !important;border-radius:2px !important}
.board-view .board-view-info .view-photo .view-user-icon {width:46px;height:46px;font-size:30px;line-height:46px;text-align:center;background:#797b8a;color:#fff;margin-right:3px;-webkit-border-radius:2px !important;-moz-border-radius:2px !important;border-radius:2px !important;display:inline-block;white-space:nowrap;vertical-align:baseline}
.board-view .board-view-info .info-box-top {display:block;margin:3px 0 5px}
.board-view .board-view-info .info-box-top .view-nick {font-size:13px;margin-right:3px}
.board-view .board-view-info .info-box-top .view-lv-icon {display:inline-block;margin-right:3px}
.board-view .board-view-info .info-box-bottom {display:block;font-size:11px}
.board-view .board-view-info .info-box-bottom span {margin-right:7px}
.board-view .board-view-info .info-box-bottom i {color:#b5b5b5;margin-right:4px}
.board-view .board-view-file {font-size:11px}
.board-view .board-view-file ul {margin-bottom:0}
.board-view .board-view-file li {padding:7px 0;border-bottom:1px dotted #e5e5e5}
.board-view .board-view-file a:hover {text-decoration:underline}
.board-view .board-view-file span {margin-left:7px}
.board-view .board-view-file span i {margin-right:4px;color:#b5b5b5}
.board-view .board-view-link {font-size:11px}
.board-view .board-view-link ul {margin-bottom:0}
.board-view .board-view-link li {padding:7px 0;border-bottom:1px dotted #e5e5e5}
.board-view .board-view-link a {color:#0078FF;text-decoration:underline}
.board-view .board-view-link a:hover {color:#0053B0}
.board-view .board-view-star {padding:6px 0;border-bottom:1px solid #eee;font-size:11px}
.board-view .board-view-star ul {margin-bottom:0}
.board-view .board-view-star .star-ratings-view li {padding:0;float:left;margin-right:1px}
.board-view .board-view-star .star-ratings-view li .rating {color:#a5a5a5;line-height:normal}
.board-view .board-view-star .star-ratings-view li .rating-selected {color:#FF2900}
.board-view .board-view-short-url {font-size:11px}
.board-view .board-view-short-url ul {margin-bottom:0}
.board-view .board-view-short-url li {padding:5px 0;border-bottom:1px solid #474A5E}
.board-view .board-view-short-url a:hover {text-decoration:underline}
.board-view .view-top-btn {padding:20px 0 5px}
.board-view .view-top-btn:after {display:block;visibility:hidden;clear:both;content:""}
.board-view .view-top-btn .top-btn-left li {float:left;margin-right:5px}
.board-view .view-top-btn .top-btn-right li {float:left;margin-left:5px;margin-bottom:5px}
.board-view .board-view-atc {min-height:200px}
.board-view .board-view-atc-title {position:absolute;font-size:0;line-height:0;overflow:hidden}
.board-view .board-view-file-conts {position:relative;overflow:hidden}
.board-view .board-view-file-conts #bo_v_img img {display:block;width:100% \9;max-width:100%;height:auto;margin-bottom:10px}
.board-view .board-view-con {position:relative;overflow:hidden;margin-bottom:30px;width:100%;word-break:break-all}
.board-view .board-view-con img {max-width:100%;height:auto}
.board-view .board-view-good-btn {margin-bottom:30px;text-align:center}
.board-view .board-view-good-btn .board-view-act-gng {position:relative;margin:0 5px}
.board-view .board-view-good-btn .board-view-act-gng .act-gng-btn {position:relative;overflow:hidden;width:70px;height:70px;background:#252525;display:inline-block;white-space:nowrap;vertical-align:baseline;text-align:center;-webkit-border-radius:50% !important;-moz-border-radius:50% !important;border-radius:50% !important}
.board-view .board-view-good-btn .board-view-act-gng .act-gng-btn i {font-size:26px;color:#fff;margin:12px 0 3px}
.board-view .board-view-good-btn .board-view-act-gng .act-gng-btn:hover i {color:#59A7FF}
.board-view .board-view-good-btn .board-view-act-gng .act-gng-btn:hover i.fa-thumbs-down {color:#FF6F52}
.board-view .board-view-good-btn .board-view-act-gng .act-gng-btn strong {font-size:12px;color:#fff}
.board-view .board-view-good-btn .board-view-act-gng .act-gng-btn:hover strong {color:#252525}
.board-view .board-view-good-btn .board-view-act-gng .act-gng-btn .mask {width:100%;height:100%;position:absolute;overflow:hidden;top:0;left:0;background:#fff;-webkit-border-radius:50% !important;-moz-border-radius:50% !important;border-radius:50% !important;-ms-filter:"progid: DXImageTransform.Microsoft.Alpha(Opacity=0)";filter:alpha(opacity=0);opacity:0}
.board-view .board-view-good-btn .board-view-act-gng .act-gng-btn h5 {color:#000;font-size:12px;font-weight:bold;text-align:center;margin-top:43px;background:transparent;-webkit-transform:scale(0);-moz-transform:scale(0);-o-transform:scale(0);-ms-transform:scale(0);transform:scale(0);-webkit-transition:all 0.2s linear;-moz-transition:all 0.2s linear;-o-transition:all 0.2s linear;-ms-transition:all 0.2s linear;transition:all 0.2s linear;-ms-filter:"progid: DXImageTransform.Microsoft.Alpha(Opacity=0)";filter:alpha(opacity=0);opacity:0}
.board-view .board-view-good-btn .board-view-act-gng .act-gng-btn:hover .mask {-ms-filter:"progid: DXImageTransform.Microsoft.Alpha(Opacity=70)";filter:alpha(opacity=70);opacity:0.7}
.board-view .board-view-good-btn .board-view-act-gng .act-gng-btn:hover h5 {-webkit-transform:scale(1);-moz-transform:scale(1);-o-transform:scale(1);-ms-transform:scale(1);transform:scale(1);-ms-filter:"progid: DXImageTransform.Microsoft.Alpha(Opacity=100)";filter:alpha(opacity=100);opacity:1}
.board-view .board-view-btn {margin-bottom:30px;text-align:right}
.board-view .board-view-btn a {margin-right:5px}
.board-view .board-view-act-good,.board-view-act-nogood {display:none;position:absolute;top:30px;left:0;padding:5px 0;width:165px;background:#000;color:#fff;text-align:center}
.board-view .board-view-bot {zoom:1}
.board-view .board-view-bot:after {display:block;visibility:hidden;clear:both;content:""}
.board-view .board-view-bot h2 {position:absolute;font-size:0;line-height:0;overflow:hidden}
.board-view .board-view-bot ul {margin:0;padding:0;list-style:none}
.board-view .blind {display:none}
.board-view .map-content-wrap {width:100%;height:350px}
.board-view .map-content-wrap > div {width:100%;height:350px}
.board-view .eb-rating {width:200px;margin:0 auto;border-top:1px solid #eaeaea;padding:10px 4px 8px}
.board-view .eyoom-form .rating {font-size:12px}
.board-view .eyoom-form .rating label {font-size:14px;margin-top:3px}
.board-view .shadow-spacer {position:relative;width:230px;margin:0 auto 30px}
.board-view .shadow-spacer .shadow-mask {overflow:hidden;height:15px}
.board-view .shadow-spacer .shadow-mask:after {content:'';display:block;margin:-20px auto 0;width:100%;height:20px;border-radius:125px / 12px !important;box-shadow:0 0 8px black}
.board-view .board-view-tag {position:relative;overflow:hidden;background:#fbfbfb;border:1px solid #c5c5c5;padding:5px;margin-top:-1px}
.board-view .board-view-tag span {display:inline-block;padding:2px 8px;line-height:1;margin:2px;background:#e5e5e5;font-size:11px;border-radius:2px !important}
.board-view .board-view-tag a:hover span {background:#757575;color:#fff}
.board-view .board-view-tag .fa-tags {width:22px;height:22px;line-height:22px;text-align:center;font-size:12px;background:#454545;color:#fff;margin-right:5px;box-sizing:content-box}
.board-view pre {font-size:12px}
.board-view .caption-overflow span {left:0;right:0}
.draggable {display:block;width:100% \9;max-width:100%;height:auto;margin:0 auto}
button.mfp-close {position:fixed;color:#fff !important}
.mfp-figure .mfp-close {position:absolute}
</style>

<article class="board-view">
    <h4>
<?php if($GLOBALS["category_name"]){?>
        <span class="color-grey margin-right-5">[<?php echo $TPL_VAR["view"]["ca_name"]?>]</span>
<?php }?>
        <strong><?php echo cut_str(get_text($TPL_VAR["view"]["wr_subject"]), 70)?></strong>
    </h4>
    <div class="board-view-info">
<?php if($TPL_VAR["eyoom_board"]["bo_use_profile_photo"]== 1){?>
        <div class="view-photo-box">
<?php if($TPL_VAR["view"]["mb_photo"]){?>
            <span class="view-photo margin-right-5"><?php echo $TPL_VAR["view"]["mb_photo"]?></span>
<?php }else{?>
            <span class="view-photo margin-right-5"><span class="view-user-icon"><i class="fa fa-user"></i></span></span>
<?php }?>
        </div>
<?php }?>
        <div class="view-info-box">
	        <div class="info-box-top">
	            <span class="view-nick">
	            	<?php echo eb_nameview('basic',$TPL_VAR["view"]["mb_id"],$TPL_VAR["view"]["wr_name"],$TPL_VAR["view"]["wr_email"],$TPL_VAR["view"]["wr_homepage"])?>

	            </span>
<?php if($TPL_VAR["lv"]["gnu_icon"]){?>
	            <span class="view-lv-icon"><img src="<?php echo $TPL_VAR["lv"]["gnu_icon"]?>" align="absmiddle"></span>
<?php }?>
<?php if($TPL_VAR["lv"]["eyoom_icon"]){?>
	            <span class="view-lv-icon"><img src="<?php echo $TPL_VAR["lv"]["eyoom_icon"]?>" align="absmiddle"></span>
<?php }?>
<?php if($GLOBALS["is_ip_view"]){?>
            	<span class="margin-left-5 color-grey font-size-11"><?php echo $GLOBALS["ip"]?></span>
<?php }?>
            </div>
			<div class="info-box-bottom">
<?php if($TPL_VAR["eyoom_board"]["bo_sel_date_type"]=='1'){?>
	            <span class="color-black"><i class="fa fa-clock-o"></i><?php echo $TPL_VAR["eb"]->date_time('Y.m.d H:i',$TPL_VAR["view"]["wr_datetime"])?></span>
<?php }elseif($TPL_VAR["eyoom_board"]["bo_sel_date_type"]=='2'){?>
	            <span><i class="fa fa-clock-o"></i><?php echo $TPL_VAR["eb"]->date_format('Y.m.d H:i',$TPL_VAR["view"]["wr_datetime"])?></span>
<?php }?>
	            <span><i class="fa fa-eye"></i><?php echo number_format($TPL_VAR["view"]["wr_hit"])?></span>
	            <span class="color-red"><i class="fa fa-comments-o"></i><?php echo number_format($TPL_VAR["view"]["wr_comment"])?></span>
<?php if($GLOBALS["is_good"]){?>
		        <span class="color-green hidden-xs">
		            <i class="fa fa-thumbs-up"></i><?php echo number_format($TPL_VAR["view"]["wr_good"])?>

		        </span>
<?php }?>
<?php if($GLOBALS["is_nogood"]){?>
		        <span class="color-yellow hidden-xs">
		            <i class="fa fa-thumbs-down"></i><?php echo number_format($TPL_VAR["view"]["wr_nogood"])?>

		        </span>
<?php }?>
	        </div>
        </div>
        <div class="view-member-progress hidden-xs">
			<span class="progress-info-left"><small>LV.<strong><?php echo $TPL_VAR["lvuser"]["level"]?></strong></small></span>
			<span class="progress-info-right"><small><?php echo $TPL_VAR["lvuser"]["ratio"]?>%</small></span>
			<div class="progress progress-e progress-sm rounded progress-striped active">
			    <div class="progress-bar progress-bar-indigo" role="progressbar" aria-valuenow="<?php echo $TPL_VAR["lvuser"]["ratio"]?>" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $TPL_VAR["lvuser"]["ratio"]?>%">
			    </div>
			</div>
		</div>
    </div>

<?php if($GLOBALS["cnt"]> 0){?>
    <div class="board-view-file">
        <ul class="list-unstyled">
<?php if($TPL_view_file_1){foreach($TPL_VAR["view_file"] as $TPL_V1){?>
            <li>
                <div class="pull-left">
                    - 첨부파일 : <a href="<?php echo $TPL_V1["href"]?>" class="view_file_download"><strong><?php echo $TPL_V1["source"]?></strong> <?php echo $TPL_V1["content"]?> (<?php echo $TPL_V1["size"]?>) - <u>다운로드</u></a>
                </div>
                <div class="pull-right text-right hidden-xs">
                    <span><i class="fa fa-download"></i><?php echo $TPL_V1["download"]?></span>
                    <span><i class="fa fa-clock-o"></i><?php echo $TPL_V1["datetime"]?></span>
                </div>
                <div class="clearfix"></div>
            </li>
<?php }}?>
        </ul>
    </div>
<?php }?>

<?php if(implode('',$TPL_VAR["view"]["link"])){?>
    <div class="board-view-link">
        <ul class="list-unstyled">
<?php if($TPL_view_link_1){foreach($TPL_VAR["view_link"] as $TPL_V1){?>
            <li>
                <div class="pull-left">
                    - 관련링크 : <a href="<?php echo $TPL_V1["href"]?>" target="_blank"><?php echo $TPL_V1["link"]?></a>
                </div>
                <div class="pull-right text-right">
                    <span><?php echo $TPL_V1["hit"]?>회 연결</span>
                </div>
                <div class="clearfix"></div>
            </li>
<?php }}?>
        </ul>
    </div>
<?php }?>

<?php if($TPL_VAR["eyoom_board"]["bo_use_rating"]=='1'){?>
    <div class="board-view-star">
        <ul class="list-unstyled star-ratings-view">
	        <li><span class="margin-right-5">- 별점통계 :</span></li>
            <li><i class="rating<?php if($TPL_VAR["rating"]["star"]> 0){?>-selected fa fa-star<?php }else{?> fa fa-star-o<?php }?>"></i></li>
            <li><i class="rating<?php if($TPL_VAR["rating"]["star"]> 1){?>-selected fa fa-star<?php }else{?> fa fa-star-o<?php }?>"></i></li>
            <li><i class="rating<?php if($TPL_VAR["rating"]["star"]> 2){?>-selected fa fa-star<?php }else{?> fa fa-star-o<?php }?>"></i></li>
            <li><i class="rating<?php if($TPL_VAR["rating"]["star"]> 3){?>-selected fa fa-star<?php }else{?> fa fa-star-o<?php }?>"></i></li>
            <li><i class="rating<?php if($TPL_VAR["rating"]["star"]> 4){?>-selected fa fa-star<?php }else{?> fa fa-star-o<?php }?>"></i></li>
            <li class="margin-left-5">- [ 평점 : <span class="color-red"><?php echo $TPL_VAR["rating"]["point"]?></span>점 / <span class="color-grey"><?php echo number_format($TPL_VAR["rating"]["members"])?>명 참여</span> ]</li>
        </ul>
        <div class="clearfix"></div>
    </div>
<?php }?>

    <div class="board-view-short-url">
        <ul class="list-unstyled">
            <li>
                - 짧은주소 :
                <a href="<?php echo $TPL_VAR["short_url"]?>" target="_blank"><?php echo $TPL_VAR["short_url"]?></a>
                <button type="button" data-toggle="modal" data-target=".short-url-modal" class="copy_short_url btn-e btn-e-xs btn-e-default margin-left-5">주소복사</button>
            </li>
        </ul>
    </div>
    <div class="modal fade short-url-modal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-purple">
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                    <h4 class="modal-title">짧은 글주소 복사</h4>
                </div>
                <div class="modal-body">
                    <form name="short_url_form" class="eyoom-form margin-top-15">
						<div class="input input-button">
							<input type="text" name="short_url" id="short_url" value="<?php echo $TPL_VAR["short_url"]?>">
							<div class="button"><input type="button" class="clipboard-btn" data-clipboard-target="#short_url" data-dismiss="modal">복사하기</div>
						</div>
                        <div class="note"><strong>Note!</strong> '복사하기' 버튼을 클릭하면 내 컴퓨터 클립보드에 복사됩니다.</div>
                    </form>
                </div>
	            <div class="modal-footer">
	                <button data-dismiss="modal" class="btn-e btn-e-lg btn-e-dark" type="button">닫기</button>
	            </div>
            </div>
        </div>
    </div>

<?php if($TPL_VAR["eyoom"]["use_tag"]=='y'&&$TPL_VAR["eyoom_board"]["bo_use_tag"]=='1'&&$TPL_view_tags_1> 0){?>
    <div class="board-view-tag">
        <i class="fa fa-tags"></i>
<?php if($TPL_view_tags_1){foreach($TPL_VAR["view_tags"] as $TPL_V1){?>
        <a href="<?php echo $TPL_V1["href"]?>"><span><?php echo $TPL_V1["tag"]?></span></a>
<?php }}?>
        <div class="clearfix"></div>
    </div>
<?php }?>

    <div class="view-top-btn">
<?php if($GLOBALS["prev_href"]||$GLOBALS["next_href"]){?>
        <ul class="top-btn-left list-unstyled pull-left">
<?php if($GLOBALS["prev_href"]){?>
            <li><a href="<?php echo $GLOBALS["prev_href"]?>" class="btn-e btn-e-default rounded" type="button">이전글</a></li>
<?php }?>
<?php if($GLOBALS["next_href"]){?>
            <li><a href="<?php echo $GLOBALS["next_href"]?>" class="btn-e btn-e-default rounded" type="button">다음글</a></li>
<?php }?>
        </ul>
<?php }?>

        <ul class="top-btn-right list-unstyled pull-right">
<?php if($GLOBALS["copy_href"]){?>
            <li><a href="<?php echo $GLOBALS["copy_href"]?>" class="btn-e btn-e-default rounded" type="button" onclick="board_move(this.href); return false;">복사</a></li>
<?php }?>
<?php if($GLOBALS["move_href"]){?>
            <li><a href="<?php echo $GLOBALS["move_href"]?>" class="btn-e btn-e-default rounded" type="button" onclick="board_move(this.href); return false;">이동</a></li>
<?php }?>
<?php if($GLOBALS["search_href"]){?>
            <li><a href="<?php echo $GLOBALS["search_href"]?>" class="btn-e btn-e-dark rounded" type="button">검색</a></li>
<?php }?>
<?php if($GLOBALS["update_href"]){?>
            <li><a href="<?php echo $GLOBALS["update_href"]?>" class="btn-e btn-e-dark rounded" type="button">수정</a></li>
<?php }?>
<?php if($GLOBALS["delete_href"]){?>
            <li><a href="<?php echo $GLOBALS["delete_href"]?>" class="btn-e btn-e-dark rounded" type="button" onclick="del(this.href); return false;">삭제</a></li>
<?php }?>
            <li><a href="<?php echo $GLOBALS["list_href"]?>" <?php if($GLOBALS["wmode"]){?>onclick="close_modal();return false;"<?php }?> class="btn-e btn-e-dark rounded" type="button">목록</a></li>
<?php if(!$GLOBALS["wmode"]){?>
<?php if($GLOBALS["reply_href"]){?>
            <li><a href="<?php echo $GLOBALS["reply_href"]?>" class="btn-e btn-e-dark rounded" type="button">답변</a></li>
<?php }?>
<?php if($GLOBALS["write_href"]){?>
            <li><a href="<?php echo $GLOBALS["write_href"]?>" class="btn-e btn-e-red rounded" type="button">글쓰기</a></li>
<?php }?>
<?php }?>
        </ul>
        <div class="clearfix"></div>
    </div>

    <div class="board-view-atc">
        <h2 class="board-view-atc-title">본문</h2>
		<div class="board-view-file-conts">
        	<?php echo $GLOBALS["file_conts"]?>

        </div>

<?php if($TPL_VAR["ycard"]["yc_blind"]=='y'){?>
            <p class="text-center margin-top-40 margin-bottom-40"><strong class="color-orange font-size-13">----- <i class="fa fa-exclamation-circle"></i> 블라인드 처리된 글입니다. -----</strong></p>
<?php }?>
        <div class="board-view-con view-content"><?php echo $GLOBALS["view_content"]?></div>

<?php if($GLOBALS["good_href"]||$GLOBALS["nogood_href"]=='1'){?>
	        <div class="board-view-good-btn">
<?php if($GLOBALS["good_href"]){?>
		        <span class="board-view-act-gng">
		            <a href="<?php echo $GLOBALS["good_href"]?>&amp;<?php echo $GLOBALS["qstr"]?>" id="good_button" class="act-gng-btn" type="button"><i class="fa fa-thumbs-up display-block"></i><strong><?php echo number_format($TPL_VAR["view"]["wr_good"])?></strong><div class="mask"><h5>좋아요!</h5></div></a>
		            <b class="board-view-act-good"></b>
		        </span>
<?php }?>
<?php if($GLOBALS["nogood_href"]){?>
		        <span class="board-view-act-gng">
		            <a href="<?php echo $GLOBALS["nogood_href"]?>&amp;<?php echo $GLOBALS["qstr"]?>" id="nogood_button" class="act-gng-btn" type="button"><i class="fa fa-thumbs-down display-block"></i><strong><?php echo number_format($TPL_VAR["view"]["wr_nogood"])?></strong><div class="mask"><h5>글쎄요~</h5></div></a>
		            <b class="board-view-act-nogood"></b>
		        </span>
<?php }?>
	        </div>
<?php }else{?>
<?php if($TPL_VAR["board"]["bo_use_good"]||$TPL_VAR["board"]["bo_use_nogood"]){?>
			<div class="board-view-good-btn">
<?php if($TPL_VAR["board"]["bo_use_good"]){?>
	            <span class="board-view-act-gng">
	            	<span class="act-gng-btn"><i class="fa fa-thumbs-up display-block color-white"></i><strong class="color-white"><?php echo number_format($TPL_VAR["view"]["wr_good"])?></strong></span>
	            </span>
<?php }?>
<?php if($TPL_VAR["board"]["bo_use_nogood"]){?>
	            <span class="board-view-act-gng">
	            	<span class="act-gng-btn"><i class="fa fa-thumbs-down display-block color-white"></i><strong class="color-white"><?php echo number_format($TPL_VAR["view"]["wr_nogood"])?></strong></span>
	            </span>
<?php }?>
	            <div class="text-center color-grey font-size-11 margin-top-10"><i class="fa fa-exclamation-circle"></i> 로그인 후 평가 가능합니다.</div>
        	</div>
<?php }?>
<?php }?>

<?php if($TPL_VAR["eyoom_board"]["bo_use_rating"]=='1'&&$GLOBALS["is_member"]){?>
        <form class="eyoom-form">
        <div class="eb-rating clearfix">
            <div class="rating">
                <input type="radio" name="quality" id="quality-5" value="5">
                <label for="quality-5"><i class="fa fa-star"></i></label>
                <input type="radio" name="quality" id="quality-4" value="4">
                <label for="quality-4"><i class="fa fa-star"></i></label>
                <input type="radio" name="quality" id="quality-3" value="3">
                <label for="quality-3"><i class="fa fa-star"></i></label>
                <input type="radio" name="quality" id="quality-2" value="2">
                <label for="quality-2"><i class="fa fa-star"></i></label>
                <input type="radio" name="quality" id="quality-1" value="1">
                <label for="quality-1"><i class="fa fa-star"></i></label>
                <strong><i class="fa fa-check"></i> 별점평가하기</strong>
            </div>
        </div>
		<div class="shadow-spacer">
			<div class="shadow-mask"></div>
		</div>
        </form>
<?php }?>

<?php if($GLOBALS["scrap_href"]||$TPL_VAR["eyoom_board"]["bo_use_yellow_card"]=='1'){?>
        <div class="board-view-btn">
<?php if($GLOBALS["scrap_href"]){?>
            <a href="<?php echo $GLOBALS["scrap_href"]?>" target="_blank" class="btn-e btn-e-yellow rounded" type="button" onclick="win_scrap(this.href); return false;">스크랩</a>
<?php }?>
<?php if($TPL_VAR["eyoom_board"]["bo_use_yellow_card"]=='1'&&$GLOBALS["is_member"]){?>
<?php if(!$TPL_VAR["mb_ycard"]["mb_id"]){?>
            <button id="yellow_card" class="btn-e btn-e-dark rounded position-relative" data-toggle="modal" data-target=".yellowcard-modal">신고 <span class="badge badge-brown rounded"><?php echo number_format($TPL_VAR["ycard"]["yc_count"])?></span></button>
<?php }else{?>
            <button id="cancel_yellow_card" class="btn-e btn-e-red rounded position-relative">신고 취소 <span class="badge badge-brown rounded"><?php echo number_format($TPL_VAR["ycard"]["yc_count"])?></span></button>
<?php }?>
<?php if($GLOBALS["blind_direct"]){?>
<?php if($TPL_VAR["ycard"]["yc_blind"]!='y'){?>
            <button id="direct_blind" class="btn-e btn-e-dark rounded btn-blind">블라인드</button>
<?php }elseif($TPL_VAR["ycard"]["yc_blind"]=='y'){?>
            <button id="cancel_blind" class="btn-e btn-e-red rounded btn-blind">블라인드 취소</button>
<?php }?>
<?php }?>
<?php }?>
        </div>
<?php }?>

<?php if($GLOBALS["is_signature"]&&$TPL_VAR["view"]["mb_id"]!='anonymous'){?>
<?php $this->print_("signature_bs",$TPL_SCP,1);?>

<?php }?>

    </div>

<?php if($TPL_VAR["board"]["bo_use_sns"]){?>
<?php $this->print_("sns_bs",$TPL_SCP,1);?>

<?php }?>

    <div class="margin-hr-15"></div>

<?php $this->print_("cmt_bs",$TPL_SCP,1);?>


    <div class="board-view-bot">
        <?php echo $GLOBALS["link_buttons"]?>

    </div>
</article>

<?php if($TPL_VAR["eyoom_board"]["bo_use_yellow_card"]=='1'){?>
<div class="modal fade yellowcard-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-red">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">게시물 신고하기</h4>
            </div>
            <div class="modal-body">
                <fieldset id="bo_ycard" class="eyoom-form margin-top-20">
                    <form name="fycard">
                    <input type="hidden" name="modal_cmt_id" id="modal_cmt_id" value="">
                    <label for="sfl" class="sound_only">신고사유</label>
					<div class="panel panel-warning">
					    <div class="panel-heading">
					        <h6 class="panel-title"><strong class="font-size-13"><i class="fa fa-exclamation-circle"></i> 이 게시물을 신고하시겠습니까? 신고사유를 선택해 주세요.</strong></h6>
					    </div>
					    <div class="panel-body">
	                        <div class="inline-group">
	                            <label class="radio" for="ycard_reason_1">
	                                <input type="radio" name="ycard_reason" id="ycard_reason_1" value="1"><i class="rounded-x"></i>광고성
	                            </label>
	                            <label class="radio" for="ycard_reason_2">
	                                <input type="radio" name="ycard_reason" id="ycard_reason_2" value="2"><i class="rounded-x"></i>음란성
	                            </label>
	                            <label class="radio" for="ycard_reason_3">
	                                <input type="radio" name="ycard_reason" id="ycard_reason_3" value="3"><i class="rounded-x"></i>비방성
	                            </label>
	                            <label class="radio" for="ycard_reason_4">
	                                <input type="radio" name="ycard_reason" id="ycard_reason_4" value="4"><i class="rounded-x"></i>혐오성
	                            </label>
	                            <label class="radio" for="ycard_reason_5">
	                                <input type="radio" name="ycard_reason" id="ycard_reason_5" value="5"><i class="rounded-x"></i>기타
	                            </label>
	                        </div>
					    </div>
					</div>
                    <div class="text-center margin-top-20">
                        <button type="button" class="btn-e btn-e-lg btn-e-red">신고하기</button>
                    </div>
                    </form>
                </fieldset>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn-e btn-e-lg btn-e-dark" type="button"><i class="fa fa-close"></i> 닫기</button>
            </div>
        </div>
    </div>
</div>
<?php }?>

<script src="/js/viewimageresize.js"></script>
<script src="/eyoom/theme/basic3/plugins/sweetalert/sweetalert.min.js"></script>
<script src="/eyoom/theme/basic3/plugins/clipboard/clipboard.min.js"></script>
<script src="/eyoom/theme/basic3/plugins/magnific-popup/magnific-popup.min.js"></script>
<?php if($TPL_VAR["eyoom_board"]["bo_use_addon_coding"]=='1'){?>
<script src="/eyoom/theme/basic3/plugins/prism/prism.min.js"></script>
<?php }?>
<?php if($TPL_VAR["eyoom_board"]["bo_use_addon_map"]=='1'){?>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places&region=kr&key=구글맵_api키_입력"></script>
<script src="http://openapi.map.naver.com/openapi/v2/maps.js?clientId=네이버지도_ClientId_입력"></script>
<script src="http://apis.daum.net/maps/maps3.js?apikey=다음_api키_입력"></script>
<script src="/eyoom/theme/basic3/js/eyoom.map.js"></script>
<script>
$(function(){
    $(".map-content-wrap").each(function(){
        var id      = $(this).find('div').attr('id');
        var type    = $(this).attr('data-map-type');
        var name    = $(this).attr('data-map-name');
        var x       = $(this).attr('data-map-x');
        var y       = $(this).attr('data-map-y');
        var address = $(this).attr('data-map-address');

        switch(type) {
            case 'google':
                loading_google_map(id, x, y, name, address);
                break;
            case 'naver':
                loading_naver_map(id, x, y, name, address);
                break;
            case 'daum':
                loading_daum_map(id, x, y, name, address);
                break;
        }
    });
});
</script>
<?php }?>
<script>
new Clipboard('.clipboard-btn');

<?php if($TPL_VAR["board"]["bo_download_point"]< 0){?>
$(function() {
    $("a.view_file_download").click(function(e) {
        if(!g5_is_member) {
	        swal({
	            title: "Oops...",
	            text: "다운로드 권한이 없습니다. 로그인 후 이용 가능합니다.",
	            confirmButtonColor: "#FF2900",
	            type: "error",
	            confirmButtonText: "확인"
	        });
            return false;
        }

		e.preventDefault();
	    var linkURL = $(this).attr("href")+"&js=on";
	    view_file_download_link(linkURL);
    });
	function view_file_download_link(linkURL) {
	    swal({
		    html: true,
	        title: "안내",
	        text: "<div class='alert alert-warning font-size-12 text-left'>파일을 다운로드 하면 포인트가 <strong class='color-red'><?php echo number_format($TPL_VAR["board"]["bo_download_point"])?></strong> 점 적용됩니다.<ol class='margin-top-10'><li>포인트는 게시물당 한번만 적용되며, 다음에 다시 다운로드 하여도 중복하여 적용되지 않습니다.</li><li>본인이 올린 파일은 다운로드 하여도 포인트는가 변동되지 않습니다.</li></ol></div><strong>정말로 다운로드 하시겠습니까?</strong>",
	        type: "warning",
	        showCancelButton: true,
	        confirmButtonColor: "#FF9500",
	        confirmButtonText: "다운로드",
	        cancelButtonText: "취소",
	        closeOnConfirm: true,
	        closeOnCancel: true
	    },
	    function(){
			window.location.href = linkURL;
	    });
	}
});
<?php }?>

function close_modal(wr_id) {
    window.parent.closeModal(wr_id);
}

function board_move(href) {
    window.open(href, "boardmove", "left=50, top=50, width=500, height=550, scrollbars=1");
}

$(function() {
	$('.board-view-file-conts img').parent().attr('class', 'view-img-popup').removeAttr('target');
	$('.view-img-popup').each(function() {
		var dataSource = $(this).attr('href');
		$(this).attr('data-source', dataSource);
	});
	$('.board-view-file-conts img').each(function() {
		var imgURL = $(this).attr('src');
		$(this).parent().attr('href', imgURL);
	});
	$('.view-img-popup').magnificPopup({
		type: 'image',
		closeOnContentClick: true,
		mainClass: 'mfp-img-mobile',
		image: {
			verticalFit: true,
			titleSrc: function(item) {
				return '&middot; <a class="image-source-link" href="'+item.el.attr('data-source')+'" target="_blank">원본 이미지 보기</a>';
			}
		}
	});

	if($('.board-view-con img').parent().hasClass('view_image')) {
		$('.board-view-con img').parent().attr('class', 'view-image-popup').removeAttr('target');
		$('.view-image-popup').each(function() {
			var dataSource = $(this).attr('href');
			$(this).attr('data-source', dataSource);
		});
		$('.board-view-con img').each(function() {
			var imgURL = $(this).attr('src');
			$(this).parent().attr('href', imgURL);
		});
		$('.view-image-popup').magnificPopup({
			type: 'image',
			closeOnContentClick: true,
			mainClass: 'mfp-img-mobile',
			image: {
				verticalFit: true,
				titleSrc: function(item) {
					return '&middot; <a class="image-source-link" href="'+item.el.attr('data-source')+'" target="_blank">원본 이미지 보기</a>';
				}
			}
		});
	} else {
		$('.board-view-con img').wrap('<a class="view-image-popup">');
		$('.board-view-con img').each(function() {
			var imgURL = $(this).attr('src');
			$(this).parent().attr('href', imgURL);
		});
		$('.view-image-popup').magnificPopup({
			type: 'image',
			closeOnContentClick: true,
			mainClass: 'mfp-img-mobile',
			image: {
				verticalFit: true
			}
		});
	}

    // 이미지 리사이즈
    $(".board-view-atc").viewimageresize();

    // 추천, 비추천
    $("#good_button, #nogood_button").click(function() {
        var $tx;
        if(this.id == "good_button")
            $tx = $(".board-view-act-good");
        else
            $tx = $(".board-view-act-nogood");

        excute_good(this.href, $(this), $tx);
        return false;
    });

<?php if($TPL_VAR["eyoom_board"]["bo_use_yellow_card"]=='1'&&$GLOBALS["is_member"]){?>
    // 게시물 신고
    $('.yellowcard-modal .modal-body button').click(function () {
        var cmt_id = $("#modal_cmt_id").val();
        var yc_reason = $(':radio[name="ycard_reason"]:checked').val();
        if(!yc_reason) {
	        swal({
	            title: "알림!",
	            text: "'신고사유'를 선택해 주세요.",
	            confirmButtonColor: "#FF9500",
	            type: "warning",
	            confirmButtonText: "확인"
	        });
            return;
        } else {
            $.post(
                '<?php echo EYOOM_CORE_URL?>/board/yellow_card.php',
                { bo_table: "<?php echo $GLOBALS["bo_table"]?>", wr_id: "<?php echo $GLOBALS["wr_id"]?>", cmt_id: cmt_id, action: "add", reason: yc_reason },
                function(data) {
                    if(data.count && !data.error) {
                        if(!cmt_id) {
                            if($('#yc_card').text() == '') {
                                $('#yellow_card').wrap('<span id="yc_card"></span>');
                            } else {
                                $('#yc_card').html('');
                            }
                            $('#yellow_card').remove();
                            var yc_html = '<button id="cancel_yellow_card" class="btn-e btn-e-red rounded position-relative">신고 취소 <span class="badge badge-brown rounded">' + number_format(String(data.count)) + '</span></button>';
                            $('#yc_card').html(yc_html);
                        } else {
                            var obj = $("#cmt_yellow_card_li_"+cmt_id);
                            obj.html('');
                            obj.attr('data-original-title','신고취소');
                            $("#modal_cmt_id").val('');
                            var yc_html = '<a id="cancel_cmt_yellow_card_'+cmt_id+'" class="cancel_cmt_yellow_card" data-toggle="modal" data-target=".yellowcard-modal" data-cmt-id="'+cmt_id+'">신고하기 <small>(누적 : <span class="color-red">' + number_format(String(data.count)) + '</span>)</small></a>';
                            obj.html(yc_html);
                        }
                    }
                    if(data.msg) alert(data.msg);
                    $('.yellowcard-modal').modal('hide');
                }, "json"
            );
        }
    });

    // 게시물 신고 취소
    $('#cancel_yellow_card, .cancel_yellow_card, .cancel_cmt_yellow_card').click(function() {
        var cmt_id = $("#modal_cmt_id").val();

	    swal({
	        title: "신고취소!",
	        text: "신고취소 처리하시겠습니까?",
	        type: "warning",
	        showCancelButton: true,
	        confirmButtonColor: "#FF9500",
	        confirmButtonText: "확인",
	        cancelButtonText: "취소",
	        closeOnConfirm: true,
	        closeOnCancel: true
	    },
	    function(){
            $.post(
                '<?php echo EYOOM_CORE_URL?>/board/yellow_card.php',
                { bo_table: "<?php echo $GLOBALS["bo_table"]?>", wr_id: "<?php echo $GLOBALS["wr_id"]?>", cmt_id: cmt_id, action: "cancel" },
                function(data) {
                    if(data.count>=0 && !data.error) {
                        if(!cmt_id) {
                            if($('#yc_card').text() == '') {
                                $('#cancel_yellow_card').wrap('<span id="yc_card"></span>');
                            } else {
                                $('#yc_card').html('');
                            }
                            $('#cancel_yellow_card').remove();
                            var yc_html = '<button id="yellow_card" class="btn-e btn-e-dark rounded position-relative" data-toggle="modal" data-target=".yellowcard-modal">신고 <span class="badge badge-brown rounded">' + number_format(String(data.count)) + '</span></button>';
                        } else {
                            var obj = $("#cancel_cmt_yellow_card_li_"+cmt_id);
                            obj.html('');
                            obj.attr('data-original-title','신고');
                            $("#modal_cmt_id").val('');
                            var yc_html = '<a id="cmt_yellow_card_'+cmt_id+'" class="cmt_yellow_card" data-toggle="modal" data-target=".yellowcard-modal" data-cmt-id="'+cmt_id+'">신고하기 <small>(누적 : <span class="color-red">' + number_format(String(data.count)) + '</span>)</small></a>';
                            obj.html(yc_html);
                        }
                        $('#yc_card').html(yc_html);
                    }
                    if(data.msg) alert(data.msg);
                }, "json"
            );
	    });
    });

    // 원글의 신고취소를 위해 modal_cmt_id 값을 리셋
    $('#yellow_card').click(function() {
        $("#modal_cmt_id").val('');
    });

<?php if($GLOBALS["blind_direct"]){?>
    // 블라인드 처리 권한을 가진 회원
    $('.btn-blind, .btn-cmt-blind').click(function() {
        var id = $(this).attr('id');
        var cmt_id = $(this).attr('data-cmt-id');
        if(typeof(cmt_id) == 'undefined') var cmt_id = '';

        switch(id) {
            case 'direct_blind':
                if(confirm('본 게시물을 바로 블라인드 처리합니다.\n\n계속 진행하시겠습니까?')){
                    var action = 'db'; // direct blind
                    var re_id = !cmt_id ? 'cancel_blind' : 'cancel_cmt_blind_li_'+cmt_id;
                    var re_class = !cmt_id ? 'btn-e btn-e-red rounded btn-blind' : 'btn-e btn-e-dark btn-e-xs btn-blind';
                    var re_text = '블라인드 취소';
                }
                break;
            case 'cancel_blind':
                if(confirm('본 게시물을 블라인드 취소처리합니다.\n\n계속 진행하시겠습니까?')){
                    var action = 'cb'; // cancel blind
                    var re_id = !cmt_id ? 'direct_blind' : 'direct_cmt_blind_li_'+cmt_id;
                    var re_class = !cmt_id ? 'btn-e btn-e-dark rounded btn-blind' : 'btn-e btn-e-red btn-e-xs btn-blind';
                    var re_text = '블라인드';
                }
                break;
            case 'direct_cmt_blind_li_'+cmt_id:
                if(confirm('본 댓글을 바로 블라인드 처리합니다.\n\n계속 진행하시겠습니까?')){
                    var action = 'db'; // direct blind
                    var re_id = 'cancel_cmt_blind_li_'+cmt_id;
                    var re_class = 'btn-cmt-blind';
                }
                break;
            case 'cancel_cmt_blind_li_'+cmt_id:
                if(confirm('본 댓글을 블라인드 취소처리합니다.\n\n계속 진행하시겠습니까?')){
                    var action = 'cb'; // cancel blind
                    var re_id = 'direct_cmt_blind_li_'+cmt_id;
                    var re_class = 'btn-cmt-blind';
                }
                break;
        }

        if(typeof(action) != 'undefined') {
            $.post(
                '<?php echo EYOOM_CORE_URL?>/board/direct_blind.php',
                { bo_table: "<?php echo $GLOBALS["bo_table"]?>", wr_id: "<?php echo $GLOBALS["wr_id"]?>", cmt_id: cmt_id, action: action },
                function(data) {
                    if(!cmt_id) {
                        $('.btn-blind').attr('id', re_id);
                        $('.btn-blind').attr('class', re_class);
                        $('.btn-blind').text(re_text);
                    } else {
                        $('.btn-cmt-blind').attr('id', re_id);
                        $('.btn-cmt-blind').attr('class', re_class);
                    }
                    if(data.msg) alert(data.msg);
                }, "json"
            );
        }
    });
<?php }?>

<?php }?>

<?php if($TPL_VAR["eyoom_board"]["bo_use_rating"]=='1'&&$GLOBALS["is_member"]){?>
    $(".rating > input").click(function() {
        var score = $(this).val();
	    swal({
		    html: true,
	        title: "별점평가",
	        text: "<div class='alert alert-warning font-size-12'>별점 <strong>" + score + "</strong> 점을 클릭하였습니다.</div><strong>본 게시물의 별점평가에 참여하시겠습니까?</strong>",
	        type: "warning",
	        showCancelButton: true,
	        confirmButtonColor: "#FF9500",
	        confirmButtonText: "확인",
	        cancelButtonText: "취소",
	        closeOnConfirm: true,
	        closeOnCancel: true
	    },
	    function(){
            $.post(
                '<?php echo EYOOM_CORE_URL?>/board/star_rating.php',
                { bo_table: "<?php echo $GLOBALS["bo_table"]?>", wr_id: "<?php echo $GLOBALS["wr_id"]?>", score: score },
                function(data) {
                    if(data.msg) alert(data.msg);
                }, "json"
            );
	    });
    });
<?php }?>
});

function excute_good(href, $el, $tx) {
    $.post(
        href,
        { js: "on" },
        function(data) {
            if(data.error) {
                alert(data.error);
                return false;
            }

            if(data.count) {
                $el.find("strong").text(number_format(String(data.count)));
                if($tx.attr("id").search("nogood") > -1) {
                    $tx.text("이 글을 비추천하셨습니다.");
                    $tx.fadeIn(200).delay(2500).fadeOut(200);
                } else {
                    $tx.text("이 글을 추천하셨습니다.");
                    $tx.fadeIn(200).delay(2500).fadeOut(200);
                }
            }
        }, "json"
    );
}
</script>