<?php /* Template_ 2.2.8 2018/11/26 21:04:51 /home1/bluebamus2/public_html/eyoom/theme/basic3/skin_bs/board/basic/write.skin.html 000030593 */ 
$TPL_wr_tags_1=empty($TPL_VAR["wr_tags"])||!is_array($TPL_VAR["wr_tags"])?0:count($TPL_VAR["wr_tags"]);
$TPL_wr_link_1=empty($TPL_VAR["wr_link"])||!is_array($TPL_VAR["wr_link"])?0:count($TPL_VAR["wr_link"]);
$TPL_wr_file_1=empty($TPL_VAR["wr_file"])||!is_array($TPL_VAR["wr_file"])?0:count($TPL_VAR["wr_file"]);?>
<?php if (!defined('_GNUBOARD_')) exit;
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/basic3/plugins/sweetalert/sweetalert.min.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/basic3/plugins/venobox/venobox.css" type="text/css" media="screen">',0);
?>

<style>
.board-write .board-write-title {border-bottom:1px solid #474A5E;padding-bottom:15px}
.board-write input {vertical-align:inherit}
.board-write input.btn-e-xlg {padding:10px 25px;font-size:16px}
.board-write .blind {position:absolute;top:-10px;left:-100000px;display:none}
.board-write textarea {min-height:250px}
.board-write .btn-e-input {padding:5px 16px}
.board-write .write-collapse-box {margin-top:10px;background:#f8f8f8;border:1px solid #d5d5d5;padding:15px 10px}
/* Auto Save */
#autosave_wrapper {position:relative}
#autosave_pop {display:none;z-index:10;position:absolute;top:10px;right:10px;padding:8px;width:320px;height:auto !important;height:180px;max-height:180px;border:1px solid #565656;background:#fff;overflow-y:scroll}
html.no-overflowscrolling #autosave_pop {height:auto;max-height:10000px !important}
#autosave_pop strong {position:absolute;font-size:0;line-height:0;overflow:hidden}
#autosave_pop div {text-align:right}
#autosave_pop button {margin:0;padding:0;border:0;background:transparent;margin-left:10px}
#autosave_pop ul {margin:10px 0;padding:0;border-top:1px solid #e9e9e9;list-style:none}
#autosave_pop li {padding:8px 5px;border-bottom:1px solid #e9e9e9;zoom:1}
#autosave_pop li:after {display:block;visibility:hidden;clear:both;content:""}
#autosave_pop a {display:block;float:left}
#autosave_pop span {display:block;float:right}
.autosave_close {cursor:pointer}
.autosave_content {display:none}
/* Tag */
#tag-box {border:1px dashed #c5c5c5;min-height:20px;padding:5px;background:#fff;margin-top:15px}
#tag-cloud div {display:inline-block;line-height:1;background:#474A5E;padding:3px 7px;margin:2px 3px;font-size:11px;color:#fff;border-radius:2px !important}
/* Ckeditor */
.board-write a.cke_button {padding:2px 5px}
.board-write a.cke_button_on {padding:1px 4px}
.board-write a.cke_button_off:hover, .board-write a.cke_button_off:focus, .board-write a.cke_button_off:active {padding:1px 4px}
/* Smart Editor */
.cke_sc {margin-bottom:10px}
.btn_cke_sc {padding:0 10px}
.cke_sc_def {padding:10px;margin-bottom:10px;margin-top:10px}
.cke_sc_def button {padding:3px 15px;background:#555555;color:#fff;border:none}
/* Summernote */
.eyoom-form .note-editor *, .eyoom-form .note-editor *:after, .eyoom-form .note-editor *:before {box-sizing:border-box;-moz-box-sizing:border-box}
.eyoom-form .note-editor.panel-default>.panel-heading {background-color:#eaecee;border:0;border-bottom:1px solid #A9A9A9}
.panel-heading.note-toolbar .note-color .dropdown-menu {padding-top:6px;padding-bottom:6px;padding-left:1px}
/* Map */
#map_canvas {width:1000px;height:400px;display:none}
</style>

<div class="board-write">
	<h4 class="board-write-title"><strong><?php echo $GLOBALS["g5"]["title"]?></strong></h4>
	<form name="fwrite" id="fwrite" action="<?php echo $GLOBALS["action_url"]?>" onsubmit="return fwrite_submit(this);" method="post" enctype="multipart/form-data" autocomplete="off" class="eyoom-form">
	<input type="hidden" name="uid" value="<?php echo $GLOBALS["uid"]?>">
	<input type="hidden" name="w" value="<?php echo $GLOBALS["w"]?>">
	<input type="hidden" name="bo_table" value="<?php echo $GLOBALS["bo_table"]?>">
	<input type="hidden" name="wr_id" value="<?php echo $GLOBALS["wr_id"]?>">
	<input type="hidden" name="sca" value="<?php echo $GLOBALS["sca"]?>">
	<input type="hidden" name="sfl" value="<?php echo $GLOBALS["sfl"]?>">
	<input type="hidden" name="stx" value="<?php echo $GLOBALS["stx"]?>">
	<input type="hidden" name="spt" value="<?php echo $GLOBALS["spt"]?>">
	<input type="hidden" name="sst" value="<?php echo $GLOBALS["sst"]?>">
	<input type="hidden" name="sod" value="<?php echo $GLOBALS["sod"]?>">
	<input type="hidden" name="page" value="<?php echo $GLOBALS["page"]?>">
	<input type="hidden" name="board_skin_path" value="<?php echo EYOOM_CORE_PATH?>/board">
	<input type="hidden" name="wr_1" id="wr_1" value="<?php echo $GLOBALS["wr_1"]?>">
	<input type="hidden" name="wr_2" id="wr_2" value="<?php echo $GLOBALS["wr_2"]?>">
	<input type="hidden" name="wr_3" id="wr_3" value="<?php echo $GLOBALS["wr_3"]?>">
	<input type="hidden" name="wr_4" id="wr_4" value="<?php echo $GLOBALS["wr_4"]?>">
	<input type="hidden" name="wr_5" id="wr_5" value="<?php echo $GLOBALS["wr_5"]?>">
	<input type="hidden" name="wmode" value="<?php echo $GLOBALS["wmode"]?>">
	<?php echo $GLOBALS["option_hidden"]?>

<?php if(($GLOBALS["is_name"])||($GLOBALS["is_password"]&&!$GLOBALS["is_admin"])||($GLOBALS["is_email"])||($GLOBALS["is_homepage"])){?>
	<section class="margin-top-20">
		<div class="row">
<?php if($GLOBALS["is_name"]){?>
			<div class="col col-3">
				<label for="wr_name" class="label">이름<strong class="sound_only">필수</strong></label>
				<label class="input margin-bottom-10">
					<i class="icon-append fa fa-user"></i>
					<input type="text" name="wr_name" value="<?php echo $GLOBALS["name"]?>" id="wr_name" required size="10" maxlength="20">
				</label>
			</div>
<?php }?>
<?php if($GLOBALS["is_password"]&&!$GLOBALS["is_admin"]){?>
			<div class="col col-3">
				<label for="wr_password" class="label">비밀번호<strong class="sound_only">필수</strong></label>
				<label class="input margin-bottom-10">
					<i class="icon-append fa fa-lock"></i>
					<input type="password" name="wr_password" id="wr_password" required maxlength="20">
				</label>
			</div>
<?php }?>
<?php if($GLOBALS["is_email"]){?>
			<div class="col col-3">
				<label for="wr_email" class="label">이메일</label>
				<label class="input margin-bottom-10">
					<i class="icon-append fa fa-envelope-o"></i>
					<input type="text" name="wr_email" value="<?php echo $GLOBALS["email"]?>" id="wr_email" size="50" maxlength="100">
				</label>
			</div>
<?php }?>
<?php if($GLOBALS["is_homepage"]){?>
			<div class="col col-3">
				<label for="wr_homepage" class="label">홈페이지</label>
				<label class="input margin-bottom-10">
					<i class="icon-append fa fa-home"></i>
					<input type="text" name="wr_homepage" value="<?php echo $GLOBALS["homepage"]?>" id="wr_homepage" size="50">
				</label>
			</div>
<?php }?>
		</div>
	</section>
	<div class="margin-hr-15"></div>
<?php }?>
	<section>
		<div class="row">
<?php if($GLOBALS["is_category"]){?>
			<div class="col col-4">
				<label class="select">
					<select name="ca_name" id="ca_name" required class="form-control">
						<option value="">선택하세요 - 필수</option>
						<?php echo $GLOBALS["category_option"]?>

					</select>
					<i></i>
				</label>
			</div>
<?php }?>
			<div class="col col-8">
<?php if($GLOBALS["is_notice"]||$GLOBALS["is_html"]||$GLOBALS["is_secret"]||$GLOBALS["is_mail"]||$GLOBALS["is_anonymous"]){?>
				<div class="inline-group">
<?php if($GLOBALS["is_notice"]){?>
					<label for="notice" class="checkbox"><input type="checkbox" id="notice" name="notice" value="1" <?php echo $GLOBALS["notice_checked"]?>><i></i>공지</label>
<?php }?>

<?php if($GLOBALS["is_html"]){?>
<?php if($GLOBALS["is_dhtml_editor"]){?>
					<input type="hidden" value="html1" name="html">
<?php }else{?>
					<label for="html" class="checkbox"><input type="checkbox" id="html" name="html" onclick="html_auto_br(this);" value="<?php echo $GLOBALS["html_value"]?>" <?php echo $GLOBALS["html_checked"]?>><i></i>HTML</label>
<?php }?>
<?php }?>

<?php if($GLOBALS["is_secret"]){?>
<?php if($GLOBALS["is_admin"]||$GLOBALS["is_secret"]== 1){?>
					<label for="secret" class="checkbox"><input type="checkbox" id="secret" name="secret" value="secret" <?php echo $GLOBALS["secret_checked"]?>><i></i>비밀글</label>
<?php }else{?>
					<input type="hidden" name="secret" value="secret">
<?php }?>
<?php }?>

<?php if($GLOBALS["is_anonymous"]){?>
					<label for="anonymous" class="checkbox"><input type="checkbox" id="anonymous" name="anonymous" value="y" <?php echo $GLOBALS["anonymous_checked"]?>><i></i>익명글</label>
<?php }?>

<?php if($GLOBALS["is_mail"]){?>
					<label for="mail" class="checkbox"><input type="checkbox" id="mail" name="mail" value="mail" <?php echo $GLOBALS["recv_email_checked"]?>><i></i>답변메일받기</label>
<?php }?>
				</div>
<?php }?>
			</div>
		</div>
	</section>
	<div class="margin-hr-15"></div>
	<section>
		<div class="row">
			<div class="col col-9 md-margin-bottom-10">
				<label for="wr_subject" class="label">제목<strong class="sound_only"> 필수</strong></label>
				<label class="input">
                    <i class="icon-append fa fa-check"></i>
                    <input type="text" name="wr_subject" value="<?php echo $GLOBALS["subject"]?>" id="wr_subject" required size="50" maxlength="255">
                </label>
			</div>
<?php if($GLOBALS["is_member"]){?>
			<div class="col col-3 text-right">
				<div class="margin-bottom-25 hidden-xs"></div>
				<script src="/js/autosave.js"></script>
				<button type="button" id="btn_autosave" class="btn-e btn-e-red rounded position-relative">임시 저장된 글 <span id="autosave_count" class="badge badge-dark rounded"><?php echo $GLOBALS["autosave_count"]?></span></button>
				<div id="autosave_pop">
					<strong>임시 저장된 글 목록</strong>
					<div><span class="autosave_close"><i class="fa fa-times"></i></span></div>
					<div class="clearfix"></div>
					<ul></ul>
					<div><span class="autosave_close btn-e btn-e-dark btn-e-sm">닫기</span></div>
				</div>
			</div>
<?php }?>
		</div>
	</section>
	<div class="margin-hr-15"></div>
<?php if($TPL_VAR["eyoom"]["use_tag"]=='y'&&$TPL_VAR["eyoom_board"]["bo_use_tag"]=='1'&&$TPL_VAR["member"]["mb_level"]>=$TPL_VAR["eyoom_board"]["bo_tag_level"]){?>
	<section>
		<label class="label">태그 입력</label>
		<div class="input input-button">
			<i class="icon-prepend fa fa-tags"></i>
			<input type="text" name="tmp_tag" id="tmp_tag" size="50" maxlength="255">
			<b class="tooltip tooltip-top-left">관련 태그를 입력 후, TAB키를 누르시면 쉽게 태그를 추가할 수 있습니다.</b>
			<div class="button"><input type="button" class="add_tags"><i class="fa fa-plus"></i> 태그입력</div>
		</div>
		<div id="tag-box">
			<div id="tag-cloud">
<?php if($TPL_wr_tags_1){$TPL_I1=-1;foreach($TPL_VAR["wr_tags"] as $TPL_V1){$TPL_I1++;?>
				<div id="tag_box_<?php echo $TPL_I1?>"><?php echo $TPL_V1?> <i class="fa fa-close" onclick="del_tags('<?php echo $TPL_V1?>','<?php echo $TPL_I1?>');"></i></div>
<?php }}?>
			</div>
		</div>
		<input type="hidden" name="wr_tag" id="wr_tag" value="<?php echo $TPL_VAR["write"]["wr_tag"]?>">
		<input type="hidden" name="del_tag" id="del_tag" value="">
	</section>
	<div class="margin-hr-15"></div>
<?php }?>
	<section>
		<div class="wr_content">
			<div id="write-option">
				<div class="panel panel-default">
<?php if($TPL_VAR["eyoom_board"]["bo_use_addon_video"]=='1'){?>
					<a class="btn-e btn-e-xs btn-e-brown" data-toggle="collapse" data-parent="#write-option" href="#collapse-video-wr"><i class="fa fa-play-circle"></i> 동영상</a>
<?php }?>
<?php if($TPL_VAR["eyoom_board"]["bo_use_addon_soundcloud"]=='1'){?>
					<a class="btn-e btn-e-xs btn-e-orange" data-toggle="collapse" data-parent="#write-option" href="#collapse-sound-wr"><i class="fa fa-soundcloud"></i> 사운드클라우드</a>
<?php }?>
<?php if($TPL_VAR["eyoom_board"]["bo_use_addon_map"]=='1'){?>
					<a class="btn-e btn-e-xs btn-e-yellow" data-toggle="collapse" data-parent="#write-option" href="#collapse-map-wr"><i class="fa fa-map-marker"></i> 지도</a>
<?php }?>
<?php if($TPL_VAR["eyoom_board"]["bo_use_addon_emoticon"]=='1'){?>
					<a class="btn-e btn-e-xs btn-e-dark pull-right emoticon" data-type="iframe" title="이모티콘" href="<?php echo EYOOM_CORE_URL?>/board/emoticon.php"><i class="fa fa-smile-o"></i> 이모티콘</a>
<?php }?>
					<div class="clearfix"></div>
<?php if($TPL_VAR["eyoom_board"]["bo_use_addon_video"]=='1'){?>
					<div id="collapse-video-wr" class="panel-collapse collapse">
						<div class="write-collapse-box">
							<div class="input input-button">
								<input type="text" id="video_url" placeholder="동영상주소">
								<div class="button"><input type="button" id="btn_video" onclick="return false;">확인</div>
							</div>
						</div>
					</div>
<?php }?>
<?php if($TPL_VAR["eyoom_board"]["bo_use_addon_soundcloud"]=='1'){?>
					<div id="collapse-sound-wr" class="panel-collapse collapse">
						<div class="write-collapse-box">
							<div class="row">
								<div class="col col-8">
									<div class="input input-button">
										<input type="text" id="scloud_url" placeholder="사운드클라우드 음원주소">
										<div class="button"><input type="button" id="btn_scloud" onclick="return false;">확인</div>
									</div>
								</div>
								<div class="col col-4 text-right">
									<a href="https://soundcloud.com/" target="_blank" class="btn-e btn-e-xs btn-e-orange margin-top-5"><i class="fa fa-location-arrow"></i> 사운드클라우드 바로가기</a>
								</div>
							</div>
						</div>
					</div>
<?php }?>
<?php if($TPL_VAR["eyoom_board"]["bo_use_addon_map"]=='1'){?>
					<div id="collapse-map-wr" class="panel-collapse collapse">
						<div class="write-collapse-box">
							<div class="row">
			                    <div class="col col-2 md-margin-bottom-10">
									<label class="input">
										<i class="icon-append fa fa-question-circle"></i>
			                        	<input type="text" name="map_zip" id="map_zip" size="5" maxlength="6">
			                        	<b class="tooltip tooltip-top-right">우편번호</b>
									</label>
			                    </div>
			                    <div class="col col-2 text-right md-margin-bottom-10">
			                    	<button type="button" onclick="win_zip('fwrite', 'map_zip', 'map_addr1', 'map_addr2', 'map_addr3', 'map_addr_jibeon');" class="btn-e btn-e-yellow rounded btn-e-input">주소 검색</button>
			                    </div>
			                    <div class="col col-6 inline-group">
					                <label class="radio" for="map_type_1">
					                	<input type="radio" name="map_type" id="map_type_1" value="1" checked="checked"><i class="rounded-x"></i> Google지도
					                </label>
					                <label class="radio" for="map_type_2">
					                	<input type="radio" name="map_type" id="map_type_2" value="2"><i class="rounded-x"></i> 네이버지도
					                </label>
					                <label class="radio" for="map_type_3">
					                	<input type="radio" name="map_type" id="map_type_3" value="3"><i class="rounded-x"></i> 다음지도
					                </label>
			                    </div>
		                    </div>
		                    <div class="margin-bottom-10"></div>
		                    <div class="row">
			                    <div class="col col-12">
					                <label class="input">
					                	<input type="text" name="map_addr1" id="map_addr1" size="50">
					                </label>
					                <div class="note margin-bottom-10"><strong>Note:</strong> 기본주소</div>
			                    </div>
		                    </div>
		                    <div class="row">
			                    <div class="col col-5">
					                <label class="input">
					                	<input type="text" name="map_addr2" id="map_addr2" size="50">
					                </label>
					                <div class="note margin-bottom-10"><strong>Note:</strong> 상세주소</div>
			                    </div>
			                    <div class="col col-5">
					                <label class="input">
					                	<input type="text" name="map_name" id="map_name" size="50">
					                </label>
					                <div class="note margin-bottom-10"><strong>Note:</strong> 장소명</div>
			                    </div>
			                    <input type="hidden" name="map_addr3" id="map_addr3" value="">
			                    <input type="hidden" name="map_addr_jibeon" value="">
			                    <div class="col col-2 text-right">
				                    <button type="button" class="btn-e btn-e-red rounded btn-e-input" id="btn_map" onclick="return false;">적용하기</button>
			                    </div>
		                    </div>
						</div>
					</div>
<?php }?>
				</div>
			</div>
			<div class="margin-bottom-15"></div>
			<label class="label">본문 내용</label>
			<label class="textarea textarea-resizable">
				<?php echo $GLOBALS["editor_html"]?>

			</label>
		</div>
	</section>
	<div class="margin-hr-15"></div>
	<section>
<?php if($TPL_wr_link_1){foreach($TPL_VAR["wr_link"] as $TPL_K1=>$TPL_V1){?>
		<div class="row">
			<div class="col col-12">
				<label class="label">관련 링크 <?php echo $TPL_K1?></label>
                <label class="input">
                    <i class="icon-append fa fa-link"></i>
                    <input type="text" name="wr_link<?php echo $TPL_K1?>" value="<?php if($GLOBALS["w"]=='u'){?><?php echo $TPL_V1["link_val"]?><?php }?>" id="wr_link<?php echo $TPL_K1?>" class="form-control" size="50">
                    <b class="tooltip tooltip-top-right">링크주소를 입력 해 주세요.</b>
                </label>
			</div>
		</div>
		<div class="margin-hr-15"></div>
<?php }}?>
	</section>
	<section>
<?php if($TPL_wr_file_1){foreach($TPL_VAR["wr_file"] as $TPL_K1=>$TPL_V1){?>
		<div class="row">
			<div class="col col-12">
				<label class="label">파일 <?php echo $TPL_K1+ 1?> 업로드</label>
				<label for="file" class="input input-file">
					<div class="button bg-color-light-grey"><input type="file" name="bf_file[]" value="사진선택" title="파일첨부 <?php echo $TPL_K1+ 1?> : 용량 <?php echo $GLOBALS["upload_max_filesize"]?> 이하만 업로드 가능" onchange="this.parentNode.nextSibling.value = this.value">파일<?php echo $TPL_K1+ 1?> 선택</div><input type="text" readonly>
				</label>
			</div>
<?php if($GLOBALS["is_file_content"]){?>
			<div class="col col-12 margin-top-10">
                <label class="input">
                	<i class="icon-append fa fa-question-circle"></i>
                    <input type="text" name="bf_content[]" value="<?php if($GLOBALS["w"]=='u'){?><?php echo $TPL_V1["bf_content"]?><?php }?>" class="form-control" size="50" placeholder="파일<?php echo $TPL_K1+ 1?> 설명">
                    <b class="tooltip tooltip-top-right">파일 <?php echo $TPL_K1+ 1?> 설명을 입력 해 주세요.</b>
                </label>
			</div>
			<div class="clearfix"></div>
<?php }?>
<?php if($GLOBALS["w"]=='u'&&$TPL_V1["file"]){?>
			<div class="col col-6">
				<label for="bf_file_del<?php echo $TPL_K1?>" class="checkbox"><input type="checkbox" id="bf_file_del<?php echo $TPL_K1?>" name="bf_file_del[<?php echo $TPL_K1?>]" value="1"><i></i><?php echo $TPL_V1["source"]?> (<?php echo $TPL_V1["size"]?>) 파일삭제</label>
			</div>
<?php }?>
		</div>
		<div class="margin-hr-15"></div>
<?php }}?>
	</section>
<?php if(!$GLOBALS["is_member"]){?>
	<section>
		<label class="label">자동등록방지</label>
		<div class="vc-captcha"><?php echo $GLOBALS["captcha_html"]?></div>
		<div class="margin-bottom-20"></div>
	</section>
<?php }?>

	<div class="text-center">
		<input type="submit" value="작성완료" id="btn_submit" accesskey="s" class="btn-e btn-e-xlg btn-e-red">
		<a href="<?php if($GLOBALS["wmode"]){?>javascript:history.go(-1)<?php }else{?>./board.php?bo_table=<?php echo $GLOBALS["bo_table"]?><?php }?>" class="btn-e btn-e-xlg btn-e-dark">취소</a>
	</div>
	</form>
</div>
<div id="map_canvas"></div>

<script src="/eyoom/theme/basic3/plugins/sweetalert/sweetalert.min.js"></script>
<?php if($TPL_VAR["eyoom_board"]["bo_use_addon_emoticon"]=='1'){?>
<script src="/eyoom/theme/basic3/plugins/venobox/venobox.min.js"></script>
<?php }?>
<?php if($TPL_VAR["eyoom_board"]["bo_use_addon_map"]=='1'){?>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places&region=kr&key=AIzaSyDLwh6INUxDyVpAoNexQEEn7tnq_VH7tmE"></script>
<script type="text/javascript" src="https://openapi.map.naver.com/openapi/v3/maps.js?clientId=_R45VwtZy4e2pUtLraoG&submodules=geocoder"></script>
<?php }?>
<script>
$(document).ready(function(){
<?php if($TPL_VAR["eyoom_board"]["bo_use_addon_emoticon"]=='1'){?>
	$(".emoticon").venobox({border:'3px'});
<?php }?>

<?php if($TPL_VAR["eyoom_board"]["bo_use_addon_video"]=='1'){?>
	// 동영상 추가
	$("#btn_video").click(function(){
		var v_url = $("#video_url").val();
		if(!v_url){
	        swal({
	            title: "Oops...",
	            text: "동영상 주소를 입력해 주세요.",
	            confirmButtonColor: "#FF2900",
	            type: "error",
	            confirmButtonText: "확인"
	        });
		} else {
			set_textarea_contents('video',v_url);
		}
		$("#video_url").val('');
	});
<?php }?>

<?php if($TPL_VAR["eyoom_board"]["bo_use_addon_soundcloud"]=='1'){?>
	// 사운드크라우드 추가
	$("#btn_scloud").click(function(){
		var s_url = $("#scloud_url").val();
		if(!s_url){
	        swal({
	            title: "Oops...",
	            text: "사운드클라우드 주소를 입력해 주세요.",
	            confirmButtonColor: "#FF2900",
	            type: "error",
	            confirmButtonText: "확인"
	        });
		} else {
			set_textarea_contents('sound',s_url);
		}
	});
	$("#scloud_url").val('');
<?php }?>

<?php if($TPL_VAR["eyoom_board"]["bo_use_addon_map"]=='1'){?>
	// 지도 추가
	$("#btn_map").click(function(){
		var map_type = $("input[name='map_type']:checked").val();
		var map_addr1 = $("#map_addr1").val();
		var map_addr2 = $("#map_addr2").val();
		var map_name = $("#map_name").val();

		set_map_address(map_type, map_addr1, map_addr2, map_name);
	});
<?php }?>
});

<?php if($TPL_VAR["eyoom_board"]["bo_use_addon_emoticon"]=='1'){?>
function set_emoticon(emoticon) {
	var type='emoticon';
	set_textarea_contents(type,emoticon);
}
<?php }?>

<?php if($TPL_VAR["eyoom_board"]["bo_use_addon_map"]=='1'){?>
function set_map_address(map_type, map_addr1, map_addr2, map_name) {
	geocoder = new google.maps.Geocoder();
	var subgps;
	var latlng = new google.maps.LatLng('');
	var myOptions = {
		zoom: 16,
		center: latlng,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	gmap = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

	var address = map_addr1 + " " + map_addr2;
	geocoder.geocode( { 'address': address}, function(results, status) {
		if (status == google.maps.GeocoderStatus.OK) {
			gmap.setCenter(results[0].geometry.location);
			subgps = gmap.getCenter();
			set_textarea_contents('map', map_type+'^|^'+address+'^|^'+map_name+'^|^'+subgps);
		} else {
	        swal({
	            title: "Oops...",
	            text: "잘못된 주소입니다.",
	            confirmButtonColor: "#FF2900",
	            type: "error",
	            confirmButtonText: "확인"
	        });
		}
	});
}
<?php }?>

function set_textarea_contents(type,value) {
	var type_text = '';
	var content = '';
	var mobile = <?php if(G5_IS_MOBILE){?>true<?php }else{?>false<?php }?>;
	switch(type) {
		case 'emoticon': type_text = '이모티콘'; break;
		case 'video': type_text = '동영상'; break;
		case 'code': type_text = 'code'; break;
		case 'sound': type_text = 'soundcloud'; break;
		case 'map': type_text = '지도'; break;
	}
	if(type_text != 'code') {
		content = '{'+type_text+':'+value+'}';
	} else {
		content = '{code:'+value+'}<br><br>{/code}<br>'
	}
	if(g5_editor.indexOf('ckeditor')!=-1 && !mobile) {
		CKEDITOR.instances.wr_content.insertHtml(content);
	} else if(g5_editor.indexOf('smarteditor')!=-1 && !mobile) {
		oEditors.getById["wr_content"].exec("PASTE_HTML", [content]);
	} else if(g5_editor.indexOf('summernote')!=-1) {
		$('.summernote').summernote('pasteHTML', content);
	} else {
		var wr_html = $("#wr_content").val();
		var wr_emo = content;
		wr_html += wr_emo;
		$("#wr_content").val(wr_html);
	}
}

<?php if($GLOBALS["write_min"]||$GLOBALS["write_max"]){?>
// 글자수 제한
var char_min = parseInt(<?php echo $GLOBALS["write_min"]?>); // 최소
var char_max = parseInt(<?php echo $GLOBALS["write_max"]?>); // 최대
check_byte("wr_content", "char_count");

$(function() {
	$("#wr_content").on("keyup", function() {
		check_byte("wr_content", "char_count");
	});
});
<?php }?>

function html_auto_br(obj) {
	if (obj.checked) {
	    swal({
	        title: "자동 줄바꿈",
	        text: "자동 줄바꿈을 하시겠습니까?\n자동 줄바꿈은 게시물 내용 중 줄바뀐 곳을 <br>태그로 변환하는 기능입니다.",
	        type: "warning",
	        showCancelButton: true,
	        confirmButtonColor: "#FF9500",
	        confirmButtonText: "승인",
	        cancelButtonText: "취소",
	        closeOnConfirm: true,
	        closeOnCancel: true
	    },
	    function(isConfirm){
	        if (isConfirm) {
			    obj.value = "html2";
	        } else {
		        obj.value = "html1";
	        }
	    });
	}
	else
		obj.value = "";
}

function fwrite_submit(f) {
	<?php echo $GLOBALS["editor_js"]?> // 에디터 사용시 자바스크립트에서 내용을 폼필드로 넣어주며 내용이 입력되었는지 검사함

<?php if($GLOBALS["is_anonymous"]){?>
	var wr_1 = '<?php echo $GLOBALS["wr_1"]?>';
	if($("#anonymous").is(':checked')) {
		wr_1 = wr_1+'|y';
		$("#wr_1").val(wr_1);
	}
<?php }?>

	var subject = "";
	var content = "";
	$.ajax({
		url: g5_bbs_url+"/ajax.filter.php",
		type: "POST",
		data: {
			"subject": f.wr_subject.value,
			"content": f.wr_content.value
		},
		dataType: "json",
		async: false,
		cache: false,
		success: function(data, textStatus) {
			subject = data.subject;
			content = data.content;
		}
	});

	if (subject) {
        swal({
	        html: true,
            title: "알림!",
            text: "제목에 금지단어 '<strong class='color-red'>"+subject+"</strong>' 단어가 포함되어있습니다.",
            confirmButtonColor: "#FF9500",
            type: "warning",
            confirmButtonText: "확인"
        });
		f.wr_subject.focus();
		return false;
	}

	if (content) {
        swal({
	        html: true,
            title: "알림!",
            text: "내용에 금지단어 '<strong class='color-red'>"+content+"</strong>' 단어가 포함되어있습니다.",
            confirmButtonColor: "#FF9500",
            type: "warning",
            confirmButtonText: "확인"
        });
		if (typeof(ed_wr_content) != "undefined")
			ed_wr_content.returnFalse();
		else
			f.wr_content.focus();
		return false;
	}

	if (document.getElementById("char_count")) {
		if (char_min > 0 || char_max > 0) {
			var cnt = parseInt(check_byte("wr_content", "char_count"));
			if (char_min > 0 && char_min > cnt) {
		        swal({
			        html: true,
		            title: "알림!",
		            text: "내용은 <strong class='color-red'>"+char_min+"</strong> 글자 이상 쓰셔야 합니다.",
		            confirmButtonColor: "#FF9500",
		            type: "warning",
		            confirmButtonText: "확인"
		        });
				return false;
			}
			else if (char_max > 0 && char_max < cnt) {
		        swal({
			        html: true,
		            title: "알림!",
		            text: "내용은 <strong class='color-red'>"+char_max+"</strong> 글자 이하로 쓰셔야 합니다.",
		            confirmButtonColor: "#FF9500",
		            type: "warning",
		            confirmButtonText: "확인"
		        });
				return false;
			}
		}
	}

	<?php echo $GLOBALS["captcha_js"]?> // 캡챠 사용시 자바스크립트에서 입력된 캡챠를 검사함

	document.getElementById("btn_submit").disabled = "disabled";

	return true;
}
<?php if($TPL_VAR["eyoom"]["use_tag"]=='y'&&$TPL_VAR["eyoom_board"]["bo_use_tag"]=='1'&&$TPL_VAR["member"]["mb_level"]>=$TPL_VAR["eyoom_board"]["bo_tag_level"]){?>
var tag_size = <?php if($TPL_wr_tags_1){?><?php echo $TPL_wr_tags_1?><?php }else{?>0<?php }?>;
$(function(){
	$(".add_tags").click(function(){
		add_tags();
	});
	$("#tmp_tag").blur(function(){
		var tag = $('#tmp_tag').val();
		if(tag) add_tags();
	});

	var add_tags = function() {
		var obj = $('#tmp_tag');
		var tag = obj.val();
		if(!tag) {
			obj.focus();
		} else {
<?php if(!$GLOBALS["is_admin"]){?>
			var count = $('#tag-cloud > div:not(.blind)').length;
			var limit = '<?php echo $TPL_VAR["eyoom_board"]["bo_tag_limit"]?>';
			var max = parseInt(limit)-1;
			if(count > max) {
		        swal({
			        html: true,
		            title: "알림!",
		            text: "태그는 <strong class='color-red'>"+limit+"</strong> 개까지 등록가능합니다.",
		            confirmButtonColor: "#FF9500",
		            type: "warning",
		            confirmButtonText: "확인"
		        });
				obj.val('');
				obj.focus();
				return;
			}
<?php }?>
			var duplicate = false;
			$('#tag-cloud > div:not(.blind)').each(function(){
				if($(this).text().trim() == tag) {
					duplicate = true;
				}
			});
			if(duplicate) {
		        swal({
		            title: "알림!",
		            text: "중복된 태그입니다.",
		            confirmButtonColor: "#FF9500",
		            type: "warning",
		            confirmButtonText: "확인"
		        });
				obj.val('');
				obj.focus();
				return;
			}
			var tag_html = $('#tag-cloud').html();
			tag_html += '<div id="tag_box_'+tag_size+'">'+tag+' <i class="fa fa-close" onclick="del_tags(\''+tag+'\',\''+tag_size+'\');"></i></div>';
			$('#tag-cloud').html(tag_html);

			var add_tags = $('#wr_tag').val();
			if(add_tags) {
				add_tags += ',';
			}
			add_tags += tag;
			$('#wr_tag').val(add_tags);

			tag_size++;
			obj.val('');
			obj.focus();
		}
	}
});

function del_tags(tag, num) {
	var del_tags = $('#del_tag').val();
	if(del_tags) {
		del_tags += ',';
	}
	del_tags += tag;
	$('#del_tag').val(del_tags);
	$('#tag_box_'+num).addClass('blind');
}
<?php }?>
</script>