<?php /* Template_ 2.2.8 2018/11/26 21:04:51 /home1/bluebamus2/public_html/eyoom/theme/basic3/skin_bs/board/basic/list.skin.html 000022948 */  $this->include_("eb_nameview","eb_paging");
$TPL__bocate_1=empty($GLOBALS["bocate"])||!is_array($GLOBALS["bocate"])?0:count($GLOBALS["bocate"]);
$TPL_list_1=empty($TPL_VAR["list"])||!is_array($TPL_VAR["list"])?0:count($TPL_VAR["list"]);
$TPL__ex_sfl_1=empty($GLOBALS["ex_sfl"])||!is_array($GLOBALS["ex_sfl"])?0:count($GLOBALS["ex_sfl"]);?>
<?php if (!defined('_GNUBOARD_')) exit;
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/basic3/plugins/sly/tab_scroll_category.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/basic3/plugins/sweetalert/sweetalert.min.css" type="text/css" media="screen">',0);
?>

<style>
.board-list .eyoom-form .radio i, .board-list .eyoom-form .checkbox i {top:2px}
.board-list .table-list-eb .table thead > tr > th {border-bottom:1px solid #000;text-align:center;padding:10px 5px}
.board-list .table-list-eb .table tbody > tr > td {border-top:1px solid #ededed;padding:7px 5px}
.board-list .table-list-eb thead {border-top:2px solid #000;border-bottom:1px solid #000}
.board-list .table-list-eb th {color:#000;font-weight:bold;white-space:nowrap;font-size:13px}
.board-list .table-list-eb .td-comment {display:inline-block;white-space:nowrap;vertical-align:baseline;text-align:center;min-width:35px;padding:2px;font-size:10px;line-height:1;color:#fff;background-color:#757575;margin-right:5px}
.board-list .table-list-eb .td-comment .cnt_cmt {margin:0;font-weight:normal}
.board-list .table-list-eb .td-subject {width:300px}
.board-list .table-list-eb .td-subject a {color:#000}
.board-list .table-list-eb .td-subject a:hover {color:#005cff;text-decoration:underline}
.board-list .table-list-eb .td-subject .fa {color:#FF2900}
.board-list .table-list-eb .td-photo {display:inline-block;width:26px;height:26px;margin-right:2px;border:1px solid #e5e5e5;padding:1px;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;-webkit-border-radius:50% !important;-moz-border-radius:50% !important;border-radius:50% !important}
.board-list .table-list-eb .td-photo img {width:100%;height:auto;-webkit-border-radius:50% !important;-moz-border-radius:50% !important;border-radius:50% !important}
.board-list .table-list-eb .td-photo .td-user-icon {width:22px;height:22px;font-size:14px;line-height:22px;text-align:center;background:#959595;color:#fff;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;display:inline-block;white-space:nowrap;vertical-align:baseline;-webkit-border-radius:50% !important;-moz-border-radius:50% !important;border-radius:50% !important}
.board-list .table-list-eb .td-lv-icon {display:inline-block;margin-right:2px}
.board-list .table-list-eb .td-star-icon {display:inline-block;margin-right:2px;margin-bottom:-4px}
.board-list .table-list-eb .td-name b {font-weight:normal;font-size:12px}
.board-list .table-list-eb .td-date {text-align:center;color:#959595;font-size:12px}
.board-list .table-list-eb .td-num {text-align:center;font-size:12px}
.board-list .table-list-eb .td-mobile td {border-top:1px solid #f0f0f0;padding:3px 5px !important;font-size:11px;color:#959595;background:#fcfcfc}
.board-list .table-list-eb .td-mobile td span {margin-right:5px}
.board-list .table-list-eb .td-mobile td .td-mobile-name b {font-weight:normal}
.board-list .star-ratings-list {width:50px;margin:0 auto}
.board-list .star-ratings-list li {padding:0;float:left;margin-right:0}
.board-list .star-ratings-list li .rating {color:#a5a5a5;font-size:10px;line-height:normal}
.board-list .star-ratings-list li .rating-selected {color:#FF2900;font-size:10px}
.board-list .bo_current {color:#FF2900}
.board-list .board-notice {background:#FFF8EC}
.board-list .board-notice .td-subject a {color:#AA3510}
.board-list .board-notice .td-subject a:hover {color:#AA3510}
.board-list .board-btn-adm li {float:left;margin-right:5px}
@media (max-width:1199px) {
	.board-list .table-list-eb .td-subject {width:240px}
}
@media (max-width:767px) {
	.board-list .table-list-eb .table tbody > tr > td.td-subj-wrap {padding:15px 5px}
	.board-list .table-list-eb .td-subject {width:250px}
	.board-list .table-list-eb .td-subject .subject {font-size:13px;font-weight:bold}
}
</style>

<div class="board-list">
    <div class="board-info margin-bottom-20">
        <div class="pull-left margin-top-5 font-size-12 color-grey">
            <u>전체 <?php echo number_format($GLOBALS["total_count"])?> 건 - <?php echo $GLOBALS["page"]?> 페이지</u>
        </div>
<?php if($GLOBALS["write_href"]){?>
		<div class="pull-right">
<?php if($GLOBALS["admin_href"]){?>
	        <a href="<?php echo $GLOBALS["admin_href"]?>" class="btn-e btn-e-dark rounded" type="button">관리자</a>
<?php }?>
<?php if($GLOBALS["eyoom_href"]){?>
			<a href="<?php echo $GLOBALS["eyoom_href"]?>" class="btn-e btn-e-dark rounded" type="button">이윰설정</a>
<?php }?>
<?php if($GLOBALS["extend_href"]){?>
			<a href="<?php echo $GLOBALS["extend_href"]?>" class="btn-e btn-e-dark rounded" type="button">확장필드 (<?php echo number_format($TPL_VAR["board"]["bo_ex_cnt"])?>)</a>
<?php }?>
<?php if($GLOBALS["write_href"]){?>
	        <a href="<?php echo $GLOBALS["write_href"]?>" class="btn-e btn-e-red rounded" type="button">글쓰기</a>
<?php }?>
		</div>
<?php }?>
        <div class="clearfix"></div>
    </div>

<?php if($TPL_VAR["eyoom_board"]["bo_use_hotgul"]== 1){?>
	<div class="margin-bottom-30">
		<?php echo $TPL_VAR["latest"]->latest_hot('basic','count=5||cut_subject=30||photo=y')?>

	</div>
<?php }?>

<?php if($GLOBALS["is_category"]){?>
    <div class="tab-scroll-category">
		<div class="scrollbar">
			<div class="handle">
				<div class="mousearea"></div>
			</div>
		</div>
		<div id="tab-category">
			<div class="category-list">
				<span <?php if(!$GLOBALS["decode_sca"]){?>class="active"<?php }?>><a href="<?php echo $GLOBALS["category_href"]?>">전체분류 (<?php echo number_format($TPL_VAR["board"]["bo_count_write"])?>)</a></span>
<?php if($TPL__bocate_1){foreach($GLOBALS["bocate"] as $TPL_V1){?>
		        <span <?php if($GLOBALS["decode_sca"]==$TPL_V1["ca_name"]){?>class="active"<?php }?>><a href="<?php echo $GLOBALS["category_href"]?>&sca=<?php echo $TPL_V1["ca_sca"]?>"><?php echo $TPL_V1["ca_name"]?> (<?php echo $TPL_V1["ca_count"]?>)</a></span>
<?php }}?>
		        <span class="fake-span"></span>
			</div>
			<div class="controls">
				<button class="btn prev"><i class="fa fa-caret-left"></i></button>
				<button class="btn next"><i class="fa fa-caret-right"></i></button>
			</div>
		</div>
		<div class="tab-category-divider"></div>
	</div>
<?php }?>

<?php if($GLOBALS["is_admin"]){?>
    <form name="fboardlist" id="fboardlist" action="./board_list_update.php" onsubmit="return fboardlist_submit(this);" method="post" class="eyoom-form">
    <input type="hidden" name="bo_table" value="<?php echo $GLOBALS["bo_table"]?>">
    <input type="hidden" name="sfl" value="<?php echo $GLOBALS["sfl"]?>">
    <input type="hidden" name="stx" value="<?php echo $GLOBALS["stx"]?>">
    <input type="hidden" name="spt" value="<?php echo $GLOBALS["spt"]?>">
    <input type="hidden" name="sca" value="<?php echo $GLOBALS["sca"]?>">
    <input type="hidden" name="page" value="<?php echo $GLOBALS["page"]?>">
    <input type="hidden" name="sw" value="">
<?php }?>
	<div class="table-list-eb margin-bottom-20">
	    <div class="board-list-body">
	        <table class="table table-hover">
	            <thead>
	                <tr>
			            <th class="hidden-md hidden-sm">번호</th>
<?php if($GLOBALS["is_checkbox"]){?>
			            <th>
			                <label for="chkall" class="sound_only">현재 페이지 게시물 전체</label>
			                <label class="checkbox">
			                	<input type="checkbox" id="chkall" onclick="if (this.checked) all_checked(true); else all_checked(false);"><i></i>
			                </label>
			            </th>
<?php }?>
			            <th>제목</th>
			            <th class="hidden-xs">글쓴이</th>
			            <th class="hidden-xs"><?php echo subject_sort_link('wr_datetime',$GLOBALS["qstr2"], 1)?>날짜</a></th>
			            <th class="hidden-xs"><?php echo subject_sort_link('wr_hit',$GLOBALS["qstr2"], 1)?>뷰</a></th>
<?php if($GLOBALS["is_good"]){?>
			            <th class="hidden-xs"><?php echo subject_sort_link('wr_good',$GLOBALS["qstr2"], 1)?>추천</a></th>
<?php }?>
<?php if($GLOBALS["is_nogood"]){?>
			            <th class="hidden-xs"><?php echo subject_sort_link('wr_nogood',$GLOBALS["qstr2"], 1)?>비추</a></th>
<?php }?>
<?php if($TPL_VAR["eyoom_board"]["bo_use_rating"]=='1'&&$TPL_VAR["eyoom_board"]["bo_use_rating_list"]=='1'){?>
			            <th class="hidden-xs">별점</th>
<?php }?>
	                </tr>
	            </thead>
	            <tbody>
<?php if($TPL_list_1){foreach($TPL_VAR["list"] as $TPL_K1=>$TPL_V1){?>
			        <tr class="<?php if($TPL_V1["is_notice"]){?>board-notice<?php }?>">
			            <td class="td-num hidden-md hidden-sm">
<?php if($TPL_V1["is_notice"]){?>
							<strong class="color-red">공지</strong>
<?php }elseif($GLOBALS["wr_id"]==$TPL_V1["wr_id"]){?>
							<strong class="color-red">열람</strong>
<?php }else{?>
							<?php echo number_format($TPL_V1["num"])?>

<?php }?>
			            </td>
<?php if($GLOBALS["is_checkbox"]){?>
			            <td>
			                <label for="chk_wr_id_<?php echo $TPL_K1?>" class="sound_only"><?php echo $TPL_V1["subject"]?></label>
			                <label class="checkbox">
			                	<input type="checkbox" name="chk_wr_id[]" value="<?php echo $TPL_V1["wr_id"]?>" id="chk_wr_id_<?php echo $TPL_K1?>"><i></i>
			                </label>
			            </td>
<?php }?>
			            <td class="td-subj-wrap">
				            <div class="td-subject ellipsis">
<?php if($TPL_V1["icon_reply"]){?>
								<i class="fa fa-reply fa-rotate-180" style="margin-left:<?php echo $TPL_V1["reply"]?>px;"></i>
<?php }?>
				                <a href="<?php echo $TPL_V1["href"]?>">
<?php if($TPL_V1["comment_cnt"]){?>
					                <span class="sound_only">댓글</span><span class="td-comment">+<?php echo $TPL_V1["comment_cnt"]?></span><span class="sound_only">개</span>
<?php }?>
<?php if($GLOBALS["is_category"]&&$TPL_V1["ca_name"]){?>
									<span class="color-grey margin-right-5">[<?php echo $TPL_V1["ca_name"]?>]</span>
<?php }?>
<?php if($TPL_V1["icon_new"]){?>
									<i class="fa fa-circle margin-right-5"></i>
<?php }?>
<?php if($TPL_V1["icon_secret"]){?>
									<i class="fa fa-lock margin-right-5"></i>
<?php }?>
<?php if($TPL_V1["is_notice"]){?>
									<strong><?php echo $TPL_V1["subject"]?></strong>
<?php }elseif($GLOBALS["wr_id"]==$TPL_V1["wr_id"]){?>
									<strong><?php echo $TPL_V1["subject"]?></strong>
<?php }else{?>
					                <span class="subject"><?php echo $TPL_V1["subject"]?></span>
<?php }?>
				                </a>
				            </div>
			            </td>
			            <td class="td-name hidden-xs">
<?php if($TPL_VAR["eyoom_board"]["bo_use_profile_photo"]== 1){?>
				            <span class="td-photo">
<?php if($TPL_V1["mb_photo"]){?>
								<?php echo $TPL_V1["mb_photo"]?>

<?php }else{?>
								<span class="td-user-icon"><i class="fa fa-user"></i></span>
<?php }?>
							</span>
<?php }?>
<?php if($TPL_V1["gnu_icon"]){?>
							<span class="td-lv-icon"><img src="<?php echo $TPL_V1["gnu_icon"]?>" align="absmiddle"></span>
<?php }?>
<?php if($TPL_V1["eyoom_icon"]){?>
							<span class="td-lv-icon"><img src="<?php echo $TPL_V1["eyoom_icon"]?>" align="absmiddle"></span>
<?php }?>
							<span class="td-name-in"><?php echo eb_nameview('basic',$TPL_V1["mb_id"],$TPL_V1["wr_name"],$TPL_V1["wr_email"],$TPL_V1["homepage"])?></span>
						</td>
			            <td class="td-date hidden-xs">
<?php if($TPL_VAR["eyoom_board"]["bo_sel_date_type"]=='1'){?>
							<?php echo $TPL_VAR["eb"]->date_time('Y.m.d',$TPL_V1["wr_datetime"])?>

<?php }elseif($TPL_VAR["eyoom_board"]["bo_sel_date_type"]=='2'){?>
							<?php echo $TPL_VAR["eb"]->date_format('Y.m.d',$TPL_V1["wr_datetime"])?>

<?php }?>
						</td>
			            <td class="td-num hidden-xs"><?php echo number_format($TPL_V1["wr_hit"])?></td>
<?php if($GLOBALS["is_good"]){?>
			            <td class="td-num hidden-xs color-green"><?php echo number_format($TPL_V1["wr_good"])?></td>
<?php }?>
<?php if($GLOBALS["is_nogood"]){?>
			            <td class="td-num hidden-xs  color-yellow"><?php echo number_format($TPL_V1["wr_nogood"])?></td>
<?php }?>
<?php if($TPL_VAR["eyoom_board"]["bo_use_rating"]=='1'&&$TPL_VAR["eyoom_board"]["bo_use_rating_list"]=='1'){?>
			            <td class="hidden-xs">
				            <ul class="list-unstyled star-ratings-list">
				                <li><i class="rating<?php if($TPL_V1["star"]> 0){?>-selected fa fa-star<?php }else{?> fa fa-star-o<?php }?>"></i></li>
				                <li><i class="rating<?php if($TPL_V1["star"]> 1){?>-selected fa fa-star<?php }else{?> fa fa-star-o<?php }?>"></i></li>
				                <li><i class="rating<?php if($TPL_V1["star"]> 2){?>-selected fa fa-star<?php }else{?> fa fa-star-o<?php }?>"></i></li>
				                <li><i class="rating<?php if($TPL_V1["star"]> 3){?>-selected fa fa-star<?php }else{?> fa fa-star-o<?php }?>"></i></li>
				                <li><i class="rating<?php if($TPL_V1["star"]> 4){?>-selected fa fa-star<?php }else{?> fa fa-star-o<?php }?>"></i></li>
				            </ul>
			            </td>
<?php }?>
			        </tr>
			        <tr class="td-mobile visible-xs">				        <td colspan="<?php echo $TPL_VAR["colspan"]?>">
<?php if($TPL_VAR["eyoom_board"]["bo_use_profile_photo"]== 1){?>
				            <span class="td-photo">
<?php if($TPL_V1["mb_photo"]){?><?php echo $TPL_V1["mb_photo"]?><?php }else{?><span class="td-user-icon"><i class="fa fa-user"></i></span><?php }?>
							</span>
<?php }?>
<?php if($TPL_V1["gnu_icon"]){?>
							<span class="td-lv-icon"><img src="<?php echo $TPL_V1["gnu_icon"]?>" align="absmiddle"></span>
<?php }?>
<?php if($TPL_V1["eyoom_icon"]){?>
							<span class="td-lv-icon"><img src="<?php echo $TPL_V1["eyoom_icon"]?>" align="absmiddle"></span>
<?php }?>
							<span class="td-mobile-name"><?php echo eb_nameview('basic',$TPL_V1["mb_id"],$TPL_V1["wr_name"],$TPL_V1["wr_email"],$TPL_V1["homepage"])?></span>
<?php if($TPL_VAR["eyoom_board"]["bo_sel_date_type"]=='1'){?>
							<span><i class="fa fa-clock-o"></i> <?php echo $TPL_VAR["eb"]->date_time('Y.m.d',$TPL_V1["wr_datetime"])?></span>
<?php }elseif($TPL_VAR["eyoom_board"]["bo_sel_date_type"]=='2'){?>
							<span><i class="fa fa-clock-o"></i> <?php echo $TPL_VAR["eb"]->date_format('Y.m.d',$TPL_V1["wr_datetime"])?></span>
<?php }?>
				            <span><i class="fa fa-eye"></i> <?php echo number_format($TPL_V1["wr_hit"])?></span>
<?php if($GLOBALS["is_good"]){?>
				            <span><i class="fa fa-thumbs-up"></i> <?php echo number_format($TPL_V1["wr_good"])?></span>
<?php }?>
<?php if($GLOBALS["is_nogood"]){?>
				            <span><i class="fa fa-thumbs-down"></i> <?php echo number_format($TPL_V1["wr_nogood"])?></span>
<?php }?>
				        </td>
			        </tr>
<?php }}else{?>
					<tr>
						<td colspan="<?php echo $TPL_VAR["colspan"]?>" class="text-center"><span class="color-grey"><i class="fa fa-exclamation-circle"></i> 게시물이 없습니다.</span></td>
					</tr>
<?php }?>
	            </tbody>
	        </table>
	    </div>
	</div>
    <div class="board-list-footer">
	    <div class="pull-left">
<?php if($GLOBALS["is_checkbox"]){?>
	        <ul class="list-unstyled board-btn-adm pull-left">
	            <li><button class="btn-e btn-e-default rounded" type="submit" name="btn_submit" value="선택삭제" onclick="document.pressed=this.value">선택삭제</button></li>
	            <li><button class="btn-e btn-e-default rounded" type="submit" name="btn_submit" value="선택복사" onclick="document.pressed=this.value">선택복사</button></li>
	            <li><button class="btn-e btn-e-default rounded" type="submit" name="btn_submit" value="선택이동" onclick="document.pressed=this.value">선택이동</button></li>
	        </ul>
<?php }?>
	        <span class="pull-left">
<?php if($GLOBALS["rss_href"]){?>
	        	<a href="<?php echo $GLOBALS["rss_href"]?>" class="btn-e btn-e-yellow rounded" type="button"><i class="fa fa-rss"></i></a>
<?php }?>
				<a class="btn-e btn-e-dark rounded" type="button" data-toggle="modal" data-target=".search-modal"><i class="fa fa-search"></i></a>
	        </span>
	    </div>
	    <div class="pull-right">
<?php if($GLOBALS["list_href"]||$GLOBALS["write_href"]){?>
	        <ul class="list-unstyled">
<?php if($GLOBALS["write_href"]){?>
	            <li><a href="<?php echo $GLOBALS["write_href"]?>" class="btn-e btn-e-red rounded" type="button">글쓰기</a></li>
<?php }?>
	        </ul>
<?php }?>
	    </div>
	    <div class="clearfix"></div>
	</div>
<?php if($GLOBALS["is_admin"]){?>
	</form>
<?php }?>
</div>

<div class="modal fade search-modal" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
				<h5 class="modal-title"><i class="fa fa-search color-grey"></i> <strong><?php echo $TPL_VAR["board"]["bo_subject"]?> 검색</strong></h5>
			</div>
			<div class="modal-body">
				<fieldset id="bo_sch" class="eyoom-form">
					<!--legend>게시물 검색</legend-->
				    <form name="fsearch" method="get">
				    <input type="hidden" name="bo_table" value="<?php echo $GLOBALS["bo_table"]?>">
				    <input type="hidden" name="sca" value="<?php echo $GLOBALS["sca"]?>">
				    <input type="hidden" name="sop" value="and">
				    <label for="sfl" class="sound_only">검색대상</label>
				    <section class="margin-top-10">
					    <label class="select">
						    <select name="sfl" id="sfl" class="form-control">
						        <option value="wr_subject"<?php echo get_selected($GLOBALS["sfl"],'wr_subject',true)?>>제목</option>
						        <option value="wr_content"<?php echo get_selected($GLOBALS["sfl"],'wr_content')?>>내용</option>
						        <option value="wr_subject||wr_content"<?php echo get_selected($GLOBALS["sfl"],'wr_subject||wr_content')?>>제목+내용</option>
						        <option value="mb_id,1"<?php echo get_selected($GLOBALS["sfl"],'mb_id,1')?>>회원아이디</option>
						        <option value="mb_id,0"<?php echo get_selected($GLOBALS["sfl"],'mb_id,0')?>>회원아이디(코)</option>
						        <option value="wr_name,1"<?php echo get_selected($GLOBALS["sfl"],'wr_name,1')?>>글쓴이</option>
						        <option value="wr_name,0"<?php echo get_selected($GLOBALS["sfl"],'wr_name,0')?>>글쓴이(코)</option>
<?php if($TPL__ex_sfl_1){foreach($GLOBALS["ex_sfl"] as $TPL_K1=>$TPL_V1){?>
						        <option value="<?php echo $TPL_K1?>"<?php echo get_selected($GLOBALS["sfl"],$TPL_K1,true)?>><?php echo $TPL_V1?></option>
<?php }}?>
						    </select>
						    <i></i>
					    </label>
				    </section>
				    <section>
				        <label for="stx" class="sound_only">검색어<strong class="sound_only"> 필수</strong></label>
				        <div class="input input-button">
				        	<input type="text" name="stx" value="<?php echo stripslashes($GLOBALS["stx"])?>" required id="stx">
				        	<div class="button"><input type="submit" value="검색">검색</div>
				        </div>
				    </section>
				    </form>
				</fieldset>
			</div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn-e btn-e-lg btn-e-dark rounded" type="button"><i class="fa fa-close"></i> 닫기</button>
            </div>
		</div>
	</div>
</div>
<iframe name="photoframe" id="photoframe" style="display:none;"></iframe>

<?php if($GLOBALS["is_checkbox"]){?>
<noscript>
<p>자바스크립트를 사용하지 않는 경우<br>별도의 확인 절차 없이 바로 선택삭제 처리하므로 주의하시기 바랍니다.</p>
</noscript>
<?php }?>

<?php echo eb_paging('basic')?>


<script src="/eyoom/theme/basic3/plugins/sweetalert/sweetalert.min.js"></script>
<?php if($GLOBALS["is_category"]){?>
<script src="/eyoom/theme/basic3/plugins/sly/vendor_plugins.min.js"></script>
<script src="/eyoom/theme/basic3/plugins/sly/sly.min.js"></script>
<script>
$(function() {
	var $frame = $('#tab-category');
	var $wrap  = $frame.parent();
	$frame.sly({
		horizontal: 1,
		itemNav: 'centered',
		smart: 1,
		activateOn: 'click',
		mouseDragging: 1,
		touchDragging: 1,
		releaseSwing: 1,
		scrollBar: $wrap.find('.scrollbar'),
		scrollBy: 1,
		speed: 300,
		elasticBounds: 1,
		easing: 'easeOutExpo',
		dragHandle: 1,
		dynamicHandle: 1,
		clickBar: 1,
		prev: $wrap.find('.prev'),
		next: $wrap.find('.next')
	});
	var tabWidth = $('#tab-category').width();
	var categoryWidth = $('.category-list').width();
	if (tabWidth < categoryWidth) {
		$('.controls').show();
	}
});
</script>
<?php }?>
<?php if($GLOBALS["is_checkbox"]){?>
<script>
function all_checked(sw) {
    var f = document.fboardlist;
    for (var i=0; i<f.length; i++) {
        if (f.elements[i].name == "chk_wr_id[]")
            f.elements[i].checked = sw;
    }
}

function fboardlist_submit(f) {
    var chk_count = 0;
    for (var i=0; i<f.length; i++) {
        if (f.elements[i].name == "chk_wr_id[]" && f.elements[i].checked)
            chk_count++;
    }
    if (!chk_count) {
        swal({
	        html: true,
            title: "Oops...",
            text: "<strong class='color-red'>" + document.pressed + "</strong> 할 게시물을 하나 이상 선택하세요.",
            confirmButtonColor: "#FF2900",
            type: "error",
            confirmButtonText: "확인"
        });
        return false;
    }
    if(document.pressed == "선택복사") {
        select_copy("copy");
        return;
    }
    if(document.pressed == "선택이동") {
        select_copy("move");
        return;
    }
    if(document.pressed == "선택삭제") {
        if (!confirm("선택한 게시물을 정말 삭제하시겠습니까?\n\n한번 삭제한 자료는 복구할 수 없습니다\n\n답변글이 있는 게시글을 선택하신 경우\n답변글도 선택하셔야 게시글이 삭제됩니다."))
            return false;
        f.removeAttribute("target");
        f.action = "./board_list_update.php";
    }
    return true;
}

// 선택한 게시물 복사 및 이동
function select_copy(sw) {
    var f = document.fboardlist;
    if (sw == "copy")
        str = "복사";
    else
        str = "이동";

    var sub_win = window.open("", "move", "left=50, top=50, width=500, height=550, scrollbars=1");
    f.sw.value = sw;
    f.target = "move";
    f.action = "./move.php";
    f.submit();
}
</script>
<?php }?>