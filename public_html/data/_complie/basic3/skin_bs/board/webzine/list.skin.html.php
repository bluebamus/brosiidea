<?php /* Template_ 2.2.8 2018/01/25 01:09:46 /home/bluebamus1/public_html/eyoom/theme/basic3/skin_bs/board/webzine/list.skin.html 000027348 */  $this->include_("eb_nameview","eb_paging");
$TPL__bocate_1=empty($GLOBALS["bocate"])||!is_array($GLOBALS["bocate"])?0:count($GLOBALS["bocate"]);
$TPL_list_1=empty($TPL_VAR["list"])||!is_array($TPL_VAR["list"])?0:count($TPL_VAR["list"]);
$TPL__ex_sfl_1=empty($GLOBALS["ex_sfl"])||!is_array($GLOBALS["ex_sfl"])?0:count($GLOBALS["ex_sfl"]);?>
<?php if (!defined('_GNUBOARD_')) exit;
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/basic3/plugins/sly/tab_scroll_category.css" type="text/css" media="screen">',0);
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/basic3/plugins/sweetalert/sweetalert.min.css" type="text/css" media="screen">',0);
?>

<style>
.board-list .bo_current {color:#FF2900}
.board-list .board-btn-adm li {float:left;margin-right:5px}
.board-list .board-list-footer {margin-top:20px}
.board-webzine .webzine-item {position:relative;font-size:12px;margin-bottom:20px;border-top:1px solid #d5d5d5;background:#fff;width:100%}
.board-webzine .webzine-item-heading {position:relative;padding:6px 0;border-bottom:1px solid #e5e5e5;background:#f8f8f8;-webkit-border-radius:2px 2px 0 0 !important;-moz-border-radius:2px 2px 0 0 !important;border-radius:2px 2px 0 0 !important}
.board-webzine .webzine-item-heading .webzine-photo {display:inline-block;width:26px;height:26px;margin-right:2px;border:1px solid #e5e5e5;padding:1px;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;-webkit-border-radius:50% !important;-moz-border-radius:50% !important;border-radius:50% !important}
.board-webzine .webzine-item-heading .webzine-photo img {width:100%;height:auto;-webkit-border-radius:50% !important;-moz-border-radius:50% !important;border-radius:50% !important}
.board-webzine .webzine-item-heading .webzine-photo .webzine-user-icon {width:22px;height:22px;font-size:14px;line-height:22px;text-align:center;background:#959595;color:#fff;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;display:inline-block;white-space:nowrap;vertical-align:baseline;-webkit-border-radius:50% !important;-moz-border-radius:50% !important;border-radius:50% !important}
.board-webzine .webzine-item-heading .webzine-lv-icon {display:inline-block;margin-left:2px}
.board-webzine .webzine-item-heading .webzine-date {margin-left:7px;font-size:12px}
.board-webzine .webzine-item-heading .webzine-date i {color:#959595}
.board-webzine .webzine-item-heading .wezine-cate {display:inline-block;position:absolute;top:10px;right:10px;color:#959595;padding:1px 5px;background:#f8f8f8}
.board-webzine .webzine-item-body {position:relative;overflow:hidden;padding:15px 0;min-height:80px;box-sizing:content-box}
.board-webzine .webzine-item-body .webzine-img {position:absolute;top:15px;left:0;width:150px;z-index:1}
.board-webzine .webzine-item-body .webzine-desc {position:relative}
.board-webzine .webzine-item-body .webzine-noimg-desc {position:relative}
.board-webzine .webzine-item-body .webzine-img-box {position:relative;overflow:hidden;max-height:150px}
.board-webzine .webzine-item-body .webzine-img-box-in {position:relative;overflow:hidden}
.board-webzine .webzine-item-body .webzine-img-box-in:after {content:"";text-align:center;position:absolute;display:block;left:0;top:0;opacity:0;-moz-transition:all 0.2s ease 0s;-webkit-transition:all 0.2s ease 0s;-ms-transition:all 0.2s ease 0s;-o-transition:all 0.2s ease 0s;transition:all 0.2s ease 0s;width:100%;height:100%;background:rgba(0,0,0,0.3)}
.board-webzine .webzine-item-body .webzine-img-box-in .movie-icon {display:inline-block;position:absolute;top:7px;left:7px;color:#e5e5e5;font-size:26px;line-height:1;z-index:1}
.board-webzine .webzine-item:hover .webzine-img-box-in:after {opacity:1}
.board-webzine .webzine-item-body h4 {font-size:15px;color:#000;margin-left:165px;margin-top:0}
.board-webzine .webzine-item:hover .webzine-item-body h4 {text-decoration:underline;color:#005cff}
.board-webzine .webzine-item-body .webzine-checkbox {display:inline-block;position:absolute;top:-6px;right:-5px;z-index:1}
.board-webzine .webzine-item-body .webzine-cont {position:relative;font-weight:300;color:#757575;margin-left:165px}
.board-webzine .webzine-item-body .webzine-info {position:relative;border-top:1px solid #f2f2f2;padding-top:7px;margin-left:165px}
.board-webzine .webzine-item-body .webzine-info span {color:#959595;font-size:11px}
.board-webzine .webzine-item-body .webzine-info span i {margin-right:5px}
.board-webzine .webzine-item-body .webzine-info strong {font-weight:normal;margin-right:10px}
.board-webzine .webzine-item-body .webzine-noimg-desc h4 {margin-left:0}
.board-webzine .webzine-item-body .webzine-noimg-desc .webzine-cont {margin-left:0}
.board-webzine .webzine-item-body .webzine-noimg-desc .webzine-info {margin-left:0}
.board-webzine .webzine-ratings {position:absolute;top:6px;right:0;width:53px;height:18px;background:#fff}
.board-webzine .webzine-ratings .star-ratings-list li {padding:0;float:left;margin-right:0}
.board-webzine .webzine-ratings .star-ratings-list li .rating {color:#a5a5a5;font-size:11px;line-height:normal}
.board-webzine .webzine-ratings .star-ratings-list li .rating-selected {color:#FF2900;font-size:11px}
.board-webzine .webzine-item-notice {position:relative;overflow:hidden;border:1px solid #e5e5e5;background:#fff;padding:8px 10px;margin-bottom:30px;-webkit-border-radius:2px !important;-moz-border-radius:2px !important;border-radius:2px !important}
.board-webzine .webzine-item-notice .label {font-size:11px;font-weight:normal;margin-bottom:0}
#infscr-loading {text-align:center;z-index:100;position:absolute;left:50%;bottom:0;width:200px;margin-left:-100px;padding:10px;background:#000;opacity:0.6;color:#fff}
.board-list .view-infinite-more {margin-top:20px;margin-bottom:40px}
.board-list .view-infinite-more .btn-e-lg {padding:10px 50px;font-size:17px;font-weight:bold;border:1px solid #959595;color:#000}
@media (max-width: 550px) {
	.board-webzine .webzine-item-body .webzine-img {top:43px;left:0;width:100px}
	.board-webzine .webzine-item-body .webzine-img-box {max-height:72px}
	.board-webzine .webzine-item-body .webzine-img-box-in .movie-icon {font-size:20px}
	.board-webzine .webzine-item-body h4 {margin-left:0}
	.board-webzine .webzine-item-body .webzine-cont {margin-left:115px;height:72px;font-size:12px;overflow:hidden;margin-bottom:15px}
	.board-webzine .webzine-item-body .webzine-info {margin-left:0;padding-top:10px}
	.board-webzine .webzine-item-body .webzine-noimg-desc .webzine-cont {height:70px;font-size:12px;overflow:hidden}
}
@media (min-width: 768px) {
	.board-view-modal {width:750px;margin:10px auto}
	.board-view-modal .modal-header, .board-view-modal .modal-body, .board-view-modal .modal-footer {padding:10px 20px}
}
@media (min-width: 992px) {
	.board-view-modal {width:970px}
}
@media (min-width: 1200px) {
	.board-view-modal {width:1170px}
}
</style>

<div class="board-list">
    <div class="board-info margin-bottom-20">
        <div class="pull-left margin-top-5 font-size-12 color-grey">
            <u>전체 <?php echo number_format($GLOBALS["total_count"])?> 건 - <?php echo $GLOBALS["page"]?> 페이지</u>
        </div>
<?php if($GLOBALS["write_href"]){?>
		<div class="pull-right">
<?php if($GLOBALS["admin_href"]){?>
	        <a href="<?php echo $GLOBALS["admin_href"]?>" class="btn-e btn-e-dark rounded" type="button">관리자</a>
<?php }?>
<?php if($GLOBALS["eyoom_href"]){?>
			<a href="<?php echo $GLOBALS["eyoom_href"]?>" class="btn-e btn-e-dark rounded" type="button">이윰설정</a>
<?php }?>
<?php if($GLOBALS["write_href"]){?>
	        <a href="<?php echo $GLOBALS["write_href"]?>" class="btn-e btn-e-red rounded" type="button">글쓰기</a>
<?php }?>
		</div>
<?php }?>
        <div class="clearfix"></div>
    </div>

<?php if($TPL_VAR["eyoom_board"]["bo_use_hotgul"]== 1){?>
	<div class="margin-bottom-30">
		<?php echo $TPL_VAR["latest"]->latest_hot('basic','count=5||cut_subject=30||photo=y')?>

	</div>
<?php }?>

<?php if($GLOBALS["is_category"]){?>
    <div class="tab-scroll-category">
		<div class="scrollbar">
			<div class="handle">
				<div class="mousearea"></div>
			</div>
		</div>
		<div id="tab-category">
			<div class="category-list">
				<span <?php if(!$GLOBALS["decode_sca"]){?>class="active"<?php }?>><a href="<?php echo $GLOBALS["category_href"]?>">전체분류 (<?php echo number_format($TPL_VAR["board"]["bo_count_write"])?>)</a></span>
<?php if($TPL__bocate_1){foreach($GLOBALS["bocate"] as $TPL_V1){?>
		        <span <?php if($GLOBALS["decode_sca"]==$TPL_V1["ca_name"]){?>class="active"<?php }?>><a href="<?php echo $GLOBALS["category_href"]?>&sca=<?php echo $TPL_V1["ca_sca"]?>"><?php echo $TPL_V1["ca_name"]?> (<?php echo $TPL_V1["ca_count"]?>)</a></span>
<?php }}?>
		        <span class="fake-span"></span>
			</div>
			<div class="controls">
				<button class="btn prev"><i class="fa fa-caret-left"></i></button>
				<button class="btn next"><i class="fa fa-caret-right"></i></button>
			</div>
		</div>
		<div class="tab-category-divider"></div>
	</div>
<?php }?>

<?php if($GLOBALS["is_admin"]){?>
    <form name="fboardlist" id="fboardlist" action="./board_list_update.php" onsubmit="return fboardlist_submit(this);" method="post" class="eyoom-form">
    <input type="hidden" name="bo_table" value="<?php echo $GLOBALS["bo_table"]?>">
    <input type="hidden" name="sfl" value="<?php echo $GLOBALS["sfl"]?>">
    <input type="hidden" name="stx" value="<?php echo $GLOBALS["stx"]?>">
    <input type="hidden" name="spt" value="<?php echo $GLOBALS["spt"]?>">
    <input type="hidden" name="sca" value="<?php echo $GLOBALS["sca"]?>">
    <input type="hidden" name="page" value="<?php echo $GLOBALS["page"]?>">
    <input type="hidden" name="sw" value="">
<?php }?>
	<div class="margin-bottom-20">
<?php if($GLOBALS["is_checkbox"]){?>
        <ul class="list-unstyled board-btn-adm pull-left">
            <li><button class="btn-e btn-e-default rounded" type="submit" name="btn_submit" value="선택삭제" onclick="document.pressed=this.value">선택삭제</button></li>
            <li><button class="btn-e btn-e-default rounded" type="submit" name="btn_submit" value="선택복사" onclick="document.pressed=this.value">선택복사</button></li>
            <li><button class="btn-e btn-e-default rounded" type="submit" name="btn_submit" value="선택이동" onclick="document.pressed=this.value">선택이동</button></li>
        </ul>
<?php }?>
        <span class="pull-left">
<?php if($GLOBALS["rss_href"]){?>
        	<a href="<?php echo $GLOBALS["rss_href"]?>" class="btn-e btn-e-yellow rounded" type="button"><i class="fa fa-rss"></i></a>
<?php }?>
			<a class="btn-e btn-e-dark rounded" type="button" data-toggle="modal" data-target=".search-modal"><i class="fa fa-search"></i></a>
        </span>
	    <div class="clearfix"></div>
	</div>
<?php if($GLOBALS["is_checkbox"]){?>
    <div class="margin-bottom-15">
        <label class="checkbox"><input type="checkbox" id="chkall" onclick="if (this.checked) all_checked(true); else all_checked(false);"><i></i>현재 페이지 게시물 전체선택</label>
    </div>
<?php }?>
	<div class="board-webzine">
<?php if($TPL_list_1){foreach($TPL_VAR["list"] as $TPL_K1=>$TPL_V1){?>
<?php if($TPL_V1["is_notice"]){?>
		<div class="webzine-item-notice">
			<div class="ellipsis">
				<span class="label label-dark color-white margin-right-5">공지</span><a href="<?php echo $TPL_V1["href"]?>" <?php if($TPL_VAR["wmode"]){?>onclick="eb_modal(this.href); return false;"<?php }?>><?php echo $TPL_V1["subject"]?></a>
			</div>
		</div>
<?php }else{?>
		<div class="webzine-item">
		    <div class="webzine-item-heading">
<?php if($TPL_VAR["eyoom_board"]["bo_use_profile_photo"]== 1){?>
	            <span class="webzine-photo">
<?php if($TPL_V1["mb_photo"]){?>
					<?php echo $TPL_V1["mb_photo"]?>

<?php }else{?>
					<span class="webzine-user-icon"><i class="fa fa-user"></i></span>
<?php }?>
				</span>
<?php }?>
				<span><?php echo eb_nameview('basic',$TPL_V1["mb_id"],$TPL_V1["wr_name"],$TPL_V1["wr_email"],$TPL_V1["homepage"])?></span>
<?php if($TPL_V1["gnu_icon"]){?>
				<span class="webzine-lv-icon"><img src="<?php echo $TPL_V1["gnu_icon"]?>"></span>
<?php }?>
<?php if($TPL_V1["eyoom_icon"]){?>
				<span class="webzine-lv-icon"><img src="<?php echo $TPL_V1["eyoom_icon"]?>"></span>
<?php }?>
				<span class="webzine-date">
<?php if($TPL_VAR["eyoom_board"]["bo_sel_date_type"]=='1'){?>
					<i class="fa fa-clock-o"></i> <?php echo $TPL_VAR["eb"]->date_time('Y.m.d',$TPL_V1["wr_datetime"])?>

<?php }elseif($TPL_VAR["eyoom_board"]["bo_sel_date_type"]=='2'){?>
					<i class="fa fa-clock-o"></i> <?php echo $TPL_VAR["eb"]->date_format('Y.m.d',$TPL_V1["wr_datetime"])?>

<?php }?>
				</span>
<?php if($GLOBALS["is_category"]&&$TPL_V1["ca_name"]){?>
				<span class="wezine-cate"><?php echo $TPL_V1["ca_name"]?></span>
<?php }?>
		    </div>
		    <div class="webzine-item-body">
<?php if($TPL_V1["img_content"]&&!preg_match('/no image/',$TPL_V1["img_content"])){?>
			    <div class="webzine-img">
				    <a href="<?php echo $TPL_V1["href"]?>" <?php if($TPL_VAR["wmode"]){?>onclick="eb_modal(this.href); return false;"<?php }?>>
					    <div class="webzine-img-box">
						    <div class="webzine-img-box-in">
								<?php echo $TPL_V1["img_content"]?>

<?php if($TPL_V1["is_video"]){?>
								<span class="movie-icon"><i class="fa fa-play-circle-o"></i></span>
<?php }?>
				        	</div>
				        </div>
			        </a>
			    </div>
			    <div class="webzine-desc">
<?php }else{?>
			    <div class="webzine-noimg-desc">
<?php }?>
					<h4 class="ellipsis">
						<a href="<?php echo $TPL_V1["href"]?>" <?php if($TPL_VAR["wmode"]){?>onclick="eb_modal(this.href); return false;"<?php }?>>
<?php if($GLOBALS["wr_id"]==$TPL_V1["wr_id"]){?>
							<strong><span class="color-red margin-right-5">열람중</span><?php echo $TPL_V1["subject"]?></strong>
<?php }else{?>
			                <strong><?php echo $TPL_V1["subject"]?></strong>
<?php }?>
		                </a>
<?php if($GLOBALS["is_checkbox"]){?>
						<span class="webzine-checkbox">
			                <label for="chk_wr_id_<?php echo $TPL_K1?>" class="sound_only"><?php echo $TPL_V1["subject"]?></label>
			                <label class="checkbox">
			                	<input type="checkbox" name="chk_wr_id[]" value="<?php echo $TPL_V1["wr_id"]?>" id="chk_wr_id_<?php echo $TPL_K1?>"><i></i>
			                </label>
						</span>
<?php }?>
					</h4>
					<p class="webzine-cont"><?php echo $TPL_V1["content"]?></p>
					<div class="webzine-info">
						<span><i class="fa fa-eye"></i><strong class="color-black"><?php echo number_format($TPL_V1["wr_hit"])?></strong></span>
<?php if($TPL_V1["wr_comment"]> 0){?>
						<span><i class="fa fa-comments-o"></i><strong class="color-red"><?php echo number_format($TPL_V1["wr_comment"])?></strong></span>
<?php }?>
<?php if($GLOBALS["is_good"]&&($TPL_V1["wr_good"]> 0)){?>
			            <span><i class="fa fa-thumbs-up"></i><strong class="color-green"><?php echo number_format($TPL_V1["wr_good"])?></strong></span>
<?php }?>
<?php if($GLOBALS["is_nogood"]&&($TPL_V1["wr_nogood"]> 0)){?>
			            <span><i class="fa fa-thumbs-down"></i><strong class="color-brown"><?php echo number_format($TPL_V1["wr_nogood"])?></strong></span>
<?php }?>

<?php if($TPL_VAR["eyoom_board"]["bo_use_rating"]=='1'&&$TPL_VAR["eyoom_board"]["bo_use_rating_list"]=='1'){?>
						<div class="webzine-ratings">
				            <ul class="list-unstyled star-ratings-list">
				                <li><i class="rating<?php if($TPL_V1["star"]> 0){?>-selected fa fa-star<?php }else{?> fa fa-star-o<?php }?>"></i></li>
				                <li><i class="rating<?php if($TPL_V1["star"]> 1){?>-selected fa fa-star<?php }else{?> fa fa-star-o<?php }?>"></i></li>
				                <li><i class="rating<?php if($TPL_V1["star"]> 2){?>-selected fa fa-star<?php }else{?> fa fa-star-o<?php }?>"></i></li>
				                <li><i class="rating<?php if($TPL_V1["star"]> 3){?>-selected fa fa-star<?php }else{?> fa fa-star-o<?php }?>"></i></li>
				                <li><i class="rating<?php if($TPL_V1["star"]> 4){?>-selected fa fa-star<?php }else{?> fa fa-star-o<?php }?>"></i></li>
				            </ul>
						</div>
<?php }?>
					</div>
			    </div>
		    </div>
		</div>
<?php }?>
<?php }}else{?>
		<div class="text-center color-grey font-size-14"><i class="fa fa-exclamation-circle"></i> 게시물이 없습니다.</div>
<?php }?>
	</div>
<?php if($TPL_VAR["list"]&&$TPL_VAR["eyoom_board"]["bo_use_infinite_scroll"]=='1'){?>
	<div class="view-infinite-more text-center">
		<a id="view-infinite-more" href="#" class="btn btn-default btn-e-lg">더 보기 <i class="fa fa-arrow-circle-o-down"></i></a>
	</div>
<?php }?>
    <div class="board-list-footer">
	    <div class="pull-left">
<?php if($GLOBALS["is_checkbox"]){?>
	        <ul class="list-unstyled board-btn-adm pull-left">
	            <li><button class="btn-e btn-e-default rounded" type="submit" name="btn_submit" value="선택삭제" onclick="document.pressed=this.value">선택삭제</button></li>
	            <li><button class="btn-e btn-e-default rounded" type="submit" name="btn_submit" value="선택복사" onclick="document.pressed=this.value">선택복사</button></li>
	            <li><button class="btn-e btn-e-default rounded" type="submit" name="btn_submit" value="선택이동" onclick="document.pressed=this.value">선택이동</button></li>
	        </ul>
<?php }?>
	        <span class="pull-left">
<?php if($GLOBALS["rss_href"]){?>
	        	<a href="<?php echo $GLOBALS["rss_href"]?>" class="btn-e btn-e-yellow rounded" type="button"><i class="fa fa-rss"></i></a>
<?php }?>
				<a class="btn-e btn-e-dark rounded" type="button" data-toggle="modal" data-target=".search-modal"><i class="fa fa-search"></i></a>
	        </span>
	    </div>
	    <div class="pull-right">
<?php if($GLOBALS["list_href"]||$GLOBALS["write_href"]){?>
	        <ul class="list-unstyled">
<?php if($GLOBALS["write_href"]){?>
	            <li><a href="<?php echo $GLOBALS["write_href"]?>" class="btn-e btn-e-red rounded" type="button">글쓰기</a></li>
<?php }?>
	        </ul>
<?php }?>
	    </div>
	    <div class="clearfix"></div>
	</div>
<?php if($GLOBALS["is_admin"]){?>
	</form>
<?php }?>
</div>

<div class="modal fade search-modal" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
				<h5 class="modal-title"><i class="fa fa-search color-grey"></i> <strong><?php echo $TPL_VAR["board"]["bo_subject"]?> 검색</strong></h5>
			</div>
			<div class="modal-body">
				<fieldset id="bo_sch" class="eyoom-form">
					<!--legend>게시물 검색</legend-->
				    <form name="fsearch" method="get">
				    <input type="hidden" name="bo_table" value="<?php echo $GLOBALS["bo_table"]?>">
				    <input type="hidden" name="sca" value="<?php echo $GLOBALS["sca"]?>">
				    <input type="hidden" name="sop" value="and">
				    <label for="sfl" class="sound_only">검색대상</label>
				    <section class="margin-top-10">
					    <label class="select">
						    <select name="sfl" id="sfl" class="form-control">
						        <option value="wr_subject"<?php echo get_selected($GLOBALS["sfl"],'wr_subject',true)?>>제목</option>
						        <option value="wr_content"<?php echo get_selected($GLOBALS["sfl"],'wr_content')?>>내용</option>
						        <option value="wr_subject||wr_content"<?php echo get_selected($GLOBALS["sfl"],'wr_subject||wr_content')?>>제목+내용</option>
						        <option value="mb_id,1"<?php echo get_selected($GLOBALS["sfl"],'mb_id,1')?>>회원아이디</option>
						        <option value="mb_id,0"<?php echo get_selected($GLOBALS["sfl"],'mb_id,0')?>>회원아이디(코)</option>
						        <option value="wr_name,1"<?php echo get_selected($GLOBALS["sfl"],'wr_name,1')?>>글쓴이</option>
						        <option value="wr_name,0"<?php echo get_selected($GLOBALS["sfl"],'wr_name,0')?>>글쓴이(코)</option>
<?php if($TPL__ex_sfl_1){foreach($GLOBALS["ex_sfl"] as $TPL_K1=>$TPL_V1){?>
						        <option value="<?php echo $TPL_K1?>"<?php echo get_selected($GLOBALS["sfl"],$TPL_K1,true)?>><?php echo $TPL_V1?></option>
<?php }}?>
						    </select>
						    <i></i>
					    </label>
				    </section>
				    <section>
				        <label for="stx" class="sound_only">검색어<strong class="sound_only"> 필수</strong></label>
				        <div class="input input-button">
				        	<input type="text" name="stx" value="<?php echo stripslashes($GLOBALS["stx"])?>" required id="stx">
				        	<div class="button"><input type="submit" value="검색">검색</div>
				        </div>
				    </section>
				    </form>
				</fieldset>
			</div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn-e btn-e-lg btn-e-dark rounded" type="button"><i class="fa fa-close"></i> 닫기</button>
            </div>
		</div>
	</div>
</div>
<iframe name="photoframe" id="photoframe" style="display:none;"></iframe>

<?php if($TPL_VAR["wmode"]){?>
<div class="modal fade view-iframe-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog board-view-modal">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title"><strong><i class="fa fa-search"></i> 게시글 상세 보기</strong></h4>
            </div>
            <div class="modal-body">
                <iframe id="view-iframe" width="100%" frameborder="0"></iframe>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn-e btn-e-lg btn-e-dark" type="button">닫기</button>
            </div>
        </div>
    </div>
</div>
<?php }?>

<?php if($GLOBALS["is_checkbox"]){?>
<noscript>
<p>자바스크립트를 사용하지 않는 경우<br>별도의 확인 절차 없이 바로 선택삭제 처리하므로 주의하시기 바랍니다.</p>
</noscript>
<?php }?>

<div class="board-pagination">
<?php if($TPL_VAR["eyoom_board"]["bo_use_infinite_scroll"]!='1'){?>
	<?php echo eb_paging('basic')?>

<?php }else{?>
	<div id="infinite_pagination">
	    <a class="next" href="<?php echo G5_BBS_URL?>/board.php?bo_table=<?php echo $GLOBALS["bo_table"]?>&sca=<?php echo $GLOBALS["sca"]?>&page=<?php echo $TPL_VAR["page"]+ 1?>"></a>
	</div>
<?php }?>
</div>

<script src="/eyoom/theme/basic3/plugins/sweetalert/sweetalert.min.js"></script>
<script src="/eyoom/theme/basic3/plugins/masonry/jquery.masonry.min.js"></script>
<?php if($TPL_VAR["eyoom_board"]["bo_use_infinite_scroll"]=='1'){?>
<script src="/eyoom/theme/basic3/plugins/infinite-scroll/jquery.infinitescroll.min.js"></script>
<?php }?>
<?php if($GLOBALS["is_category"]){?>
<script src="/eyoom/theme/basic3/plugins/sly/vendor_plugins.min.js"></script>
<script src="/eyoom/theme/basic3/plugins/sly/sly.min.js"></script>
<script>
$(function() {
	var $frame = $('#tab-category');
	var $wrap  = $frame.parent();
	$frame.sly({
		horizontal: 1,
		itemNav: 'centered',
		smart: 1,
		activateOn: 'click',
		mouseDragging: 1,
		touchDragging: 1,
		releaseSwing: 1,
		scrollBar: $wrap.find('.scrollbar'),
		scrollBy: 1,
		speed: 300,
		elasticBounds: 1,
		easing: 'easeOutExpo',
		dragHandle: 1,
		dynamicHandle: 1,
		clickBar: 1,
		prev: $wrap.find('.prev'),
		next: $wrap.find('.next')
	});
	var tabWidth = $('#tab-category').width();
	var categoryWidth = $('.category-list').width();
	if (tabWidth < categoryWidth) {
		$('.controls').show();
	}
});
</script>
<?php }?>
<script>
<?php if($TPL_VAR["eyoom_board"]["bo_use_infinite_scroll"]=='1'){?>
function eb_modal(href) {
    $('.view-iframe-modal').modal('show').on('hidden.bs.modal', function () {
        $("#view-iframe").attr("src", "");
        $('html').css({overflow: ''});
    });
    $('.view-iframe-modal').modal('show').on('shown.bs.modal', function () {
        $("#view-iframe").attr("src", href);
        $('#view-iframe').height(parseInt($(window).height() * 0.85));
        $('html').css({overflow: 'hidden'});
    });
    return false;
}

$(document).ready(function () {
    $(window).resize(function () {
        $('#view-iframe').height(parseInt($(window).height() * 0.7));
    });
    window.closeModal = function(wr_id){
        $('.view-iframe-modal').modal('hide');
        if(wr_id) {
            var w_id = wr_id.split('|');
            for(var i=0;i<w_id.length;i++) {
                $("#list-item-"+w_id[i]).hide();
            }
        }
    };
});

$(function(){
    var $container = $('.board-webzine');
	$container.infinitescroll({
		navSelector  : "#infinite_pagination",
		nextSelector : "#infinite_pagination .next",
		itemSelector : ".webzine-item",
		loading: {
			finishedMsg: 'END',
			img: '/eyoom/theme/basic3/image/loading.gif'
		}
	},
	function( newElements ) {
		var $newElems = $( newElements ).css({ opacity: 0 });
		$newElems.imagesLoaded(function(){
			$newElems.animate({ opacity: 1 });
		});
	});
	$(window).unbind('.infscr');
	$('#view-infinite-more').click(function(){
		$container.infinitescroll('retrieve');
		$('#infinite_pagination').show();
		return false;
	});
});
<?php }?>
</script>
<?php if($GLOBALS["is_checkbox"]){?>
<script>
function all_checked(sw) {
    var f = document.fboardlist;
    for (var i=0; i<f.length; i++) {
        if (f.elements[i].name == "chk_wr_id[]")
            f.elements[i].checked = sw;
    }
}

function fboardlist_submit(f) {
    var chk_count = 0;
    for (var i=0; i<f.length; i++) {
        if (f.elements[i].name == "chk_wr_id[]" && f.elements[i].checked)
            chk_count++;
    }
    if (!chk_count) {
        swal({
	        html: true,
            title: "Oops...",
            text: "<strong class='color-red'>" + document.pressed + "</strong> 할 게시물을 하나 이상 선택하세요.",
            confirmButtonColor: "#FF2900",
            type: "error",
            confirmButtonText: "확인"
        });
        return false;
    }
    if(document.pressed == "선택복사") {
        select_copy("copy");
        return;
    }
    if(document.pressed == "선택이동") {
        select_copy("move");
        return;
    }
    if(document.pressed == "선택삭제") {
        if (!confirm("선택한 게시물을 정말 삭제하시겠습니까?\n\n한번 삭제한 자료는 복구할 수 없습니다\n\n답변글이 있는 게시글을 선택하신 경우\n답변글도 선택하셔야 게시글이 삭제됩니다."))
            return false;
        f.removeAttribute("target");
        f.action = "./board_list_update.php";
    }
    return true;
}

// 선택한 게시물 복사 및 이동
function select_copy(sw) {
    var f = document.fboardlist;
    if (sw == "copy")
        str = "복사";
    else
        str = "이동";

    var sub_win = window.open("", "move", "left=50, top=50, width=500, height=550, scrollbars=1");
    f.sw.value = sw;
    f.target = "move";
    f.action = "./move.php";
    f.submit();
}
</script>
<?php }?>