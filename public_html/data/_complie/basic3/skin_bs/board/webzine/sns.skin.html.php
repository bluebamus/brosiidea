<?php /* Template_ 2.2.8 2018/01/25 01:09:46 /home/bluebamus1/public_html/eyoom/theme/basic3/skin_bs/board/webzine/sns.skin.html 000001447 */ ?>
<?php if (!defined('_GNUBOARD_')) exit; ?>

<?php if(G5_IS_MOBILE&&$TPL_VAR["config"]["cf_kakao_js_apikey"]){?>
<script src="https://developers.kakao.com/sdk/js/kakao.min.js"></script>
<script src="/js/kakaolink.js"></script>
<script>
    // 사용할 앱의 Javascript 키를 설정해 주세요.
    Kakao.init("<?php echo $TPL_VAR["config"]["cf_kakao_js_apikey"]?>");
</script>
<?php }?>

<ul class="board-view-sns social-icons social-icons-color">
    <li><a href="<?php echo $GLOBALS["facebook_url"]?>" target="_blank" title="Facebook" class="social_facebook"></a></li>
    <li><a href="<?php echo $GLOBALS["twitter_url"]?>" target="_blank" title="Twitter" class="social_twitter"></a></li>
    <li><a href="<?php echo $GLOBALS["gplus_url"]?>" target="_blank" title="Google Plus" class="social_google"></a></li>
<?php if(G5_IS_MOBILE&&$TPL_VAR["config"]["cf_kakao_js_apikey"]){?>
    <li><a href="javascript:kakaolink_send('<?php echo $GLOBALS["sns_msg"]?>', '<?php echo $GLOBALS["longurl"]?>');" title="Kakao" class="social_kakao"></a></li>
<?php }?>
    <li><a href="<?php echo $GLOBALS["kakaostory_url"]?>" target="_blank" title="Kakao Story" class="social_kakaostory"></a></li>
    <li><a href="<?php echo $GLOBALS["band_url"]?>" target="_blank" title="Band" class="social_band"></a></li>
</ul>