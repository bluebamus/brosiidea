<?php /* Template_ 2.2.8 2018/11/26 21:04:52 /home1/bluebamus2/public_html/eyoom/theme/basic3/skin_bs/mypage/basic/mypage.skin.html 000001814 */ ?>
<?php if (!defined("_GNUBOARD_")) exit;
add_stylesheet('<link rel="stylesheet" href="/eyoom/theme/basic3/plugins/sly/tab_scroll_category.css" type="text/css" media="screen">',0);
?>

<div class="tab-scroll-category">
<?php $this->print_("tab_category",$TPL_SCP,1);?>

</div>

<div class="my-page">
<?php if(!($TPL_VAR["mymain"]=='timeline')&&!($TPL_VAR["mymain"]=='favorite')&&!($TPL_VAR["mymain"]=='followinggul')){?>
<?php $this->print_("mybox_bs",$TPL_SCP,1);?>

<?php }?>

<?php if($TPL_VAR["mymain"]=='respond'){?>
<?php $this->print_("respond_bs",$TPL_SCP,1);?>

<?php }elseif($TPL_VAR["mymain"]=='timeline'){?>
<?php $this->print_("timeline_bs",$TPL_SCP,1);?>

<?php }elseif($TPL_VAR["mymain"]=='favorite'){?>
<?php $this->print_("favorite_bs",$TPL_SCP,1);?>

<?php }elseif($TPL_VAR["mymain"]=='followinggul'){?>
<?php $this->print_("following_bs",$TPL_SCP,1);?>

<?php }?>
</div>

<script src="/eyoom/theme/basic3/plugins/sly/vendor_plugins.min.js"></script>
<script src="/eyoom/theme/basic3/plugins/sly/sly.min.js"></script>
<script>
$(function() {
	var $frame = $('#tab-category');
	var $wrap  = $frame.parent();
	$frame.sly({
		horizontal: 1,
		itemNav: 'centered',
		smart: 1,
		activateOn: 'click',
		mouseDragging: 1,
		touchDragging: 1,
		releaseSwing: 1,
		scrollBar: $wrap.find('.scrollbar'),
		scrollBy: 1,
		speed: 300,
		elasticBounds: 1,
		easing: 'easeOutExpo',
		dragHandle: 1,
		dynamicHandle: 1,
		clickBar: 1,
		prev: $wrap.find('.prev'),
		next: $wrap.find('.next')
	});
	var tabWidth = $('#tab-category').width();
	var categoryWidth = $('.category-list').width();
	if (tabWidth < categoryWidth) {
		$('.controls').show();
	}
});
</script>