<?php /* Template_ 2.2.8 2018/11/26 21:04:52 /home1/bluebamus2/public_html/eyoom/theme/basic3/skin_bs/mypage/basic/myhomebox.skin.html 000006276 */ 
$TPL_box_friends_1=empty($TPL_VAR["box_friends"])||!is_array($TPL_VAR["box_friends"])?0:count($TPL_VAR["box_friends"]);
$TPL_box_follower_1=empty($TPL_VAR["box_follower"])||!is_array($TPL_VAR["box_follower"])?0:count($TPL_VAR["box_follower"]);
$TPL_box_following_1=empty($TPL_VAR["box_following"])||!is_array($TPL_VAR["box_following"])?0:count($TPL_VAR["box_following"]);?>
<?php if (!defined("_GNUBOARD_")) exit; ?>

<style>
.follow-panel {position:relative;margin-bottom:20px}
.follow-panel .nav-tabs .active {font-weight:bold}
.follow-panel .panel-default > .panel-heading {background:#fafafa}
.follow-panel .follow-list {list-style-type:none;padding:0;margin-bottom:0}
.follow-panel .follow-list li {position:relative;overflow:hidden;width:70px;height:70px;float:left;margin:3px;-webkit-border-radius:2px !important;-moz-border-radius:2px !important;border-radius:2px !important}
.follow-panel .follow-list li:after {position:absolute;top:0;left:0;width:100%;height:100%;content:"";background:rgba(0,0,0,0.5)}
.follow-panel .follow-list li img {display:block;width:100% \9;max-width:100%;height:auto}
.follow-panel .follow-list li span {display:inline-block;color:#fff;z-index:1;position:absolute;top:0;left:0;width:70px;line-height:70px;text-align:center;padding:0 5px;font-size:12px}
.follow-panel .follow-list li:hover:after {display:none}
.follow-panel .follow-list li:hover span {display:none}
@media (max-width:767px) {
	.follow-panel .follow-list li {width:50px;height:50px;margin:1px}
	.follow-panel .follow-list li span {width:50px;line-height:50px;font-size:11px}
}
</style>

<div class="follow-panel">
	<div class="tab-brd tab-brd-dark">
		<ul class="nav nav-tabs">
			<li class="active"><a href="#myfriends" data-toggle="tab">맞팔친구</a></li>
			<li><a href="#myfollower" data-toggle="tab">팔로워</a></li>
			<li><a href="#myfollowing" data-toggle="tab">팔로윙</a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane fade in active" id="myfriends">
				<div class="panel panel-default">
					<div class="panel-heading">
						<strong class="color-blue font-size-14 margin-right-5">맞팔친구</strong>
						<span class="badge badge-dark rounded-3x helvetica-neue">
<?php if($TPL_VAR["user"]["cnt_friends"]){?><?php echo $TPL_VAR["user"]["cnt_friends"]?><?php }else{?>0<?php }?>
						</span>
						<a href="<?php echo G5_URL?>/?<?php echo $TPL_VAR["user"]["mb_id"]?>&friends" class="btn-e btn-e-xs btn-e-dark pull-right">
							전체보기
						</a>
					</div>
					<div class="panel-body">
						<ul class="follow-list clearfix">
<?php if($TPL_box_friends_1){foreach($TPL_VAR["box_friends"] as $TPL_V1){?>
							<li class="tooltips" type="button" data-placement="top" data-toggle="tooltip" data-original-title="<?php echo $TPL_V1["mb_nick"]?>">
								<a href="<?php echo G5_URL?>/?<?php echo $TPL_V1["mb_id"]?>">
<?php if($TPL_V1["mb_photo"]){?>
									<?php echo $TPL_V1["mb_photo"]?>

<?php }else{?>
									<img src="/eyoom/theme/basic3/skin_bs/mypage/basic/img/user.jpg">
<?php }?>
								</a>
								<span class="ellipsis"><?php echo $TPL_V1["mb_nick"]?></span>
							</li>
<?php }}else{?>
							<div class="text-center color-grey"><i class="fa fa-exclamation-circle"></i> 맞팔친구가 없습니다.</div>
<?php }?>
						</ul>
					</div>
				</div>
			</div>
			<div class="tab-pane fade in" id="myfollower">
				<div class="panel panel-default">
					<div class="panel-heading">
						<strong class="color-blue font-size-14 margin-right-5">팔로워</strong>
						<span class="badge badge-dark rounded-3x helvetica-neue">
<?php if($TPL_VAR["user"]["cnt_follower"]){?><?php echo $TPL_VAR["user"]["cnt_follower"]?><?php }else{?>0<?php }?>
						</span>
						<a href="<?php echo G5_URL?>/?<?php echo $TPL_VAR["user"]["mb_id"]?>&follower" class="btn-e btn-e-xs btn-e-dark pull-right">
							전체보기
						</a>
					</div>
					<div class="panel-body">
						<ul class="follow-list clearfix">
<?php if($TPL_box_follower_1){foreach($TPL_VAR["box_follower"] as $TPL_V1){?>
							<li class="tooltips" type="button" data-placement="top" data-toggle="tooltip" data-original-title="<?php echo $TPL_V1["mb_nick"]?>">
								<a href="<?php echo G5_URL?>/?<?php echo $TPL_V1["mb_id"]?>">
<?php if($TPL_V1["mb_photo"]){?>
									<?php echo $TPL_V1["mb_photo"]?>

<?php }else{?>
									<img src="/eyoom/theme/basic3/skin_bs/mypage/basic/img/user.jpg">
<?php }?>
								</a>
								<span class="ellipsis"><?php echo $TPL_V1["mb_nick"]?></span>
							</li>
<?php }}else{?>
							<div class="text-center color-grey"><i class="fa fa-exclamation-circle"></i> 팔로워가 없습니다.</div>
<?php }?>
						</ul>
					</div>
				</div>
			</div>
			<div class="tab-pane fade in" id="myfollowing">
				<div class="panel panel-default">
					<div class="panel-heading">
						<strong class="color-blue font-size-14 margin-right-5">팔로윙</strong>
						<span class="badge badge-dark rounded-3x helvetica-neue">
<?php if($TPL_VAR["user"]["cnt_following"]){?><?php echo $TPL_VAR["user"]["cnt_following"]?><?php }else{?>0<?php }?>
						</span>
						<a href="<?php echo G5_URL?>/?<?php echo $TPL_VAR["user"]["mb_id"]?>&following" class="btn-e btn-e-xs btn-e-dark pull-right">
							전체보기
						</a>
					</div>
					<div class="panel-body">
						<ul class="follow-list clearfix">
<?php if($TPL_box_following_1){foreach($TPL_VAR["box_following"] as $TPL_V1){?>
							<li class="tooltips" type="button" data-placement="top" data-toggle="tooltip" data-original-title="<?php echo $TPL_V1["mb_nick"]?>">
								<a href="<?php echo G5_URL?>/?<?php echo $TPL_V1["mb_id"]?>">
<?php if($TPL_V1["mb_photo"]){?>
									<?php echo $TPL_V1["mb_photo"]?>

<?php }else{?>
									<img src="/eyoom/theme/basic3/skin_bs/mypage/basic/img/user.jpg">
<?php }?>
								</a>
								<span class="ellipsis"><?php echo $TPL_V1["mb_nick"]?></span>
							</li>
<?php }}else{?>
							<div class="text-center color-grey"><i class="fa fa-exclamation-circle"></i> 팔로윙이 없습니다.</div>
<?php }?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>