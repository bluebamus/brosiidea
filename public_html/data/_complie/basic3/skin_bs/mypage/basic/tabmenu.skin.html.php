<?php /* Template_ 2.2.8 2018/11/26 21:04:52 /home1/bluebamus2/public_html/eyoom/theme/basic3/skin_bs/mypage/basic/tabmenu.skin.html 000001428 */ ?>
<?php if (!defined("_GNUBOARD_")) exit;?>
<div id="tab-category">
	<div class="category-list">
        <span<?php if($GLOBALS["mpid"]=='mypage'&&!$GLOBALS["t"]){?> class="active"<?php }?>><a href="<?php echo G5_URL?>/mypage/">마이페이지</a></span>
        <span<?php if($GLOBALS["t"]=='timeline'){?> class="active"<?php }?>><a href="<?php echo G5_URL?>/mypage/?t=timeline">타임라인</a></span>
        <span<?php if($GLOBALS["t"]=='favorite'){?> class="active"<?php }?>><a href="<?php echo G5_URL?>/mypage/?t=favorite">관심게시판</a></span>
        <span<?php if($GLOBALS["t"]=='followinggul'){?> class="active"<?php }?>><a href="<?php echo G5_URL?>/mypage/?t=followinggul">팔로윙글</a></span>
        <span<?php if($GLOBALS["mpid"]=='activity'){?> class="active"<?php }?>><a href="<?php echo G5_URL?>/mypage/activity.php">활동기록</a></span>
        <span<?php if($GLOBALS["mpid"]=='config'){?> class="active"<?php }?>><a href="<?php echo G5_URL?>/mypage/config.php">환경설정</a></span>
        <span class="fake-span"></span>
	</div>
	<div class="controls">
		<button class="btn prev"><i class="fa fa-caret-left"></i></button>
		<button class="btn next"><i class="fa fa-caret-right"></i></button>
	</div>
</div>
<div class="tab-category-divider"></div>