<?php /* Template_ 2.2.8 2018/11/26 21:04:52 /home1/bluebamus2/public_html/eyoom/theme/basic3/skin_bs/mypage/basic/mybox.skin.html 000007323 */ ?>
<?php if (!defined("_GNUBOARD_")) exit; ?>

<style>
.my-info {position:relative;margin-bottom:30px}
.my-info .info-title {position:relative;margin-bottom:30px}
.my-info .info-title-btn {text-align:right;padding-top:7px}
@media (max-width: 767px) {
	.my-info .info-title-name, .my-info .info-title-btn {text-align:center}
}
.my-info .info-box {position:relative;width:280px;margin:0 auto 30px;padding:15px 15px 30px;border:1px solid #e5e5e5;border-radius:3px !important}
.my-info .info-photo {position:relative;overflow:hidden;width:90px;height:90px;margin:10px auto 20px;text-align:center;background-color:#abacb5;border:5px solid #fff;-webkit-border-radius:50% !important;-moz-border-radius:50% !important;border-radius:50% !important;box-shadow:0 0 1px rgba(0,0,0,.3)}
.my-info .info-photo img {display:block;width:100% \9;max-width:100%;height:auto}
.my-info .info-follow {border-top:1px dotted #e5e5e5;padding-top:20px;margin-top:20px}
.my-info .info-follow p {padding:3px 0;font-size:12px}
.my-info .info-follow span.badge {padding:3px 10px;min-width:80px;text-align:right}
.my-info .info-box-bottom {position:relative;padding:15px;border:1px solid #e5e5e5;border-radius:3px !important;font-size:12px}
</style>

<div class="my-info">
	<div class="info-title">
		<div class="row">
			<div class="col-sm-6 sm-margin-bottom-30">
				<h4 class="info-title-name">
					<strong><i class="fa fa-user-circle"></i> <?php if($TPL_VAR["eyoomer"]["mb_nick"]){?><?php echo $TPL_VAR["eyoomer"]["mb_nick"]?><?php }else{?><?php echo $TPL_VAR["eyoomer"]["mb_name"]?><?php }?></strong> <small>님의 페이지</small>
				</h4>
			</div>
			<div class="col-sm-6">
				<div class="info-title-btn">
<?php if($TPL_VAR["eyoom"]["is_community_theme"]=='y'){?>
					<a href="<?php echo G5_URL?>/?<?php echo $TPL_VAR["member"]["mb_id"]?>" class="btn-e btn-e-dark rounded"><i class="fa fa-user-circle-o margin-right-5"></i>마이홈</a>
<?php }?>
					<a href="<?php echo G5_BBS_URL?>/member_confirm.php?url=register_form.php" class="btn-e btn-e-dark rounded"><i class="fa fa-info-circle margin-right-5"></i>정보수정</a>
<?php if($TPL_VAR["eyoom"]["use_shop_itemtype"]=='y'){?>
					<a href="<?php echo G5_SHOP_URL?>/mypage.php" class="btn-e btn-e-red rounded"><i class="fa fa-shopping-cart margin-right-5"></i>쇼핑몰마이페이지</a>
<?php }?>
				</div>
			</div>
		</div>
	</div>
	<div class="info-box">
		<div class="info-photo">
<?php if($TPL_VAR["eyoomer"]["mb_photo"]){?><?php echo $TPL_VAR["eyoomer"]["mb_photo"]?><?php }else{?><img src="/eyoom/theme/basic3/skin_bs/mypage/basic/img/user.jpg"><?php }?>
		</div>
		<a href="#" class="btn-e btn-e-lg btn-e-default btn-e-block" data-toggle="modal" data-target=".profile-modal">프로필 사진 변경</a>
		<div class="info-content">
	        <div class="info-point margin-top-20">
	            <div class="width-50 pull-left font-size-12">
	                <p class="margin-bottom-0"><?php echo $TPL_VAR["levelset"]["gnu_name"]?> - <a <?php if(!G5_IS_MOBILE){?>href="javascript:void(0);" onclick="point_modal();"<?php }else{?>href="<?php echo G5_BBS_URL?>/point.php" target="_blank"<?php }?>><u>내역보기</u></span></p>
	                <p class="color-red font-size-13"><?php echo number_format($TPL_VAR["member"]["mb_point"])?></p></a>
	            </div>
<?php if($TPL_VAR["levelset"]["use_eyoom_level"]!='n'){?>
	            <div class="widht-50 pull-right font-size-12 text-right">
	                <p class="margin-bottom-0"><?php echo $TPL_VAR["levelset"]["eyoom_name"]?></p>
	                <p class="color-red font-size-13"><?php echo number_format($TPL_VAR["eyoomer"]["level_point"])?></p>
	            </div>
<?php }?>
	            <div class="clearfix"></div>
	        </div>
<?php if($TPL_VAR["levelset"]["use_eyoom_level"]!='n'){?>
	        <div class="info-statistics">
				<span class="progress-info-left">[레벨 <?php echo $TPL_VAR["eyoomer"]["level"]?>] - 진행률</span>
				<span class="progress-info-right"><?php echo $TPL_VAR["lvinfo"]["ratio"]?>%</span>
				<div class="progress progress-e progress-xxs progress-striped active margin-top-5 margin-bottom-0">
				    <div class="progress-bar progress-bar-yellow" role="progressbar" aria-valuenow="<?php echo $TPL_VAR["lvinfo"]["ratio"]?>" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $TPL_VAR["lvinfo"]["ratio"]?>%">
				    </div>
				</div>
	        </div>
<?php }?>
			<div class="info-follow">
				<p><span class="pull-left">&middot; 맞팔친구</span><a href="<?php echo G5_URL?>/?<?php echo $TPL_VAR["member"]["mb_id"]?>&friends"><span class="badge badge-yellow pull-right tooltips" data-placement="left" data-toggle="tooltip" data-original-title="맞팔친구"><?php if($TPL_VAR["eyoomer"]["cnt_friends"]){?><?php echo $TPL_VAR["eyoomer"]["cnt_friends"]?>명<?php }else{?>0명<?php }?></span></a></p>
				<div class="clearfix"></div>
				<p><span class="pull-left">&middot; 팔로워</span><a href="<?php echo G5_URL?>/?<?php echo $TPL_VAR["member"]["mb_id"]?>&follower"><span class="badge badge-dark pull-right tooltips" data-placement="left" data-toggle="tooltip" data-original-title="팔로워"><?php if($TPL_VAR["eyoomer"]["cnt_follower"]){?><?php echo $TPL_VAR["eyoomer"]["cnt_follower"]?>명<?php }else{?>0명<?php }?></span></a></p>
				<div class="clearfix"></div>
				<p><span class="pull-left">&middot; 팔로윙</span><a href="<?php echo G5_URL?>/?<?php echo $TPL_VAR["member"]["mb_id"]?>&following"><span class="badge badge-dark pull-right tooltips" data-placement="left" data-toggle="tooltip" data-original-title="팔로윙"><?php if($TPL_VAR["eyoomer"]["cnt_following"]){?><?php echo $TPL_VAR["eyoomer"]["cnt_following"]?>명<?php }else{?>0명<?php }?></span></a></p>
				<div class="clearfix"></div>
			</div>
        </div>
	</div>
    <div class="info-box-bottom">
		- <strong>가입일</strong> : <?php echo $TPL_VAR["member"]["mb_datetime"]?>

		<div class="margin-hr-10"></div>
		- <strong>이메일</strong> : <?php echo $TPL_VAR["member"]["mb_email"]?>

<?php if($TPL_VAR["member"]["mb_homepage"]){?>
		<div class="margin-hr-10"></div>
		- <strong>홈페이지</strong> : <a href="<?php echo $TPL_VAR["member"]["mb_homepage"]?>" target="_blank"><?php echo $TPL_VAR["member"]["mb_homepage"]?></a>
<?php }?>
<?php if($TPL_VAR["member"]["mb_tel"]){?>
		<div class="margin-hr-10"></div>
		- <strong>전화번호</strong> : <?php echo $TPL_VAR["member"]["mb_tel"]?>

<?php }?>
<?php if($TPL_VAR["member"]["mb_hp"]){?>
		<div class="margin-hr-10"></div>
		- <strong>휴대폰</strong> : <?php echo $TPL_VAR["member"]["mb_hp"]?>

<?php }?>
<?php if($TPL_VAR["member"]["mb_signature"]){?>
		<div class="margin-hr-10"></div>
		- <strong>서명</strong>
		<div class="alert alert-info rounded margin-top-10 margin-bottom-0">
		    <p><?php echo stripslashes($TPL_VAR["member"]["mb_signature"])?></p>
		</div>
<?php }?>
<?php if($TPL_VAR["member"]["mb_profile"]){?>
		<div class="margin-hr-10"></div>
		- <strong>자기소개</strong>
		<div class="alert alert-success rounded margin-top-10 margin-bottom-0">
		    <p><?php echo stripslashes($TPL_VAR["member"]["mb_profile"])?></p>
		</div>
<?php }?>
    </div>
</div>