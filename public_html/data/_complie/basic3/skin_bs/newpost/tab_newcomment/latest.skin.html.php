<?php /* Template_ 2.2.8 2018/01/24 01:16:23 /home1/bluebamus3/public_html/petmari/eyoom/theme/basic3/skin_bs/newpost/tab_newcomment/latest.skin.html 000002557 */ 
$TPL_comment_1=empty($TPL_VAR["comment"])||!is_array($TPL_VAR["comment"])?0:count($TPL_VAR["comment"]);?>
<?php if (!defined('_GNUBOARD_')) exit; ?>

<style>
.new-comment {position:relative;overflow:hidden;padding:6px 0;border-top:1px dotted #e5e5e5}
.new-comment:first-child {border-top:0}
.new-comment .comment-subject {font-size:12px}
.new-comment a:hover .comment-subject {text-decoration:underline}
.new-comment .comment-photo {display:inline-block;width:26px;height:26px;margin-right:2px;border:1px solid #e5e5e5;padding:1px;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;-webkit-border-radius:50% !important;-moz-border-radius:50% !important;border-radius:50% !important}
.new-comment .comment-photo img {width:100%;height:auto;-webkit-border-radius:50% !important;-moz-border-radius:50% !important;border-radius:50% !important}
.new-comment .comment-photo .comment-user-icon {width:22px;height:22px;font-size:14px;line-height:22px;text-align:center;background:#959595;color:#fff;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;display:inline-block;white-space:nowrap;vertical-align:baseline;-webkit-border-radius:50% !important;-moz-border-radius:50% !important;border-radius:50% !important}
.new-comment .comment-nick {font-size:11px;color:#757575}
.new-comment .comment-time {font-size:11px;color:#757575;margin-left:5px}
.new-comment .comment-time .i-color {color:#b5b5b5}
</style>

<div class="tab-newcomment">
<?php if($TPL_comment_1){foreach($TPL_VAR["comment"] as $TPL_V1){?>
    <article class="new-comment">
		<a href="<?php echo $TPL_V1["href"]?>">
			<div class="comment-subject ellipsis">
				<?php echo $TPL_V1["wr_subject"]?>

			</div>
			<span class="comment-photo">
<?php if($TPL_V1["mb_photo"]){?>
				<?php echo $TPL_V1["mb_photo"]?>

<?php }else{?>
				<span class="comment-user-icon"><i class="fa fa-user"></i></span>
<?php }?>
			</span>
			<span class="comment-nick"><?php echo $TPL_V1["mb_nick"]?></span>
			<span class="comment-time"><i class="fa fa-clock-o <?php if($TPL_V1["new"]){?>color-red<?php }else{?>i-color<?php }?>"></i> <?php echo $TPL_VAR["eb"]->date_time('Y-m-d',$TPL_V1["datetime"])?></span>
		</a>
    </article>
<?php }}else{?>
    <p class="text-center color-grey font-size-13 margin-top-10"><i class="fa fa-exclamation-circle"></i> 최신글이 없습니다.</p>
<?php }?>
</div>