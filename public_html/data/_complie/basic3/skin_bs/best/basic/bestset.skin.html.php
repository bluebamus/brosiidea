<?php /* Template_ 2.2.8 2018/01/24 01:16:22 /home1/bluebamus3/public_html/petmari/eyoom/theme/basic3/skin_bs/best/basic/bestset.skin.html 000013156 */ 
$TPL_today_1=empty($TPL_VAR["today"])||!is_array($TPL_VAR["today"])?0:count($TPL_VAR["today"]);
$TPL_week_1=empty($TPL_VAR["week"])||!is_array($TPL_VAR["week"])?0:count($TPL_VAR["week"]);
$TPL_month_1=empty($TPL_VAR["month"])||!is_array($TPL_VAR["month"])?0:count($TPL_VAR["month"]);?>
<?php if (!defined('_GNUBOARD_')) exit;?>

<style>
.basic-best .nav-tabs {background:#f5f5f5}
.basic-best .nav-tabs li {width:33.33333%}
.basic-best .nav-tabs li a {text-align:center;border-top-left-radius:0 !important;border-top-right-radius:0 !important;margin-right:0;margin-left:-1px;color:#959595;border:1px solid #b5b5b5;padding:8px 5px}
.basic-best .nav-tabs li a:hover {background:#fff;border-bottom:1px solid #b5b5b5}
.basic-best .nav-tabs li.active a {z-index:1;background:#fff;font-weight:bold}
.basic-best .tab-content {padding:20px 0 0}
.basic-best .tab-content ul {margin-bottom:0;margin-left:-15px;margin-right:-15px}
.basic-best .tab-content li {position:relative;padding:2px 15px;font-size:12px}
.basic-best .tab-content li.no-latest {width:100%}
.basic-best .tab-content .txt-subj {position:relative;width:70%;padding-right:0;padding-left:0;display:block;text-overflow:ellipsis;white-space:nowrap;word-wrap:normal;overflow:hidden;float:left}
.basic-best .tab-content .txt-comment {display:inline-block;min-width:38px;padding:1px 3px;font-size:10px;font-weight:300;line-height:11px;color:#fff;text-align:center;white-space:nowrap;vertical-align:baseline;background-color:#757575;margin-right:3px}
.basic-best .tab-content .txt-member {position:relative;overflow:hidden;display:block;text-overflow:ellipsis;white-space:nowrap;word-wrap:normal;overflow:hidden;width:30%;float:left;text-align:right}
.basic-best .tab-content .txt-photo img {width:17px;height:17px;margin-left:2px}
.basic-best .tab-content .txt-photo .txt-user-icon {width:17px;height:17px;font-size:11px;line-height:17px;text-align:center;background:#858585;color:#fff;margin-left:2px;display:inline-block;white-space:nowrap;vertical-align:baseline}
.basic-best .tab-content .txt-nick {font-size:12px;color:#757575}
.basic-best .tab-content a:hover .txt-subj {color:#FF4948;text-decoration:underline}
.basic-best .tab-content a:hover .txt-nick {color:#000}
.basic-best .best-num {display:inline-block;min-width:30px;padding:1px 3px;font-size:10px;font-weight:normal;line-height:11px;color:#fff;text-align:center;white-space:nowrap;vertical-align:baseline;background:#2E3340;margin-right:3px}
.basic-best .best-num.best-num-1 {background:#FF4948}
.basic-best .best-num.best-num-2 {background:#FFA72C}
.basic-best .best-num.best-num-3 {background:#FFA72C}
@media (max-width: 650px) {
	.basic-best .nav-tabs li a {font-size:12px}
	.basic-best .tab-content li {padding:3px 15px}
}
</style>

<div class="basic-best">
    <div class="tab-brd tab-brd-default main-text-tab">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#best-tlb-1" data-toggle="tab">일간인기글</a></li>
            <li><a href="#best-tlb-2" data-toggle="tab">주간인기글</a></li>
            <li class="last"><a href="#best-tlb-3" data-toggle="tab">월간인기글</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade active in" id="best-tlb-1">
				<div class="row">
					<div class="col-sm-6">
						<ul class="list-unstyled">
<?php if($TPL_today_1){$TPL_I1=-1;foreach($TPL_VAR["today"] as $TPL_V1){$TPL_I1++;?>
<?php if($TPL_I1<= 4){?>
							<li>
								<a href="<?php echo $TPL_V1["href"]?>">
									<div class="txt-subj <?php if(!G5_IS_MOBILE){?>tooltips<?php }?>" <?php if(!G5_IS_MOBILE){?>data-toggle="tooltip" data-placement="right" data-original-title="<?php echo $TPL_VAR["eb"]->date_time('Y-m-d',$TPL_V1["datetime"])?>"<?php }?>>
										<strong class="best-num <?php if($TPL_I1<= 2){?>best-num-<?php echo $TPL_I1+ 1?><?php }?>"><?php echo $TPL_I1+ 1?></strong>
<?php if($TPL_V1["wr_comment"]){?><span class="txt-comment">+<?php echo number_format($TPL_V1["wr_comment"])?></span><?php }?><?php if($TPL_V1["new"]){?><i class="fa fa-bolt color-red margin-right-5"></i><?php }?>
										<?php echo $TPL_V1["wr_subject"]?>

									</div>
									<div class="txt-member">
										<span class="txt-nick"><?php echo $TPL_V1["mb_nick"]?></span>
<?php if($TPL_VAR["photo"]=='y'){?>
										<span class="txt-photo">
<?php if($TPL_V1["mb_photo"]){?>
											<?php echo $TPL_V1["mb_photo"]?>

<?php }else{?>
											<span class="txt-user-icon"><i class="fa fa-user"></i></span>
<?php }?>
										</span>
<?php }?>
									</div>
									<div class="clearfix"></div>
								</a>
							</li>
<?php }?>
<?php }}else{?>
							<li class="no-latest"><p class="text-center color-grey font-size-13 margin-top-30"><i class="fa fa-exclamation-circle"></i> 최신글이 없습니다.</p></li>
<?php }?>
						</ul>
					</div>
					<div class="col-sm-6">
						<ul class="list-unstyled">
<?php if($TPL_today_1){$TPL_I1=-1;foreach($TPL_VAR["today"] as $TPL_V1){$TPL_I1++;?>
<?php if($TPL_I1> 4){?>
							<li>
								<a href="<?php echo $TPL_V1["href"]?>">
									<div class="txt-subj <?php if(!G5_IS_MOBILE){?>tooltips<?php }?>" <?php if(!G5_IS_MOBILE){?>data-toggle="tooltip" data-placement="right" data-original-title="<?php echo $TPL_VAR["eb"]->date_time('Y-m-d',$TPL_V1["datetime"])?>"<?php }?>>
										<strong class="best-num <?php if($TPL_I1<= 2){?>best-num-<?php echo $TPL_I1+ 1?><?php }?>"><?php echo $TPL_I1+ 1?></strong>
<?php if($TPL_V1["wr_comment"]){?><span class="txt-comment">+<?php echo number_format($TPL_V1["wr_comment"])?></span><?php }?><?php if($TPL_V1["new"]){?><i class="fa fa-bolt color-red margin-right-5"></i><?php }?>
										<?php echo $TPL_V1["wr_subject"]?>

									</div>
									<div class="txt-member">
										<span class="txt-nick"><?php echo $TPL_V1["mb_nick"]?></span>
<?php if($TPL_VAR["photo"]=='y'){?>
										<span class="txt-photo">
<?php if($TPL_V1["mb_photo"]){?>
											<?php echo $TPL_V1["mb_photo"]?>

<?php }else{?>
											<span class="txt-user-icon"><i class="fa fa-user"></i></span>
<?php }?>
										</span>
<?php }?>
									</div>
									<div class="clearfix"></div>
								</a>
							</li>
<?php }?>
<?php }}else{?>
							<li class="no-latest"><p class="text-center color-grey font-size-13 margin-top-30"><i class="fa fa-exclamation-circle"></i> 최신글이 없습니다.</p></li>
<?php }?>
						</ul>
					</div>
				</div>
            </div>
            <div class="tab-pane fade in" id="best-tlb-2">
				<div class="row">
					<div class="col-sm-6">
						<ul class="list-unstyled">
<?php if($TPL_week_1){$TPL_I1=-1;foreach($TPL_VAR["week"] as $TPL_V1){$TPL_I1++;?>
<?php if($TPL_I1<= 4){?>
							<li>
								<a href="<?php echo $TPL_V1["href"]?>">
									<div class="txt-subj <?php if(!G5_IS_MOBILE){?>tooltips<?php }?>" <?php if(!G5_IS_MOBILE){?>data-toggle="tooltip" data-placement="right" data-original-title="<?php echo $TPL_VAR["eb"]->date_time('Y-m-d',$TPL_V1["datetime"])?>"<?php }?>>
										<strong class="best-num <?php if($TPL_I1<= 2){?>best-num-<?php echo $TPL_I1+ 1?><?php }?>"><?php echo $TPL_I1+ 1?></strong>
<?php if($TPL_V1["wr_comment"]){?><span class="txt-comment">+<?php echo number_format($TPL_V1["wr_comment"])?></span><?php }?><?php if($TPL_V1["new"]){?><i class="fa fa-bolt color-red margin-right-5"></i><?php }?>
										<?php echo $TPL_V1["wr_subject"]?>

									</div>
									<div class="txt-member">
										<span class="txt-nick"><?php echo $TPL_V1["mb_nick"]?></span>
<?php if($TPL_VAR["photo"]=='y'){?>
										<span class="txt-photo">
<?php if($TPL_V1["mb_photo"]){?>
											<?php echo $TPL_V1["mb_photo"]?>

<?php }else{?>
											<span class="txt-user-icon"><i class="fa fa-user"></i></span>
<?php }?>
										</span>
<?php }?>
									</div>
									<div class="clearfix"></div>
								</a>
							</li>
<?php }?>
<?php }}else{?>
							<li class="no-latest"><p class="text-center color-grey font-size-13 margin-top-30"><i class="fa fa-exclamation-circle"></i> 최신글이 없습니다.</p></li>
<?php }?>
						</ul>
					</div>
					<div class="col-sm-6">
						<ul class="list-unstyled">
<?php if($TPL_week_1){$TPL_I1=-1;foreach($TPL_VAR["week"] as $TPL_V1){$TPL_I1++;?>
<?php if($TPL_I1> 4){?>
							<li>
								<a href="<?php echo $TPL_V1["href"]?>">
									<div class="txt-subj <?php if(!G5_IS_MOBILE){?>tooltips<?php }?>" <?php if(!G5_IS_MOBILE){?>data-toggle="tooltip" data-placement="right" data-original-title="<?php echo $TPL_VAR["eb"]->date_time('Y-m-d',$TPL_V1["datetime"])?>"<?php }?>>
										<strong class="best-num <?php if($TPL_I1<= 2){?>best-num-<?php echo $TPL_I1+ 1?><?php }?>"><?php echo $TPL_I1+ 1?></strong>
<?php if($TPL_V1["wr_comment"]){?><span class="txt-comment">+<?php echo number_format($TPL_V1["wr_comment"])?></span><?php }?><?php if($TPL_V1["new"]){?><i class="fa fa-bolt color-red margin-right-5"></i><?php }?>
										<?php echo $TPL_V1["wr_subject"]?>

									</div>
									<div class="txt-member">
										<span class="txt-nick"><?php echo $TPL_V1["mb_nick"]?></span>
<?php if($TPL_VAR["photo"]=='y'){?>
										<span class="txt-photo">
<?php if($TPL_V1["mb_photo"]){?>
											<?php echo $TPL_V1["mb_photo"]?>

<?php }else{?>
											<span class="txt-user-icon"><i class="fa fa-user"></i></span>
<?php }?>
										</span>
<?php }?>
									</div>
									<div class="clearfix"></div>
								</a>
							</li>
<?php }?>
<?php }}else{?>
							<li class="no-latest"><p class="text-center color-grey font-size-13 margin-top-30"><i class="fa fa-exclamation-circle"></i> 최신글이 없습니다.</p></li>
<?php }?>
						</ul>
					</div>
				</div>
            </div>
            <div class="tab-pane fade in" id="best-tlb-3">
				<div class="row">
					<div class="col-sm-6">
						<ul class="list-unstyled">
<?php if($TPL_month_1){$TPL_I1=-1;foreach($TPL_VAR["month"] as $TPL_V1){$TPL_I1++;?>
<?php if($TPL_I1<= 4){?>
							<li>
								<a href="<?php echo $TPL_V1["href"]?>">
									<div class="txt-subj <?php if(!G5_IS_MOBILE){?>tooltips<?php }?>" <?php if(!G5_IS_MOBILE){?>data-toggle="tooltip" data-placement="right" data-original-title="<?php echo $TPL_VAR["eb"]->date_time('Y-m-d',$TPL_V1["datetime"])?>"<?php }?>>
										<strong class="best-num <?php if($TPL_I1<= 2){?>best-num-<?php echo $TPL_I1+ 1?><?php }?>"><?php echo $TPL_I1+ 1?></strong>
<?php if($TPL_V1["wr_comment"]){?><span class="txt-comment">+<?php echo number_format($TPL_V1["wr_comment"])?></span><?php }?><?php if($TPL_V1["new"]){?><i class="fa fa-bolt color-red margin-right-5"></i><?php }?>
										<?php echo $TPL_V1["wr_subject"]?>

									</div>
									<div class="txt-member">
										<span class="txt-nick"><?php echo $TPL_V1["mb_nick"]?></span>
<?php if($TPL_VAR["photo"]=='y'){?>
										<span class="txt-photo">
<?php if($TPL_V1["mb_photo"]){?>
											<?php echo $TPL_V1["mb_photo"]?>

<?php }else{?>
											<span class="txt-user-icon"><i class="fa fa-user"></i></span>
<?php }?>
										</span>
<?php }?>
									</div>
									<div class="clearfix"></div>
								</a>
							</li>
<?php }?>
<?php }}else{?>
							<li class="no-latest"><p class="text-center color-grey font-size-13 margin-top-30"><i class="fa fa-exclamation-circle"></i> 최신글이 없습니다.</p></li>
<?php }?>
						</ul>
					</div>
					<div class="col-sm-6">
						<ul class="list-unstyled">
<?php if($TPL_month_1){$TPL_I1=-1;foreach($TPL_VAR["month"] as $TPL_V1){$TPL_I1++;?>
<?php if($TPL_I1> 4){?>
							<li>
								<a href="<?php echo $TPL_V1["href"]?>">
									<div class="txt-subj <?php if(!G5_IS_MOBILE){?>tooltips<?php }?>" <?php if(!G5_IS_MOBILE){?>data-toggle="tooltip" data-placement="right" data-original-title="<?php echo $TPL_VAR["eb"]->date_time('Y-m-d',$TPL_V1["datetime"])?>"<?php }?>>
										<strong class="best-num <?php if($TPL_I1<= 2){?>best-num-<?php echo $TPL_I1+ 1?><?php }?>"><?php echo $TPL_I1+ 1?></strong>
<?php if($TPL_V1["wr_comment"]){?><span class="txt-comment">+<?php echo number_format($TPL_V1["wr_comment"])?></span><?php }?><?php if($TPL_V1["new"]){?><i class="fa fa-bolt color-red margin-right-5"></i><?php }?>
										<?php echo $TPL_V1["wr_subject"]?>

									</div>
									<div class="txt-member">
										<span class="txt-nick"><?php echo $TPL_V1["mb_nick"]?></span>
<?php if($TPL_VAR["photo"]=='y'){?>
										<span class="txt-photo">
<?php if($TPL_V1["mb_photo"]){?>
											<?php echo $TPL_V1["mb_photo"]?>

<?php }else{?>
											<span class="txt-user-icon"><i class="fa fa-user"></i></span>
<?php }?>
										</span>
<?php }?>
									</div>
									<div class="clearfix"></div>
								</a>
							</li>
<?php }?>
<?php }}else{?>
							<li class="no-latest"><p class="text-center color-grey font-size-13 margin-top-30"><i class="fa fa-exclamation-circle"></i> 최신글이 없습니다.</p></li>
<?php }?>
						</ul>
					</div>
				</div>
            </div>
        </div>
    </div>
</div>