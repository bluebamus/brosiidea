<?php /* Template_ 2.2.8 2018/11/26 21:04:51 /home1/bluebamus2/public_html/eyoom/theme/basic3/skin_bs/nameview/basic/nameview.skin.html 000003707 */ ?>
<?php if (!defined('_GNUBOARD_')) exit; ?>

<span class="sv_wrap">
<?php if(!$TPL_VAR["is_anonymous"]){?>
	<a href="<?php echo $TPL_VAR["head"]["link"]?>" data-toggle="dropdown" title="<?php echo $TPL_VAR["head"]["title"]?>" target="_blank" onclick="return false;"><b><?php echo $TPL_VAR["head"]["name"]?></b></a>
<?php }else{?>
	<b><?php echo $TPL_VAR["head"]["name"]?></b>
<?php }?>
<?php  ob_start(); ?>
<?php if(!$TPL_VAR["is_anonymous"]){?>
	<ul class="sv dropdown-menu" role="menu">
<?php if($TPL_VAR["mb_id"]&&$TPL_VAR["member"]["mb_id"]){?>

<?php if($TPL_VAR["eyoom"]["is_community_theme"]=='y'){?>
<?php if($TPL_VAR["mb_id"]!=$TPL_VAR["member"]["mb_id"]&&$TPL_VAR["eyoomer"]["onoff_social"]=='on'){?>
			<li>
				<span id="follow">
<?php if(!$TPL_VAR["follow"]){?>
					<button type="button" class="btn-e btn-e-xs btn-e-yellow btn-e-block <?php echo $TPL_VAR["mb_id"]?>" name="<?php echo $TPL_VAR["mb_id"]?>" value="following" title="친구맺기를 신청합니다.">친구맺기 신청 <i class="fa fa-smile-o color-white"></i></button>
<?php }else{?>
					<button type="button" class="btn-e btn-e-xs btn-e-dark btn-e-block <?php echo $TPL_VAR["mb_id"]?>" name="<?php echo $TPL_VAR["mb_id"]?>" value="unfollow" title="친구관계를 해제합니다.">팔로윙 해제 <i class="fa fa-meh-o color-white"></i></button>
<?php }?>
				</span>
			</li>
<?php }?>
<?php if(!$GLOBALS["wmode"]){?>
			<li><a href="<?php echo $TPL_VAR["link"]["userpage"]?>"><strong><?php echo $TPL_VAR["head"]["name"]?></strong>님의 홈</a></li>
<?php }?>
<?php }?>

		<li><a <?php if(!G5_IS_MOBILE){?>href="javascript:void(0);" onclick="memo_send_modal(this.title);"<?php }else{?>href="<?php echo G5_BBS_URL?>/memo_form.php?me_recv_mb_id=<?php echo $TPL_VAR["mb_id"]?>" target="_blank"<?php }?> title="<?php echo $TPL_VAR["mb_id"]?>" id="ol_after_memo">쪽지보내기</a></li>
		<li><a href="<?php echo $TPL_VAR["link"]["profile"]?>" onclick="member_profile_modal(this.href); return false;">자기소개</a></li>
<?php if(!$GLOBALS["wmode"]){?>
		<li><a href="<?php echo $TPL_VAR["link"]["article"]?>">전체게시물</a></li>
<?php }?>
<?php }?>

<?php if($TPL_VAR["email"]&&$TPL_VAR["mb_id"]&&$TPL_VAR["member"]["mb_id"]){?>
		<li><a href="<?php echo $TPL_VAR["link"]["email"]?>" onclick="win_email(this.href); return false;">메일보내기</a></li>
<?php }?>

<?php if($TPL_VAR["home"]&&$TPL_VAR["mb_id"]&&$TPL_VAR["member"]["mb_id"]){?>
		<li><a href="<?php echo $TPL_VAR["link"]["home"]?>" target="_blank">홈페이지</a></li>
<?php }?>

<?php if($TPL_VAR["bo_table"]&&!$GLOBALS["wmode"]){?>
<?php if($TPL_VAR["mb_id"]){?>
		<li><a href="<?php echo $TPL_VAR["link"]["sid"]?>">아이디로 검색</a></li>
<?php }else{?>
		<li><a href="<?php echo $TPL_VAR["link"]["sname"]?>">이름으로 검색</a></li>
<?php }?>
<?php }?>
<?php if($TPL_VAR["g5"]["sms5_use_sideview"]&&$TPL_VAR["mb_id"]&&$TPL_VAR["member"]["mb_id"]){?>
		<li><a href="<?php echo $TPL_VAR["link"]["sms"]?>" class="win_sms5" target="_blank">문자보내기</a></li>
<?php }?>
<?php if($TPL_VAR["is_admin"]&&$TPL_VAR["mb_id"]){?>
		<li><a href="<?php echo $TPL_VAR["link"]["info"]?>" target="_blank">회원정보변경</a></li>
		<li><a href="<?php echo $TPL_VAR["link"]["point"]?>" target="_blank"><?php echo $TPL_VAR["levelset"]["gnu_name"]?>내역</a></li>
<?php }?>
	</ul>
<?php }?>
<?php
    $mb_name = ob_get_contents();
    ob_end_clean();
?>
<?php echo $mb_name; ?>
<?php if(!$TPL_VAR["is_anonymous"]){?>
<noscript class="sv_nojs"><?php echo $noscript;?></noscript>
<?php }?>
</span>