<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018-05-01
 * Time: 오전 10:38
 */

    class mysql_handler{
        protected $host = '115.68.226.52';
        protected $user = 'brosiidea_solutions';
        protected $pw = 'bam1324';
        protected $dbName = 'brosiidea_solutions';
        protected $dbConnection = null;

        function get_host(){
            return $this->host;
        }

        function get_user(){
            return $this->user;
        }

        function get_pw(){
            return $this->pw;
        }

        function get_dbName(){
            return $this->dbName;
        }

        function set_host($host){
            $this->host = $host;
            return $this->host;
        }

        function set_user($user){
            $this->user = $user;
            return $this->user;
        }

        function set_pw($pw){
            $this->pw= $pw;
            return $this->pw;
        }

        function set_dbName($dbName){
            $this->dbName = $dbName;
            return $this->dbName;
        }

        function make_connection(){
            $this->dbConnection = mysqli_connect($this->host, $this->user, $this->pw, $this->dbName);

            if ($this->dbConnection->connect_errno()) {
                return 'Connect Error: '.mysqli_connect_error();
            }
            return 0;
        }

/*        function request_default($sql_query)
        {//insert, delete, update

        }

        function  request_select($sql_query)
        {

        }*/

        function set_dbMemory_clean($result){
            $result->free();
        }

        function set_dbClose(){
            $this->dbConnection->close();
        }

    }