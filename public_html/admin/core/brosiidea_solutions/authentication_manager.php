<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018-05-01
 * Time: 오전 10:36
 */
$sub_menu = "600100";
if (!defined('_EYOOM_IS_ADMIN_')) exit;

require_once('db_handler.php');

$db_handler = new mysql_handler();

$sql = "select * from 'authentication'";
$result = sql_fetch($sql);

for ($i=0; $row=sql_fetch_array($result); $i++) {
    $board_list[$i] = $row;
    $gr_select = str_replace('"', "'", get_group_select("gr_id[$i]", $row['gr_id']));
    $board_list[$i]['gr_select'] = preg_replace("/\\n/", "", $gr_select);

    $skin_select = str_replace('"', "'", get_skin_select('board', 'bo_skin_' . $i, "bo_skin[$i]", $row['bo_skin']));
    $board_list[$i]['skin_select'] = preg_replace("/\\n/", "", $skin_select);

    $mobile_skin_select = str_replace('"', "'", get_mobile_skin_select('board', 'bo_mobile_skin_' . $i, "bo_mobile_skin[$i]", $row['bo_mobile_skin']));
    $board_list[$i]['mobile_skin_select'] = preg_replace("/\\n/", "", $mobile_skin_select);
}

    include EYOOM_ADMIN_INC_PATH . "/atpl.assign.php";

    $atpl->assign(array(
        'board_list' => $board_list,
    ));

