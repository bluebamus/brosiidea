<?php
$sub_menu = "200300";
if (!defined('_EYOOM_IS_ADMIN_')) exit;

auth_check($auth[$sub_menu], 'w');

check_demo();

check_admin_token();

require_once(G5_PATH.'/mail_service/std_mail_modules.php');
require_once(G5_PATH.'/mail_service/mailgun/Mailgun_send.php');
require_once(G5_PATH.'/mail_service/sendgrid/sendgrid-php/sendgrid-php.php');

$str_mail_modules= new stdmailmodules\std_mail_modules();

$dateinfo = $_REQUEST['date_info'];

//modify for mail service by lim
$sql = " select cf_1,cf_2,cf_3,cf_4,cf_5,cf_6,cf_7,cf_8,cf_9 from {$g5['config_table']} ";
$result = sql_query($sql);
for ($i=0; $row=sql_fetch_array($result); $i++) {
    $mail_service_info[$i] = $row;
}

if($dateinfo != null){
    $mailgun_time = $str_mail_modules->convertTimeForMailgun($dateinfo);
    $sendgrid_time = $str_mail_modules->convertTimeForSendgrid($dateinfo);
    if($mailgun_time == "error" || $sendgrid_time == "error" )
    {
        $err_msg='Sending reservation time is over then 3day';
        header('location:'.EYOOM_ADMIN_URL.'/?dir=member&pid=mail_list&error_msg='.$err_msg);
        exit;
    }
}

//check selected mail service
if($mail_service_info[0]['cf_2'] == 1) {
    $selected_mail_service = 'mailgun';
}elseif($mail_service_info[0]['cf_6'] == 1){
    $selected_mail_service = 'sendgrid';
}else{
    include_once(G5_LIB_PATH.'/mailer.lib.php');
    $selected_mail_service = 'local_server';
}

//system parameter setting
$countgap = 10; // 몇건씩 보낼지 설정
$maxscreen = 500; // 몇건씩 화면에 보여줄건지?
$sleepsec = 200;  // 천분의 몇초간 쉴지 설정

//get and set the mail parameter from mail modules
$mailgun_sending_limit = $str_mail_modules->getMailgunSendingLimit();

$mailgun_api_key = $mail_service_info[0]['cf_4'];
$sendgrid_api_key = $mail_service_info[0]['cf_8'];

$send_count= $mail_service_info[0]['cf_3'];
$over_send_set_state= $mail_service_info[0]['cf_5'];
$use_con_set_state= $mail_service_info[0]['cf_9'];
$last_sending_success_user_id='';
$sendgrid_contn_send_set=0;

$domain = "brosiidea.com";

//check mail service using flag state
$result = $str_mail_modules->CheckServiceOptionsState($send_count, $over_send_set_state, $use_con_set_state, $selected_mail_service);

if($result==1){
    $err_msg='maximum_milgun_sending_count';
    header('location:'.EYOOM_ADMIN_URL.'/?dir=member&pid=mail_list&error_msg='.$err_msg);
    exit;
}
if($result==2){
    $err_msg='check_sendgrid_continue_sending_flag';
    header('location:'.EYOOM_ADMIN_URL.'/?dir=member&pid=mail_list&error_msg='.$err_msg);
    exit;
}
if($result==3) {
    $sendgrid_contn_send_set = 1;
}


$ma_id = trim($_POST['ma_id']);
$select_member_list = trim($_POST['ma_list']);

//print_r2($_POST); EXIT;
$member_list = explode("\n", conv_unescape_nl($select_member_list));

// 메일내용 가져오기
$sql = "select ma_subject, ma_content from {$g5['mail_table']} where ma_id = '$ma_id' ";
$ma = sql_fetch($sql);

$subject = $ma['ma_subject'];

$cnt = 0;
for ($i=0; $i<count($member_list); $i++) {
    list($to_email, $mb_id, $name, $nick, $datetime) = explode("||", trim($member_list[$i]));

    $sw = preg_match("/[0-9a-zA-Z_]+(\.[0-9a-zA-Z_]+)*@[0-9a-zA-Z_]+(\.[0-9a-zA-Z_]+)*/", $to_email);
    // 올바른 메일 주소만
    if ($sw == true) {

        $cnt++;


        $mb_md5 = md5($mb_id . $to_email . $datetime);

        $content = $ma['ma_content'];
        $content = preg_replace("/{이름}/", $name, $content);
        $content = preg_replace("/{닉네임}/", $nick, $content);
        $content = preg_replace("/{회원아이디}/", $mb_id, $content);
        $content = preg_replace("/{이메일}/", $to_email, $content);

        // switch mail service
        if ($selected_mail_service == 'mailgun' && $sendgrid_contn_send_set == 0) {

            $send_count++;

            $from_name = $config['cf_admin_email_name'];
            $from = $config['cf_admin_email'];
            $to_name = $name . '(' . $nick . ',' . $mb_id . ')';
            $to = $to_email;
            if ($dateinfo != null)
                $delivery_time = $mailgun_time;
            else
                $delivery_time = '';

            $mailgun = new MailgunMailer();

            if(strpos($content, "div") == false) {
                //$content = $content . "<hr size=0><p><span style='font-size:9pt; font-familye:굴림'>▶ 더 이상 정보 수신을 원치 않으시면 [<a href='".G5_BBS_URL."/email_stop.php?mb_id={$mb_id}&amp;mb_md5={$mb_md5}' target='_blank'>수신거부</a>] 해 주십시오.</span></p>";

               $content=$content.$str_mail_modules->addUnsubscribe($selected_mail_service,0,$mb_id,$mb_md5);
                $text = $content;
                if ($delivery_time == '')
                    $response = $mailgun->mailer($mailgun_api_key, $domain, $from_name, $from, $to_name, $to, $subject, 0, $text, '', $cc = '', $bcc = '', '', $delivery_time = '');
                else
                    $response = $mailgun->mailer($mailgun_api_key, $domain, $from_name, $from, $to_name, $to, $subject, 0, $text, '', $cc = '', $bcc = '', '', $delivery_time);
            }else{

                $content=$content.$str_mail_modules->addUnsubscribe($selected_mail_service,1,$mb_id,$mb_md5);
                $html = $content;
                if ($delivery_time == '')
                    $response = $mailgun->mailer($mailgun_api_key, $domain, $from_name, $from, $to_name, $to, $subject, 1, $html, '', $cc = '', $bcc = '', '', $delivery_time = '');
                else
                    $response = $mailgun->mailer($mailgun_api_key, $domain, $from_name, $from, $to_name, $to, $subject, 1, $html, '', $cc = '', $bcc = '', '', $delivery_time);
            }

            if ($response == "Queued. Thank you.") {
                $sql = " update {$g5['config_table']} set cf_10 = '" . $to_email . "' where 1";
                sql_query($sql);
            } else {
                $err_msg = $member['mb_nick'] . '(' . $member['mb_email'] . ')_mailgun_sending_failed'.$response;
                header('location:' . EYOOM_ADMIN_URL . '/?dir=member&pid=mail_list&error_msg=' . $err_msg);
                exit;
            }

            if ($send_count == $mailgun_sending_limit) {
                $result = $str_mail_modules->CheckServiceOptionsState($send_count, $over_send_set_state, $use_con_set_state, $selected_mail_service);

                if ($result == 1) {
                    $err_msg = 'maximum_milgun_sending_count';
                    header('location:' . EYOOM_ADMIN_URL . '/?dir=member&pid=mail_list&error_msg=' . $err_msg);
                    exit;
                }

                if ($result == 2) {
                    $err_msg = 'check_sendgrid_continue_sending_flag';
                    header('location:' . EYOOM_ADMIN_URL . '/?dir=member&pid=mail_list&error_msg=' . $err_msg);
                    exit;
                }
                if ($result == 3) {
                    $sendgrid_contn_send_set = 1;
                }
            }
        } elseif ($selected_mail_service == 'sendgrid' || $sendgrid_contn_send_set == 1) {

            $api_key = trim($sendgrid_api_key);
            $from_name = $config['cf_admin_email_name'];
            $from = $config['cf_admin_email'];
            $from = new SendGrid\Email($from_name, $from);
            $to_name = $name . '(' . $nick . ',' . $mb_id . ')';
            $to = $to_email;
            $to = new SendGrid\Email($to_name, $to);
            $subject = $subject;
            if ($dateinfo != null)
                $delivery_time = $sendgrid_time;
            else
                $delivery_time = '';

            if(strpos($content, "div") == false) {
                $content=$content.$str_mail_modules->addUnsubscribe($selected_mail_service,0,$mb_id,$mb_md5);
                $content = new SendGrid\Content("text/plain", $content);
            }else{
                $content=$content.$str_mail_modules->addUnsubscribe($selected_mail_service,1,$mb_id,$mb_md5);
                $content = new SendGrid\Content("text/html", $content);
            }


            $mail = new SendGrid\Mail($from, $subject, $to, $content);
            if ($delivery_time != '')
                $mail->setSendAt($delivery_time);

            $sg = new \SendGrid($api_key);
            $response = $sg->client->mail()->send()->post($mail);

            if ($response->statusCode() == "202") {
                $sql = " update {$g5['config_table']} set cf_10 = '" . $to_email . "' where 1";
                sql_query($sql);
            } else {
                $err_msg = $member['mb_nick'] . '(' . $member['mb_email'] . ')_sendgrid_sending_failed'.$response->body();
                header('location:' . EYOOM_ADMIN_URL . '/?dir=member&pid=mail_list&error_msg=' . $err_msg);
                exit;
            }

        } else {

            //var_export("보내는 사람 이름 : ".$config['cf_admin_email_name']."<br><br>보내는 사람 주소 : ".$config['cf_admin_email']."<br><br>".$to_email."<br><br>".$subject."<br><br>");

           // $content = $content . "<hr size=0><p><span style='font-size:9pt; font-familye:굴림'>▶ 더 이상 정보 수신을 원치 않으시면 [<a href='" . G5_BBS_URL . "/email_stop.php?mb_id={$mb_id}&amp;mb_md5={$mb_md5}' target='_blank'>수신거부</a>] 해 주십시오.</span></p>";
            $content=$content.$str_mail_modules->addUnsubscribe($selected_mail_service,1,$mb_id,$mb_md5);
            mailer($config['cf_admin_email_name'], $config['cf_admin_email'], $to_email, $subject, $content, 1);
            //update succeed sending user id
            $sql = " update {$g5['config_table']} set cf_10 = '" . $to_email . "' where 1";
            sql_query($sql);

        }

        echo "<script> document.all.cont.innerHTML += '$cnt. $to_email ($mb_id : $name)<br>'; </script>\n";
        usleep($sleepsec);
        if ($cnt % $countgap == 0)
            echo "<script> document.all.cont.innerHTML += '<br>'; document.body.scrollTop += 1000; </script>\n";

        // 화면을 지운다... 부하를 줄임
        if ($cnt % $maxscreen == 0)
            echo "<script> document.all.cont.innerHTML = ''; document.body.scrollTop += 1000; </script>\n";
    }
}

include EYOOM_ADMIN_INC_PATH . "/atpl.assign.php";

$atpl->assign(array(
    'mail_list' => $mail_list,
));