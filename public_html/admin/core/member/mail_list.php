<?php
$sub_menu = '200300';
if (!defined('_EYOOM_IS_ADMIN_')) exit;

auth_check($auth[$sub_menu], 'r');

//alert(G5_URL.'/mail_service/update_mail_service_total_state.php');

require_once(G5_PATH.'/mail_service/mailgun/Mailgun.php');
require_once(G5_PATH.'/mail_service/sendgrid/sendgrid-php/sendgrid-php.php');
require_once(G5_PATH.'/mail_service/std_mail_modules.php');

$action_url = EYOOM_ADMIN_URL . '/?dir=member&amp;pid=mail_delete&amp;smode=1';

$sql_common = " from {$g5['mail_table']} ";

// 테이블의 전체 레코드수만 얻음
$sql = " select COUNT(*) as cnt {$sql_common} ";
$row = sql_fetch($sql);
$total_count = $row['cnt'];

$page = 1;

$sql = " select * {$sql_common} order by ma_id desc ";
$result = sql_query($sql);
for ($i=0; $row=sql_fetch_array($result); $i++) {
    $num = number_format($total_count - ($page - 1) * $config['cf_page_rows'] - $i);

    $mail_list[$i] = $row;
    $mail_list[$i]['num'] = $num;
}

//mail service list information
$sql = " select cf_4,cf_8 from {$g5['config_table']} ";
$result = sql_query($sql);
for ($i=0; $row=sql_fetch_array($result); $i++) {
    $mail_service_info[$i] = $row;
}

//mail sending count checking
$mailgun_api_key=$mail_service_info[0]['cf_4'];
$sendgrid_api_key=$mail_service_info[0]['cf_8'];
$domain = "brosiidea.com";

//get mailgun sending count
$mailgun = new Mailgun($mailgun_api_key,$domain);

$messages = $mailgun->get("/$domain/stats/total", array(
    'event' => array('accepted', 'delivered', 'failed'),
    'duration' => '1m'
));

//get first and last days info
$str_mail_modules= new stdmailmodules\std_mail_modules();

$first_day =  $str_mail_modules->getFirstDayinfo();
$last_day = $str_mail_modules->getLastDayinfo();

//get sendgrid sending count information from server
$sendgrid = new \SendGrid($sendgrid_api_key);
$sendgrid_query_params = json_decode('{"aggregated_by": "month", "limit": 1, "start_date": "'.$first_day.'", "end_date": "'.$last_day.'", "offset": 1}');
$response = $sendgrid->client->stats()->get(null, $sendgrid_query_params);
$result =json_decode($response->body(), true);


//update sendgrid mailgun sending count information in DB table
$sql = " update {$g5['config_table']} set cf_3 = '{$messages->stats[0]->delivered->total}', cf_7 = '{$result[0]['stats'][0]['metrics']['delivered']}'";
sql_query($sql);

//mail service list information
$sql = " select cf_1,cf_2,cf_3,cf_5,cf_6,cf_7,cf_9,cf_10 from {$g5['config_table']} ";
$result = sql_query($sql);
for ($i=0; $row=sql_fetch_array($result); $i++) {
    $mail_service_info[$i] = $row;
}

//set send count and state to use it in html code
$mailgun_sending_count = $mail_service_info[0]['cf_3'];
$over_send_set_state = $mail_service_info[0]['cf_5'];
$sendgrid_sending_count = $mail_service_info[0]['cf_7'];
$set_use_continue = $mail_service_info[0]['cf_9'];

$sending_error_msg= $_GET['error_msg'];

//var_export($over_send_set_state.' ::: '.$over_send_set_state.' ::: '.$sendgrid_sending_count.' ::: '.$set_use_continue);

if($mail_service_info[0]['cf_1']==1)
    $mail_service_state='local_server';
elseif($mail_service_info[0]['cf_2']==1)
    $mail_service_state='mailgun';
else
    $mail_service_state='sendgrid';
// Paging
$paging = $thema->pg_pages($tpl_name,"./?dir=member&amp;pid=mail_list&amp;".$qstr."&amp;page=");

include EYOOM_ADMIN_INC_PATH . "/atpl.assign.php";

$atpl->assign(array(
	'mail_list' => $mail_list,
    'mail_service_info' => $mail_service_info,
));